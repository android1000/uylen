package thousand.group.uylen.view_models.main

import android.location.Location
import android.util.Log
import com.google.android.gms.location.LocationResult
import com.google.gson.Gson
import com.google.gson.JsonArray
import io.github.centrifugal.centrifuge.*
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import org.json.JSONObject
import thousand.group.data.entities.remote.simple.ChatListItem
import thousand.group.data.entities.remote.simple.NewDataNotification
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.utils.GsonHelper
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.chat.ChatUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.constants.AppConstants.BundleConstants.RELOAD_ACTIVITY
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager

class MainViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val chatUsecase: ChatUsecase,
    private val utilUsecase: UtilUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldOpenHomePage = OnceLiveData<Boolean>()
    val ldOpenHomePageInitially = OnceLiveData<Unit>()
    val ldOpenChatListPage = OnceLiveData<Unit>()
    val ldOpenChatListPageInitially = OnceLiveData<Unit>()
    val ldOpenFavouritePage = OnceLiveData<Unit>()
    val ldOpenProfilePage = OnceLiveData<Unit>()
    val ldOpenSearchPage = OnceLiveData<Unit>()
    val ldOpenEditProfilePage = OnceLiveData<Unit>()
    val ldOpenSettingsPage = OnceLiveData<Unit>()
    val ldShowChatBadge = OnceLiveData<Boolean>()
    val ldShowFavouriteBadge = OnceLiveData<Boolean>()
    val ldShowOpenMutualSympathyPage = OnceLiveData<Unit>()

    private var token = ""
    private var channelName = ""

    private var reloadActivity = false
    private var fromSignUpPage = false

    private var user = authUsecase.getUserModel()

    private val eventListener = object : EventListener() {
        override fun onConnect(client: Client?, event: ConnectEvent?) {
            Log.i(vmTag, "onConnect -> event:${event}")
            super.onConnect(client, event)
        }

        override fun onDisconnect(client: Client?, event: DisconnectEvent?) {
            Log.i(vmTag, "onDisconnect -> event:${event}")
            super.onDisconnect(client, event)
        }
    }

    private val subscriptionListener = object : SubscriptionEventListener() {
        override fun onSubscribeSuccess(sub: Subscription?, event: SubscribeSuccessEvent?) {
            super.onSubscribeSuccess(sub, event)

            Log.i(vmTag, "onSubscribeSuccess -> event:${event}")

            getLastNotification()
        }

        override fun onSubscribeError(sub: Subscription?, event: SubscribeErrorEvent?) {
            super.onSubscribeError(sub, event)

            Log.i(vmTag, "onSubscribeSuccess -> event:${event}")
        }

        override fun onPublish(sub: Subscription?, event: PublishEvent?) {
            super.onPublish(sub, event)

            val data = String(event!!.data, Charsets.UTF_8)

            parseSocketDataMessage(data)

            Log.i(vmTag, "subscriptionListener -> data:${data}")
        }
    }

    private var notificationSubsc: Subscription? = null

    private val client = Client(
        RemoteMainConstants.SOCKET_URL,
        Options(),
        eventListener
    )

    init {
        generateSocketToken()
    }

    override fun onCleared() {
        super.onCleared()
        unsubcribeChannel()
    }

    fun parseArgs(params: ParamsContainer) {
        params.apply {
            getString(AppConstants.BundleConstants.PUSH_OBJECT)?.apply {
                parsePushData(JSONObject(this))
            }
            getBoolean(RELOAD_ACTIVITY)?.apply {
                reloadActivity = this
            }
            getBoolean(AppConstants.BundleConstants.FROM_SIGN_UP_PAGE)?.apply {
                fromSignUpPage = this
            }
        }
    }

    fun openSpecificPage() {
        user?.apply {
            if (this.images.size < 2) {
                openEditProfilePage()
            } else if (reloadActivity) {
                openSettingsPage()

                reloadActivity = false
            } else {
                openHomePage()
            }
        }
    }

    fun openSettingsPage() {
        ldOpenSettingsPage.call()
    }

    fun openHomePage() {
        ldOpenHomePage.postValue(fromSignUpPage)
    }

    fun openHomePageInitially() {
        ldOpenHomePageInitially.call()
    }

    fun openChatListPage() {
        ldOpenChatListPage.call()
    }
/*
    fun openChatListPageInitially() {
        ldOpenChatListPageInitially.call()
    }*/

    fun openFavouritePage() {
        ldOpenFavouritePage.call()
    }

    fun openProfilePage() {
        ldOpenProfilePage.call()
    }

    fun openSearchPage() {
        ldOpenSearchPage.call()
    }

    fun openEditProfilePage() {
        ldOpenEditProfilePage.call()
    }

    fun updateLocation(locationResult: LocationResult) {
        doWork {
            val location = locationResult.locations.get(0)
            val latPart = RequestBodyHelper.getRequestBodyText(location.latitude)
            val longPart = RequestBodyHelper.getRequestBodyText(location.longitude)

            utilUsecase.updateLocation(latPart, longPart)
                .catch { it.printStackTrace() }
                .collect()
        }
    }

    fun updateLocation(location: Location) {
        doWork {
            val latPart = RequestBodyHelper.getRequestBodyText(location.latitude)
            val longPart = RequestBodyHelper.getRequestBodyText(location.longitude)

            utilUsecase.updateLocation(latPart, longPart)
                .catch { it.printStackTrace() }
                .collect()
        }
    }

    fun parsePushData(data: JSONObject) {
        doWork {
            //{"data":{"is_read":0,"file":null,"reaction":null,"updated_at":"2022-03-13 17:06:15","conversation_id":200,"receiver_id":134,"date_message":"2022-03-13","created_at":"17:06","id":1523,"message":"sdaasdads","sender_id":130},"success":true,"message":"sdaasdads","type":"message"}
            val type = data.getString(RemoteConstants.TYPE)

            when (type) {
                RemoteConstants.MESSAGE, RemoteConstants.REACTION -> {
                    openChatListPage()
                }
                RemoteConstants.MATCHED -> {
                    ldShowOpenMutualSympathyPage.call()
                }
                RemoteConstants.LIKE -> {
                    openFavouritePage()
                }
            }
        }
    }

    fun sendUserID(uId: String) {
        doWork {
            val uidPart = RequestBodyHelper.getRequestBodyText(uId)

            utilUsecase.sendDeviceToken(uidPart)
                .catch { it.printStackTrace() }
                .collect()
        }
    }

    private fun parseSocketDataMessage(json: String) {
        doWork {
            try {
                val data = Gson().fromJson(json, JsonArray::class.java)

                val notification = GsonHelper.getListFromJsonArray(
                    data,
                    NewDataNotification::class.java
                ).get(0)

                ldShowChatBadge.postValue(notification.chat || notification.matched)
                ldShowFavouriteBadge.postValue(notification.like)

                Log.i(vmTag, "parseSocketDataMessage -> json:${json}")
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    private fun generateSocketToken() {
        doWork {
            chatUsecase.generateSocketToken()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    token = it
                    doSubscription()
                }
        }
    }

    private fun doSubscription() {
        try {
            client.setToken(token)

            channelName = String.format(
                RemoteMainConstants.NOTIFICATION_SIGNAL_CHANNEL,
                authUsecase.getUserId()
            )

            notificationSubsc = client.newSubscription(
                channelName,
                subscriptionListener
            )

            Log.i(
                vmTag,
                "doSubscription -> chatMessagesSubscription -> channel: ${notificationSubsc?.channel}"
            )
            Log.i(vmTag, "doSubscription -> chatMessagesSubscription: ${notificationSubsc}")

            notificationSubsc?.subscribe()

            client.connect()

        } catch (ex: DuplicateSubscriptionException) {
            ex.printStackTrace()
        }
    }

    private fun unsubcribeChannel() {
        notificationSubsc?.unsubscribe()
        notificationSubsc?.apply {
            client.removeSubscription(this)
            client.disconnect()
        }
    }

    private fun getLastNotification() {
        doWork {
            utilUsecase.getLastNotification()
                .catch { it.printStackTrace() }
                .collect()
        }
    }


}