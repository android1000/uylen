package thousand.group.uylen.views.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.onesignal.OneSignal
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ActivityMainBinding
import thousand.group.uylen.utils.base.BaseActivity
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.system.FragmentTransactionFutures
import thousand.group.uylen.view_models.main.MainViewModel
import thousand.group.uylen.view_models.profile.EditProfileViewModel
import thousand.group.uylen.views.chat.ChatListFragment
import thousand.group.uylen.views.chat.MutualSympathyFragment
import thousand.group.uylen.views.favorite.FavouriteFragment
import thousand.group.uylen.views.home.HomeFragment
import thousand.group.uylen.views.profile.EditProfileFragment
import thousand.group.uylen.views.profile.ProfileFragment
import thousand.group.uylen.views.profile.SettingsFragment
import thousand.group.uylen.views.search.SearchFragment
import java.util.*


class MainActivity :
    BaseActivity<ActivityMainBinding, MainViewModel>(MainViewModel::class),
    FragmentTransactionFutures {

    private val customBackStack = mutableMapOf<String, Fragment>()

    private var mLocationRequest: LocationRequest? = null

    private val UPDATE_INTERVAL = (4 * 60 * 1000 /* 10 secs */).toLong()
    private val FASTEST_INTERVAL = (4 * 60 * 1000 /* 2 sec */).toLong()

    private val PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private val locationListener = object : LocationCallback() {
        @SuppressLint("MissingPermission")
        override fun onLocationResult(locationResult: LocationResult) {
            // do work here
            Log.i(activityTag, "startLocationUpdates -> locationResult:${locationResult}")

            viewModel.updateLocation(locationResult)
        }
    }

    private var permReqLauncher: ActivityResultLauncher<Array<String>>

    companion object {
        var isAlive = false
    }

    init {
        permReqLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                Log.i(activityTag, "registerForActivityResult -> permissions: ${permissions}")

                val granted = permissions.entries.all {
                    it.value == true
                }

                if (granted) {
                    startLocation()
                }
            }
    }

    override fun getBindingObject() = ActivityMainBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initIntent(intent: Intent?) {
        Log.i(activityTag, "initIntent")

        intent?.apply {
            parseIntent(this)?.apply {
                Log.i(activityTag, "initIntent -> params: ${this}")

                viewModel.parseArgs(this)
            }
        }

    }

    override fun initView(savedInstanceState: Bundle?) {

        viewModel.openSpecificPage()
//        setBottomPadding()

        OneSignal.getDeviceState()?.userId?.apply {
            viewModel.sendUserID(this)
        }
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldOpenHomePage) {
            openHomePage(it)
        }
/*        observeLiveData(viewModel.ldOpenHomePageInitially) {
            openHomePageInitially()
        }*/
        observeLiveData(viewModel.ldOpenChatListPage) {
            openChatListPage()
        }
/*        observeLiveData(viewModel.ldOpenChatListPageInitially) {
            openChatListPageIntiially()
        }*/
        observeLiveData(viewModel.ldOpenFavouritePage) {
            openFavouritePage()
        }
        /* observeLiveData(viewModel.ldOpenFavouritePageInitially) {
             openFavouritePageInitially()
         }*/
        observeLiveData(viewModel.ldOpenProfilePage) {
            openProfilePage()
        }
        observeLiveData(viewModel.ldOpenSearchPage) {
            openSearchPage()
        }
        observeLiveData(viewModel.ldShowChatBadge) {
            showChatBadge(it)
        }
        observeLiveData(viewModel.ldOpenSettingsPage) {
            openSettingsFragment()
        }
        observeLiveData(viewModel.ldShowFavouriteBadge) {
            showFavouriteBadge(it)
        }
        observeLiveData(viewModel.ldShowOpenMutualSympathyPage) {
            openMutualSympathyFragment()
        }
        observeLiveData(viewModel.ldOpenEditProfilePage) {
            openEditProfilePage()
        }
        /*observeLiveData(viewModel.ldShowOpenMutualSympathyPageInitially) {
            openMutualSympathyFragmentInitially()
        }*/
    }

    override fun initController() {
        binding.bottomNavMain.setOnItemSelectedListener {
            supportFragmentManager.apply {

                val fragment: Fragment?

                if (!this.fragments.isNullOrEmpty()) {
                    fragment = fragments[fragments.size - 1]
                } else {
                    fragment = null
                }

                when (it.itemId) {
                    R.id.nav_home -> {
                        doIfInternetConnected {
                            if (fragment !is HomeFragment) {
                                viewModel.openHomePage()
                            }
                        }
                    }
                    R.id.nav_search -> {
                        doIfInternetConnected {
                            if (fragment !is SearchFragment) {
                                viewModel.openSearchPage()
                            }
                        }
                    }
                    R.id.nav_messages -> {
                        doIfInternetConnected {
                            if (fragment !is ChatListFragment) {
                                viewModel.openChatListPage()
                            }
                        }
                    }
                    R.id.nav_heart -> {
                        doIfInternetConnected {
                            if (fragment !is FavouriteFragment) {
                                viewModel.openFavouritePage()
                            }
                        }
                    }
                    R.id.nav_profile -> {
                        doIfInternetConnected {
                            if (fragment !is ProfileFragment) {
                                viewModel.openProfilePage()
                            }
                        }
                    }
                }
            }

            true
        }

    }

    override fun fragmentLifeCycleController(fragmentTag: String) {
        binding.bottomNavMain.setVisibilityState(MainFragmentHelper.isShowBottomNavBar(fragmentTag))

        setStatusBarParams(
            MainFragmentHelper.isLightStatusBar(fragmentTag)
        )

        if (MainFragmentHelper.getNavBarItemPos(fragmentTag) != null) {
            binding.bottomNavMain.setChecked(MainFragmentHelper.getNavBarItemPos(fragmentTag)!!)
        } else {
            binding.bottomNavMain.uncheckAllItems()
        }
    }

    override fun onDestroy() {
        isAlive = false
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        requestLocationPermissions()
    }

    override fun onStop() {
        super.onStop()

        LocationServices.getFusedLocationProviderClient(this)
            .removeLocationUpdates(locationListener)
    }

    override fun replaceFragment(
        containerViewId: Int,
        newFragment: Fragment,
        tag: String
    ) {
        supportFragmentManager.apply {
            val transaction = beginTransaction()
            val oldFragment = customBackStack.get(tag)
            val currentFragment = this.findFragmentById(containerViewId)

            currentFragment?.apply {
                transaction.detach(currentFragment)
            }

            if (oldFragment == null) {
                transaction.add(containerViewId, newFragment, tag)
                customBackStack.put(tag, newFragment)
            } else {
                transaction.attach(oldFragment)
                customBackStack.remove(tag)
                customBackStack.put(tag, oldFragment)
            }

            transaction.commit()
        }
    }

    override fun removeAndReplaceWithAnotherFragment(
        containerViewId: Int,
        newFragment: Fragment,
        newTag: String,
        oldTag: String
    ) {
        supportFragmentManager.apply {
            val transaction = beginTransaction()

            customBackStack.remove(oldTag)

            findFragmentByTag(oldTag)?.apply {
                transaction.remove(this)
            }

            transaction.add(containerViewId, newFragment, newTag)
            customBackStack.put(newTag, newFragment)

            transaction.commit()
        }
    }

    override fun goBackFragment(containerViewId: Int) {
        Log.i(activityTag, "goBackFragment -> customBackStack:${customBackStack}")

        supportFragmentManager.apply {
            val transaction = beginTransaction()

            customBackStack.remove(customBackStack.keys.last())

            findFragmentById(containerViewId)?.apply {
                transaction.remove(this)
            }

            val lastFragmentTag = customBackStack.keys.last()

            customBackStack.get(lastFragmentTag)?.let {
/*                beginTransaction().replace(
                    containerViewId,
                    it,
                    lastFragmentTag
                ).commit();*/

                transaction.attach(it)
            }

            transaction.commit()
        }
    }

    override fun getFragmentMap() = customBackStack

    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fl_fragment_container)

        if (fragment is SettingsFragment && customBackStack.size == 1) {
            customBackStack.clear()

/*            supportFragmentManager.replaceFragmentWithCheck(
                R.id.fl_fragment_container,
                ProfileFragment.newInstance(),
                ProfileFragment.fragmentOpenTag
            )*/

            replaceFragment(
                R.id.fl_fragment_container,
                ProfileFragment.newInstance(),
                ProfileFragment.fragmentOpenTag
            )
        } else {
            if (fragment is EditProfileFragment) {
                viewModel.sendLocaleCallback(
                    EditProfileViewModel::class,
                    AppConstants.MessageKeysConstants.ON_BACK_PRESSED
                )
            } else {
                doOnBackPressed()
            }
        }
    }

    private fun requestLocationPermissions() {
        if (hasPermissions(PERMISSIONS)) {
            startLocation()
        } else {
            permReqLauncher.launch(PERMISSIONS)
        }
    }

    private fun hasPermissions(permissions: Array<String>): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(
                this,
                it
            ) == PackageManager.PERMISSION_GRANTED
        }

    private fun setBottomPadding() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            binding.root.updatePadding(bottom = getBottomPadding())
        }
    }

    private fun getBottomPadding(): Int {
        val bottomPaddingRes =
            resources.getIdentifier("navigation_bar_height", "dimen", "android")
        val bottomPadding = resources.getDimensionPixelSize(bottomPaddingRes)

        return bottomPadding
    }

    private fun cancelBottomPadding() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            binding.root.updatePadding(bottom = 0)
        }
    }

    private fun openHomePage(fromSignUpPage: Boolean) {
        val fragment = HomeFragment.newInstance(fromSignUpPage)

/*        supportFragmentManager.replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            HomeFragment.fragmentOpenTag
        )*/

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            HomeFragment.fragmentOpenTag
        )
    }

    private fun openEditProfilePage() {
        val fragment = EditProfileFragment.newInstance()

/*        supportFragmentManager.replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            HomeFragment.fragmentOpenTag
        )*/

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            EditProfileFragment.fragmentOpenTag
        )
    }


/*    private fun openHomePageInitially() {
        val fragment = HomeFragment.newInstance()

        supportFragmentManager.clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            HomeFragment.fragmentOpenTag
        )
    }*/

    private fun openChatListPage() {
        val fragment = ChatListFragment.newInstance()

/*        supportFragmentManager.replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            ChatListFragment.fragmentOpenTag
        )*/

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            ChatListFragment.fragmentOpenTag
        )
    }

/*    private fun openChatListPageIntiially() {
        val fragment = ChatListFragment.newInstance()

        supportFragmentManager.clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            ChatListFragment.fragmentOpenTag
        )
    }*/

    private fun openFavouritePage() {
        val fragment = FavouriteFragment.newInstance()
/*
        supportFragmentManager.replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            FavouriteFragment.fragmentOpenTag
        )*/

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            FavouriteFragment.fragmentOpenTag
        )
    }

/*    private fun openFavouritePageInitially() {
        val fragment = FavouriteFragment.newInstance()

        supportFragmentManager.clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            FavouriteFragment.fragmentOpenTag
        )
    }*/

    private fun openProfilePage() {
        val fragment = ProfileFragment.newInstance()

/*        supportFragmentManager.replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            ProfileFragment.fragmentOpenTag
        )*/

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            ProfileFragment.fragmentOpenTag
        )
    }

    private fun openSearchPage() {
        val fragment = SearchFragment.newInstance()

/*        supportFragmentManager.replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            SearchFragment.fragmentOpenTag
        )*/

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            SearchFragment.fragmentOpenTag
        )
    }

    private fun openSettingsFragment() {
        val fragment = SettingsFragment.newInstance()

/*        supportFragmentManager.replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            SettingsFragment.fragmentOpenTag
        )*/

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            MutualSympathyFragment.fragmentOpenTag
        )
    }

    private fun openMutualSympathyFragment() {
        val fragment = MutualSympathyFragment.newInstance()

/*        supportFragmentManager.replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            MutualSympathyFragment.fragmentOpenTag
        )*/

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            MutualSympathyFragment.fragmentOpenTag
        )
    }

/*    private fun openMutualSympathyFragmentInitially() {
        val fragment = MutualSympathyFragment.newInstance()

        supportFragmentManager.clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            MutualSympathyFragment.fragmentOpenTag
        )
    }*/

    @SuppressLint("MissingPermission")
    protected fun startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = LocationRequest.create()
        mLocationRequest?.setPriority(com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY)
        mLocationRequest?.setInterval(UPDATE_INTERVAL)
        mLocationRequest?.setFastestInterval(FASTEST_INTERVAL)

        Log.i(activityTag, "startLocationUpdates -> mLocationRequest:${mLocationRequest}")

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        mLocationRequest?.let {
            LocationServices.getFusedLocationProviderClient(this)
                .requestLocationUpdates(
                    it,
                    locationListener,
                    Looper.myLooper()
                )
        }

    }

    private fun showChatBadge(show: Boolean) {
        binding.bottomNavMain.getOrCreateBadge(R.id.nav_messages).isVisible = show
    }

    private fun showFavouriteBadge(show: Boolean) {
        binding.bottomNavMain.getOrCreateBadge(R.id.nav_heart).isVisible = show
    }

    private fun doOnBackPressed() {
        if (customBackStack.size == 1) {
            finish()
            //additional code
        } else {
            this.goBackFragment(
                R.id.fl_fragment_container
            )
        }
    }

    private fun startLocation() {
        Log.i(activityTag, "startLocation")

        Log.i(activityTag, "startLocation -> startLocationUpdates")
        startLocationUpdates()
    }

}