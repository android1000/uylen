package thousand.group.uylen.view_models.auth

import android.location.Location
import android.util.Log
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import thousand.group.data.entities.remote.simple.User
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.favourite.FavouriteViewModel
import thousand.group.uylen.view_models.profile.ProfileViewModel
import thousand.group.uylen.view_models.registration.FirstRegistrationViewModel
import thousand.group.uylen.view_models.registration.ThirdRegistrationViewModel
import kotlin.reflect.KClass

class AuthViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetTabPosition = OnceLiveData<Int>()
    val ldOpenRegistrationPage = OnceLiveData<Unit>()
    val ldOpenLoginPage = OnceLiveData<Unit>()
    val ldOpenMainPage = OnceLiveData<Unit>()
    val ldOpenThirdRegisterPage = OnceLiveData<Pair<User, String>>()

    private var selectedTabPos: Int? = null

    private var lat = 0.0
    private var longg = 0.0

    private var reloadActivity = false

    init {
        setStringMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.REQUIRE_LOCATION -> {
                    val targetVm = when (message) {
                        LoginViewModel::class.simpleName -> {
                            LoginViewModel::class
                        }
                        FirstRegistrationViewModel::class.simpleName -> {
                            FirstRegistrationViewModel::class
                        }
                        else -> {
                            ThirdRegistrationViewModel::class
                        }
                    }

                    if (lat != 0.0 && longg != 0.0) {
                        sendLocaleMessage(
                            targetVm,
                            AppConstants.MessageKeysConstants.RESPONSE_LOCATION,
                            Pair(lat, longg)
                        )
                    } else {
                        sendLocaleCallback(
                            targetVm,
                            AppConstants.MessageKeysConstants.RESPONSE_LOCATION_ERROR
                        )
                    }

                }
            }
        }

        checkTokenAndOpenMainPage()
    }

    fun parseArgs(params: ParamsContainer) {
        params.apply {
            getBoolean(AppConstants.BundleConstants.RELOAD_ACTIVITY)?.apply {
                reloadActivity = this
            }
        }
    }

    fun selectTab(tabPos: Int?) {
        tabPos?.apply {
            ldSetTabPosition.postValue(this)
        }
    }

    fun updateLocation(locationResult: Location) {
        doWork {
            lat = locationResult.latitude
            longg = locationResult.longitude
        }
    }

    fun onTabSelectedListener(pos: Int) {
        selectedTabPos = pos

        when (selectedTabPos) {
            0 -> ldOpenRegistrationPage.call()
            1 -> ldOpenLoginPage.call()
        }
    }

    private fun checkTokenAndOpenMainPage() {
        if (authUsecase.tokenIsNotEmpty()) {
            Log.i(vmTag, "checkTokenAndOpenMainPage -> 1")
            ldOpenMainPage.call()
        } else if (authUsecase.tokenTempIsNotEmpty()) {
            Log.i(vmTag, "checkTokenAndOpenMainPage -> 2")

            authUsecase.getTempAccessToken().let { token ->
                Log.i(vmTag, "checkTokenAndOpenMainPage -> token:${token}")

                authUsecase.getTempUserModel()?.let { user ->

                    Log.i(vmTag, "checkTokenAndOpenMainPage -> user:${user}")

                    ldOpenThirdRegisterPage.postValue(Pair(user, token))
                }
            }
        } else {

            Log.i(vmTag, "checkTokenAndOpenMainPage -> 3")

            openSpecificPage()
        }
    }

    fun openSpecificPage() {
        if (reloadActivity) {
            onTabSelectedListener(0)
            reloadActivity = false
        } else {
            onTabSelectedListener(1)
        }
    }
}