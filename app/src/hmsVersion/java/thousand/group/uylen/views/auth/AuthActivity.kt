package thousand.group.uylen.views.auth

import android.Manifest
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.view.updatePadding
import com.google.android.material.tabs.TabLayout
import com.huawei.hmf.tasks.Task
import com.huawei.hms.common.ApiException
import com.huawei.hms.common.ResolvableApiException
import com.huawei.hms.location.*
import thousand.group.data.entities.remote.simple.User
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ActivityAuthBinding
import thousand.group.uylen.utils.base.BaseActivity
import thousand.group.uylen.utils.extensions.clearAndReplaceFragment
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setVisibilityState
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.view_models.auth.AuthViewModel
import thousand.group.uylen.views.main.MainActivity
import thousand.group.uylen.views.registration.FirstRegistrationFragment
import thousand.group.uylen.views.registration.ThirdRegistrationFragment

class AuthActivity : BaseActivity<ActivityAuthBinding, AuthViewModel>(AuthViewModel::class) {

    private val tabListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabSelected(tab: TabLayout.Tab?) {
            tab?.apply {
                viewModel.onTabSelectedListener(position)
            }
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
        }

        override fun onTabReselected(tab: TabLayout.Tab?) {
        }
    }

    private var mLocationRequest: LocationRequest? = null

    private val UPDATE_INTERVAL = (20 * 1000 /* 10 secs */).toLong()
    private val FASTEST_INTERVAL = (20 * 1000 /* 2 sec */).toLong()

    private val PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    private var permReqLauncher: ActivityResultLauncher<Array<String>>

    // Define a location provider client.
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    // Define a device setting cl ient.
    private lateinit var settingsClient: SettingsClient

    var mLocationRequestHuawei: com.huawei.hms.location.LocationRequest? = null

    var mLocationCallback = object : com.huawei.hms.location.LocationCallback() {
        override fun onLocationResult(locationResult: com.huawei.hms.location.LocationResult?) {
            if (locationResult != null) {
                val locations = locationResult.locations

                if (locations.isNotEmpty()) {

                    viewModel.updateLocation(locations.get(0))

                    for (location in locations) {
                        Log.i(
                            activityTag,
                            "onLocationResult location[Longitude,Latitude,Accuracy]:${location.longitude} , ${location.latitude} , ${location.accuracy}"
                        )
                    }
                }
            }
        }

        override fun onLocationAvailability(locationAvailability: com.huawei.hms.location.LocationAvailability?) {
            locationAvailability?.let {
                val flag: Boolean = locationAvailability.isLocationAvailable
                Log.i(activityTag, "onLocationAvailability isLocationAvailable:$flag")
            }
        }
    }

    init {
        permReqLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                Log.i(activityTag, "registerForActivityResult -> permissions: ${permissions}")

                val granted = permissions.entries.all {
                    it.value == true
                }

                if (granted) {
                    startLocation()
                }
            }
    }

    override fun getBindingObject() = ActivityAuthBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initIntent(intent: Intent?) {
        intent?.apply {
            parseIntent(this)?.apply {
                Log.i(activityTag, "initIntent -> params: ${this}")

                viewModel.parseArgs(this)
            }
        }

    }

    override fun initView(savedInstanceState: Bundle?) {

        fusedLocationProviderClient =
            com.huawei.hms.location.LocationServices.getFusedLocationProviderClient(this);
        settingsClient = com.huawei.hms.location.LocationServices.getSettingsClient(this);

        mLocationRequestHuawei = com.huawei.hms.location.LocationRequest().apply {
            interval = 10 * 1000L
            needAddress = true
            priority = com.huawei.hms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
        }

    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetTabPosition) {
            with(binding.tbltAuth) {
                removeOnTabSelectedListener(tabListener)
                getTabAt(it)?.select()
                addOnTabSelectedListener(tabListener)
            }
        }
        observeLiveData(viewModel.ldOpenLoginPage) {
            openLoginFragment()
        }
        observeLiveData(viewModel.ldOpenRegistrationPage) {
            openRegistrationFragment()
        }
        observeLiveData(viewModel.ldOpenMainPage) {
            openMainActivity()
        }
        observeLiveData(viewModel.ldOpenThirdRegisterPage) {
            openThirdRegistrationFragment(it)
        }

    }

    override fun initController() {
        binding.tbltAuth.addOnTabSelectedListener(tabListener)
    }

    override fun fragmentLifeCycleController(fragmentTag: String) {
        setStatusBarParams(
            AuthFragmentHelper.isLightStatusBar(fragmentTag)
        )

        binding.tbltAuth.setVisibilityState(
            AuthFragmentHelper.isTabsVisible(fragmentTag)
        )

        viewModel.selectTab(
            AuthFragmentHelper.getTabsPosition(fragmentTag)
        )
    }

    override fun onResume() {
        super.onResume()

        requestLocationPermissions()
    }

    override fun onStop() {
        super.onStop()

        removeLocationUpdatesWithCallback()
    }

    private fun openLoginFragment() {
        val fragment = LoginFragment.newInstance()

        supportFragmentManager.clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            LoginFragment.fragmentOpenTag
        )
    }

    private fun openRegistrationFragment() {
        val fragment = FirstRegistrationFragment.newInstance()

        supportFragmentManager.clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            FirstRegistrationFragment.fragmentOpenTag
        )
    }

    private fun setBottomPadding() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            binding.root.updatePadding(bottom = getBottomPadding())
        }
    }

    private fun getBottomPadding(): Int {
        val bottomPaddingRes =
            resources.getIdentifier("navigation_bar_height", "dimen", "android")
        val bottomPadding = resources.getDimensionPixelSize(bottomPaddingRes)

        return bottomPadding
    }

    private fun cancelBottomPadding() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            binding.root.updatePadding(bottom = 0)
        }
    }

    private fun openMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtras(getIntent())

        startActivity(intent)
        finish()
    }

    private fun requestLocationPermissions() {
        if (hasPermissions(PERMISSIONS)) {
            startLocation()
        } else {
            permReqLauncher.launch(PERMISSIONS)
        }
    }

    private fun hasPermissions(permissions: Array<String>): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(
                this,
                it
            ) == PackageManager.PERMISSION_GRANTED
        }

    private fun startLocationUpdatesHuawei() {
        try {
            val builder = LocationSettingsRequest.Builder()
            builder.addLocationRequest(mLocationRequestHuawei)

            val locationSettingsRequest = builder.build()
            // Check the device settings before requesting location updates.
            val locationSettingsResponseTask: Task<LocationSettingsResponse> =
                settingsClient.checkLocationSettings(locationSettingsRequest)

            locationSettingsResponseTask.addOnSuccessListener { locationSettingsResponse: LocationSettingsResponse? ->
                Log.i(activityTag, "check location settings success  {$locationSettingsResponse}")
                // Request location updates.
                fusedLocationProviderClient.requestLocationUpdates(
                    mLocationRequestHuawei,
                    mLocationCallback,
                    Looper.getMainLooper()
                )
                    .addOnSuccessListener {
                        Log.i(activityTag, "requestLocationUpdatesWithCallback onSuccess")
                    }
                    .addOnFailureListener { e ->
                        Log.e(
                            activityTag,
                            "requestLocationUpdatesWithCallback onFailure:${e.message}"
                        )
                    }
            }
                .addOnFailureListener { e: Exception ->
                    Log.e(activityTag, "checkLocationSetting onFailure:${e.message}")
                    when ((e as ApiException).statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                            val rae = e as ResolvableApiException
                            rae.startResolutionForResult(
                                this, 0
                            )
                        } catch (sie: IntentSender.SendIntentException) {
                            Log.e(activityTag, "PendingIntent unable to execute request.")
                        }
                    }
                }
        } catch (e: Exception) {
            Log.e(activityTag, "requestLocationUpdatesWithCallback exception:${e.message}")
        }
    }

    private fun removeLocationUpdatesWithCallback() {
        try {
            fusedLocationProviderClient.removeLocationUpdates(mLocationCallback)
                .addOnSuccessListener {
                    Log.i(
                        activityTag,
                        "removeLocationUpdatesWithCallback onSuccess"
                    )
                }
                .addOnFailureListener { e ->
                    Log.e(
                        activityTag,
                        "removeLocationUpdatesWithCallback onFailure:${e.message}"
                    )
                }
        } catch (e: Exception) {
            Log.e(
                activityTag,
                "removeLocationUpdatesWithCallback exception:${e.message}"
            )
        }
    }

    private fun startLocation() {
        Log.i(activityTag, "startLocation")
        Log.i(activityTag, "startLocation -> field_huawei")
        startLocationUpdatesHuawei()
    }

    private fun openThirdRegistrationFragment(params: Pair<User, String>) {
        val fragment =
            ThirdRegistrationFragment.newInstance(params.first, params.second)

        getSupportFragmentManager().clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            ThirdRegistrationFragment.fragmentOpenTag
        )
    }

}