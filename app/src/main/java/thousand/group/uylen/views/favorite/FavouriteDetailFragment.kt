package thousand.group.uylen.views.favorite

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isEmpty
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.Reaction
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentHomeDetailBinding
import thousand.group.uylen.utils.adapters.recycler_view.NoneSelectableInterestsAdapter
import thousand.group.uylen.utils.adapters.recycler_view.ReactionsAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.decorations.HorizontalListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.favourite.FavouriteDetailViewModel
import thousand.group.uylen.views.chat.ChatListFragment
import thousand.group.uylen.views.home.NewPairFragment

class FavouriteDetailFragment :
    BaseFragment<FragmentHomeDetailBinding, FavouriteDetailViewModel>(
        FavouriteDetailViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true
                )
            )

        fun newInstance(id: Long, position: Int, showBtn: Boolean = true): FavouriteDetailFragment {
            val fragment = FavouriteDetailFragment()
            val args = Bundle()
            //passing arguments

            args.putLong(AppConstants.BundleConstants.ID, id)
            args.putInt(AppConstants.BundleConstants.SELECTED_POS, position)
            args.putBoolean(AppConstants.BundleConstants.SHOW_BTN, showBtn)

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: NoneSelectableInterestsAdapter
    private lateinit var reactionsAdapter: ReactionsAdapter

    override fun getBindingObject() = FragmentHomeDetailBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        setLayoutManager()
        setAdapter()
        setReactionsAdapter()
        setDecor()
        setReactionsDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetCurrentImage) {
            setBannerParams(it.first, it.second, it.third)
        }
        observeLiveData(viewModel.ldSetEmptyList) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldSetInterestList) {
            setList(it)
            setDecor()
        }
        observeLiveData(viewModel.ldSetTextParams) {
            setTextParmas(it)
        }
        observeLiveData(viewModel.ldSetReactionsList) {
            setReactionsList(it)
        }
        observeLiveData(viewModel.ldClosePage) {
            requireActivity().onBackPressed()
        }
        observeLiveData(viewModel.ldOpenChatListPage) {
            openChatListPage()
        }
        observeLiveData(viewModel.ldShowBtns) {
            binding.fabCancel.setVisibilityState(it)
            binding.fabLike.setVisibilityState(it)
//            binding.fabStars.setVisibilityState(it)
        }
        observeLiveData(viewModel.ldOpenNewPairPage) {
            openNewPairFragment(it.first, it.second)
        }
        observeLiveData(viewModel.ldShowComplainReasonMenu) {
            showComplainReasonMenu()
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.flLeftPlane.setSafelyClickListener {
            viewModel.onLeftPlaneClicked()
        }

        binding.flRightPlane.setSafelyClickListener {
            viewModel.onRightPlaneClicked()
        }

        binding.fabBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.fabCancel.setSafelyClickListener {
            doIfInternetConnected {
                viewModel.onCancelBtnClicked()
            }
        }

        binding.fabLike.setSafelyClickListener {
            doIfInternetConnected {
                viewModel.onLikeBtnClicked()
            }
        }

        binding.fabStars.setSafelyClickListener {
            doIfInternetConnected {
                viewModel.onSuperLikeBtnClicked()
            }
        }

        binding.fabComplainMenu.setSafelyClickListener {
            showComplainMenu()
        }
    }

    private fun setBannerParams(
        currentImage: String,
        allImagesSize: Int,
        currentActiveImagePos: Int
    ) {
        Picasso.get()
            .load(currentImage)
            .error(R.drawable.ic_error_avatar)
            .into(binding.ivAvatar)

        updateOrCreateProgressPositions(allImagesSize, currentActiveImagePos)
    }

    private fun updateOrCreateProgressPositions(listSize: Int, currentPos: Int) {
        if (binding.llProgress.isEmpty()) {
            for (index in 0 until listSize) {
                val frameProgress = FrameLayout(binding.root.context)
                val backgroundColor =
                    if (index <= currentPos) R.color.colorWhite else R.color.colorWhiteOp32

                frameProgress.setBackgroundResource(backgroundColor)
                frameProgress.layoutParams =
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        1f
                    )

                val marginLp = frameProgress.layoutParams as ViewGroup.MarginLayoutParams
                marginLp.leftMargin =
                    if (index != 0) resources.getDimensionPixelSize(R.dimen.progressMargin) else 0

                frameProgress.layoutParams = marginLp

                binding.llProgress.addView(frameProgress)
            }
        } else {
            for (index in 0 until listSize) {
                val backgroundColor =
                    if (index <= currentPos) R.color.colorWhite else R.color.colorWhiteOp32

                binding.llProgress.getChildAt(index).setBackgroundResource(backgroundColor)
            }
        }

    }

    private fun setTextParmas(params: ParamsContainer) {
        with(params) {
            getString(AppConstants.BundleConstants.NAME)?.apply {
                binding.tvUsersName.setText(this)
            }
            getString(AppConstants.BundleConstants.AGE)?.apply {
                binding.tvUsersAge.setText(this)
            }
            getString(AppConstants.BundleConstants.SALARY)?.apply {
                binding.tvIncomeDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.ABOUT)?.apply {
                binding.tvDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.CITY)?.apply {
                binding.tvCityDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.RU)?.apply {
                binding.tvRuDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.FAMILY_STATUS)?.apply {
                binding.tvFamilyStatusDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.GENDER)?.apply {
                binding.tvGenderDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.BIRTHDATE)?.apply {
                binding.tvBirthdateDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.WEIGHT)?.apply {
                binding.tvWeightDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.HEIGHT)?.apply {
                binding.tvHeightDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.ZODIAC)?.apply {
                binding.tvZodiacDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.BAD_HABITS)?.apply {
                binding.tvBadHabitsDesc.setText(this)
            }
            getString(AppConstants.BundleConstants.KALYM)?.apply {
                binding.tvKalymDesc.setText(this)
            }
            getBoolean(AppConstants.BundleConstants.KALYM_VISIBILITY)?.apply {
                binding.tvKalymTitle.setVisibilityState(this)
                binding.tvKalymDesc.setVisibilityState(this)
            }
        }
    }


    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.tvEmptyList.setVisibilityState(isEmpty)
        binding.rcvInterests.setVisibilityState(!isEmpty)
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = NoneSelectableInterestsAdapter()
        }

        binding.rcvInterests.adapter = adapter
    }

    private fun setReactionsAdapter() {
        if (!::reactionsAdapter.isInitialized) {
            reactionsAdapter = ReactionsAdapter { reaction, pos ->
                viewModel.onReactionClicked(reaction, pos)
            }
        }

        binding.rcvReactions.adapter = reactionsAdapter
    }

    private fun setList(list: MutableList<Interest>) {
        adapter.setData(list)
    }

    private fun setReactionsList(list: MutableList<Reaction>) {
        reactionsAdapter.setData(list)
    }

    private fun setLayoutManager() {
        val layoutManager = FlowLayoutManager()
        layoutManager.maxItemsPerLine(5)
        layoutManager.isAutoMeasureEnabled = true
        binding.rcvInterests.layoutManager = layoutManager
    }

    private fun setDecor() {
        binding.rcvInterests.clearItemDecorations()

        binding.rcvInterests.addItemDecoration(object : RecyclerView.ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                super.getItemOffsets(outRect, view, parent, state)

                val dimen = resources.getDimensionPixelOffset(R.dimen.rightAnswersSpace)

                with(outRect) {
                    right = dimen
                    bottom = dimen
                }
            }
        })
    }

    private fun setReactionsDecor() {
        binding.rcvReactions.clearItemDecorations()

        binding.rcvReactions.addItemDecoration(
            HorizontalListItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.reactionsSpace),
                reactionsAdapter.itemCount
            )
        )
    }

    private fun openChatListPage() {
        val fragment = ChatListFragment.newInstance()

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            ChatListFragment.fragmentOpenTag
        )
    }

    private fun openNewPairFragment(firstUser: UserCardData, secondUser: User) {
        val fragment = NewPairFragment.newInstance(firstUser, secondUser)

        removeAndReplaceWithAnotherFragment(
            R.id.fl_fragment_container,
            fragment,
            NewPairFragment.fragmentOpenTag,
            fragmentOpenTag
        )
    }

    private fun showComplainMenu() {
        val alertB = AlertDialog.Builder(requireContext())
        alertB.setItems(
            R.array.user_detail_menu
        ) { dialog, which ->
            viewModel.onComplainMenuItemSelected(which)
        }

        val alert = alertB.show()

        alert.setPositiiveBtnTextColor(R.color.colorAccent)
        alert.setNegativeBtnTextColor(R.color.colorAccent)
    }

    private fun showComplainReasonMenu() {
        val alertB = AlertDialog.Builder(requireContext())
        alertB.setItems(
            R.array.complain_menu
        ) { dialog, which ->
            viewModel.onComplainReasonMenuItemClicked(which)
        }

        val alert = alertB.show()

        alert.setPositiiveBtnTextColor(R.color.colorAccent)
        alert.setNegativeBtnTextColor(R.color.colorAccent)
    }
}