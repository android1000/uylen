package thousand.group.uylen.views.auth

import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.util.Log
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import thousand.group.uylen.databinding.FragmentEnterSingleInfoBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.goBack
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.locale.EnterSingleInfo
import thousand.group.uylen.view_models.auth.EnterSingleInfoViewModel


class EnterSingleInfoFragment :
    BaseFragment<FragmentEnterSingleInfoBinding, EnterSingleInfoViewModel>(
        EnterSingleInfoViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = AuthFragmentHelper.getJsonFragmentTag(
            AuthFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true,
                isTabsVisible = false
            )
        )

        var fragmentOpenTag2 = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true
            )
        )


        fun newInstance(tag: String, enterSingleInfo: EnterSingleInfo): EnterSingleInfoFragment {
            val fragment = EnterSingleInfoFragment()
            val args = Bundle()
            //passing arguments
            args.putString(AppConstants.BundleConstants.VM_TAG, tag)
            args.putParcelable(AppConstants.BundleConstants.ENTER_TYPE, enterSingleInfo)

            fragment.arguments = args
            return fragment
        }

        fun newInstance(
            tag: String,
            enterSingleInfo: EnterSingleInfo,
            currentText: String
        ): EnterSingleInfoFragment {
            val fragment = EnterSingleInfoFragment()
            val args = Bundle()
            //passing arguments
            args.putString(AppConstants.BundleConstants.VM_TAG, tag)
            args.putParcelable(AppConstants.BundleConstants.ENTER_TYPE, enterSingleInfo)
            args.putString(AppConstants.BundleConstants.CURRENT_ENTER_TEXT, currentText)

            fragment.arguments = args
            return fragment
        }
    }

    override fun getBindingObject() = FragmentEnterSingleInfoBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {

    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetSingleInfoParams) {
            setEditParams(it)
        }
        observeLiveData(viewModel.ldClosePage) {
            goBack()
        }
        observeLiveData(viewModel.ldSetCurrentText) {
            binding.etRestoreCode.setText(it)
        }
    }

    override fun initController() {
        binding.ivBack.setSafelyClickListener {
            goBack()
        }

        binding.btnReady.setSafelyClickListener {
            viewModel.onReadyBtnClicked(binding.etRestoreCode.text.toString().trim())
        }

        checkToZeroEdit(binding.etRestoreCode)
    }

    private fun setEditParams(editType: EnterSingleInfo) {
        Log.i(fragmentTag, "setEditParams -> ${editType}")

        binding.tvTitle.setText(editType.title)
//        binding.tvDesc.setText(editType.desc)
        binding.etRestoreCode.inputType = editType.inputType
        binding.etRestoreCode.setHint(editType.hint)

        if (editType.inputType == InputType.TYPE_CLASS_NUMBER) {
            binding.etRestoreCode.setFilters(arrayOf<InputFilter>(LengthFilter(10)))
        }
    }

    private fun checkToZeroEdit(editText: EditText) {
        editText.addTextChangedListener {
            it?.let {
                if (it.startsWith("0")) {
                    editText.setText("")
                }
            }
        }
    }

}