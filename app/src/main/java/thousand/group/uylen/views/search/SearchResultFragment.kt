package thousand.group.uylen.views.search

import android.os.Bundle
import thousand.group.data.entities.remote.simple.SearchUser
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentSearchResultBinding
import thousand.group.uylen.utils.adapters.recycler_view.SearchResultAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.custom_views.TouchDetectableScrollView
import thousand.group.uylen.utils.decorations.VerticalGridListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.search.SearchResultViewModel

class SearchResultFragment :
    BaseFragment<FragmentSearchResultBinding, SearchResultViewModel>(
        SearchResultViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true
            )
        )

        fun newInstance(params: ParamsContainer): SearchResultFragment {
            val fragment = SearchResultFragment()
            val args = Bundle()
            //passing arguments

            args.putSerializable(AppConstants.BundleConstants.PARAMS_CONTAINER, params)

            fragment.arguments = args

            return fragment
        }
    }

    private lateinit var adapter: SearchResultAdapter

    override fun getBindingObject() = FragmentSearchResultBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        binding.toolbar.tvTitle.setText(R.string.label_search_results)

        setAdapter()
        setListDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetListEmptyParams) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldShowBottomProgress) {
            showBottomProgress(it)
        }
        observeLiveData(viewModel.ldSetList) {
            setList(it)
            setListDecor()
        }
        observeLiveData(viewModel.ldOpenDetailPage) {
            openDetailPage(it.first, it.second)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.tdsvSearch.setMyScrollChangeListener(object :
            TouchDetectableScrollView.OnMyScrollChangeListener {
            override fun onScrollUp() {

            }

            override fun onScrollDown() {
            }

            override fun onBottomReached() {
                viewModel.onBottomReached()
            }

            override fun onTopReached() {

            }
        })
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = SearchResultAdapter { data, pos ->
                viewModel.onItemClicked(data, pos)
            }
        }

        binding.recyclerLiked.adapter = adapter
    }

    private fun setList(list: MutableList<SearchUser>) {
        adapter.setData(list)
    }

    private fun showBottomProgress(show: Boolean) {
        binding.pbBottom.setVisibilityState(show)
    }

    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.tvEmptyList.setVisibilityState(isEmpty)
        binding.recyclerLiked.setVisibilityState(!isEmpty)
    }

    private fun setListDecor() {
        binding.recyclerLiked.clearItemDecorations()

        binding.recyclerLiked.addItemDecoration(
            VerticalGridListItemDecoration(
                resources.getDimension(R.dimen.rightAnswersSpace).toInt(),
                2
            )
        )
    }

    private fun openDetailPage(userId: Long, showChatBtn: Boolean) {
        val fragment = SearchDetailFragment.newInstance(userId, showChatBtn)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            SearchDetailFragment.fragmentOpenTag
        )
    }
}