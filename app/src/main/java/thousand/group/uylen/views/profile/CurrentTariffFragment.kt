package thousand.group.uylen.views.profile

import android.os.Bundle
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.TariffPrice
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentCurrentTariffBinding
import thousand.group.uylen.utils.adapters.recycler_view.CurrentPeriodAdapter
import thousand.group.uylen.utils.adapters.recycler_view.RecentSearchUsersAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.decorations.VerticalGridListItemDecoration
import thousand.group.uylen.utils.decorations.VerticalListItemDecoration
import thousand.group.uylen.utils.extensions.clearItemDecorations
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.profile.CurrentTariffViewModel

class CurrentTariffFragment :
    BaseFragment<FragmentCurrentTariffBinding, CurrentTariffViewModel>(
        CurrentTariffViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true
            )
        )

        fun newInstance(): CurrentTariffFragment {
            val fragment = CurrentTariffFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: CurrentPeriodAdapter

    override fun getBindingObject() = FragmentCurrentTariffBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        binding.toolbar.tvTitle.setText(R.string.label_my_tariff)

    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetPeriodsAdapter) {
            setAdapter(it.first, it.second)
        }
        observeLiveData(viewModel.ldSetPeriodsList) {
            setList(it)
        }
        observeLiveData(viewModel.ldSetTarifParams) {
            setParams(it)
        }
        observeLiveData(viewModel.ldClosePage) {
            requireActivity().onBackPressed()
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.btnCancelTariff.setSafelyClickListener {
            viewModel.onCancelTariffClicked()
        }
    }

    private fun setAdapter(priceId: Long, endDate: String) {
        if (!::adapter.isInitialized) {
            adapter = CurrentPeriodAdapter(priceId, endDate)
        }

        binding.rcvPeriods.adapter = adapter
    }

    private fun setList(list: MutableList<TariffPrice>) {
        adapter.setData(list)
    }

    private fun setParams(params: ParamsContainer) {
        with(params) {
            getString(AppConstants.BundleConstants.NAME)?.apply {
                binding.tvTitleTarif.setText(this)
            }
            getString(AppConstants.BundleConstants.PHOTO)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error)
                    .into(binding.ivAvatar)
            }
        }
    }
}