package thousand.group.uylen.views.main

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.FileProvider
import com.github.dhaval2404.imagepicker.ImagePicker
import thousand.group.uylen.databinding.FragmentPhotoSelectBinding
import thousand.group.uylen.utils.base.BaseDialogFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.main.PhotoVideoSelectionViewModel
import java.io.File

class PhotoVideoSelectionDialogFragment :
    BaseDialogFragment<FragmentPhotoSelectBinding, PhotoVideoSelectionViewModel>(
        PhotoVideoSelectionViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isShowBottomNavBar = true,
                    isLightStatusBar = true,
                    navBarItemPos = 4
                )
            )

        val fragmentOpenTag2 = AuthFragmentHelper.getJsonFragmentTag(
            AuthFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true,
                isTabsVisible = false
            )
        )

        val fragmentOpenTag3 = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true
            )
        )

        fun newInstance(tag: String, contentType: String): PhotoVideoSelectionDialogFragment {
            val fragment = PhotoVideoSelectionDialogFragment()
            val args = Bundle()
            //passing arguments
            args.putString(AppConstants.BundleConstants.VM_TAG, tag)
            args.putString(AppConstants.CameraActionConstants.CONTENT_TYPE, contentType)

            fragment.arguments = args
            return fragment
        }
    }

    private val selectImageFromGalleryResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                viewModel.onGalleryResultSuccess(it.data?.data)
            }
        }

    private val cameraActivityResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                viewModel.onCameraResultSuccess(it.data?.data)
            }
        }

    override fun getBindingObject() = FragmentPhotoSelectBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldDismissDialog) {
            dismiss()
        }
        observeLiveData(viewModel.ldOpenGallery) {
            loadFromGallery(it)
        }
        observeLiveData(viewModel.ldSetPhotoCameraOptions) {
            createUriAndSendCameraPhoto(it)
        }
        observeLiveData(viewModel.ldOpenCameraForPhoto) {
            openCameraForPhoto()
        }
        observeLiveData(viewModel.ldOpenCameraForVideo) {
            openCameraForVideo()
        }
    }

    override fun initController() {
        binding.btnGetFromGallery.setSafelyClickListener {
            viewModel.onOpenGalleryBtnClicked()
        }

        binding.btnTakePhoto.setSafelyClickListener {
            viewModel.onCameraBtnClicked()
        }
    }

    private fun loadFromGallery(type: String) {
        ImagePicker.with(this)
            .galleryOnly()
            .createIntent {
                it.setType(type)

                selectImageFromGalleryResult.launch(it)
            }
    }

    private fun createUriAndSendCameraPhoto(file: File) {
        val uri = FileProvider.getUriForFile(
            requireContext(),
            "${requireActivity().packageName}.fileprovider",
            file
        )

        viewModel.setCameraUri(uri)

        openCamera(uri, MediaStore.ACTION_IMAGE_CAPTURE)
    }

    private fun openCamera(uri: Uri?, action: String) {
        val intent = Intent(action)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)

        uri?.apply {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        }

        cameraActivityResult.launch(intent)
    }

    private fun openCameraForPhoto() {
        requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)?.apply {
            viewModel.createAndOpenPhotoCamera(this)
        }
    }

    private fun openCameraForVideo() {
        openCamera(null, MediaStore.ACTION_VIDEO_CAPTURE)
    }

}