package thousand.group.uylen.views.main

import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import thousand.group.uylen.databinding.FragmentCropBitmapBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.main.CropBitmapViewModel

class CropBitmapFragment :
    BaseFragment<FragmentCropBitmapBinding, CropBitmapViewModel>(
        CropBitmapViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true
                )
            )

        fun newInstance(tag: String, uri: Uri): CropBitmapFragment {
            val fragment = CropBitmapFragment()
            val args = Bundle()
            //passing arguments

            args.putString(AppConstants.BundleConstants.VM_TAG, tag)
            args.putParcelable(AppConstants.BundleConstants.URI, uri)

            fragment.arguments = args
            return fragment
        }

        fun newInstance(tag: String, bitmap: Bitmap): CropBitmapFragment {
            val fragment = CropBitmapFragment()
            val args = Bundle()
            //passing arguments

            args.putString(AppConstants.BundleConstants.VM_TAG, tag)
            args.putParcelable(AppConstants.BundleConstants.BITMAP, bitmap)

            fragment.arguments = args
            return fragment
        }
    }

    override fun getBindingObject() = FragmentCropBitmapBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetBitmap) {
            binding.civ.setImageBitmap(it)
        }
        observeLiveData(viewModel.ldClosePage) {
            requireActivity().onBackPressed()
        }
    }

    override fun initController() {
        binding.toolbar.ivCheckedMark.setSafelyClickListener {
            viewModel.showProgressBar(true)

            viewModel.doWork {
                viewModel.onReadyBtnClicked(binding.civ.croppedImage)
                viewModel.showProgressBar(false)
            }
        }
        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.root.setSafelyClickListener { }
    }

}
