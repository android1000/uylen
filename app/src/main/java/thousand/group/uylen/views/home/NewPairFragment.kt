package thousand.group.uylen.views.home

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentNewPairBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.extensions.getSupportFragmentManager
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.home.NewPairViewModel
import thousand.group.uylen.views.chat.ChatMessagesFragment
import thousand.group.uylen.views.chat.MutualSympathyFragment
import thousand.group.uylen.views.favorite.FavouriteFragment
import thousand.group.uylen.views.main.PhotoVideoSelectionDialogFragment

class NewPairFragment :
    BaseFragment<FragmentNewPairBinding, NewPairViewModel>(
        NewPairViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true
                )
            )

        fun newInstance(firstUser: UserCardData, secondUser: User): NewPairFragment {
            val fragment = NewPairFragment()
            val args = Bundle()
            //passing arguments

            args.putParcelable(AppConstants.BundleConstants.USER_1, firstUser)
            args.putParcelable(AppConstants.BundleConstants.USER_2, secondUser)

            fragment.arguments = args
            return fragment
        }
    }

    private val PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )

    private lateinit var permReqLauncher: ActivityResultLauncher<Array<String>>

    private var dialogFragmentPhotoVideo: PhotoVideoSelectionDialogFragment? = null

    override fun getBindingObject() = FragmentNewPairBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetInitialParams) {
            setInitialParams(it)
        }
        observeLiveData(viewModel.ldOpenChatPage) {
            Log.i(fragmentTag, "ldOpenChatPage stop")
            openChatMessagesPage(it)
        }
        observeLiveData(viewModel.ldShowPhotoSelectDialog) {
            showPhotoVideoSelectDialog(it)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.ivClose.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.btnContinueSwiping.setSafelyClickListener {
            openMutualSympathyFragment()
        }

        binding.btnSendMessage.setSafelyClickListener {
            viewModel.onWriteMessageClicked()
        }

        binding.ivSendMessage.setSafelyClickListener {
            viewModel.sendMessage(binding.etWriteMessage.text.toString())
        }

        binding.ivPlus.setSafelyClickListener {
            requestPermissionParams()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        permReqLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                Log.i(fragmentTag, "registerForActivityResult -> permissions: ${permissions}")

                val granted = permissions.entries.all {
                    it.value == true
                }

                if (granted) {
                    viewModel.openPhotoVideoDialogFragment()
                } else if (hasPermissionBlockedOrNeverAsked(PERMISSIONS)) {
                    parseSettingsParams()
                }
            }
    }

    private fun requestPermissionParams() {
        if (hasPermissions(PERMISSIONS)) {
            viewModel.openPhotoVideoDialogFragment()
        } else {
            permReqLauncher.launch(PERMISSIONS)
        }
    }

    private fun hasPermissions(permissions: Array<String>): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(
                requireContext(),
                it
            ) == PackageManager.PERMISSION_GRANTED
        }

    private fun hasPermissionBlockedOrNeverAsked(permissions: Array<String>) = permissions.all {
        ActivityCompat.checkSelfPermission(
            requireContext(),
            it
        ) == PackageManager.PERMISSION_DENIED
                && !shouldShowRequestPermissionRationale(it)

    }

    private fun showPhotoVideoSelectDialog(pair: Pair<String, String>) {
        dialogFragmentPhotoVideo =
            PhotoVideoSelectionDialogFragment.newInstance(pair.first, pair.second)

        dialogFragmentPhotoVideo?.show(
            getSupportFragmentManager(),
            PhotoVideoSelectionDialogFragment.fragmentOpenTag3
        )
    }

    private fun parseSettingsParams() {
        val uri = Uri.fromParts(
            requireContext().getString(R.string.field_package),
            requireContext().packageName,
            null
        )
        val intent = Intent()

        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.data = uri

        requireActivity().startActivity(intent)
    }

    private fun setInitialParams(params: ParamsContainer) {
        params.getString(AppConstants.BundleConstants.USER_PHOTO_1)?.apply {
            Picasso.get()
                .load(this)
                .error(R.drawable.ic_error_avatar)
                .into(binding.ivLeftAvatar)
        }
        params.getString(AppConstants.BundleConstants.USER_PHOTO_2)?.apply {
            Picasso.get()
                .load(this)
                .error(R.drawable.ic_error_avatar)
                .into(binding.ivRightAvatar)
        }
        params.getString(AppConstants.BundleConstants.MATCH_TEXT)?.apply {
            binding.tvNewPairUser.setText(this)
        }
    }

    private fun openChatMessagesPage(receiverId: Long) {
        val fragment = ChatMessagesFragment.newInstance(receiverId)

        removeAndReplaceWithAnotherFragment(
            R.id.fl_fragment_container,
            fragment,
            ChatMessagesFragment.fragmentOpenTag,
            fragmentOpenTag
        )
    }

    private fun openMutualSympathyFragment() {
        val fragment = FavouriteFragment.newInstance()

        removeAndReplaceWithAnotherFragment(
            R.id.fl_fragment_container,
            fragment,
            FavouriteFragment.fragmentOpenTag,
            fragmentOpenTag
        )
    }


}