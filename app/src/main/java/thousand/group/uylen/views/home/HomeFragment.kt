package thousand.group.uylen.views.home

import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import com.yuyakaido.android.cardstackview.*
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.uylen.databinding.FragmentHomeBinding
import thousand.group.uylen.utils.adapters.recycler_view.HomeCardAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.home.HomeViewModel
import thousand.group.uylen.R
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.*

class HomeFragment :
    BaseFragment<FragmentHomeBinding, HomeViewModel>(
        HomeViewModel::class
    ), CardStackListener {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true,
                    isShowBottomNavBar = true,
                    navBarItemPos = 0
                )
            )

        fun newInstance(fromSignUpPage:Boolean = false): HomeFragment {
            val fragment = HomeFragment()
            val args = Bundle()
            //passing arguments

            args.putBoolean(AppConstants.BundleConstants.FROM_SIGN_UP_PAGE, fromSignUpPage)

            fragment.arguments = args
            return fragment
        }
    }

    private var cardManager: CardStackLayoutManager? = null
    private lateinit var cardAdapter: HomeCardAdapter

    private val rewindSettings = RewindAnimationSetting.Builder()
        .setDirection(Direction.Bottom)
        .setDuration(Duration.Normal.duration)
        .setInterpolator(DecelerateInterpolator())
        .build()

    private val superLikeSettings = SwipeAnimationSetting.Builder()
        .setDirection(Direction.Top)
        .setDuration(Duration.Normal.duration)
        .setInterpolator(AccelerateInterpolator())
        .build()

    private val cancelSetting = SwipeAnimationSetting.Builder()
        .setDirection(Direction.Left)
        .setDuration(Duration.Normal.duration)
        .setInterpolator(AccelerateInterpolator())
        .build()

    private val likeSetting = SwipeAnimationSetting.Builder()
        .setDirection(Direction.Right)
        .setDuration(Duration.Normal.duration)
        .setInterpolator(AccelerateInterpolator())
        .build()

    override fun getBindingObject() = FragmentHomeBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        binding.csvCards.itemAnimator = null

        viewModel.setAdapter()
        setUpCardManager()

    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetList) {
            setList(it.first, it.second)
        }
        observeLiveData(viewModel.ldSetItem) {
            cardAdapter.setItem(it.first, it.second)
        }
        observeLiveData(viewModel.ldOpenDetailPage) {
            openDetailPage(it.first, it.second)
        }
        observeLiveData(viewModel.ldCancelCard) {
            cardManager?.apply {
                setSwipeAnimationSetting(cancelSetting)
                binding.csvCards.swipe()
            }
        }
        observeLiveData(viewModel.ldReplyCard) {
            cardManager?.apply {
                setRewindAnimationSetting(rewindSettings)
                binding.csvCards.rewind()
            }
        }
        observeLiveData(viewModel.ldLikeCard) {
            cardManager?.apply {
                setSwipeAnimationSetting(likeSetting)
                binding.csvCards.swipe()
            }

        }
        observeLiveData(viewModel.ldSuperLikeCard) {
            cardManager?.apply {
                setSwipeAnimationSetting(superLikeSettings)
                binding.csvCards.swipe()
            }

        }
        observeLiveData(viewModel.ldActivateCancelFab) {
            activateCancelFab()
        }
        observeLiveData(viewModel.ldActivateLikeFab) {
            activateLikeFab()
        }
        observeLiveData(viewModel.ldActivateSuperLikeFab) {
            activateSuperLikeFab()
        }
        observeLiveData(viewModel.ldActivateRewindFab) {
            activateRewindFab()
        }
        observeLiveData(viewModel.ldCancelAllActivation) {
            cancelFabActivation()
        }
        observeLiveData(viewModel.ldCancelSuperLikeFab) {
            binding.fabStars.imageTintList = null
            binding.fabStars.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        }
        observeLiveData(viewModel.ldCancelLikeFab) {
            binding.fabLike.imageTintList = null
            binding.fabLike.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        }
        observeLiveData(viewModel.ldCancelCancelFab) {
            binding.fabCancel.imageTintList = null
            binding.fabCancel.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        }
        observeLiveData(viewModel.ldSetListEmptyParams) {
            setEmptyList(it)
        }
        observeLiveData(viewModel.ldSetAdapter) {
            setAdapter(it)
        }
        observeLiveData(viewModel.ldOpenNewPairPage) {
            openNewPairFragment(it.first, it.second)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.fabReply.setSafelyClickListener {
            cardManager?.apply {
                viewModel.replyCard(topPosition)
            }
        }

        binding.fabCancel.setSafelyClickListener {
            Log.i(fragmentTag, "fabCancel -> click")

            cardManager?.apply {
                viewModel.cancelCard(topPosition)
            }
        }

        binding.fabLike.setSafelyClickListener {
            Log.i(fragmentTag, "fabLike -> click")
            cardManager?.apply {
                viewModel.likeCard(topPosition)
            }
        }

        binding.fabStars.setSafelyClickListener {
            cardManager?.apply {
                viewModel.superLikeCard(topPosition)
            }
        }
    }

    override fun onCardDragging(direction: Direction?, ratio: Float) {
        Log.i(fragmentTag, "onCardDragging -> direction:$direction, ratio:$ratio")

        direction?.apply {
            viewModel.parseCardSwipeDirection(direction)
        }
    }

    override fun onCardSwiped(direction: Direction?) {
        Log.i(fragmentTag, "onCardSwiped -> direction:$direction")

        cardManager?.topPosition?.apply {
            direction?.let {
                viewModel.onCardSwipe(this, it)
            }
            viewModel.paginateOnCardSwiped(this)
        }

        cancelFabActivation()
    }

    override fun onCardRewound() {
        Log.i(fragmentTag, "onCardRewound")
        cancelFabActivation()
    }

    override fun onCardCanceled() {
        Log.i(fragmentTag, "onCardCanceled")
        cancelFabActivation()
    }

    override fun onCardAppeared(view: View?, position: Int) {
        Log.i(fragmentTag, "onCardAppeared -> position:$position")
    }

    override fun onCardDisappeared(view: View?, position: Int) {
        Log.i(fragmentTag, "onCardDisappeared -> position:$position")

    }

    private fun setUpCardManager() {
        cardManager = CardStackLayoutManager(requireContext(), this)

        cardManager?.apply {
            setStackFrom(StackFrom.Top)
            setVisibleCount(3)
            setTranslationInterval(3.0f)
            setScaleInterval(0.95f)
            setSwipeThreshold(0.3f)
            setMaxDegree(40.0f)
            setDirections(mutableListOf(Direction.Top, Direction.Left, Direction.Right))
            setCanScrollHorizontal(true)
            setCanScrollVertical(true)
            setSwipeableMethod(SwipeableMethod.AutomaticAndManual)
            setOverlayInterpolator(LinearInterpolator())
        }

        binding.csvCards.layoutManager = cardManager

        binding.csvCards.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
    }

    private fun setAdapter(topPosition: Int) {
        if (!::cardAdapter.isInitialized) {
            cardAdapter = HomeCardAdapter(
                { model, position ->
                    doIfInternetConnected {
                        viewModel.onLeftPlaneClicked(model, position)
                    }
                },
                { model, position ->
                    doIfInternetConnected {
                        viewModel.onRightPlaneClicked(model, position)
                    }
                },
                { model, position ->
                    doIfInternetConnected {
                        viewModel.onBottomPlaneClicked(model, position)
                    }
                }
            )
        }

        binding.csvCards.adapter = cardAdapter

        if (cardAdapter.itemCount > 0) {
            cardManager?.topPosition = topPosition
        }
    }

    private fun setList(list: MutableList<UserCardData>, topPosition: Int) {
        cardAdapter.setData(list)
        cardManager?.topPosition = topPosition
    }

    private fun openDetailPage(model: UserCardData, position: Int) {
        val fragment = HomeDetailFragment.newInstance(model, position)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            HomeDetailFragment.fragmentOpenTag
        )
    }

    private fun activateLikeFab() {
        binding.fabLike.imageTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        binding.fabLike.backgroundTintList = ColorStateList.valueOf(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorBlueGradientStart
            )
        )
    }

    private fun activateSuperLikeFab() {
        binding.fabStars.imageTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        binding.fabStars.backgroundTintList = ColorStateList.valueOf(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorBlueGradientStart
            )
        )
    }

    private fun activateCancelFab() {
        binding.fabCancel.imageTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        binding.fabCancel.backgroundTintList = ColorStateList.valueOf(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorBlueGradientStart
            )
        )
    }

    private fun activateRewindFab() {
        binding.fabReply.imageTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        binding.fabReply.backgroundTintList = ColorStateList.valueOf(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorBlueGradientStart
            )
        )
    }

    private fun cancelFabActivation() {
        binding.fabLike.imageTintList = null
        binding.fabLike.backgroundTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        binding.fabStars.imageTintList = null
        binding.fabStars.backgroundTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        binding.fabCancel.imageTintList = null
        binding.fabCancel.backgroundTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
        binding.fabReply.imageTintList = null
        binding.fabReply.backgroundTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorWhite))
    }

    private fun setEmptyList(showEmptyText: Boolean) {
        binding.csvCards.setVisibilityState(!showEmptyText)
        binding.tvEmptyList.setVisibilityState(showEmptyText)

        binding.fabLike.isClickable = !showEmptyText
        binding.fabLike.isFocusable = !showEmptyText

        binding.fabStars.isClickable = !showEmptyText
        binding.fabStars.isFocusable = !showEmptyText

        binding.fabCancel.isClickable = !showEmptyText
        binding.fabCancel.isFocusable = !showEmptyText

    }

    private fun openNewPairFragment(firstUser: UserCardData, secondUser: User) {
        val fragment = NewPairFragment.newInstance(firstUser, secondUser)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            NewPairFragment.fragmentOpenTag
        )
    }

}