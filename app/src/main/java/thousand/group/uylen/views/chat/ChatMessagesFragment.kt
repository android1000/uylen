package thousand.group.uylen.views.chat

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.ChatLocaleDateMessage
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentChatBinding
import thousand.group.uylen.utils.adapters.recycler_view.ChatMessagesAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.custom_views.TouchDetectableScrollView
import thousand.group.uylen.utils.decorations.VerticalListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.chat.ChatMessagesViewModel
import thousand.group.uylen.views.favorite.FavouriteDetailFragment
import thousand.group.uylen.views.main.PhotoVideoSelectionDialogFragment


class ChatMessagesFragment :
    BaseFragment<FragmentChatBinding, ChatMessagesViewModel>(
        ChatMessagesViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true,
                isShowBottomNavBar = false
            )
        )

        fun newInstance(receiverId: Long): ChatMessagesFragment {
            val fragment = ChatMessagesFragment()
            val args = Bundle()
            //passing arguments

            args.putLong(AppConstants.BundleConstants.RECEIVER_ID, receiverId)

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: ChatMessagesAdapter

    private val PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )

    private lateinit var permReqLauncher: ActivityResultLauncher<Array<String>>

    private var dialogFragmentPhotoVideo: PhotoVideoSelectionDialogFragment? = null

    override fun getBindingObject() = FragmentChatBinding.inflate(layoutInflater)

    override fun internetSuccess() {
        viewModel.onInternetSuccess()
    }

    override fun internetError() {
        viewModel.onInternetError()
    }

    override fun initView(savedInstanceState: Bundle?) {

        //0x10 - Adjust Resize
        requireActivity().window?.setSoftInputMode(0x10)

        binding.rcv.itemAnimator = null
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldClearMessage) {
            binding.etWriteMessage.text.clear()
            binding.etWriteMessage.requestFocus()
        }
        observeLiveData(viewModel.ldSetChatList) {
            setList(it)
            setListDecor()
        }
        observeLiveData(viewModel.ldSetEmptyListParams) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldShowTopProgress) {
            showTopProgress(it)
        }
        observeLiveData(viewModel.ldSetAdapter) {
            setAdapter(it.first, it.second)
            setListDecor()
        }
        observeLiveData(viewModel.ldSetUsersPair) {
            setUsersPairParams(it)
        }
        observeLiveData(viewModel.ldOpenDetailPage) {
            openDetailPage(it.first, it.second)
        }
        observeLiveData(viewModel.ldScrollDown) {
            binding.tdsvChat.post {
                binding.tdsvChat.fullScroll(View.FOCUS_DOWN)

                binding.etWriteMessage.requestFocus()
            }
        }
        observeLiveData(viewModel.ldShowPhotoSelectDialog) {
            showPhotoVideoSelectDialog(it)
        }
        observeLiveData(viewModel.ldSetBottomEnterVisibility) {
            binding.ctlBottomPlane.setVisibilityState(it)
        }
        observeLiveData(viewModel.ldUpdateMessageItem) {
            adapter.setItem(it.first, it.second)
        }

    }

    override fun initController() {
        binding.root.setSafelyClickListener { }

        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.toolbar.ivAvatar.setSafelyClickListener {
            viewModel.onReceiverAvatarClicked()
        }

        binding.ivSendMessage.setSafelyClickListener {
            viewModel.sendMessage(binding.etWriteMessage.text.toString())
        }

        binding.tdsvChat.setMyScrollChangeListener(object :
            TouchDetectableScrollView.OnMyScrollChangeListener {
            override fun onScrollUp() {

            }

            override fun onScrollDown() {
            }

            override fun onBottomReached() {
//                viewModel.onBottomReached()
            }

            override fun onTopReached() {
                Log.i(fragmentTag, "onTopReached")

                viewModel.onTopReached()
            }
        })

        binding.rcv.setOnTouchListener { v, event ->
            viewModel.closeKeyBoard()
            true
        }

        binding.ivPlus.setSafelyClickListener {
            viewModel.closeKeyBoard()
            requestPermissionParams()
        }

/*        requireActivity().window.decorView.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            //r will be populated with the coordinates of your view that area still visible.
            //r will be populated with the coordinates of your view that area still visible.
            requireView().getWindowVisibleDisplayFrame(r)

            val heightDiff =  requireView().rootView.height - (r.bottom - r.top)
            if (heightDiff > 500) { // if more than 100 pixels, its probably a keyboard...
                binding.gdlHor.setGuidelinePercent(0.6f)
            }else{
                binding.gdlHor.setGuidelinePercent(0.77f)
            }

        }*/

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        permReqLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                Log.i(fragmentTag, "registerForActivityResult -> permissions: ${permissions}")

                val granted = permissions.entries.all {
                    it.value == true
                }

                if (granted) {
                    viewModel.openPhotoVideoDialogFragment()
                } else if (hasPermissionBlockedOrNeverAsked(PERMISSIONS)) {
                    parseSettingsParams()
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()

        requireActivity().window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    private fun requestPermissionParams() {
        if (hasPermissions(PERMISSIONS)) {
            viewModel.openPhotoVideoDialogFragment()
        } else {
            permReqLauncher.launch(PERMISSIONS)
        }
    }

    private fun hasPermissions(permissions: Array<String>): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(
                requireContext(),
                it
            ) == PackageManager.PERMISSION_GRANTED
        }

    private fun hasPermissionBlockedOrNeverAsked(permissions: Array<String>) = permissions.all {
        ActivityCompat.checkSelfPermission(
            requireContext(),
            it
        ) == PackageManager.PERMISSION_DENIED
                && !shouldShowRequestPermissionRationale(it)

    }

    private fun showPhotoVideoSelectDialog(pair: Pair<String, String>) {
        dialogFragmentPhotoVideo =
            PhotoVideoSelectionDialogFragment.newInstance(pair.first, pair.second)

        dialogFragmentPhotoVideo?.show(
            getSupportFragmentManager(),
            PhotoVideoSelectionDialogFragment.fragmentOpenTag3
        )
    }

    private fun parseSettingsParams() {
        val uri = Uri.fromParts(
            requireContext().getString(R.string.field_package),
            requireContext().packageName,
            null
        )
        val intent = Intent()

        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.data = uri

        requireActivity().startActivity(intent)
    }

    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.ivLeftAvatar.setVisibilityState(isEmpty)
        binding.ivRightAvatar.setVisibilityState(isEmpty)
        binding.tvPairTitle.setVisibilityState(isEmpty)
        binding.tvTimeAgo.setVisibilityState(isEmpty)
        binding.rcv.setVisibilityState(!isEmpty)
    }

    private fun setAdapter(senderId: Long, receiverId: Long) {
        if (!::adapter.isInitialized) {
            adapter = ChatMessagesAdapter(senderId, receiverId) {
                openPhotoDetailPage(it)
            }
        }

        binding.rcv.adapter = adapter
    }

    private fun setList(list: MutableList<ChatLocaleDateMessage>) {
        adapter.setData(list)
    }

    private fun setListDecor() {
        binding.rcv.clearItemDecorations()

        binding.rcv.addItemDecoration(
            VerticalListItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.chatMessageSize),
                adapter.itemCount
            )
        )
    }

    private fun showTopProgress(show: Boolean) {
        binding.pbTop.setVisibilityState(show)
    }

    private fun setUsersPairParams(params: ParamsContainer) {
        with(params) {
            getString(AppConstants.BundleConstants.USER_PHOTO_1)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.ivLeftAvatar)

                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.toolbar.ivAvatar)
            }
            getString(AppConstants.BundleConstants.USER_PHOTO_2)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.ivRightAvatar)
            }
            getString(AppConstants.BundleConstants.CHAT_PAIR_TEXT)?.apply {
                binding.tvPairTitle.setText(this)
            }
            getString(AppConstants.BundleConstants.CHAT_PAIR_TEXT_DATE)?.apply {
                binding.tvTimeAgo.setText(this)
            }
            getString(AppConstants.BundleConstants.NAME)?.apply {
                binding.toolbar.tvTitle.setText(this)
            }
        }
    }

    private fun openDetailPage(userId: Long, selPos: Int) {
        val fragment = FavouriteDetailFragment.newInstance(userId, selPos)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            FavouriteDetailFragment.fragmentOpenTag
        )
    }

    private fun openPhotoDetailPage(imagePath: String) {
        val fragment = ChatPhotoDetailFragment.newInstance(imagePath)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            ChatPhotoDetailFragment.fragmentOpenTag
        )
    }

}