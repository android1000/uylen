package thousand.group.uylen.views.registration

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.onesignal.OneSignal
import com.redmadrobot.inputmask.MaskedTextChangedListener
import thousand.group.data.entities.remote.simple.User
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentFirstRegistrationBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.extensions.getSupportFragmentManager
import thousand.group.uylen.utils.extensions.makeLinks
import thousand.group.uylen.utils.extensions.replaceFragmentWithBackStack
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.registration.FirstRegistrationViewModel
import thousand.group.uylen.views.auth.AuthActivity
import thousand.group.uylen.views.profile.PrivacyTermsFragment

class FirstRegistrationFragment :
    BaseFragment<FragmentFirstRegistrationBinding, FirstRegistrationViewModel>(
        FirstRegistrationViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            AuthFragmentHelper.getJsonFragmentTag(
                AuthFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true,
                    isTabsVisible = true,
                    tabsPosition = 0
                )
            )

        fun newInstance(): FirstRegistrationFragment {
            val fragment = FirstRegistrationFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    override fun getBindingObject() = FragmentFirstRegistrationBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        getOneSignalUserId()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldOpenVerifyPage) {
            openVerifyRegistrationPage(it)
        }
        observeLiveData(viewModel.ldOpenTermPage) {
            openTermsPage(it)
        }
        observeLiveData(viewModel.ldReloadActivity) {
            reloadActivity()
        }
        observeLiveData(viewModel.ldSetLanguageText) {
            binding.cttaLanguage.setText(it)
        }
        observeLiveData(viewModel.ldOpenSecondRegistrationPage) {
            openSecondRegistrationFragment(it.first.first, it.first.second, it.second)
        }
    }

    override fun initController() {
        MaskedTextChangedListener.installOn(
            binding.etPhone,
            requireContext().getString(R.string.format_phone_number),
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(
                    maskFilled: Boolean,
                    extractedValue: String,
                    formattedValue: String
                ) {
                    viewModel.checkPhone(maskFilled, extractedValue, formattedValue)
                }
            }
        )

        binding.tvPrivacy.makeLinks(
            Triple(
                resources.getString(R.string.label_part_privacy),
                ContextCompat.getColor(requireContext(), R.color.colorBlack),
                View.OnClickListener {
                    viewModel.openPrivacyPolicy()
                }),
            Triple(
                resources.getString(R.string.label_part_terms),
                ContextCompat.getColor(requireContext(), R.color.colorBlack),
                View.OnClickListener {
                    viewModel.openTermsOfUse()
                })
        )


        binding.btnAfter.setSafelyClickListener {
            viewModel.checkLocation(
                binding.etName.getText(),
                binding.etPassword.getText(),
                binding.stvCourseActive.isChecked
            )
        }

        binding.cttaLanguage.setSafelyClickListener {
            viewModel.onLanguageTextClicked()
        }
    }

    private fun openVerifyRegistrationPage(pair: Pair<String, Pair<Double, Double>>) {
        val fragment = VerifyRegistrationFragment.newInstance(pair.first, pair.second)

        getSupportFragmentManager().replaceFragmentWithBackStack(
            R.id.fl_fragment_container,
            fragment,
            VerifyRegistrationFragment.fragmentOpenTag
        )
    }

    private fun openSecondRegistrationFragment(
        user: User,
        token: String,
        location: Pair<Double, Double>
    ) {
        val fragment = SecondRegistrationFragment.newInstance(user, token, location)

        getSupportFragmentManager().replaceFragmentWithBackStack(
            R.id.fl_fragment_container,
            fragment,
            SecondRegistrationFragment.fragmentOpenTag
        )
    }

    private fun getOneSignalUserId() {
        val userId = OneSignal.getDeviceState()?.userId
        viewModel.setOneSignalUserId(userId)
    }

    private fun openTermsPage(type: String) {
        val fragment = PrivacyTermsFragment.newInstance(type)

        getSupportFragmentManager().replaceFragmentWithBackStack(
            R.id.fl_fragment_container,
            fragment,
            PrivacyTermsFragment.fragmentOpenTag
        )
    }

    private fun reloadActivity() {
        with(requireActivity()) {
            val intent = Intent(this, AuthActivity::class.java)

            intent.putExtra(AppConstants.BundleConstants.RELOAD_ACTIVITY, true)

            overridePendingTransition(0, 0)
            finish()
            overridePendingTransition(0, 0)
            startActivity(intent)
            overridePendingTransition(0, 0)

            viewModelStore.clear()
        }
    }

}