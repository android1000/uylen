package thousand.group.uylen.views.restore_password

import android.os.Bundle
import android.os.CountDownTimer
import androidx.core.widget.addTextChangedListener
import thousand.group.data.entities.remote.simple.User
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentSendRestoreCodeBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.clearAndReplaceFragment
import thousand.group.uylen.utils.extensions.getSupportFragmentManager
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.restore_password.SendRestoreCodeViewModel

class SendRestoreCodeFragment :
    BaseFragment<FragmentSendRestoreCodeBinding, SendRestoreCodeViewModel>(
        SendRestoreCodeViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            AuthFragmentHelper.getJsonFragmentTag(
                AuthFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true,
                    isTabsVisible = false
                )
            )

        fun newInstance(phone: String, user: User): SendRestoreCodeFragment {
            val fragment = SendRestoreCodeFragment()
            val args = Bundle()
            //passing arguments

            args.putString(AppConstants.BundleConstants.PHONE_NUMBER, phone)
            args.putParcelable(AppConstants.BundleConstants.USER, user)

            fragment.arguments = args
            return fragment
        }
    }

    private var timer = object : CountDownTimer(10000, 1000) {
        override fun onFinish() {
            viewModel.onTimerFinished()
        }

        override fun onTick(millisUntilFinished: Long) {
            viewModel.onTimerTicker(millisUntilFinished)
        }
    }

    override fun getBindingObject() = FragmentSendRestoreCodeBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldTimerText) {
            binding.btnCode.setText(it)
        }
        observeLiveData(viewModel.ldTimerTextEnabled) {
            binding.btnCode.isEnabled = it
        }
        observeLiveData(viewModel.ldStartTimer) {
            startTimer()
        }
        observeLiveData(viewModel.ldStopTimer) {
            stopTimer()
        }
        observeLiveData(viewModel.ldClosePage) {
            requireActivity().onBackPressed()
        }
        observeLiveData(viewModel.ldSetPhoneText) {
            binding.tvPhone.setText(it)
        }
        observeLiveData(viewModel.ldOpenRestorePasswordPage) {
            openRestorePasswordFragment(it.first, it.second)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.etRestoreCode.addTextChangedListener {
            viewModel.onCodeChanged(it.toString())
        }

        binding.btnCode.setSafelyClickListener {
            viewModel.onSendCodeBtnClicked()
        }

    }

    private fun startTimer() {
        timer.start()
    }

    private fun stopTimer() {
        timer.cancel()
        viewModel.onTimerStop()
    }

    private fun openRestorePasswordFragment(token: String, phone: String) {
        val fragment = RestorePasswordFragment.newInstance(token, phone)

        getSupportFragmentManager().clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            RestorePasswordFragment.fragmentOpenTag
        )
    }

}