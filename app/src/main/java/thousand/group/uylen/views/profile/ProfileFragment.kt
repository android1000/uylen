package thousand.group.uylen.views.profile

import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.TariffBanner
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentProfileBinding
import thousand.group.uylen.utils.adapters.recycler_view.ProfileTariffBannerAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.profile.ProfileViewModel
import timber.log.Timber

class ProfileFragment :
    BaseFragment<FragmentProfileBinding, ProfileViewModel>(
        ProfileViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true,
                    isShowBottomNavBar = true,
                    navBarItemPos = 4
                )
            )

        fun newInstance(): ProfileFragment {
            val fragment = ProfileFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var bannersAdapter: ProfileTariffBannerAdapter

    private val viewPagerListener = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            viewModel.onViewPagerPositionChanged(position)
        }
    }

    override fun getBindingObject() = FragmentProfileBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
//        setBannerIndicator()
//        setBannerAdapter()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetProfileData) {
            setProfileParams(it)
        }
/*        observeLiveData(viewModel.ldSetTariffVisibleParams) {
            binding.ctlTariff.setVisibilityState(it)
            binding.ctlNoTariff.setVisibilityState(!it)
        }*/
/*        observeLiveData(viewModel.ldSetBannerList) {
            setBannerList(it)
        }*/
       /* observeLiveData(viewModel.ldSetTarifDesc) {
            binding.tvTitleTarif.setText(it.first)
            binding.tvDesc.setText(it.second)
        }*/
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.fabSettingsProfile.setSafelyClickListener {
            openSettingsFragment()
        }

        binding.fabEditProfile.setSafelyClickListener {
            openEditProfileFragment()
        }

        /*binding.btnNextToShopDetails.setSafelyClickListener {
            openTarifsFragment()
        }

        binding.vpBanner.registerOnPageChangeCallback(viewPagerListener)

        binding.btnNextToShopDetails.setSafelyClickListener {
            openTarifsFragment()
        }

        binding.btnNextToShopDetailsTarif.setSafelyClickListener {
            openCurrentTariffFragment()
        }
*/

    }

    private fun openEditProfileFragment() {
        val fragment = EditProfileFragment.newInstance()

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            EditProfileFragment.fragmentOpenTag
        )
    }

    private fun openSettingsFragment() {
        val fragment = SettingsFragment.newInstance()

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            SettingsFragment.fragmentOpenTag
        )
    }

    private fun openTarifsFragment() {
        val fragment = TarifsFragment.newInstance(ProfileViewModel::class.java.simpleName)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            TarifsFragment.fragmentOpenTag
        )
    }

    private fun openCurrentTariffFragment() {
        val fragment = CurrentTariffFragment.newInstance()

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            CurrentTariffFragment.fragmentOpenTag
        )
    }

    private fun setProfileParams(params: ParamsContainer) {
        with(params) {
            getString(AppConstants.BundleConstants.PHOTO)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.imgAvaProfile)
            }
            getString(AppConstants.BundleConstants.NAME)?.apply {
                binding.txtNameProfile.setText(this)
            }
            getString(AppConstants.BundleConstants.AGE)?.apply {
                binding.txtDescProfile.setText(this)
            }
        }
    }

    /*private fun setBannerAdapter() {
        if (!::bannersAdapter.isInitialized) {
            bannersAdapter = ProfileTariffBannerAdapter()
        }

        binding.vpBanner.adapter = bannersAdapter
    }

    private fun setBannerList(list: MutableList<TariffBanner>) {
        Timber.i("setBannerList -> list:%s", list.toString())

        bannersAdapter.setData(list)
    }

    private fun setBannerIndicator() {
        binding.indvBanners.setSliderWidth(
            resources.getDimension(R.dimen.normalSliderWidth),
            resources.getDimension(R.dimen.selectedSliderWidth)
        )

        binding.indvBanners.setSliderGap(resources.getDimension(R.dimen.sliderGapMargin))

        binding.indvBanners.setupWithViewPager(binding.vpBanner)
    }*/

}
