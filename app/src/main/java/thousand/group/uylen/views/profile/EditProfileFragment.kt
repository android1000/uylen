package thousand.group.uylen.views.profile

import android.Manifest
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.InputFilter
import android.util.Log
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.EditText
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
/*import com.canhub.cropper.CropImageContract
import com.canhub.cropper.CropImageView
import com.canhub.cropper.options*/
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.UserPhoto
import thousand.group.uylen.R
import thousand.group.uylen.databinding.*
import thousand.group.uylen.utils.adapters.recycler_view.UserPhotoAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.decorations.VerticalGridListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.extensions.getSupportFragmentManager
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.locale.EnterSingleInfo
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.main.CropBitmapViewModel
import thousand.group.uylen.view_models.profile.EditProfileViewModel
import thousand.group.uylen.views.auth.EnterSingleInfoFragment
import thousand.group.uylen.views.auth.InterestsFragment
import thousand.group.uylen.views.main.CropBitmapFragment
import thousand.group.uylen.views.main.PhotoVideoSelectionDialogFragment

class EditProfileFragment :
    BaseFragment<FragmentEditProfileBinding, EditProfileViewModel>(
        EditProfileViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true
                )
            )

        fun newInstance(): EditProfileFragment {
            val fragment = EditProfileFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    private val PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )

    private lateinit var permReqLauncher: ActivityResultLauncher<Array<String>>

    /* private val cropImageResult = registerForActivityResult(CropImageContract()) { result ->
         if (result.isSuccessful) {
             // use the returned uri
             result.uriContent?.apply {

                 Log.i(fragmentTag, "cropImageResult -> uriContent: ${this}")

                 viewModel.uploadImage(this)
             }

         } else {
             // an error occurred
             val exception = result.error
         }
     }*/

    private var dialogFragmentPhotoVideo: PhotoVideoSelectionDialogFragment? = null

    private val openningDatePickerListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
            viewModel.onOpenningDateSelected(year, month, dayOfMonth)
        }
    }

    private var zhuzDialogBinding: DialogRuBinding? = null
    private var regionDialogBinding: DialogCityRegionBinding? = null

    private lateinit var adapter: UserPhotoAdapter

    override fun getBindingObject() = FragmentEditProfileBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {

        binding.toolbar.tvTitle.setText(R.string.label_edit_)

        binding.cttaSalaryEdit.getEditText()
            .setFilters(arrayOf<InputFilter>(InputFilter.LengthFilter(10)))

        binding.rcvImages.itemAnimator = null

        setAdapter()
        setListDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetNameAge) {
            binding.tvUsersName.setText(it.first)
            binding.tvUsersAge.setText(it.second)
        }
        observeLiveData(viewModel.ldShowPhotoSelectDialog) {
            showPhotoVideoSelectDialog(it)
        }
        observeLiveData(viewModel.ldSetList) {
            setList(it)
            setListDecor()
        }
        observeLiveData(viewModel.ldRequestPhotoPermission) {
            requestPermissionParams()
        }
        observeLiveData(viewModel.ldSetItem) {
            adapter.setItem(it.first, it.second)
        }
        observeLiveData(viewModel.ldSetSimpleParams) {
            binding.cttaNameEdit.setText(it.first)
            binding.cttaSalaryEdit.setText(it.second)
            binding.cttaAboutEdit.setText(it.third)
        }
        observeLiveData(viewModel.ldOpenInterestsPage) {
            openInterestsFragment(it.first, it.second)
        }
        observeLiveData(viewModel.ldSetInterests) {
            binding.cttaInterestsEdit.setText(it)
        }
        observeLiveData(viewModel.ldOpenCityDialog) {
            showSelectCityDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedCity) {
            binding.cttaCityEdit.setText(it)
        }
        observeLiveData(viewModel.ldSetSelectedBirthdate) {
            binding.cttaBirthdayEdit.setText(it)
        }
        observeLiveData(viewModel.ldOpenRuDialog) {
            showZhuzDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedRu) {
            binding.cttaRuEdit.setText(it)
        }
        observeLiveData(viewModel.ldSetRuParams) {
            zhuzDialogBinding?.apply {
                npSelectRu.maxValue = it.second
                npSelectRu.minValue = 0
                npSelectRu.displayedValues = it.third.toTypedArray()
                npSelectRu.value = it.first
            }
        }
        /*observeLiveData(viewModel.ldOpenGenderDialog) {
            showSelectGenderDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedGender) {
            binding.cttaGenderEdit.setText(it)
        }*/
        observeLiveData(viewModel.ldOpenEnterSingleInfoPage) {
            openEnterSingleInfoFragment(it.first, it.second, it.third)
        }
        observeLiveData(viewModel.ldOpenFamilyStatusDialog) {
            showSelectFamilyStatusDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedFamilyStatus) {
            binding.cttaFamilyStatusEdit.setText(it)
        }
        observeLiveData(viewModel.ldClosePage) {
            requireActivity().onBackPressed()
        }
        observeLiveData(viewModel.ldClearRuPicker) {
            zhuzDialogBinding?.npSelectRu?.displayedValues = null
            zhuzDialogBinding?.npSelectRu?.maxValue = 0
        }
        observeLiveData(viewModel.ldSetKalym) {
            binding.cttaKalym.setText(it)
        }
        observeLiveData(viewModel.ldSetBadHabitsText) {
            binding.cttaBadHabits.setText(it)
        }
        observeLiveData(viewModel.ldOpenZodiacDialog) {
            showSelectZodiacDialog(it)
        }
        observeLiveData(viewModel.ldOpenBadHabitsDialog) {
            showBadHabitsDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedZodiac) {
            binding.cttaZodiac.setText(it)
        }
        observeLiveData(viewModel.ldSetWeight) {
            binding.cttaWeight.setText(it)
        }
        observeLiveData(viewModel.ldSetHeight) {
            binding.cttaHeight.setText(it)
        }
        observeLiveData(viewModel.ldOpenCropPage) {
            openCropPage(it)
        }
        observeLiveData(viewModel.ldSetKalymVisibility) {
            binding.cttaKalym.setVisibilityState(it)
        }
        observeLiveData(viewModel.ldOpenBirthdayDialog) {
            showOpenningDateDialog(it.first, it.second.first, it.second.second, it.second.third)
        }
        observeLiveData(viewModel.ldGoBack) {
            if (getFragmentMap().size < 2) {
                openProfileFragment()
            } else {
                goBackFragment(R.id.fl_fragment_container)
            }
        }
        observeLiveData(viewModel.ldOpenCropBitmapPage) {
            openCropBitmapPage(it)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener { }

        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.cttaCityEdit.setSafelyClickListener {
            viewModel.openCityDialog()
        }

        binding.cttaBirthdayEdit.setSafelyClickListener {
            viewModel.openBirthdayDialog()
        }

        binding.cttaRuEdit.setSafelyClickListener {
            viewModel.openZhuzDialog()
        }
/*
        binding.cttaGenderEdit.setSafelyClickListener {
            viewModel.openGenderDialog()
        }*/

        binding.cttaFamilyStatusEdit.setSafelyClickListener {
            viewModel.openFamilyStatusDialog()
        }

        binding.cttaInterestsEdit.setSafelyClickListener {
            viewModel.openInterestsPage()
        }

        binding.btnSaveEdit.setSafelyClickListener {
            viewModel.onSaveBtnClicked(
                binding.cttaNameEdit.getText(),
                binding.cttaAboutEdit.getText(),
                binding.cttaSalaryEdit.getText(),
                binding.cttaKalym.getText(),
                binding.cttaBadHabits.getText(),
                binding.cttaWeight.getText(),
                binding.cttaHeight.getText(),
                binding.cttaRuEdit.getText()
            )
        }

        binding.cttaKalym.setSafelyClickListener {
            viewModel.openKalymEditPage(binding.cttaKalym.getText())
        }

        binding.cttaBadHabits.setSafelyClickListener {
            viewModel.openBadHabitsDialog()
        }

        binding.cttaZodiac.setSafelyClickListener {
            viewModel.openZodiacDialog()
        }

        binding.cttaWeight.setSafelyClickListener {
            viewModel.openWeightEditPage(binding.cttaWeight.getText())
        }

        binding.cttaHeight.setSafelyClickListener {
            viewModel.openHeightEditPage(binding.cttaHeight.getText())
        }

        checkToZeroEdit(binding.cttaSalaryEdit.getEditText())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        permReqLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                Log.i(fragmentTag, "registerForActivityResult -> permissions: ${permissions}")

                val granted = permissions.entries.all {
                    it.value == true
                }

                if (granted) {
                    viewModel.openPhotoVideoDialogFragment()
                } else if (hasPermissionBlockedOrNeverAsked(PERMISSIONS)) {
                    parseSettingsParams()
                }
            }
    }

    private fun requestPermissionParams() {
        if (hasPermissions(PERMISSIONS)) {
            viewModel.openPhotoVideoDialogFragment()
        } else {
            permReqLauncher.launch(PERMISSIONS)
        }
    }

    private fun hasPermissions(permissions: Array<String>): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(
                requireContext(),
                it
            ) == PackageManager.PERMISSION_GRANTED
        }

    private fun hasPermissionBlockedOrNeverAsked(permissions: Array<String>) = permissions.all {
        ActivityCompat.checkSelfPermission(
            requireContext(),
            it
        ) == PackageManager.PERMISSION_DENIED
                && !shouldShowRequestPermissionRationale(it)

    }

    private fun showPhotoVideoSelectDialog(pair: Pair<String, String>) {
        dialogFragmentPhotoVideo =
            PhotoVideoSelectionDialogFragment.newInstance(pair.first, pair.second)

        dialogFragmentPhotoVideo?.show(
            getSupportFragmentManager(),
            PhotoVideoSelectionDialogFragment.fragmentOpenTag3
        )
    }

    private fun parseSettingsParams() {
        val uri = Uri.fromParts(
            requireContext().getString(R.string.field_package),
            requireContext().packageName,
            null
        )
        val intent = Intent()

        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.data = uri

        requireActivity().startActivity(intent)
    }

    private fun setListDecor() {
        binding.rcvImages.clearItemDecorations()

        binding.rcvImages.addItemDecoration(
            VerticalGridListItemDecoration(
                resources.getDimension(R.dimen.itemSpaceSize).toInt(),
                3
            )
        )
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = UserPhotoAdapter(
                { model: UserPhoto?, pos: Int ->
                    viewModel.addItemPhoto(model, pos)
                },
                { model: UserPhoto?, pos: Int ->
                    viewModel.removeItemPhoto(model, pos)
                }
            )
        }

        binding.rcvImages.adapter = adapter
    }

    private fun setList(list: MutableList<UserPhoto?>) {
        adapter.setData(list)
    }

    private fun openInterestsFragment(vmTag: String, interestList: MutableList<Interest>) {
        val fragment = InterestsFragment.newInstance(vmTag, interestList)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            InterestsFragment.fragmentOpenTag
        )
    }

    private fun openProfileFragment() {
        val fragment = ProfileFragment.newInstance()

        removeAndReplaceWithAnotherFragment(
            R.id.fl_fragment_container,
            fragment,
            ProfileFragment.fragmentOpenTag,
            fragmentOpenTag
        )
    }

    private fun showSelectCityDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogCityBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectCity(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun showOpenningDateDialog(maxTime: Long, year: Int, month: Int, day: Int) {
        val alert = DatePickerDialog(
            requireContext(),
            openningDatePickerListener,
            year,
            month,
            day
        )

        alert.datePicker.maxDate = maxTime

        alert.show()

        alert.getButton(DatePickerDialog.BUTTON_POSITIVE)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
        alert.getButton(DatePickerDialog.BUTTON_NEGATIVE)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
    }

    private fun showZhuzDialog(params: ParamsContainer) {
        val selectedPos = params.getInt(AppConstants.BundleConstants.SELECTED_POS) ?: 0
        val selectedPos2 = params.getInt(AppConstants.BundleConstants.SELECTED_POS_2) ?: 0
        val maxPos = params.getInt(AppConstants.BundleConstants.MAX_POS) ?: 0
        val maxPos2 = params.getInt(AppConstants.BundleConstants.MAX_POS_2) ?: 0
        val zhuzList = params.getList<String>(AppConstants.BundleConstants.PARENT_LIST)
        val ruList = params.getList<String>(AppConstants.BundleConstants.CHILD_LIST)

        if (zhuzList == null || ruList == null) return

        zhuzDialogBinding = DialogRuBinding.inflate(layoutInflater)

        val builder = AlertDialog.Builder(requireContext())

        zhuzDialogBinding?.apply {
            builder.setView(this.root)

            val alert = builder.show()

            alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            alert.window?.setLayout(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            npSelectZhuz.maxValue = maxPos
            npSelectRu.maxValue = maxPos2
            npSelectZhuz.minValue = 0
            npSelectRu.minValue = 0
            npSelectZhuz.displayedValues = zhuzList.toTypedArray()
            npSelectRu.displayedValues = ruList.toTypedArray()
            npSelectZhuz.value = selectedPos
            npSelectRu.value = selectedPos2

            npSelectZhuz.setOnValueChangedListener { numberPicker, i, i2 ->
                viewModel.selectZhuz(i2)
            }

            btnReady.setSafelyClickListener {
                viewModel.selectRu(npSelectRu.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }

            alert.setOnDismissListener {
                zhuzDialogBinding = null
            }
        }
    }

    private fun openEnterSingleInfoFragment(
        vmTag: String,
        enterInfo: EnterSingleInfo,
        currentText: String
    ) {
        Log.i(vmTag, "openEnterSingleInfoFragment -> enterInfo: ${enterInfo}")

        val fragment = EnterSingleInfoFragment.newInstance(vmTag, enterInfo, currentText)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            EnterSingleInfoFragment.fragmentOpenTag2
        )
    }

    private fun showSelectFamilyStatusDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogFamilyStatusBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )


        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectFamilyStatus(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun showSelectZodiacDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogSelectZodiacBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectZodiac(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun showBadHabitsDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogSelectZodiacBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            tvTitle.setText(R.string.label_bad_habits_desc)

            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectBadHabit(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun openCropPage(uri: Uri) {
/*        cropImageResult.launch(
            options(uri = uri) {
                setGuidelines(CropImageView.Guidelines.ON)
                setOutputCompressFormat(Bitmap.CompressFormat.PNG)
            }
        )*/
    }

    private fun checkToZeroEdit(editText: EditText) {
        editText.addTextChangedListener {
            it?.let {
                if (it.startsWith("0")) {
                    editText.setText("")
                }
            }
        }
    }

    private fun openCropBitmapPage(uri: Uri) {
        EditProfileViewModel::class.simpleName?.apply {
            val fragment = CropBitmapFragment.newInstance(this, uri)

            replaceFragment(
                R.id.fl_fragment_container,
                fragment,
                CropBitmapFragment.fragmentOpenTag
            )
        }
    }


}