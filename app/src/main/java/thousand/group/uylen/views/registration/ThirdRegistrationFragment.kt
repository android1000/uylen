package thousand.group.uylen.views.registration

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
/*import com.canhub.cropper.CropImageContract
import com.canhub.cropper.CropImageView
import com.canhub.cropper.options*/
import thousand.group.data.entities.remote.simple.User
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentThirdRegistrationBinding
import thousand.group.uylen.utils.adapters.recycler_view.RegistrationPhotoAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.decorations.VerticalGridListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.extensions.getSupportFragmentManager
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.models.locale.RegistrationPhoto
import thousand.group.uylen.view_models.main.CropBitmapViewModel
import thousand.group.uylen.view_models.registration.ThirdRegistrationViewModel
import thousand.group.uylen.views.main.CropBitmapFragment
import thousand.group.uylen.views.main.MainActivity
import thousand.group.uylen.views.main.PhotoVideoSelectionDialogFragment

class ThirdRegistrationFragment :
    BaseFragment<FragmentThirdRegistrationBinding, ThirdRegistrationViewModel>(
        ThirdRegistrationViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            AuthFragmentHelper.getJsonFragmentTag(
                AuthFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true,
                    isTabsVisible = false
                )
            )

        fun newInstance(
            user: User,
            token: String,
            location: Pair<Double, Double>
        ): ThirdRegistrationFragment {
            val fragment = ThirdRegistrationFragment()
            val args = Bundle()
            //passing arguments

            args.putParcelable(AppConstants.BundleConstants.USER, user)
            args.putString(AppConstants.BundleConstants.TOKEN, token)
            args.putSerializable(AppConstants.BundleConstants.LOCATION, location)

            fragment.arguments = args
            return fragment
        }

        fun newInstance(
            user: User,
            token: String
        ): ThirdRegistrationFragment {
            val fragment = ThirdRegistrationFragment()
            val args = Bundle()
            //passing arguments

            args.putParcelable(AppConstants.BundleConstants.USER, user)
            args.putString(AppConstants.BundleConstants.TOKEN, token)

            fragment.arguments = args
            return fragment
        }
    }

    private val PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )

    private lateinit var permReqLauncher: ActivityResultLauncher<Array<String>>

    private var dialogFragmentPhotoVideo: PhotoVideoSelectionDialogFragment? = null

    private lateinit var adapter: RegistrationPhotoAdapter

   /* private val cropImageResult = registerForActivityResult(CropImageContract()) { result ->
        if (result.isSuccessful) {
            // use the returned uri
            result.uriContent?.apply {

                Log.i(fragmentTag, "cropImageResult -> uriContent: ${this}")

                viewModel.uploadImage(this)
            }

        } else {
            // an error occurred
            val exception = result.error
        }
    }
*/
    override fun getBindingObject() = FragmentThirdRegistrationBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        setAdapter()
        setListDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldShowPhotoSelectDialog) {
            showPhotoVideoSelectDialog(it)
        }
        observeLiveData(viewModel.ldSetList) {
            setList(it)
            setListDecor()
        }
        observeLiveData(viewModel.ldRequestPhotoPermission) {
            requestPermissionParams()
        }
        observeLiveData(viewModel.ldSetItem) {
            adapter.setItem(it.first, it.second)
        }
        observeLiveData(viewModel.ldOpenCropPage) {
            openCropBitmapPage(it)
        }
        observeLiveData(viewModel.ldOpenMainActivity) {
            openMainActivity()
        }
        observeLiveData(viewModel.ldSetCrossVisibility) {
            binding.ivGoRegistration.setVisibilityState(it)
            binding.ivBack.setVisibilityState(!it)
        }
        observeLiveData(viewModel.ldOpenRegistrationPage) {
            openRegistrationPage()
        }
    }

    override fun initController() {
        binding.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.ivGoRegistration.setSafelyClickListener {
            viewModel.onCrossIconClicked()
        }

        binding.btnCompleteRegistration.setSafelyClickListener {
            viewModel.onCompleteBtnClicked(binding.etAboutYourself.text.toString())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        permReqLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                Log.i(fragmentTag, "registerForActivityResult -> permissions: ${permissions}")

                val granted = permissions.entries.all {
                    it.value == true
                }

                if (granted) {
                    viewModel.openPhotoVideoDialogFragment()
                } else if (hasPermissionBlockedOrNeverAsked(PERMISSIONS)) {
                    parseSettingsParams()
                }
            }
    }

    private fun requestPermissionParams() {
        if (hasPermissions(PERMISSIONS)) {
            viewModel.openPhotoVideoDialogFragment()
        } else {
            permReqLauncher.launch(PERMISSIONS)
        }
    }

    private fun hasPermissions(permissions: Array<String>): Boolean =
        permissions.all {
            ActivityCompat.checkSelfPermission(
                requireContext(),
                it
            ) == PackageManager.PERMISSION_GRANTED
        }

    private fun hasPermissionBlockedOrNeverAsked(permissions: Array<String>) = permissions.all {
        ActivityCompat.checkSelfPermission(
            requireContext(),
            it
        ) == PackageManager.PERMISSION_DENIED
                && !shouldShowRequestPermissionRationale(it)

    }

    private fun showPhotoVideoSelectDialog(pair: Pair<String, String>) {
        dialogFragmentPhotoVideo =
            PhotoVideoSelectionDialogFragment.newInstance(pair.first, pair.second)

        dialogFragmentPhotoVideo?.show(
            getSupportFragmentManager(),
            PhotoVideoSelectionDialogFragment.fragmentOpenTag2
        )
    }

    private fun parseSettingsParams() {
        val uri = Uri.fromParts(
            requireContext().getString(R.string.field_package),
            requireContext().packageName,
            null
        )
        val intent = Intent()

        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.data = uri

        requireActivity().startActivity(intent)
    }

    private fun setListDecor() {
        binding.rcvImages.clearItemDecorations()

        binding.rcvImages.addItemDecoration(
            VerticalGridListItemDecoration(
                resources.getDimension(R.dimen.itemSpaceSize).toInt(),
                3
            )
        )
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = RegistrationPhotoAdapter(
                { model: RegistrationPhoto, pos: Int ->
                    viewModel.addItemPhoto(model, pos)
                },
                { model: RegistrationPhoto, pos: Int ->
                    viewModel.removeItemPhoto(model, pos)
                }
            )
        }

        binding.rcvImages.adapter = adapter
    }

    private fun setList(list: MutableList<RegistrationPhoto>) {
        adapter.setData(list)
    }

    private fun openCropPage(uri: Uri) {
      /*  cropImageResult.launch(
            options(uri = uri) {
                setGuidelines(CropImageView.Guidelines.ON)
                setOutputCompressFormat(Bitmap.CompressFormat.PNG)
            }
        )*/
    }

    private fun openMainActivity() {
        val intent = Intent(requireActivity(), MainActivity::class.java)

        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

        intent.putExtra(AppConstants.BundleConstants.FROM_SIGN_UP_PAGE, true)

        startActivity(intent).also {
            requireActivity().finish()
        }
    }

    private fun openRegistrationPage() {
        val fragment = FirstRegistrationFragment.newInstance()

        getSupportFragmentManager().replaceFragmentWithCheck(
            R.id.fl_fragment_container,
            fragment,
            FirstRegistrationFragment.fragmentOpenTag
        )
    }

    private fun openCropBitmapPage(uri: Uri) {
        ThirdRegistrationViewModel::class.simpleName?.apply {
            val fragment = CropBitmapFragment.newInstance(this, uri)

            getSupportFragmentManager().replaceFragmentWithCheck(
                R.id.fl_fragment_container,
                fragment,
                CropBitmapFragment.fragmentOpenTag
            )
        }
    }


}