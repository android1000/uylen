package thousand.group.uylen.views.profile

import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import thousand.group.data.entities.remote.simple.TariffBanner
import thousand.group.data.entities.remote.simple.TariffPrice
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentTarifsBinding
import thousand.group.uylen.utils.adapters.recycler_view.TarifPricesAdapter
import thousand.group.uylen.utils.adapters.recycler_view.TariffBannerAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.decorations.VerticalGridListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.profile.TarifsViewModel

class TarifsFragment :
    BaseFragment<FragmentTarifsBinding, TarifsViewModel>(
        TarifsViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = false
            )
        )

        fun newInstance(tag: String): TarifsFragment {
            val fragment = TarifsFragment()
            val args = Bundle()

            //passing arguments
            args.putString(AppConstants.BundleConstants.VM_TAG, tag)

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var tariffAdapter: TarifPricesAdapter
    private lateinit var bannersAdapter: TariffBannerAdapter

    private val viewPagerListener = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            viewModel.onViewPagerPositionChanged(position)
        }
    }

    override fun getBindingObject() = FragmentTarifsBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        binding.toolbar.tvTitle.setText(R.string.label_tariffs)

        setBannerIndicator()

        setAdapter()
        setBannerAdapter()
        setDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetTariffPrices) {
            setList(it)
            setDecor()
        }
        observeLiveData(viewModel.ldSetTariffsEmpty) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldSetTariffPriceItem) {
            tariffAdapter.setItem(it.first, it.second)
        }
        observeLiveData(viewModel.ldSetBannerList) {
            setBannerList(it)
        }
        observeLiveData(viewModel.ldSetTarifDesc) {
            binding.tvTitleTarif.setText(it.first)
            binding.tvDesc.setText(it.second)
        }
        observeLiveData(viewModel.ldClosePage) {
            requireActivity().onBackPressed()
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.vpBanner.registerOnPageChangeCallback(viewPagerListener)

        binding.btnSelectTarif.setSafelyClickListener {
            viewModel.onBuyTarifClicked()
        }
    }

    private fun setAdapter() {
        if (!::tariffAdapter.isInitialized) {
            tariffAdapter = TarifPricesAdapter { price, pos ->
                viewModel.onPriceItemClicked(price, pos)
            }
        }

        binding.rcvTarifs.adapter = tariffAdapter
    }

    private fun setList(list: MutableList<TariffPrice>) {
        tariffAdapter.setData(list)
    }

    private fun setDecor() {
        binding.rcvTarifs.clearItemDecorations()

        binding.rcvTarifs.addItemDecoration(
            VerticalGridListItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.rightAnswersSpace),
                3
            )
        )
    }

    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.tvEmptyList.setVisibilityState(isEmpty)
        binding.rcvTarifs.setVisibilityState(!isEmpty)
    }

    private fun setBannerAdapter() {
        if (!::bannersAdapter.isInitialized) {
            bannersAdapter = TariffBannerAdapter()
        }
        binding.vpBanner.adapter = bannersAdapter
    }

    private fun setBannerList(list: MutableList<TariffBanner>) {
        bannersAdapter.setData(list)
    }

    private fun setBannerIndicator() {
        binding.indvBanners.setSliderWidth(
            resources.getDimension(R.dimen.normalSliderWidth),
            resources.getDimension(R.dimen.selectedSliderWidth)
        )

        binding.indvBanners.setSliderGap(resources.getDimension(R.dimen.sliderGapMargin))

        binding.indvBanners.setupWithViewPager(binding.vpBanner)
    }
}
