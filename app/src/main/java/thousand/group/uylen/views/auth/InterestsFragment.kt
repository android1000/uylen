package thousand.group.uylen.views.auth

import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentInterestsBinding
import thousand.group.uylen.global_utils.extentions.putMutableParcelableList
import thousand.group.uylen.utils.adapters.recycler_view.InterestsAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.clearItemDecorations
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.extensions.setVisibilityState
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.auth.InterestsViewModel


class InterestsFragment :
    BaseFragment<FragmentInterestsBinding, InterestsViewModel>(
        InterestsViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = AuthFragmentHelper.getJsonFragmentTag(
            AuthFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true,
                isTabsVisible = false
            )
        )

        fun newInstance(tag: String, interestList: MutableList<Interest>): InterestsFragment {
            val fragment = InterestsFragment()
            val args = Bundle()
            //passing arguments

            args.putString(AppConstants.BundleConstants.VM_TAG, tag)
            args.putMutableParcelableList(AppConstants.BundleConstants.LIST, interestList)

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: InterestsAdapter

    override fun getBindingObject() = FragmentInterestsBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        binding.toolbar.tvTitle.setText(R.string.label_interests)

        setLayoutManager()
        setAdapter()
        setDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetEmptyList) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldSetInterestList) {
            setList(it)
            setDecor()
        }
        observeLiveData(viewModel.ldSetItem) {
            adapter.setItem(it.first, it.second)
        }
    }

    override fun initController() {
        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.root.setSafelyClickListener { }
    }

    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.tvEmptyList.setVisibilityState(isEmpty)
        binding.rcvInterests.setVisibilityState(!isEmpty)
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = InterestsAdapter(
                { model: Interest, pos: Int ->
                    viewModel.onItemClicked(model, pos)
                }
            )
        }

        binding.rcvInterests.adapter = adapter
    }

    private fun setList(list: MutableList<Interest>) {
        adapter.setData(list)
    }

    private fun setLayoutManager() {
        val layoutManager = FlowLayoutManager()
        layoutManager.maxItemsPerLine(6)
        layoutManager.isAutoMeasureEnabled = true
        binding.rcvInterests.layoutManager = layoutManager
    }

    private fun setDecor() {
        binding.rcvInterests.clearItemDecorations()

        binding.rcvInterests.addItemDecoration(object : ItemDecoration() {
            override fun getItemOffsets(
                outRect: Rect,
                view: View,
                parent: RecyclerView,
                state: RecyclerView.State
            ) {
                super.getItemOffsets(outRect, view, parent, state)

                val dimen = resources.getDimensionPixelOffset(R.dimen.itemSpaceSize)

                with(outRect) {
                    right = dimen
                    bottom = dimen
                }
            }
        })
    }

}