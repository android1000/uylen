package thousand.group.uylen.views.chat

import android.os.Bundle
import android.util.Log
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.ChatListItem
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentChatListBinding
import thousand.group.uylen.utils.adapters.recycler_view.ChatListAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.custom_views.TouchDetectableScrollView
import thousand.group.uylen.utils.decorations.VerticalListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.chat.ChatListViewModel

class ChatListFragment :
    BaseFragment<FragmentChatListBinding, ChatListViewModel>(
        ChatListViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true,
                isShowBottomNavBar = true,
                navBarItemPos = 2
            )
        )

        fun newInstance(): ChatListFragment {
            val fragment = ChatListFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: ChatListAdapter

    override fun getBindingObject() = FragmentChatListBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        binding.rcv.itemAnimator = null

        setAdapter()
        setListDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetChatList) {
            setList(it)
            setListDecor()
        }
        observeLiveData(viewModel.ldSetEmptyListParams) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldOpenChat) {
            openChatPage(it.first, it.second, it.third)
        }
        observeLiveData(viewModel.ldShowBottomProgress) {
            showBottomProgress(it)
        }
        observeLiveData(viewModel.ldSetMatchedUsersParams) {
            setMatchedUsersParams(it)
        }
        observeLiveData(viewModel.ldSetChatListItem) {
            adapter.setItem(it.first, it.second)
        }
        observeLiveData(viewModel.ldChatItemRemoved) {
            adapter.removeItem(it)
        }
        observeLiveData(viewModel.ldChatItemInserted) {
            adapter.addItem(it.first, it.second)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener { }

        binding.ctlLikedPairsList.setSafelyClickListener {
            viewModel.onMatchedClicked()
            openMutualSympathyPage()
        }

        binding.tdsvChat.setMyScrollChangeListener(object :
            TouchDetectableScrollView.OnMyScrollChangeListener {
            override fun onScrollUp() {

            }

            override fun onScrollDown() {
            }

            override fun onBottomReached() {
                viewModel.onBottomReached()
            }

            override fun onTopReached() {

            }
        })
    }

    override fun onResume() {
        super.onResume()

        viewModel.getMatchedUsers()
    }

    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.tvNothing.setVisibilityState(isEmpty)
        binding.rcv.setVisibilityState(!isEmpty)
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = ChatListAdapter { chatItem, pos ->
                doIfInternetConnected {
                    viewModel.onItemClicked(chatItem, pos)
                }
            }
        }

        binding.rcv.adapter = adapter
    }

    private fun setList(list: MutableList<ChatListItem>) {
        adapter.setData(list)
    }

    private fun setListDecor() {
        binding.rcv.clearItemDecorations()

        binding.rcv.addItemDecoration(
            VerticalListItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.chatListItemSpace),
                adapter.itemCount
            )
        )
    }

    private fun openMutualSympathyPage() {
        val fragment = MutualSympathyFragment.newInstance()

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            MutualSympathyFragment.fragmentOpenTag
        )
    }

    private fun openChatPage(id: Long, model: ChatListItem, pos: Int) {
        adapter.setItem(model, pos)

        val fragment = ChatMessagesFragment.newInstance(id)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            ChatMessagesFragment.fragmentOpenTag
        )
    }

    private fun showBottomProgress(show: Boolean) {
        binding.pbBottom.setVisibilityState(show)
    }

    private fun setMatchedUsersParams(params: ParamsContainer) {
        Log.i(fragmentTag, "setMatchedUsersParams -> params:$params")

        with(params) {
            getBoolean(AppConstants.BundleConstants.IV_VISIBILITY_1)?.apply {
                binding.ivFirstAvatar.setVisibilityState(this)
            }
            getString(AppConstants.BundleConstants.IV_LINK_1)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.ivFirstAvatar)
            }
            getBoolean(AppConstants.BundleConstants.IV_VISIBILITY_2)?.apply {
                binding.ivSecondAvatar.setVisibilityState(this)
            }
            getString(AppConstants.BundleConstants.IV_LINK_2)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.ivSecondAvatar)
            }
            getBoolean(AppConstants.BundleConstants.IV_VISIBILITY_3)?.apply {
                binding.ivThirdAvatar.setVisibilityState(this)
            }
            getString(AppConstants.BundleConstants.IV_LINK_3)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.ivThirdAvatar)
            }
            getBoolean(AppConstants.BundleConstants.IV_VISIBILITY_4)?.apply {
                binding.ivFourthAvatar.setVisibilityState(this)
            }
            getString(AppConstants.BundleConstants.IV_LINK_4)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.ivFourthAvatar)
            }
            getBoolean(AppConstants.BundleConstants.IV_VISIBILITY_5)?.apply {
                binding.ivFifthAvatar.setVisibilityState(this)
            }
            getString(AppConstants.BundleConstants.IV_LINK_5)?.apply {
                Picasso.get()
                    .load(this)
                    .error(R.drawable.ic_error_avatar)
                    .into(binding.ivFifthAvatar)
            }
            getBoolean(AppConstants.BundleConstants.SIZE_VISIBILITY)?.apply {
                binding.tvLikedOtherSize.setVisibilityState(this)
            }
            getString(AppConstants.BundleConstants.SIZE_TEXT)?.apply {
                binding.tvLikedOtherSize.setText(this)
            }
            getString(AppConstants.BundleConstants.MATCHED_USERS_COUNT_TEXT)?.apply {
                binding.tvLikedUsersNames.setText(this)
            }
        }
    }
}