package thousand.group.uylen.views.registration

import android.app.DatePickerDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.User
import thousand.group.uylen.R
import thousand.group.uylen.databinding.*
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.extensions.getSupportFragmentManager
import thousand.group.uylen.utils.extensions.replaceFragmentWithBackStack
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.locale.EnterSingleInfo
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.registration.SecondRegistrationViewModel
import thousand.group.uylen.views.auth.EnterSingleInfoFragment
import thousand.group.uylen.views.auth.InterestsFragment
import java.util.*

class SecondRegistrationFragment :
    BaseFragment<FragmentSecondRegistrationBinding, SecondRegistrationViewModel>(
        SecondRegistrationViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            AuthFragmentHelper.getJsonFragmentTag(
                AuthFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true,
                    isTabsVisible = false
                )
            )

        fun newInstance(
            user: User,
            token: String,
            location: Pair<Double, Double>
        ): SecondRegistrationFragment {
            val fragment = SecondRegistrationFragment()
            val args = Bundle()
            //passing arguments

            args.putParcelable(AppConstants.BundleConstants.USER, user)
            args.putString(AppConstants.BundleConstants.TOKEN, token)
            args.putSerializable(AppConstants.BundleConstants.LOCATION, location)

            fragment.arguments = args
            return fragment
        }
    }

    private val openningDatePickerListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
            viewModel.onOpenningDateSelected(year, month, dayOfMonth)
        }
    }

    private var zhuzDialogBinding: DialogRuBinding? = null
    private var regionDialogBinding: DialogCityRegionBinding? = null

    override fun getBindingObject() = FragmentSecondRegistrationBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldOpenGenderDialog) {
            showSelectGenderDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedGender) {
            binding.cttaGender.setText(it)
        }
        observeLiveData(viewModel.ldOpenFamilyStatusDialog) {
            showSelectFamilyStatusDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedFamilyStatus) {
            binding.cttaFamilyStatus.setText(it)
        }
        observeLiveData(viewModel.ldOpenCityDialog) {
            showSelectCityDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedCity){
            binding.cttaCity.setText(it)
        }
        observeLiveData(viewModel.ldSetSelectedBirthdate) {
            binding.cttaBirthday.setText(it)
        }
        observeLiveData(viewModel.ldOpenRuDialog) {
            showZhuzDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedRu) {
            binding.cttaRu.setText(it)
        }
        observeLiveData(viewModel.ldSetRuParams) {
            zhuzDialogBinding?.apply {
                npSelectRu.maxValue = it.second
                npSelectRu.minValue = 0
                npSelectRu.displayedValues = it.third.toTypedArray()
                npSelectRu.value = it.first
            }
        }
        observeLiveData(viewModel.ldOpenEnterSingleInfoPage) {
            openEnterSingleInfoFragment(it.first, it.second, it.third)
        }
        observeLiveData(viewModel.ldSetIncome) {
            binding.cttaIncome.setText(it)
        }
        observeLiveData(viewModel.ldOpenInterestsPage) {
            openInterestsFragment(it.first, it.second)
        }
        observeLiveData(viewModel.ldSetInterests) {
            binding.cttaInterests.setText(it)
        }
        observeLiveData(viewModel.ldOpenThirdRegisterPage) {
            openThirdRegistrationFragment(it)
        }
        observeLiveData(viewModel.ldClearRuPicker) {
            zhuzDialogBinding?.npSelectRu?.displayedValues = null
            zhuzDialogBinding?.npSelectRu?.maxValue = 0
        }
        observeLiveData(viewModel.ldSetKalym) {
            binding.cttaKalym.setText(it)
        }
        observeLiveData(viewModel.ldSetBadHabitsText) {
            binding.cttaBadHabits.setText(it)
        }
        observeLiveData(viewModel.ldOpenZodiacDialog) {
            showSelectZodiacDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedZodiac) {
            binding.cttaZodiac.setText(it)
        }
        observeLiveData(viewModel.ldSetWeight) {
            binding.cttaWeight.setText(it)
        }
        observeLiveData(viewModel.ldSetHeight) {
            binding.cttaHeight.setText(it)
        }
        observeLiveData(viewModel.ldSetCityRegionParams) {
            regionDialogBinding?.apply {
                npSelectCity.maxValue = it.second
                npSelectCity.minValue = 0
                npSelectCity.displayedValues = it.third.toTypedArray()
                npSelectCity.value = it.first
            }
        }
        observeLiveData(viewModel.ldSetKalymVisibility) {
            binding.cttaKalym.setVisibilityState(it)
        }
        observeLiveData(viewModel.ldOpenBirthdayDialog) {
            showOpenningDateDialog(it.first, it.second.first, it.second.second, it.second.third)
        }
        observeLiveData(viewModel.ldOpenBadHabitsDialog) {
            showBadHabitsDialog(it)
        }
    }

    override fun initController() {
        binding.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.cttaGender.setSafelyClickListener {
            viewModel.openGenderDialog()
        }

        binding.cttaFamilyStatus.setSafelyClickListener {
            viewModel.openFamilyStatusDialog()
        }

        binding.cttaCity.setSafelyClickListener {
            viewModel.openCityDialog()
        }

        binding.cttaBirthday.setSafelyClickListener {
            viewModel.openBirthdayDialog()
        }

        binding.cttaRu.setSafelyClickListener {
            viewModel.openZhuzDialog()
        }

        binding.cttaIncome.setSafelyClickListener {
            viewModel.openIncomeEditPage(binding.cttaIncome.getText())
        }

        binding.cttaInterests.setSafelyClickListener {
            viewModel.openInterestsPage()
        }

        binding.cttaKalym.setSafelyClickListener {
            viewModel.openKalymEditPage(binding.cttaKalym.getText())
        }

        binding.cttaBadHabits.setSafelyClickListener {
            viewModel.openBadHabitsDialog()
        }

        binding.cttaZodiac.setSafelyClickListener {
            viewModel.openZodiacDialog()
        }

        binding.cttaWeight.setSafelyClickListener {
            viewModel.openWeightEditPage(binding.cttaWeight.getText())
        }

        binding.cttaHeight.setSafelyClickListener {
            viewModel.openHeightEditPage(binding.cttaHeight.getText())
        }

        binding.btnAfter.setSafelyClickListener {
            viewModel.onAfterBtnClicked(
                binding.cttaFamilyStatus.getText(),
                binding.cttaIncome.getText(),
                binding.cttaKalym.getText(),
                binding.cttaBadHabits.getText(),
                binding.cttaZodiac.getText(),
                binding.cttaWeight.getText(),
                binding.cttaHeight.getText(),
                binding.cttaRu.getText()
            )
        }
    }

    private fun showSelectGenderDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogGenderBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectGender(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun showSelectFamilyStatusDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogFamilyStatusBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectFamilyStatus(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun showSelectCityDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogCityBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectCity(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun showSelectZodiacDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogSelectZodiacBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectZodiac(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun showOpenningDateDialog(maxTime:Long, year: Int, month: Int, day: Int) {
        val alert = DatePickerDialog(
            requireContext(),
            openningDatePickerListener,
            year,
            month,
            day
        )

        alert.datePicker.maxDate = maxTime

        alert.show()

        alert.getButton(DatePickerDialog.BUTTON_POSITIVE)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
        alert.getButton(DatePickerDialog.BUTTON_NEGATIVE)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
    }

    private fun showZhuzDialog(params: ParamsContainer) {
        val selectedPos = params.getInt(AppConstants.BundleConstants.SELECTED_POS) ?: 0
        val selectedPos2 = params.getInt(AppConstants.BundleConstants.SELECTED_POS_2) ?: 0
        val maxPos = params.getInt(AppConstants.BundleConstants.MAX_POS) ?: 0
        val maxPos2 = params.getInt(AppConstants.BundleConstants.MAX_POS_2) ?: 0
        val zhuzList = params.getList<String>(AppConstants.BundleConstants.PARENT_LIST)
        val ruList = params.getList<String>(AppConstants.BundleConstants.CHILD_LIST)

        if (zhuzList == null || ruList == null) return

        zhuzDialogBinding = DialogRuBinding.inflate(layoutInflater)

        val builder = AlertDialog.Builder(requireContext())

        zhuzDialogBinding?.apply {
            builder.setView(this.root)

            val alert = builder.show()

            alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            alert.window?.setLayout(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            npSelectZhuz.maxValue = maxPos
            npSelectRu.maxValue = maxPos2
            npSelectZhuz.minValue = 0
            npSelectRu.minValue = 0
            npSelectZhuz.displayedValues = zhuzList.toTypedArray()
            npSelectRu.displayedValues = ruList.toTypedArray()
            npSelectZhuz.value = selectedPos
            npSelectRu.value = selectedPos2

            npSelectZhuz.setOnValueChangedListener { numberPicker, i, i2 ->
                viewModel.selectZhuz(i2)
            }

            btnReady.setSafelyClickListener {
                viewModel.selectRu(npSelectRu.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }

            alert.setOnDismissListener {
                zhuzDialogBinding = null
            }
        }
    }

    private fun openEnterSingleInfoFragment(
        vmTag: String,
        enterInfo: EnterSingleInfo,
        currentText: String
    ) {
        Log.i(vmTag, "openEnterSingleInfoFragment -> enterInfo: ${enterInfo}")

        val fragment = EnterSingleInfoFragment.newInstance(vmTag, enterInfo, currentText)

        getSupportFragmentManager().replaceFragmentWithBackStack(
            R.id.fl_fragment_container,
            fragment,
            EnterSingleInfoFragment.fragmentOpenTag
        )
    }

    private fun openInterestsFragment(vmTag: String, interestList:MutableList<Interest>) {
        val fragment = InterestsFragment.newInstance(vmTag, interestList)

        getSupportFragmentManager().replaceFragmentWithBackStack(
            R.id.fl_fragment_container,
            fragment,
            InterestsFragment.fragmentOpenTag
        )
    }

    private fun openThirdRegistrationFragment(params: Triple<User, String, Pair<Double, Double>>) {
        val fragment =
            ThirdRegistrationFragment.newInstance(params.first, params.second, params.third)

        getSupportFragmentManager().replaceFragmentWithBackStack(
            R.id.fl_fragment_container,
            fragment,
            ThirdRegistrationFragment.fragmentOpenTag
        )
    }

    private fun showBadHabitsDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogSelectZodiacBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            tvTitle.setText(R.string.label_bad_habits_desc)

            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectBadHabit(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

   /* private fun showRegionsDialog(params: ParamsContainer) {
        val selectedPos = params.getInt(AppConstants.BundleConstants.SELECTED_POS) ?: 0
        val selectedPos2 = params.getInt(AppConstants.BundleConstants.SELECTED_POS_2) ?: 0
        val maxPos = params.getInt(AppConstants.BundleConstants.MAX_POS) ?: 0
        val maxPos2 = params.getInt(AppConstants.BundleConstants.MAX_POS_2) ?: 0
        val regionsList = params.getList<String>(AppConstants.BundleConstants.PARENT_LIST)
        val citiesList = params.getList<String>(AppConstants.BundleConstants.CHILD_LIST)

        regionDialogBinding = DialogCityRegionBinding.inflate(layoutInflater)

        val builder = AlertDialog.Builder(requireContext())

        regionDialogBinding?.apply {
            builder.setView(this.root)

            val alert = builder.show()

            alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            alert.window?.setLayout(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            npSelectRegion.maxValue = maxPos
            npSelectCity.maxValue = maxPos2
            npSelectRegion.minValue = 0
            npSelectCity.minValue = 0
            npSelectRegion.displayedValues = regionsList?.toTypedArray()
            npSelectCity.displayedValues = citiesList?.toTypedArray()
            npSelectRegion.value = selectedPos
            npSelectCity.value = selectedPos2

            npSelectRegion.setOnValueChangedListener { numberPicker, i, i2 ->
                viewModel.selectRegion(i2)
            }

            btnReady.setSafelyClickListener {
                viewModel.selectCity(npSelectCity.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }

            alert.setOnDismissListener {
                regionDialogBinding = null
            }
        }
    }*/

}