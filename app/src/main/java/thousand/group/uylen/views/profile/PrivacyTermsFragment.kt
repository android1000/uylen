package thousand.group.uylen.views.profile

import android.os.Bundle
import thousand.group.uylen.databinding.FragmentPrivacyTermsBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.profile.PrivacyTermsViewModel

class PrivacyTermsFragment :
    BaseFragment<FragmentPrivacyTermsBinding, PrivacyTermsViewModel>(
        PrivacyTermsViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true
                )
            )

        val fragmentOpenTag2 =
            AuthFragmentHelper.getJsonFragmentTag(
                AuthFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true
                )
            )

        fun newInstance(type: String): PrivacyTermsFragment {
            val fragment = PrivacyTermsFragment()
            val args = Bundle()
            //passing arguments

            args.putString(AppConstants.BundleConstants.TERMS_TYPE, type)

            fragment.arguments = args
            return fragment
        }
    }

    override fun getBindingObject() = FragmentPrivacyTermsBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetWebViewContent) {
            binding.webView.loadUrl(it)
        }
        observeLiveData(viewModel.ldSetTitle) {
            binding.toolbar.tvTitle.setText(it)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }
    }
}
