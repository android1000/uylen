package thousand.group.uylen.views.restore_password

import android.os.Bundle
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentRestorePasswordBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.clearAndReplaceFragment
import thousand.group.uylen.utils.extensions.getSupportFragmentManager
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.restore_password.RestorePasswordViewModel
import thousand.group.uylen.views.auth.LoginFragment

class RestorePasswordFragment :
    BaseFragment<FragmentRestorePasswordBinding, RestorePasswordViewModel>(
        RestorePasswordViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            AuthFragmentHelper.getJsonFragmentTag(
                AuthFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true,
                    isTabsVisible = false
                )
            )

        fun newInstance(token: String, phone: String): RestorePasswordFragment {
            val fragment = RestorePasswordFragment()
            val args = Bundle()
            //passing arguments

            args.putString(AppConstants.BundleConstants.TOKEN, token)
            args.putString(AppConstants.BundleConstants.PHONE_NUMBER, phone)

            fragment.arguments = args
            return fragment
        }
    }

    override fun getBindingObject() = FragmentRestorePasswordBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldOpenLoginPage) {
            openLoginFragment()
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.btnReady.setSafelyClickListener {
            viewModel.onReadyBtnClicked(
                binding.etNewPassword.getText(),
                binding.etRepeatPassword.getText()
            )
        }
    }

    private fun openLoginFragment() {
        val fragment = LoginFragment.newInstance()

        getSupportFragmentManager().clearAndReplaceFragment(
            R.id.fl_fragment_container,
            fragment,
            LoginFragment.fragmentOpenTag
        )
    }

}
