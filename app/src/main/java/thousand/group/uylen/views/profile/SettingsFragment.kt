package thousand.group.uylen.views.profile

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.SeekBar
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.onesignal.OneSignal
import com.redmadrobot.inputmask.MaskedTextChangedListener
import thousand.group.uylen.R
import thousand.group.uylen.databinding.*
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.extensions.setNegativeBtnTextColor
import thousand.group.uylen.utils.extensions.setPositiiveBtnTextColor
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.profile.SettingsViewModel
import thousand.group.uylen.views.auth.AuthActivity
import thousand.group.uylen.views.main.MainActivity


class SettingsFragment :
    BaseFragment<FragmentSettingsBinding, SettingsViewModel>(
        SettingsViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true
            )
        )

        fun newInstance(): SettingsFragment {
            val fragment = SettingsFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    private val showAnketaListener = object : CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
            viewModel.setOnShowAnketaChecked(p1)
        }
    }

    private val activePushListener = object : CompoundButton.OnCheckedChangeListener {
        override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
            viewModel.setOnActivePushChecked(p1)
        }
    }

    private var regionDialogBinding: DialogCityRegionBinding? = null

    override fun getBindingObject() = FragmentSettingsBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        binding.toolbar.tvTitle.setText(R.string.label_settings)
        whetherWhatsAppIsInstalled()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetVersionName) {
            binding.txtDescSettings.setText(it)
        }
        observeLiveData(viewModel.ldSetPhone) {
            binding.cttaPhoneNumberSettings.setText(it)
        }
        observeLiveData(viewModel.ldSetEmail) {
            binding.cttaMailSettings.setText(it)
        }
        observeLiveData(viewModel.ldSetShowAnketaStatus) {
            binding.stvProfileActive.setOnCheckedChangeListener(null)
            binding.stvProfileActive.isChecked = it
            binding.stvProfileActive.setOnCheckedChangeListener(showAnketaListener)
        }
        observeLiveData(viewModel.ldSetActivePush) {
            binding.stvProfilePush.setOnCheckedChangeListener(null)
            binding.stvProfilePush.isChecked = it
            binding.stvProfilePush.setOnCheckedChangeListener(activePushListener)
        }
        observeLiveData(viewModel.ldShowExitDialog) {
            showExitDialog(it)
        }
        observeLiveData(viewModel.ldShowDeleteDialog) {
            showDeleteDialog(it)
        }
        observeLiveData(viewModel.ldCloseActivity) {
            val intent = Intent(requireActivity(), AuthActivity::class.java)
            startActivity(intent)
            requireActivity().finish()
        }
        observeLiveData(viewModel.ldSetDistanceText) {
            binding.cttaDistanceSettings.setText(it)
        }
        observeLiveData(viewModel.ldOpenCityDialog) {
            showSelectCityDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedCity) {
            binding.cttaCitySettings.setText(it)
        }
        observeLiveData(viewModel.ldSetDistanceVal) {
            binding.seekBarSettings.setProgress(it)
        }
        observeLiveData(viewModel.ldReloadActivity) {
            reloadActivity()
        }
        observeLiveData(viewModel.ldSetLanguageText) {
            binding.cttaLanguage.setText(it)
        }
        observeLiveData(viewModel.ldOpenTermPage) {
            openTermsPage(it)
        }
        observeLiveData(viewModel.ldOpenVerifyChangePhonePage) {
            openVerifyChangedPhoneFragment(it)
        }
        observeLiveData(viewModel.ldOpenWhatsApp) {
            openWhatsApp(it)
        }
        observeLiveData(viewModel.ldDoOnWhatsAppNotInstalled) {
            onWhatsAppNotFound(it)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener { }

        binding.txtTextDescSettings.setSafelyClickListener {
            viewModel.onWhatsAppBtnClicked()
        }

        binding.txtTextDescSettingsNumber.setSafelyClickListener {
            viewModel.onWhatsAppBtnClicked()
        }

/*        requireActivity().getOnBackPressedDispatcher()
            .addCallback(this, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                   Log.i(fragmentTag, "handleOnBackPressed -> backStackEntry:${getSupportFragmentManager().backStackEntryCount}")

                    getSupportFragmentManager().replaceFragmentWithCheck(
                        R.id.fl_fragment_container,
                        ProfileFragment.newInstance(),
                        ProfileFragment.fragmentOpenTag
                    )
                }
            })*/

        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.cttaPhoneNumberSettings.setSafelyClickListener {
            showPhoneDialog()
        }

/*        binding.cttaMailSettings.setSafelyClickListener {
            showEmailDialog()
        }*/

        binding.cttaCitySettings.setSafelyClickListener {
            viewModel.openCityDialog()
        }

        binding.cttaLanguage.setSafelyClickListener {
            viewModel.onLanguageTextClicked()
        }

        binding.seekBarSettings.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                Log.i(fragmentTag, "onProgressChanged -> p1:${p1}, p2:${p2}")

                viewModel.onDistanceRangeChanged(p1)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                Log.i(fragmentTag, "onStartTrackingTouch")
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                Log.i(fragmentTag, "onStopTrackingTouch")

                val locationManager =
                    requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
                val isGpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

                viewModel.sendDistance(isGpsOn)
            }
        })

        binding.stvProfileActive.setOnCheckedChangeListener(showAnketaListener)

        binding.stvProfilePush.setOnCheckedChangeListener(activePushListener)

        binding.btnExit.setSafelyClickListener {
            viewModel.onExitBtnClicked()
        }

        binding.cttaDeleteAccount.setSafelyClickListener {
            viewModel.onDeleteBtnClicked()
        }

        binding.cttaTermsUseSettings.setSafelyClickListener {
            viewModel.openTermsOfUse()
        }

        binding.cttaPrivacyPolicySettings.setSafelyClickListener {
            viewModel.openPrivacyPolicy()
        }

        OneSignal.getDeviceState()?.userId?.apply {
            viewModel.setDeviceId(this)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.setVersionName(
            requireContext().getPackageManager()
                .getPackageInfo(requireContext().getPackageName(), 0)
                .versionName
        )
    }

    private fun showPhoneDialog() {
        val dialogBinding = DialogEnterPhoneBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            MaskedTextChangedListener.installOn(
                dialogBinding.etRestoreCode,
                requireContext().getString(R.string.format_phone_number_settings),
                object : MaskedTextChangedListener.ValueListener {
                    override fun onTextChanged(
                        maskFilled: Boolean,
                        extractedValue: String,
                        formattedValue: String
                    ) {
                        viewModel.checkPhone(maskFilled, extractedValue, formattedValue)
                    }
                }
            )

            btnReady.setSafelyClickListener {
                viewModel.changePhone()
                alert.dismiss()
            }
        }
    }

    private fun showEmailDialog() {
        val dialogBinding = DialogEnterEmailBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            btnReady.setSafelyClickListener {
                viewModel.changeEmail(dialogBinding.etRestoreCode.text.toString())
                alert.dismiss()
            }
        }
    }

    private fun showExitDialog(it: Pair<Int, Int>) {
        val alertBD = AlertDialog.Builder(requireContext())

        alertBD.setTitle(it.first)
        alertBD.setMessage(it.second)

        alertBD.setPositiveButton(R.string.label_yes) { dialog, which ->
            viewModel.onPositiveExitDialogBtnClicked()
            dialog.dismiss()
        }
        alertBD.setNegativeButton(R.string.label_no) { dialog, which ->
            dialog.dismiss()
        }
        val alert = alertBD.show()

        alert.setPositiiveBtnTextColor(R.color.colorAccent)
        alert.setNegativeBtnTextColor(R.color.colorAccent)

    }

    private fun showDeleteDialog(it: Pair<Int, Int>) {
        val alertBD = AlertDialog.Builder(requireContext())

        alertBD.setTitle(it.first)
        alertBD.setMessage(it.second)

        alertBD.setPositiveButton(R.string.label_yes) { dialog, which ->
            viewModel.deleteAccount()
            dialog.dismiss()
        }
        alertBD.setNegativeButton(R.string.label_no) { dialog, which ->
            dialog.dismiss()
        }
        val alert = alertBD.show()

        alert.setPositiiveBtnTextColor(R.color.colorAccent)
        alert.setNegativeBtnTextColor(R.color.colorAccent)

    }

    private fun reloadActivity() {
        with(requireActivity()) {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(AppConstants.BundleConstants.RELOAD_ACTIVITY, true)

            overridePendingTransition(0, 0)
            finish()
            overridePendingTransition(0, 0)
            startActivity(intent)
            overridePendingTransition(0, 0)

            viewModelStore.clear()
        }
    }

    private fun openTermsPage(type: String) {
        val fragment = PrivacyTermsFragment.newInstance(type)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            PrivacyTermsFragment.fragmentOpenTag
        )
    }

    private fun openVerifyChangedPhoneFragment(phone: String) {
        val fragment = VerifyChangedPhoneFragment.newInstance(phone)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            VerifyChangedPhoneFragment.fragmentOpenTag
        )
    }

    private fun showSelectCityDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogCityBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectCity(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun whetherWhatsAppIsInstalled() {
        val pm = requireActivity().getPackageManager()
        val uri = resources.getString(R.string.field_whats_app_package)

        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            viewModel.setWhatsAppIsInstalled()
        } catch (ex: PackageManager.NameNotFoundException) {
            viewModel.onWhatsAppNotFound()
        }
    }

    private fun openWhatsApp(params: Uri) {
        val intent = Intent(Intent.ACTION_VIEW, params)

        startActivity(intent)
    }

    private fun onWhatsAppNotFound(uri: Uri) {
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }

}
