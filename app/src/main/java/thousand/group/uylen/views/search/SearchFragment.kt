package thousand.group.uylen.views.search

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.SeekBar
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.uylen.R
import thousand.group.uylen.databinding.*
import thousand.group.uylen.utils.adapters.recycler_view.RecentSearchUsersAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.decorations.VerticalGridListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.search.SearchViewModel
import thousand.group.uylen.views.auth.InterestsFragment


class SearchFragment :
    BaseFragment<FragmentSearchBinding, SearchViewModel>(
        SearchViewModel::class
    ) {

    companion object {
        val fragmentOpenTag =
            MainFragmentHelper.getJsonFragmentTag(
                MainFragmentHelper(
                    title = this::class.java.declaringClass.simpleName,
                    isLightStatusBar = true,
                    isShowBottomNavBar = true,
                    navBarItemPos = 1
                )
            )

        fun newInstance(): SearchFragment {
            val fragment = SearchFragment()
            val args = Bundle()
            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: RecentSearchUsersAdapter

    private var zhuzDialogBinding: DialogRuBinding? = null
    private var regionDialogBinding: DialogCityRegionBinding? = null


    private val seekBarListener = object :
        SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
            Log.i(fragmentTag, "onProgressChanged -> p1:${p1}, p2:${p2}")

            viewModel.onDistanceRangeChanged(p1)
        }

        override fun onStartTrackingTouch(p0: SeekBar?) {
            Log.i(fragmentTag, "onStartTrackingTouch")
        }

        override fun onStopTrackingTouch(p0: SeekBar?) {
            Log.i(fragmentTag, "onStopTrackingTouch")

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.i(fragmentTag, "onCreate")
    }

    override fun getBindingObject() = FragmentSearchBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        setAdapter()
        setDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetFilterActivityVisible) {
            setFilterActivityViewsVisible(it)
        }
        observeLiveData(viewModel.ldSetAgeText) {
            binding.tvAgeVal.setText(it)
        }
        /*observeLiveData(viewModel.ldSetSelectedCity) {
            binding.cttaCity.setText(it)
        }
        observeLiveData(viewModel.ldOpenCityDialog) {
            showSelectCityDialog(it)
        }*/
        observeLiveData(viewModel.ldSetEmptyRecentUsersList) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldSetRecentSearchList) {
            setList(it)
            setDecor()
        }
        observeLiveData(viewModel.ldOpenDetailPage) {
            openDetailPage(it.first, it.second)
        }
        observeLiveData(viewModel.ldRemoveRecentUser) {
            adapter.deleteItem(it)
        }
        observeLiveData(viewModel.ldOpenSearchResultPage) {
            openSearchResultPage(it)
        }
        observeLiveData(viewModel.ldOpenFamilyStatusDialog) {
            showSelectFamilyStatusDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedFamilyStatus) {
            binding.cttaFamilyStatus.setText(it)
        }
        observeLiveData(viewModel.ldOpenInterestsPage) {
            openInterestsFragment(it.first, it.second)
        }
        observeLiveData(viewModel.ldSetInterests) {
            binding.cttaInterests.setText(it)
        }
        observeLiveData(viewModel.ldSetSelectedRu) {
            binding.cttaRu.setText(it)
        }
        observeLiveData(viewModel.ldOpenRuDialog) {
            showZhuzDialog(it)
        }
        observeLiveData(viewModel.ldSetRuParams) {
            zhuzDialogBinding?.apply {
                npSelectRu.maxValue = it.second
                npSelectRu.minValue = 0
                npSelectRu.displayedValues = it.third.toTypedArray()
                npSelectRu.value = it.first
            }
        }
        observeLiveData(viewModel.ldClearRuPicker) {
            zhuzDialogBinding?.npSelectRu?.displayedValues = null
            zhuzDialogBinding?.npSelectRu?.maxValue = 0
        }
        observeLiveData(viewModel.ldOpenZodiacDialog) {
            showSelectZodiacDialog(it)
        }
        observeLiveData(viewModel.ldSetSelectedZodiac) {
            binding.cttaZodiac.setText(it)
        }
        observeLiveData(viewModel.ldSetDistanceText) {
            binding.tvDistanceVal.setText(it)
        }
        observeLiveData(viewModel.ldSetDistanceProgress) {
            binding.seekBarDistance.setOnSeekBarChangeListener(null)
            binding.seekBarDistance.progress = it
            binding.seekBarDistance.setOnSeekBarChangeListener(seekBarListener)
        }
        observeLiveData(viewModel.ldSetKalymVisibility) {
            binding.tvKalymTitle.setVisibilityState(it)
            binding.etMinKalym.setVisibilityState(it)
            binding.etMaxKalym.setVisibilityState(it)
        }
        observeLiveData(viewModel.ldClearKalymEditText) {
            binding.etMinKalym.setText("")
            binding.etMaxKalym.setText("")
        }
        observeLiveData(viewModel.ldClearEditTexts) {
            clearFilterViews()
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.toolbar.ivFilter.setSafelyClickListener {
            viewModel.onFilterActivityClicked()
        }

        binding.rbAge.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            viewModel.onRangeChanged(minValue.toInt(), maxValue.toInt())
        }

        /*binding.cttaCity.setSafelyClickListener {
            viewModel.openCitiesDialog()
        }*/

        binding.toolbar.etSearch
            .setOnEditorActionListener { textView, actionId, keyEvent ->
                when (actionId) {
                    EditorInfo.IME_ACTION_SEARCH -> {
                        viewModel.onSearchActionClicked(
                            textView.text.toString()
                        )

                        viewModel.closeKeyBoard()
                        true
                    }
                    else -> false
                }
            }

        binding.toolbar.etSearch.addTextChangedListener {
            it?.let {
                if (it.length > 0) {
                    binding.toolbar.etSearch.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.clear_search_text,
                        0
                    );
                } else {
                    binding.toolbar.etSearch.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        }

        binding.toolbar.etSearch.setOnTouchListener(OnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2
            if (event.action == MotionEvent.ACTION_UP) {
                binding.toolbar.etSearch.getCompoundDrawables()
                    .get(DRAWABLE_RIGHT)?.apply {

                        if (event.rawX >= binding.toolbar.etSearch.getRight() - this.getBounds()
                                .width()
                        ) {
                            // your action here
                            binding.toolbar.etSearch.setText("")
                            return@OnTouchListener true
                        }
                    }
            }
            false
        })

        binding.btnUse.setSafelyClickListener {
            val locationManager =
                requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val isGpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

            viewModel.onFilterUseBtnClicked(
                binding.toolbar.etSearch.text.toString(),
                binding.cttaRu.getText(),
                binding.cttaFamilyStatus.getText(),
                binding.cttaZodiac.getText(),
                binding.etMinSalary.text.toString(),
                binding.etMaxSalary.text.toString(),
                binding.etMinKalym.text.toString(),
                binding.etMaxKalym.text.toString(),
                binding.etMinHeight.text.toString(),
                binding.etMaxHeight.text.toString(),
                binding.etMinWeight.text.toString(),
                binding.etMaxWeight.text.toString(),
                isGpsOn
            )
        }

        binding.cttaFamilyStatus.setSafelyClickListener {
            viewModel.openFamilyStatusDialog()
        }

        binding.cttaInterests.setSafelyClickListener {
            viewModel.openInterestsPage()
        }

        binding.cttaRu.setSafelyClickListener {
            viewModel.openZhuzDialog()
        }

        binding.cttaZodiac.setSafelyClickListener {
            viewModel.openZodiacDialog()
        }

        binding.seekBarDistance.setOnSeekBarChangeListener(seekBarListener)

        binding.btnClearFilter.setSafelyClickListener {
            viewModel.clearFilter()
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.getRecentSearchUsers()
        viewModel.setKalymVisibility()
        viewModel.setDistanceProgress()
    }

    private fun setFilterActivityViewsVisible(it: Boolean) {
        binding.tvTitle.setVisibilityState(it)
        binding.cttaFamilyStatus.setVisibilityState(it)
        binding.cttaInterests.setVisibilityState(it)
        binding.cttaRu.setVisibilityState(it)
//        binding.cttaCity.setVisibilityState(it)
        binding.tvAgeTitle.setVisibilityState(it)
        binding.tvAgeVal.setVisibilityState(it)
        binding.rbAge.setVisibilityState(it)
        binding.tvSalaryTitle.setVisibilityState(it)
        binding.etMinSalary.setVisibilityState(it)
        binding.etMaxSalary.setVisibilityState(it)
        binding.btnUse.setVisibilityState(it)
        binding.cttaZodiac.setVisibilityState(it)
        binding.tvKalymTitle.setVisibilityState(it)
        binding.etMinKalym.setVisibilityState(it)
        binding.etMaxKalym.setVisibilityState(it)
        binding.tvWeightTitle.setVisibilityState(it)
        binding.etMinWeight.setVisibilityState(it)
        binding.etMaxWeight.setVisibilityState(it)
        binding.tvHeightTitle.setVisibilityState(it)
        binding.etMinHeight.setVisibilityState(it)
        binding.etMaxHeight.setVisibilityState(it)
        binding.tvDistanceTitle.setVisibilityState(it)
        binding.tvDistanceVal.setVisibilityState(it)
        binding.tvDistanceHint.setVisibilityState(it)
        binding.seekBarDistance.setVisibilityState(it)
        binding.btnClearFilter.setVisibilityState(it)
    }

   /* private fun showSelectCityDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogCityBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectCity(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }*/

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = RecentSearchUsersAdapter(
                { model, pos ->
                    viewModel.onRecentUserItemClicked(model, pos)
                },
                { model, pos ->
                    viewModel.onDeleteItemClicked(model, pos)
                }
            )
        }

        binding.rcv.adapter = adapter
    }

    private fun setList(list: MutableList<UserCardData>) {
        adapter.setData(list)
    }

    private fun setDecor() {
        binding.rcv.clearItemDecorations()

        binding.rcv.addItemDecoration(
            VerticalGridListItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.rightAnswersSpace),
                2
            )
        )
    }

    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.tvNothing.setVisibilityState(isEmpty)
        binding.rcv.setVisibilityState(!isEmpty)
    }

    private fun openDetailPage(userId: Long, showChatBtn: Boolean) {
        val fragment = SearchDetailFragment.newInstance(userId, showChatBtn)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            SearchDetailFragment.fragmentOpenTag
        )
    }

    private fun openSearchResultPage(params: ParamsContainer) {
        val fragment = SearchResultFragment.newInstance(params)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            SearchResultFragment.fragmentOpenTag
        )
    }

    private fun showSelectFamilyStatusDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogFamilyStatusBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {

            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectFamilyStatus(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun openInterestsFragment(vmTag: String, interestList: MutableList<Interest>) {
        val fragment = InterestsFragment.newInstance(vmTag, interestList)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            InterestsFragment.fragmentOpenTag
        )
    }

    private fun showZhuzDialog(params: ParamsContainer) {
        val selectedPos = params.getInt(AppConstants.BundleConstants.SELECTED_POS) ?: 0
        val selectedPos2 = params.getInt(AppConstants.BundleConstants.SELECTED_POS_2) ?: 0
        val maxPos = params.getInt(AppConstants.BundleConstants.MAX_POS) ?: 0
        val maxPos2 = params.getInt(AppConstants.BundleConstants.MAX_POS_2) ?: 0
        val zhuzList = params.getList<String>(AppConstants.BundleConstants.PARENT_LIST)
        val ruList = params.getList<String>(AppConstants.BundleConstants.CHILD_LIST)

        if (zhuzList == null || ruList == null) return

        zhuzDialogBinding = DialogRuBinding.inflate(layoutInflater)

        val builder = AlertDialog.Builder(requireContext())

        zhuzDialogBinding?.apply {
            builder.setView(this.root)

            val alert = builder.show()

            alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            alert.window?.setLayout(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            npSelectZhuz.maxValue = maxPos
            npSelectRu.maxValue = maxPos2
            npSelectZhuz.minValue = 0
            npSelectRu.minValue = 0
            npSelectZhuz.displayedValues = zhuzList.toTypedArray()
            npSelectRu.displayedValues = ruList.toTypedArray()
            npSelectZhuz.value = selectedPos
            npSelectRu.value = selectedPos2

            npSelectZhuz.setOnValueChangedListener { numberPicker, i, i2 ->
                viewModel.selectZhuz(i2)
            }

            btnReady.setSafelyClickListener {
                viewModel.selectRu(npSelectRu.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }

            alert.setOnDismissListener {
                zhuzDialogBinding = null
            }
        }
    }

    private fun showSelectZodiacDialog(params: Triple<Int, Int, MutableList<String>>) {
        val dialogBinding = DialogSelectZodiacBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext())

        builder.setView(dialogBinding.root)

        val alert = builder.show()

        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.window?.setLayout(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        with(dialogBinding) {
            npSelectSubject.maxValue = params.second
            npSelectSubject.minValue = 0
            npSelectSubject.displayedValues = params.third.toTypedArray()
            npSelectSubject.value = params.first

            btnReady.setSafelyClickListener {
                viewModel.selectZodiac(npSelectSubject.value)
                alert.dismiss()
            }

            tvCancel.setSafelyClickListener {
                alert.dismiss()
            }
        }
    }

    private fun clearFilterViews() {
        binding.etMinSalary.text.clear()
        binding.etMaxSalary.text.clear()
        binding.etMinKalym.text.clear()
        binding.etMaxKalym.text.clear()
        binding.etMinHeight.text.clear()
        binding.etMaxHeight.text.clear()
        binding.etMinWeight.text.clear()
        binding.etMaxWeight.text.clear()
        binding.seekBarDistance.progress = 160
    }
}