package thousand.group.uylen.views.chat

import android.os.Bundle
import com.squareup.picasso.Picasso
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentChatPhotoDetailBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.chat.ChatPhotoDetailViewModel

class ChatPhotoDetailFragment :
    BaseFragment<FragmentChatPhotoDetailBinding, ChatPhotoDetailViewModel>(
        ChatPhotoDetailViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true
            )
        )

        fun newInstance(imageAddress: String): ChatPhotoDetailFragment {
            val fragment = ChatPhotoDetailFragment()
            val args = Bundle()
            //passing arguments

            args.putString(AppConstants.BundleConstants.ADDRESS_IMAGE, imageAddress)

            fragment.arguments = args
            return fragment
        }
    }

    override fun getBindingObject() = FragmentChatPhotoDetailBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {

    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetImage) {
            Picasso.get()
                .load(it)
                .error(R.drawable.ic_error)
                .fit()
                .centerInside()
                .into(binding.ivContent)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }
    }
}