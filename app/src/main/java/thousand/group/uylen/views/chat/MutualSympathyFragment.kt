package thousand.group.uylen.views.chat

import android.os.Bundle
import thousand.group.data.entities.remote.simple.MatchedUser
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentMutableSympathyBinding
import thousand.group.uylen.utils.adapters.recycler_view.MatchedUsersAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.custom_views.TouchDetectableScrollView
import thousand.group.uylen.utils.decorations.VerticalGridListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.chat.MutualSympathyViewModel

class MutualSympathyFragment :
    BaseFragment<FragmentMutableSympathyBinding, MutualSympathyViewModel>(
        MutualSympathyViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true
            )
        )

        fun newInstance(): MutualSympathyFragment {
            val fragment = MutualSympathyFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: MatchedUsersAdapter

    override fun getBindingObject() = FragmentMutableSympathyBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {

    }

    override fun initView(savedInstanceState: Bundle?) {
        binding.toolbar.tvTitle.setText(R.string.label_mutual_sympathy)

        setAdapter()
        setDecor()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetUsersList) {
            setList(it)
            setDecor()
        }
        observeLiveData(viewModel.ldSetEmptyListParams) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldOpenChatPage) {
            openChatMessagesPage(it)
        }
        observeLiveData(viewModel.ldShowSwipeRefresh) {
            binding.swipeRefreshLayout.isRefreshing = it
        }
        observeLiveData(viewModel.ldShowBottomProgress) {
            showBottomProgress(it)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener { }

        binding.toolbar.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.refreshList()
        }

        binding.tdsv.setMyScrollChangeListener(object :
            TouchDetectableScrollView.OnMyScrollChangeListener {
            override fun onScrollUp() {

            }

            override fun onScrollDown() {
            }

            override fun onBottomReached() {
                viewModel.onBottomReached()
            }

            override fun onTopReached() {
            }
        })

    }

    private fun showBottomProgress(show: Boolean) {
        binding.pbBottom.setVisibilityState(show)
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = MatchedUsersAdapter { user, pos ->
                viewModel.onUserItemClicked(user, pos)
            }
        }

        binding.rcv.adapter = adapter
    }

    private fun setList(list: MutableList<MatchedUser>) {
        binding.rcv.setHasFixedSize(false)
        adapter.setData(list)
        binding.rcv.setHasFixedSize(true)
    }

    private fun setDecor() {
        binding.rcv.clearItemDecorations()

        binding.rcv.addItemDecoration(
            VerticalGridListItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.rightAnswersSpace),
                2
            )
        )
    }

    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.tvNothing.setVisibilityState(isEmpty)
        binding.rcv.setVisibilityState(!isEmpty)
    }

    private fun openChatMessagesPage(receiverId: Long) {
        val fragment = ChatMessagesFragment.newInstance(receiverId)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            ChatMessagesFragment.fragmentOpenTag
        )
    }

}