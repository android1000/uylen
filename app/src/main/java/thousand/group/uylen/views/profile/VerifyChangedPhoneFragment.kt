package thousand.group.uylen.views.profile

import android.os.Bundle
import android.os.CountDownTimer
import androidx.core.widget.addTextChangedListener
import thousand.group.uylen.databinding.FragmentVerifyRegistrationBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.profile.VerifyChangedPhoneViewModel
import thousand.group.uylen.views.registration.VerifyRegistrationFragment

class VerifyChangedPhoneFragment :
    BaseFragment<FragmentVerifyRegistrationBinding, VerifyChangedPhoneViewModel>(
        VerifyChangedPhoneViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true
            )
        )

        fun newInstance(phone: String): VerifyChangedPhoneFragment {
            val fragment = VerifyChangedPhoneFragment()
            val args = Bundle()
            //passing arguments

            args.putString(AppConstants.BundleConstants.PHONE_NUMBER, phone)

            fragment.arguments = args
            return fragment
        }
    }

    private var timer = object : CountDownTimer(60000, 1000) {
        override fun onFinish() {
            viewModel.onTimerFinished()
        }

        override fun onTick(millisUntilFinished: Long) {
            viewModel.onTimerTicker(millisUntilFinished)
        }
    }

    override fun getBindingObject() = FragmentVerifyRegistrationBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldTimerText) {
            binding.btnCode.setText(it)
        }
        observeLiveData(viewModel.ldTimerTextEnabled) {
            binding.btnCode.isEnabled = it
        }
        observeLiveData(viewModel.ldStartTimer) {
            startTimer()
        }
        observeLiveData(viewModel.ldStopTimer) {
            stopTimer()
        }
        observeLiveData(viewModel.ldClosePage) {
            requireActivity().onBackPressed()
        }
        observeLiveData(viewModel.ldSetPhoneText) {
            binding.tvPhone.setText(it)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.ivBack.setSafelyClickListener {
            requireActivity().onBackPressed()
        }

        binding.etRestoreCode.addTextChangedListener {
            viewModel.onCodeChanged(it.toString())
        }

        binding.btnCode.setSafelyClickListener {
            viewModel.onSendCodeBtnClicked()
        }
    }

    private fun startTimer() {
        timer.start()
    }

    private fun stopTimer() {
        timer.cancel()
        viewModel.onTimerStop()
    }


}