package thousand.group.uylen.views.auth

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.onesignal.OneSignal
import com.redmadrobot.inputmask.MaskedTextChangedListener
import thousand.group.data.entities.remote.simple.User
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentLoginBinding
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.extensions.getSupportFragmentManager
import thousand.group.uylen.utils.extensions.replaceFragmentWithBackStack
import thousand.group.uylen.utils.helpers.AuthFragmentHelper
import thousand.group.uylen.view_models.auth.LoginViewModel
import thousand.group.uylen.views.main.MainActivity
import thousand.group.uylen.views.profile.PrivacyTermsFragment
import thousand.group.uylen.views.restore_password.SendRestoreCodeFragment

class LoginFragment :
    BaseFragment<FragmentLoginBinding, LoginViewModel>(
        LoginViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = AuthFragmentHelper.getJsonFragmentTag(
            AuthFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true,
                isTabsVisible = true,
                tabsPosition = 1
            )
        )

        fun newInstance(): LoginFragment {
            val fragment = LoginFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    override fun getBindingObject() = FragmentLoginBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        getOneSignalUserId()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldOpenRestorePasswordPage) {
            openRestorePasswordFragment(it.first, it.second)
        }
        observeLiveData(viewModel.ldOpenMainActivity) {
            openMainActivity()
        }
        observeLiveData(viewModel.ldReloadActivity) {
            reloadActivity()
        }
        observeLiveData(viewModel.ldSetLanguageText) {
            binding.cttaLanguage.setText(it)
        }
        observeLiveData(viewModel.ldOpenTermPage) {
            openTermsPage(it)
        }
    }

    override fun initController() {
        MaskedTextChangedListener.installOn(
            binding.etPhone,
            requireContext().getString(R.string.format_phone_number),
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(
                    maskFilled: Boolean,
                    extractedValue: String,
                    formattedValue: String
                ) {
                    viewModel.checkPhone(maskFilled, extractedValue, formattedValue)
                }
            }
        )

        binding.tvPrivacy.makeLinks(
            Triple(
                resources.getString(R.string.label_part_privacy),
                ContextCompat.getColor(requireContext(), R.color.colorBlack),
                View.OnClickListener {
                    viewModel.openPrivacyPolicy()
                }),
            Triple(
                resources.getString(R.string.label_part_terms),
                ContextCompat.getColor(requireContext(), R.color.colorBlack),
                View.OnClickListener {
                    viewModel.openTerms()
                })
        )

        binding.btnLogin.setSafelyClickListener {
            viewModel.checkLocation(
                binding.etPassword.getText(),
                binding.stvCourseActive.isChecked
            )
        }

        binding.btnRestorePassword.setSafelyClickListener {
            viewModel.openRestorePasswordPage()
        }

        binding.cttaLanguage.setSafelyClickListener {
            viewModel.onLanguageTextClicked()
        }
    }

    private fun reloadActivity() {
        with(requireActivity()) {
            val intent = Intent(this, AuthActivity::class.java)

            overridePendingTransition(0, 0)
            finish()
            overridePendingTransition(0, 0)
            startActivity(intent)
            overridePendingTransition(0, 0)

            viewModelStore.clear()
        }
    }

    private fun openRestorePasswordFragment(phone: String, user: User) {
        val fragment = SendRestoreCodeFragment.newInstance(phone, user)

        getSupportFragmentManager().replaceFragmentWithBackStack(
            R.id.fl_fragment_container,
            fragment,
            SendRestoreCodeFragment.fragmentOpenTag
        )
    }

    private fun openMainActivity() {
        val intent = Intent(requireActivity(), MainActivity::class.java)

        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)

        startActivity(intent).also {
            requireActivity().finish()
        }
    }

    private fun getOneSignalUserId() {
        val userId = OneSignal.getDeviceState()?.userId
        viewModel.setOneSignalUserId(userId)
    }

    private fun openTermsPage(type: String) {
        val fragment = PrivacyTermsFragment.newInstance(type)

        getSupportFragmentManager().replaceFragmentWithBackStack(
            R.id.fl_fragment_container,
            fragment,
            PrivacyTermsFragment.fragmentOpenTag
        )
    }

}