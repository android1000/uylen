package thousand.group.uylen.views.favorite

import android.os.Bundle
import thousand.group.data.entities.remote.simple.LikedMeUser
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FragmentFavoriteBinding
import thousand.group.uylen.utils.adapters.recycler_view.WhoLikedAdapter
import thousand.group.uylen.utils.base.BaseFragment
import thousand.group.uylen.utils.custom_views.TouchDetectableScrollView
import thousand.group.uylen.utils.decorations.VerticalGridListItemDecoration
import thousand.group.uylen.utils.extensions.*
import thousand.group.uylen.utils.helpers.MainFragmentHelper
import thousand.group.uylen.view_models.favourite.FavouriteViewModel
import thousand.group.uylen.views.profile.TarifsFragment

class FavouriteFragment :
    BaseFragment<FragmentFavoriteBinding, FavouriteViewModel>(
        FavouriteViewModel::class
    ) {

    companion object {
        val fragmentOpenTag = MainFragmentHelper.getJsonFragmentTag(
            MainFragmentHelper(
                title = this::class.java.declaringClass.simpleName,
                isLightStatusBar = true,
                isShowBottomNavBar = true,
                navBarItemPos = 3
            )
        )

        fun newInstance(): FavouriteFragment {
            val fragment = FavouriteFragment()
            val args = Bundle()
            //passing arguments

            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var adapter: WhoLikedAdapter

    override fun getBindingObject() = FragmentFavoriteBinding.inflate(layoutInflater)

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
        setAdapter()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetUsersList) {
            setList(it)
            setDecor()
        }
        observeLiveData(viewModel.ldSetEmptyListParams) {
            setEmptyListParams(it)
        }
        observeLiveData(viewModel.ldOpenDetailPage) {
            openDetailPage(it.first, it.second)
        }
        observeLiveData(viewModel.ldDeleteItem) {
            adapter.deleteItem(it)
        }
        observeLiveData(viewModel.ldSetRefreshState) {
            binding.srlCourses.isRefreshing = it
        }
        observeLiveData(viewModel.ldShowBottomProgress){
            binding.pbBottom.setVisibilityState(it)
        }
    }

    override fun initController() {
        binding.root.setSafelyClickListener {  }

        binding.btnShowTariff.setSafelyClickListener {
            openTariffsPage()
        }

        binding.srlCourses.setOnRefreshListener {
            viewModel.onRefreshSwiped()
        }

        binding.tdsvFavourite.setMyScrollChangeListener(object :
            TouchDetectableScrollView.OnMyScrollChangeListener {
            override fun onScrollUp() {

            }

            override fun onScrollDown() {
            }

            override fun onBottomReached() {
                viewModel.onBottomReached()
            }

            override fun onTopReached() {

            }
        })
    }

    override fun onResume() {
        super.onResume()

        viewModel.setStatusReadNotification()
        viewModel.onRefreshSwiped()
    }

    private fun setAdapter() {
        if (!::adapter.isInitialized) {
            adapter = WhoLikedAdapter { user, pos ->
                viewModel.onUserItemClicked(user, pos)
            }
        }

        binding.recyclerLiked.adapter = adapter

        setDecor()
    }

    private fun setList(list: MutableList<LikedMeUser>) {
        adapter.setData(list)

        setDecor()
    }

    private fun setDecor() {
        binding.recyclerLiked.clearItemDecorations()

        binding.recyclerLiked.addItemDecoration(
            VerticalGridListItemDecoration(
                resources.getDimensionPixelOffset(R.dimen.rightAnswersSpace),
                2
            )
        )
    }

    private fun setEmptyListParams(isEmpty: Boolean) {
        binding.tvEmptyList.setVisibilityState(isEmpty)
        binding.recyclerLiked.setVisibilityState(!isEmpty)
    }

    private fun setTarifParams(isBought: Boolean) {
        binding.txtTextTitleTarif.setVisibilityState(isBought)
        binding.txtTextDescTarif.setVisibilityState(isBought)

        binding.imgLockFavorite.setVisibilityState(!isBought)
        binding.txtTextTitleFavorite.setVisibilityState(!isBought)
        binding.txtTextDescFavorite.setVisibilityState(!isBought)
        binding.btnShowTariff.setVisibilityState(!isBought)
    }

    private fun openDetailPage(userId: Long, selPos: Int) {
        val fragment = FavouriteDetailFragment.newInstance(userId, selPos)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            FavouriteDetailFragment.fragmentOpenTag
        )
    }

    private fun openTariffsPage() {
        val fragment = TarifsFragment.newInstance(FavouriteViewModel::class.java.simpleName)

        replaceFragment(
            R.id.fl_fragment_container,
            fragment,
            TarifsFragment.fragmentOpenTag
        )
    }

}