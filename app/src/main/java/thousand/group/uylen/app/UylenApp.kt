package thousand.group.uylen.app

import android.app.Application
import android.content.Intent
import android.util.Log
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import org.json.JSONObject
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import thousand.group.data.storage.utils.CreateLocaleStoragePrefs
import thousand.group.uylen.R
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.di.appModule
import thousand.group.uylen.views.main.MainActivity

class UylenApp : Application(), KoinComponent {
    override fun onCreate() {
        super.onCreate()

        CreateLocaleStoragePrefs.initLocalePrefs(this)

        Picasso.get().setLoggingEnabled(true);

        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        OneSignal.initWithContext(this)
        OneSignal.setAppId(resources.getString(R.string.const_one_signal_app_id))

        startKoin {
            androidContext(this@UylenApp)
            modules(appModule)
        }

        OneSignal.setNotificationOpenedHandler {
            Log.i(
                "TestNotifications",
                "One Signal -> setNotificationOpenedHandler -> notification_data -> data: ${it.notification.additionalData}"
            )
            it.notification.additionalData?.apply {
/*                viewModel.parseNotification(it.notification.additionalData)*/
                startActivity(this)
            }
        }

        val userId = OneSignal.getDeviceState()?.userId
        Log.i("OneSignalUserId", userId.toString())

    }

    private fun startActivity(jsonObject: JSONObject) {
        val intent: Intent = Intent(this, MainActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME or Intent.FLAG_ACTIVITY_NEW_TASK)

        intent.putExtra(AppConstants.BundleConstants.PUSH_OBJECT, jsonObject.toString())

        startActivity(intent)
    }

}