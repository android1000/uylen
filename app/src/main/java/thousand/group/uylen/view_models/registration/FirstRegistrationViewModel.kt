package thousand.group.uylen.view_models.registration

import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.auth.AuthViewModel

class FirstRegistrationViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val profileUsecase: ProfileUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldOpenVerifyPage = OnceLiveData<Pair<String, Pair<Double, Double>>>()
    val ldOpenSecondPage = OnceLiveData<Pair<User, String>>()
    val ldOpenTermPage = OnceLiveData<String>()
    val ldReloadActivity = OnceLiveData<Unit>()
    val ldOpenSecondRegistrationPage =
        OnceLiveData<Pair<Pair<User, String>, Pair<Double, Double>>>()

    val ldSetLanguageText = MutableLiveData<String>()

    private var phone: String? = null
    private var formattedPhone: String? = null

    private var name: String? = null
    private var password: String? = null
    private var oneSignalUserID: String? = null

    private var hasPrivacyApproved = false

    private val params = mutableMapOf<String, RequestBody>()

    init {
        setSerializableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.RESPONSE_LOCATION -> {
                    val location = message as Pair<Double, Double>

                    name?.let { name ->
                        password?.let { password ->
                            register(name, password, hasPrivacyApproved, location)
                        }
                    }
                }
            }
        }

        setUnitMessageReceivedListener { key ->
            when (key) {
                AppConstants.MessageKeysConstants.RESPONSE_LOCATION_ERROR -> {
                    showMessageError(R.string.error_lat_lng)
                }
            }
        }

        setLanguageText()
    }

    fun onLanguageTextClicked() {
        doWork {
            val language = profileUsecase.getLanguage()

            if (language.equals(AppConstants.Languages.kz.langCodeLocale)) {
                profileUsecase.setLanguage(AppConstants.Languages.ru.langCodeLocale)
                profileUsecase.setLanguageServer(AppConstants.Languages.ru.langCodeServer)
            } else {
                profileUsecase.setLanguage(AppConstants.Languages.kz.langCodeLocale)
                profileUsecase.setLanguageServer(AppConstants.Languages.kz.langCodeServer)
            }

            ldReloadActivity.call()
        }
    }

    fun openTermsOfUse() {
        ldOpenTermPage.postValue(AppConstants.PrivacyTermMode.TERM_OF_USE)
    }

    fun openPrivacyPolicy() {
        ldOpenTermPage.postValue(AppConstants.PrivacyTermMode.PRIVACY_POLICY)
    }

    fun checkPhone(
        maskFilled: Boolean,
        extractedValue: String,
        formatterValue: String
    ) {
        phone = if (maskFilled) extractedValue else null
        formattedPhone = formatterValue
    }

    fun register(
        name: String,
        password: String,
        hasPrivacyApproved: Boolean,
        location: Pair<Double, Double>
    ) {
        if (hasPrivacyApproved) {
            checkData(name, password) {
                createParams(name, password)

                doWork {
                    authUsecase.register(params)
                        .onStart { showProgressBar(true) }
                        .onCompletion { showProgressBar(false) }
                        .catch {
                            showMessageError(
                                ResErrorHelper.showThrowableMessage(
                                    resourceManager,
                                    vmTag,
                                    it
                                )
                            )
                        }
                        .collect {
                            it.data?.let { user ->
                                it.token?.let { token ->
                                    ldOpenSecondRegistrationPage.postValue(
                                        Pair(
                                            Pair(user, token),
                                            location
                                        )
                                    )
                                }
                            }
                        }
                }
            }
        } else {
            showMessageError(R.string.error_privacy_is_not_checked)
        }
    }

    fun setOneSignalUserId(id: String?) {
        this.oneSignalUserID = id

        Log.i(vmTag, "setOneSignalUserId -> oneSignalUserID: ${this.oneSignalUserID}")
    }

    fun checkLocation(
        name: String,
        password: String,
        hasPrivacyApproved: Boolean
    ) {
        this.name = name
        this.password = password
        this.hasPrivacyApproved = hasPrivacyApproved

        sendLocaleMessage(
            AuthViewModel::class,
            AppConstants.MessageKeysConstants.REQUIRE_LOCATION,
            vmTag
        )
    }

    private fun checkData(
        name: String,
        password: String,
        isSuccess: () -> Unit
    ) {
        when {
            phone.isNullOrEmpty() -> showMessageError(R.string.error_phone)
            name.trim().isEmpty() -> showMessageError(R.string.error_name)
            password.trim().isEmpty() -> showMessageError(R.string.error_password)
            else -> isSuccess.invoke()
        }
    }

    private fun createParams(
        name: String,
        password: String
    ) {
        params.clear()

        params.put(RemoteConstants.PHONE, RequestBodyHelper.getRequestBodyText(phone))
        params.put(RemoteConstants.NAME, RequestBodyHelper.getRequestBodyText(name))
        params.put(
            RemoteConstants.PASSWORD,
            RequestBodyHelper.getRequestBodyText(password)
        )
        params.put(
            RemoteConstants.APP_CLIENT_ID,
            RequestBodyHelper.getRequestBodyText(oneSignalUserID)
        )
    }

    private fun setLanguageText() {
        doWork {
            val lang = profileUsecase.getLanguage()

            if (AppConstants.Languages.kz.langCodeLocale.equals(lang)) {
                ldSetLanguageText.postValue(
                    resourceManager.getString(R.string.label_lang_kz)
                )
            } else {
                ldSetLanguageText.postValue(
                    resourceManager.getString(R.string.label_lang_ru)
                )
            }
        }
    }
}