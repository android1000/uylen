package thousand.group.uylen.view_models.profile

import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.City
import thousand.group.data.entities.remote.simple.Region
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.storage.constants.StorageConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.extensions.clear
import thousand.group.uylen.utils.extensions.formatToPhoneNumber
import thousand.group.uylen.utils.extensions.isEmailValid
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager

class SettingsViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val profileUsecase: ProfileUsecase,
    private val utilUsecase: UtilUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetVersionName = MutableLiveData<String>()
    val ldSetPhone = MutableLiveData<String>()
    val ldSetEmail = MutableLiveData<String>()
    val ldSetShowAnketaStatus = MutableLiveData<Boolean>()
    val ldSetDistanceText = MutableLiveData<String>()
    val ldSetSelectedCity = MutableLiveData<String>()
    val ldSetLanguageText = MutableLiveData<String>()
    val ldSetActivePush = MutableLiveData<Boolean>()

    val ldOpenCityDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldShowExitDialog = OnceLiveData<Pair<Int, Int>>()
    val ldShowDeleteDialog = OnceLiveData<Pair<Int, Int>>()
    val ldCloseActivity = OnceLiveData<Unit>()
    val ldSetDistanceVal = OnceLiveData<Int>()
    val ldReloadActivity = OnceLiveData<Unit>()
    val ldOpenTermPage = OnceLiveData<String>()
    val ldOpenVerifyChangePhonePage = OnceLiveData<String>()
    val ldDoOnWhatsAppNotInstalled = OnceLiveData<Uri>()
    val ldOpenWhatsApp = OnceLiveData<Uri>()

    private var user = authUsecase.getUserModel()

    private var phone: String? = null
    private var deviceId: String? = null

    private val params = mutableMapOf<String, RequestBody>()

    private val citiesList = mutableListOf<City>()
    private val citiesNamesList = mutableListOf<String>()

    private var selectedCityPos = 0
    private var currentDistance = 160

    private var pushIsActive = false
    private var whatsAppIsInstalled = false


    init {
        setUnitMessageReceivedListener {
            when (it) {
                AppConstants.MessageKeysConstants.EDIT_PHONE -> {
                    editPhone()
                }
            }
        }

        setLanguageText()
        setPhone()
//        setEmail()
        setCityList()
        setShowAnketaValue()
        setActivePush()

        val distance = user?.location?.distance ?: 160

        onDistanceRangeChanged(distance)
        ldSetDistanceVal.postValue(distance)
    }

    override fun onCleared() {
        sendLocaleCallback(
            ProfileViewModel::class,
            AppConstants.MessageKeysConstants.UPDATE_PROFILE
        )

        super.onCleared()
    }

    fun onDistanceRangeChanged(distance: Int) {
        currentDistance = distance

        ldSetDistanceText.postValue(distance.toString())
    }

    fun sendDistance(isGpsOn: Boolean) {
        if (isGpsOn) {
            doWork {
                val distancePart = RequestBodyHelper.getRequestBodyText(currentDistance)

                profileUsecase.setDistance(distancePart)
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect()
            }
        } else {
            showMessageError(R.string.error_lat_lng)
        }

    }

    fun deleteAccount() {
        doWork {
            profileUsecase.deleteAccount()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    authUsecase.setSound(false)
                    authUsecase.setPush(false)
                    authUsecase.setAccessToken(StorageConstants.PREF_NO_VAL)
                    authUsecase.setAccessTokenType(StorageConstants.PREF_NO_VAL)
                    authUsecase.deleteUserModel()
                    authUsecase.setUserId(0)

                    ldCloseActivity.call()
                }
        }
    }

    fun onExitBtnClicked() {
        ldShowExitDialog.postValue(
            Pair(
                R.string.ask_title_exit,
                R.string.ask_exit
            )
        )
    }

    fun onDeleteBtnClicked() {
        ldShowDeleteDialog.postValue(
            Pair(
                R.string.ask_delete_account,
                R.string.ask_title_delete_account
            )
        )
    }

    fun setDeviceId(id: String) {
        this.deviceId = id
    }

    fun onPositiveExitDialogBtnClicked() {
        deviceId?.let {
            doWork {
                val deviceIdPart = RequestBodyHelper.getRequestBodyText(it)

                authUsecase.logout(deviceIdPart)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        authUsecase.setSound(false)
                        authUsecase.setPush(false)
                        authUsecase.setAccessToken(StorageConstants.PREF_NO_VAL)
                        authUsecase.setAccessTokenType(StorageConstants.PREF_NO_VAL)
                        authUsecase.deleteUserModel()
                        authUsecase.setUserId(0)

                        ldCloseActivity.call()
                    }
                    .collect {
                        authUsecase.setSound(false)
                        authUsecase.setPush(false)
                        authUsecase.setAccessToken(StorageConstants.PREF_NO_VAL)
                        authUsecase.setAccessTokenType(StorageConstants.PREF_NO_VAL)
                        authUsecase.deleteUserModel()
                        authUsecase.setUserId(0)

                        ldCloseActivity.call()
                    }
            }
        }

    }

    fun setVersionName(versionName: String) {
        val versionNameFormat = String.format(
            resourceManager.getString(R.string.format_version),
            versionName
        )

        ldSetVersionName.postValue(versionNameFormat)
    }

    fun checkPhone(
        maskFilled: Boolean,
        extractedValue: String,
        formatterValue: String
    ) {
        phone = if (maskFilled) extractedValue else null
    }

    fun changePhone() {
        if (phone == null) {
            showMessageError(R.string.error_phone)
            return
        }

        doWork {
            val phonePart = RequestBodyHelper.getRequestBodyText(phone)

            authUsecase.sendVerificationCodeToNewNumber(phonePart)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    ldOpenVerifyChangePhonePage.postValue(phone!!)
                }

        }
    }

    fun changeEmail(email: String) {
        if (email.trim().isEmpty() || !email.trim().isEmailValid()) {
            showMessageError(R.string.error_email)
            return
        }

        doWork {
            params.clear()

            params.put(
                RemoteConstants.EMAIL, RequestBodyHelper.getRequestBodyText(
                    email.trim()
                )
            )

            updateProfile {
//                setEmail()
            }
        }
    }

    fun openCityDialog() {
        ldOpenCityDialog.postValue(
            Triple(
                selectedCityPos,
                citiesNamesList.size - 1,
                citiesNamesList
            )
        )
    }

    fun selectCity(pos: Int) {
        val cityId = citiesList.get(pos).id

        params.put(
            RemoteConstants.CITY_ID, RequestBodyHelper.getRequestBodyText(
                cityId
            )
        )

        updateProfile {
            selectedCityPos = pos

            ldSetSelectedCity.postValue(citiesList.get(selectedCityPos).name)
        }
    }

    fun setOnShowAnketaChecked(checked: Boolean) {
        doWork {
            val status =
                resourceManager.getString(if (checked) R.string.field_active else R.string.field_inactive)

            profileUsecase.editShowMyAnketa(RequestBodyHelper.getRequestBodyText(status))
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )

                    setShowAnketaValue()
                }
                .collect {
                    changeShowAnketaValue()
                    setShowAnketaValue()
                }
        }
    }

    fun setOnActivePushChecked(checked: Boolean) {
        doWork {
            val checkedStatus = RequestBodyHelper.getRequestBodyText(if (checked) 1 else 0)

            profileUsecase.changePushState(checkedStatus)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )

                    ldSetActivePush.postValue(pushIsActive)
                }
                .collect {
                    pushIsActive = it

                    ldSetActivePush.postValue(pushIsActive)
                }
        }
    }

    fun onLanguageTextClicked() {
        doWork {
            val language = profileUsecase.getLanguage()

            if (language.equals(AppConstants.Languages.kz.langCodeLocale)) {
                profileUsecase.setLanguage(AppConstants.Languages.ru.langCodeLocale)
                profileUsecase.setLanguageServer(AppConstants.Languages.ru.langCodeServer)
            } else {
                profileUsecase.setLanguage(AppConstants.Languages.kz.langCodeLocale)
                profileUsecase.setLanguageServer(AppConstants.Languages.kz.langCodeServer)
            }

            ldReloadActivity.call()
        }
    }

    fun openTermsOfUse() {
        ldOpenTermPage.postValue(AppConstants.PrivacyTermMode.TERM_OF_USE)
    }

    fun openPrivacyPolicy() {
        ldOpenTermPage.postValue(AppConstants.PrivacyTermMode.PRIVACY_POLICY)
    }

    fun onWhatsAppBtnClicked() {
        if (whatsAppIsInstalled) {
            val uri = Uri.parse("https://wa.me/+" + "777758997755")

            ldOpenWhatsApp.postValue(uri)

        } else {
            val uri = Uri.parse("market://details?id=com.whatsapp")
            ldDoOnWhatsAppNotInstalled.postValue(uri)
        }
    }

    fun setWhatsAppIsInstalled() {
        whatsAppIsInstalled = true
    }

    fun onWhatsAppNotFound() {

        whatsAppIsInstalled = false
    }

    private fun updateProfile(
        isSuccess: () -> Unit
    ) {
        doWork {
            profileUsecase.editProfile(params)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    authUsecase.saveUserModel(it)
                    authUsecase.setUserId(it.id)

                    user = it

                    isSuccess.invoke()
                }
        }
    }

    private fun setPhone() {
        user?.phone?.let {
            doWork {
                ldSetPhone.postValue(it.formatToPhoneNumber())
            }
        }
    }

    private fun setEmail() {
        user?.email?.let {
            doWork {
                ldSetEmail.postValue(it)
            }
        }
    }

    private fun setShowAnketaValue() {
        user?.let {
            val isActive =
                it.questionnaire_status.toString() in resourceManager.getString(R.string.field_active)

            ldSetShowAnketaStatus.postValue(isActive)
        }
    }

    private fun changeShowAnketaValue() {
        val isActive =
            user?.questionnaire_status.toString() in resourceManager.getString(R.string.field_active)

        user?.questionnaire_status = resourceManager.getString(
            if (isActive) {
                R.string.field_inactive
            } else {
                R.string.field_active
            }
        )
    }

    private fun setCityList() {
        authUsecase.getUserModel()?.let { user ->
            doWork {
                utilUsecase.getCitiesList()
                    .catch { it.printStackTrace() }
                    .collect {
                        citiesList.clear()
                        citiesNamesList.clear()

                        citiesList.addAll(it)

                        citiesList.forEach {
                            citiesNamesList.add(it.name)
                        }

                        setInitCityId()
                    }
            }
        }
    }

    private fun setInitCityId() {
        user?.details?.city_id?.let {
            citiesList.forEachIndexed { indexCity, city ->
                if (it.id == city.id) {
                    selectedCityPos = indexCity

                    ldSetSelectedCity.postValue(city.name)
                }
            }
        }
    }

    private fun setLanguageText() {
        doWork {
            val lang = profileUsecase.getLanguage()

            if (AppConstants.Languages.kz.langCodeLocale.equals(lang)) {
                ldSetLanguageText.postValue(
                    resourceManager.getString(R.string.label_lang_kz)
                )
            } else {
                ldSetLanguageText.postValue(
                    resourceManager.getString(R.string.label_lang_ru)
                )
            }
        }
    }

    private fun setActivePush() {
        doWork {
            profileUsecase.getPushState()
                .catch { it.printStackTrace() }
                .collect {
                    pushIsActive = it

                    ldSetActivePush.postValue(pushIsActive)
                }
        }
    }

    private fun editPhone() {
        params.clear()

        params.put(
            RemoteConstants.PHONE, RequestBodyHelper.getRequestBodyText(
                phone
            )
        )

        updateProfile {
            setPhone()
        }
    }

}