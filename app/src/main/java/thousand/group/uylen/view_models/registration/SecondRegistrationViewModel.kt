package thousand.group.uylen.view_models.registration

import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.*
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.DateParser
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.locale.EnterSingleInfo
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import java.util.*

class SecondRegistrationViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val utilUsecase: UtilUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetSelectedGender = MutableLiveData<String>()
    val ldSetSelectedFamilyStatus = MutableLiveData<String>()
    val ldSetSelectedCity = MutableLiveData<String>()
    val ldSetSelectedBirthdate = MutableLiveData<String>()

    //    val ldSetSelectedCityRegion = MutableLiveData<String>()
    val ldSetSelectedRu = MutableLiveData<String>()
    val ldSetIncome = MutableLiveData<String>()
    val ldSetInterests = MutableLiveData<String>()
    val ldSetKalym = MutableLiveData<String>()
    val ldSetBadHabitsText = MutableLiveData<String>()
    val ldSetSelectedZodiac = MutableLiveData<String>()
    val ldSetWeight = MutableLiveData<String>()
    val ldSetHeight = MutableLiveData<String>()
    val ldSetKalymVisibility = MutableLiveData<Boolean>()

    val ldOpenGenderDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenFamilyStatusDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenCityDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenRuDialog = OnceLiveData<ParamsContainer>()
    val ldSetRuParams = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenEnterSingleInfoPage = OnceLiveData<Triple<String, EnterSingleInfo, String>>()
    val ldOpenInterestsPage = OnceLiveData<Pair<String, MutableList<Interest>>>()
    val ldOpenThirdRegisterPage = OnceLiveData<Triple<User, String, Pair<Double, Double>>>()
    val ldClearRuPicker = OnceLiveData<Unit>()
    val ldOpenZodiacDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenBadHabitsDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()

    //    val ldOpenCityRegionDialog = OnceLiveData<ParamsContainer>()
    val ldSetCityRegionParams = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenBirthdayDialog = OnceLiveData<Pair<Long, Triple<Int, Int, Int>>>()
//    val ldClearCityPicker = OnceLiveData<Unit>()

    private val genderList = mutableListOf<GeneralValue>()
    private val genderNamesList = mutableListOf<String>()

    private val familyStatusList = mutableListOf<GeneralValue>()
    private val familyNamesStatusList = mutableListOf<String>()

    private val citiesList = mutableListOf<City>()
    private val citiesNamesList = mutableListOf<String>()

    private val zhuzList = mutableListOf<Zhuz>()
    private val zhuzNamesList = mutableListOf<String>()
    private val ruNamesList = mutableListOf<MutableList<String>>()

    private val interestList = mutableListOf<Interest>()

    private val zodiacList = mutableListOf<GeneralValue>()
    private val zodiacNameList = mutableListOf<String>()

    private val badHabitsList = mutableListOf<GeneralValue>()
    private val badHabitsNameList = mutableListOf<String>()

    private var selectedGenderPos: Int? = null
    private var selectedFamilyStatusPos = 0
    private var selectedCityPos = 0
    private var selectedRegionPos = 0
    private var selectedZhuzPos = 0
    private var selectedRuPos = 0
    private var selectedZodiacPos = 0
    private var birthYear = 0
    private var birthMonth = 0
    private var birthDay = 0
    private var selectedBadHabitPos = 0

    private lateinit var user: User

    private lateinit var location: Pair<Double, Double>

    private var birthDate = ""
    private var birthDateFormatted = ""
    private var token = ""
    private val AUTH_PREFIX = "Bearer"

    private val params = mutableMapOf<String, RequestBody>()

    init {
        setSerializableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.ENTER_VAL -> {
                    val params = message as Pair<Int, String>

                    parseEnterSingleValue(params.first, params.second)
                }
            }
        }

        setStringMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.SET_INTEREST_TEXT -> {
                    ldSetInterests.postValue(message)
                }
            }
        }

        setListMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.SET_INTEREST_LIST -> {
                    Log.i(
                        "TestList",
                        "AppConstants.MessageKeysConstants.SET_INTEREST_LIST -> message: $message"
                    )

                    interestList.clear()
                    interestList.addAll(message as MutableList<Interest>)
                }
            }
        }

        parseArgs()

        getGenderValues()
        getZodiacValues()
        getBadHabitsValues()
        getFamilyStatusValues()
        setCityList()
        setRuList()
    }

    fun openBirthdayDialog() {
        val calendarCurrent = Calendar.getInstance()

        var currentYear = calendarCurrent.get(Calendar.YEAR)
        currentYear -= 18

        calendarCurrent.set(Calendar.YEAR, currentYear)

        if (birthDay == 0) {
            val calendar = Calendar.getInstance()

            birthYear = calendar.get(Calendar.YEAR)
            birthMonth = calendar.get(Calendar.MONTH)
            birthDay = calendar.get(Calendar.DAY_OF_MONTH)
        }

        ldOpenBirthdayDialog.postValue(
            Pair(
                calendarCurrent.timeInMillis,
                Triple(birthYear, birthMonth, birthDay)
            )
        )
    }

    fun selectGender(pos: Int) {
        selectedGenderPos = pos
        ldSetSelectedGender.postValue(genderNamesList.get(pos))
        ldSetKalymVisibility.postValue(pos == 1)
    }

    fun openGenderDialog() {
        ldOpenGenderDialog.postValue(
            Triple(
                if (selectedGenderPos == null) 0 else selectedGenderPos!!,
                genderNamesList.size - 1,
                genderNamesList
            )
        )
    }

    fun selectFamilyStatus(pos: Int) {
        selectedFamilyStatusPos = pos

        val item = familyNamesStatusList.get(selectedFamilyStatusPos)

        ldSetSelectedFamilyStatus.postValue(item)
    }

    fun openFamilyStatusDialog() {
        val list = familyNamesStatusList

        ldOpenFamilyStatusDialog.postValue(
            Triple(
                selectedFamilyStatusPos,
                list.size - 1,
                list
            )
        )
    }

    fun selectZodiac(pos: Int) {
        selectedZodiacPos = pos

        ldSetSelectedZodiac.postValue(zodiacNameList.get(selectedZodiacPos))
    }

    fun openZodiacDialog() {
        ldOpenZodiacDialog.postValue(
            Triple(
                selectedZodiacPos,
                zodiacNameList.size - 1,
                zodiacNameList
            )
        )
    }

    fun selectBadHabit(pos: Int) {
        selectedBadHabitPos = pos

        ldSetBadHabitsText.postValue(badHabitsNameList.get(selectedBadHabitPos))
    }

    fun openBadHabitsDialog() {
        ldOpenBadHabitsDialog.postValue(
            Triple(
                selectedBadHabitPos,
                badHabitsNameList.size - 1,
                badHabitsNameList
            )
        )
    }

    fun onOpenningDateSelected(year: Int, month: Int, dayOfMonth: Int) {

        birthDate =
            String.format(
                resourceManager.getString(R.string.format_date),
                year,
                month.inc(),
                dayOfMonth
            )

        birthYear = year
        birthMonth = month
        birthDay = dayOfMonth

        birthDateFormatted = DateParser.convertOneFormatToAnother(
            birthDate,
            DateParser.format_YYYY_MM_dd,
            DateParser.format_dd_LLLL_YYYY
        )

        ldSetSelectedBirthdate.postValue(birthDateFormatted)
    }

    fun selectZhuz(pos: Int) {
        ldClearRuPicker.call()

        if (ruNamesList.isEmpty() || ruNamesList.get(pos).isEmpty()) return

        selectedZhuzPos = pos
        selectedRuPos = 0

        val list = ruNamesList.get(selectedZhuzPos)

        ldSetRuParams.postValue(
            Triple(
                selectedRuPos,
                list.size - 1,
                list
            )
        )

        setSelectedZhuzParams()
    }

    fun selectRu(pos: Int) {
        selectedRuPos = pos

        setSelectedZhuzParams()
    }


    fun openZhuzDialog() {
        val params = ParamsContainer()

        val list = ruNamesList.get(selectedZhuzPos)

        params.putInt(AppConstants.BundleConstants.SELECTED_POS, selectedZhuzPos)
        params.putInt(AppConstants.BundleConstants.SELECTED_POS_2, selectedRuPos)
        params.putInt(AppConstants.BundleConstants.MAX_POS, zhuzList.size - 1)
        params.putInt(AppConstants.BundleConstants.MAX_POS_2, list.size - 1)
        params.putList(AppConstants.BundleConstants.PARENT_LIST, zhuzNamesList)
        params.putList(AppConstants.BundleConstants.CHILD_LIST, list)

        ldOpenRuDialog.postValue(params)
    }

    fun openIncomeEditPage(income: String) {
        ldOpenEnterSingleInfoPage.postValue(
            Triple(
                vmTag,
                AppConstants.EnterSingleInfoTypes.ENTER_INCOME,
                income
            )
        )
    }

    fun openKalymEditPage(kalym: String) {
        ldOpenEnterSingleInfoPage.postValue(
            Triple(
                vmTag,
                AppConstants.EnterSingleInfoTypes.ENTER_KALYM,
                kalym
            )
        )
    }

    fun openBadHabitsEditPage(badHabits: String) {
        ldOpenEnterSingleInfoPage.postValue(
            Triple(
                vmTag,
                AppConstants.EnterSingleInfoTypes.ENTER_BAD_HABITS,
                badHabits
            )
        )
    }

    fun openWeightEditPage(weight: String) {
        ldOpenEnterSingleInfoPage.postValue(
            Triple(
                vmTag,
                AppConstants.EnterSingleInfoTypes.ENTER_WEIGHT,
                weight
            )
        )
    }

    fun openHeightEditPage(height: String) {
        ldOpenEnterSingleInfoPage.postValue(
            Triple(
                vmTag,
                AppConstants.EnterSingleInfoTypes.ENTER_HEIGHT,
                height
            )
        )
    }

    fun openInterestsPage() {
        ldOpenInterestsPage.postValue(Pair(vmTag, interestList))
    }

    fun onAfterBtnClicked(
        familyStatus: String,
        salary: String,
        kalym: String,
        badHabits: String,
        zodiac: String,
        weight: String,
        height: String,
        ruText: String
    ) {
        checkParams(familyStatus, badHabits, zodiac, weight, height, ruText) {
            createParams(
                salary,
                kalym,
                weight,
                height
            ) {
                doWork {
                    authUsecase.fillAnketa(
                        "$AUTH_PREFIX $token",
                        params
                    )
                        .onStart { showProgressBar(true) }
                        .onCompletion { showProgressBar(false) }
                        .catch {
                            showMessageError(
                                ResErrorHelper.parseErrorByStatusCode(
                                    resourceManager,
                                    vmTag,
                                    it
                                )
                            )
                        }
                        .collect {
                            ldOpenThirdRegisterPage.postValue(Triple(user, token, location))
                        }
                }
            }
        }
    }

    /* fun selectRegion(pos: Int) {
         Log.i(vmTag, "selectRegion -> citiesNamesList: ${citiesNamesList}")

         citiesList.clear()
         citiesList.addAll(regionsList.get(pos).cities)

         if (citiesNamesList.isEmpty() || citiesNamesList.get(pos).isEmpty()) {
             return
         }

         selectedRegionPos = pos
         selectedCityPos = 0

         val list = citiesNamesList.get(selectedRegionPos)

         ldSetCityRegionParams.postValue(
             Triple(
                 selectedCityPos,
                 list.size - 1,
                 list
             )
         )

         setSelectedCityRegionParams()
     }*/

    fun selectCity(pos: Int) {
        selectedCityPos = pos

        ldSetSelectedCity.postValue(citiesList.get(selectedCityPos).name)
    }

    fun openCityDialog() {
        ldOpenCityDialog.postValue(
            Triple(
                selectedCityPos,
                citiesNamesList.size - 1,
                citiesNamesList
            )
        )
    }

    /*fun openRegionsDialog() {
        if (citiesList.isNotEmpty() && regionsNamesList.isNotEmpty()) {
            val params = ParamsContainer()

            val list = citiesNamesList.get(selectedRegionPos)

            val maxPos2 = if (list.isEmpty()) 0 else list.size - 1

            params.putInt(AppConstants.BundleConstants.SELECTED_POS, selectedRegionPos)
            params.putInt(AppConstants.BundleConstants.SELECTED_POS_2, selectedCityPos)
            params.putInt(AppConstants.BundleConstants.MAX_POS, regionsList.size - 1)
            params.putInt(AppConstants.BundleConstants.MAX_POS_2, maxPos2)
            params.putList(AppConstants.BundleConstants.PARENT_LIST, regionsNamesList)

            if (list.isNotEmpty()) {
                params.putList(AppConstants.BundleConstants.CHILD_LIST, list)
            }

            ldOpenCityRegionDialog.postValue(params)

            Log.i(vmTag, "openRegionsDialog -> params: ${params}")
        }
    }*/

//    private fun setCityRegionsList() {
//        user?.let { user ->
//            doWork {
//                utilUsecase.getRegions()
//                    .catch { it.printStackTrace() }
//                    .collect {
//                        regionsList.clear()
//                        regionsList.addAll(it)
//
//                        regionsList.forEach {
//                            regionsNamesList.add(it.name)
//                        }
//
//                        regionsList.forEachIndexed { index, region ->
//                            citiesNamesList.add(mutableListOf())
//
//                            region.cities.forEach {
//                                citiesNamesList.get(index).add(it.name)
//                            }
//                        }
//
//                        citiesList.clear()
//                        citiesList.addAll(regionsList.get(selectedRegionPos).cities)
//
//                        setSelectedCityRegionParams()
//                    }
//            }
//        }
//    }

    private fun setSelectedZhuzParams() {
        val zhuzItem = zhuzNamesList.get(selectedZhuzPos)

        val ruItem = if (ruNamesList.isNotEmpty() && ruNamesList.get(selectedZhuzPos)
                .isNotEmpty()
        ) ruNamesList.get(selectedZhuzPos).get(selectedRuPos) else ""

        ldSetSelectedRu.postValue("$zhuzItem, $ruItem")
    }

    private fun parseEnterSingleValue(type: Int, text: String) {
        when (type) {
            AppConstants.EnterSingleInfoTypes.ENTER_INCOME.type -> ldSetIncome.postValue(text)
            AppConstants.EnterSingleInfoTypes.ENTER_KALYM.type -> ldSetKalym.postValue(text)
            AppConstants.EnterSingleInfoTypes.ENTER_BAD_HABITS.type -> ldSetBadHabitsText.postValue(
                text
            )
            AppConstants.EnterSingleInfoTypes.ENTER_WEIGHT.type -> ldSetWeight.postValue(text)
            AppConstants.EnterSingleInfoTypes.ENTER_HEIGHT.type -> ldSetHeight.postValue(text)
        }
    }

    private fun checkParams(
        familyStatus: String,
        badHabits: String,
        zodiac: String,
        weight: String,
        height: String,
        ruText:String,
        isSuccess: () -> Unit
    ) {
        when {
            ruText.trim().isEmpty() -> showMessageError(R.string.error_select_ru)
            familyStatus.trim().isEmpty() -> showMessageError(R.string.error_family_status_empty)
            badHabits.trim().isEmpty() -> showMessageError(R.string.error_bad_habits)
            interestList.isEmpty() -> showMessageError(R.string.error_interests_empty)
            birthDate.isEmpty() -> showMessageError(R.string.error_birthday)
            zodiac.isEmpty() -> showMessageError(R.string.error_zodiac)
//            weight.isEmpty() -> showMessageError(R.string.error_weight)
//            height.isEmpty() -> showMessageError(R.string.error_height)
            selectedGenderPos == null -> showMessageError(R.string.error_select_gender)
            else -> isSuccess.invoke()
        }
    }

    private fun createParams(
        salary: String,
        kalym: String,
        weight: String,
        height: String,
        isCreated: () -> Unit
    ) {
        params.clear()

        params.put(
            RemoteConstants.CITY_ID,
            RequestBodyHelper.getRequestBodyText(citiesList.get(selectedCityPos).id)
        )

        params.put(
            RemoteConstants.DATE_OF_BIRTH,
            RequestBodyHelper.getRequestBodyText(birthDate)
        )

        params.put(
            RemoteConstants.GENER_ID,
            RequestBodyHelper.getRequestBodyText(
                zhuzList
                    .get(selectedZhuzPos)
                    .parents
                    .get(
                        selectedRuPos
                    )
                    .id
            )
        )

        params.put(
            RemoteConstants.SEX,
            RequestBodyHelper.getRequestBodyText(
                genderList.get(selectedGenderPos!!).id
            )
        )

/*
        params.put(
            RemoteConstants.ME_STATUS,
            RequestBodyHelper.getRequestBodyText(
                statusText
            )
        )*/

        params.put(
            RemoteConstants.MARITAL_STATUS,
            RequestBodyHelper.getRequestBodyText(
                familyStatusList.get(selectedFamilyStatusPos).id
            )
        )

        params.put(
            RemoteConstants.USER_ID,
            RequestBodyHelper.getRequestBodyText(
                user.id
            )
        )

        if (salary.isNotEmpty()) {
            params.put(
                RemoteConstants.SALARY,
                RequestBodyHelper.getRequestBodyText(salary)
            )
        }

        if (selectedGenderPos == 1 && kalym.trim().isNotEmpty()) {
            params.put(
                RemoteConstants.K_MAL,
                RequestBodyHelper.getRequestBodyText(
                    kalym.trim()
                )
            )
        }

        params.put(
            RemoteConstants.BAD_HABITS,
            RequestBodyHelper.getRequestBodyText(badHabitsList.get(selectedBadHabitPos).id)
        )

        params.put(
            RemoteConstants.ZODIAC,
            RequestBodyHelper.getRequestBodyText(
                zodiacList.get(selectedZodiacPos).id
            )
        )

        if (weight.isNotEmpty()) {
            params.put(
                RemoteConstants.WEIGHT,
                RequestBodyHelper.getRequestBodyText(
                    weight.trim()
                )
            )
        }

        if (height.isNotEmpty()) {
            params.put(
                RemoteConstants.HEIGHT,
                RequestBodyHelper.getRequestBodyText(
                    height.trim()
                )
            )
        }

        if (interestList.isNotEmpty()) {
            val interestFormat = resourceManager.getString(R.string.format_interests)

            interestList.forEachIndexed { index, item ->
                val key = String.format(interestFormat, index.inc())

                params.put(key, RequestBodyHelper.getRequestBodyText(item.id))
            }
        }

        isCreated.invoke()
    }

    private fun setRuList() {
        doWork {
            utilUsecase.getRuList()
                .catch { it.printStackTrace() }
                .collect {
                    zhuzList.clear()
                    zhuzList.addAll(it)

                    zhuzList.forEach {
                        zhuzNamesList.add(it.name)
                    }

                    zhuzList.forEachIndexed { index, zhuz ->
                        ruNamesList.add(mutableListOf())

                        zhuz.parents.forEach {
                            ruNamesList.get(index).add(it.name)
                        }
                    }

                }
        }
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getParcelable<User>(AppConstants.BundleConstants.USER)?.apply {
                user = this
            }
            getString(AppConstants.BundleConstants.TOKEN)?.apply {
                token = this
            }
            getSerializable(AppConstants.BundleConstants.LOCATION)?.apply {
                location = this as Pair<Double, Double>
            }
        }
    }

    /* private fun setSelectedCityRegionParams() {
         val regionItem = regionsNamesList.get(selectedRegionPos)

         val cityItem = if (citiesNamesList.isNotEmpty() && citiesNamesList.get(selectedRegionPos)
                 .isNotEmpty()
         ) citiesNamesList.get(selectedRegionPos).get(selectedCityPos) else ""

         ldSetSelectedCityRegion.postValue("$regionItem, $cityItem")
     }
 */
    private fun getGenderValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.SEX)
                .catch { it.printStackTrace() }
                .collect {
                    genderList.clear()
                    genderNamesList.clear()

                    genderList.addAll(it)

                    genderList.sortBy { it.id }

                    genderList.forEach {
                        genderNamesList.add(it.value)
                    }
                }
        }
    }

    private fun getFamilyStatusValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.MARITAL_STATUS)
                .catch { it.printStackTrace() }
                .collect {
                    familyStatusList.clear()
                    familyNamesStatusList.clear()

                    familyStatusList.addAll(it)

                    familyStatusList.forEach {
                        familyNamesStatusList.add(it.value)
                    }
                }
        }
    }

    private fun getZodiacValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.ZODIAC)
                .catch { it.printStackTrace() }
                .collect {
                    zodiacList.clear()
                    zodiacNameList.clear()

                    zodiacList.addAll(it)

                    zodiacList.forEach {
                        zodiacNameList.add(it.value)
                    }
                }
        }
    }

    private fun setCityList() {
        doWork {
            utilUsecase.getCitiesList()
                .catch { it.printStackTrace() }
                .collect {
                    citiesList.clear()
                    citiesNamesList.clear()

                    citiesList.addAll(it)

                    citiesList.forEach {
                        citiesNamesList.add(it.name)
                    }
                }
        }
    }

    private fun getBadHabitsValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.SMOKE)
                .catch { it.printStackTrace() }
                .collect {
                    badHabitsList.clear()
                    badHabitsNameList.clear()

                    badHabitsList.addAll(it)

                    badHabitsList.forEach {
                        badHabitsNameList.add(it.value)
                    }

                    Log.i(vmTag, "badHabitsList:${badHabitsList}")
                    Log.i(vmTag, "badHabitsNameList:${badHabitsNameList}")

                }
        }
    }
}