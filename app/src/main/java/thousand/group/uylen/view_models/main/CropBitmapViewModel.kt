package thousand.group.uylen.view_models.main

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.BitmapHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.chat.ChatMessagesViewModel
import thousand.group.uylen.view_models.home.NewPairViewModel
import thousand.group.uylen.view_models.profile.EditProfileViewModel
import thousand.group.uylen.view_models.registration.ThirdRegistrationViewModel
import kotlin.reflect.KClass

class CropBitmapViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val bitmapHelper: BitmapHelper
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetBitmap = OnceLiveData<Bitmap>()
    val ldClosePage = OnceLiveData<Unit>()

    private var uri: Uri? = null
    private var bitmap: Bitmap? = null

    private var anotherVmTagName: String? = null

    private var targetVm: KClass<out BaseViewModel>? = null

    init {
        parseArgs()
        setTargetPoint()
        setParams()
    }

    fun onReadyBtnClicked(croppedBitmap: Bitmap) {
        targetVm?.apply {
            sendLocaleMessage(this, AppConstants.MessageKeysConstants.CROP_BITMAP, croppedBitmap)
            ldClosePage.call()
        }
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getParcelable<Uri>(AppConstants.BundleConstants.URI)?.apply {
                uri = this
            }
            getParcelable<Bitmap>(AppConstants.BundleConstants.BITMAP)?.apply {
                bitmap = this
            }
            getString(AppConstants.BundleConstants.VM_TAG)?.apply {
                anotherVmTagName = this

                Log.i(vmTag, "anotherVmTagName: ${anotherVmTagName}")
            }
        }
    }

    private fun setParams() {
        showProgressBar(true)

        doWork {

            uri?.apply {
                bitmapHelper.getBitmapFromUri(
                    this,
                    null,
                    null
                ) {
                    ldSetBitmap.postValue(it)
                }
            }

            bitmap?.apply {
                ldSetBitmap.postValue(this)
            }

            showProgressBar(false)
        }
    }

    private fun setTargetPoint() {
        targetVm = when (anotherVmTagName) {
            ThirdRegistrationViewModel::class.simpleName -> {
                ThirdRegistrationViewModel::class
            }
            else -> {
                EditProfileViewModel::class
            }
        }
    }

}