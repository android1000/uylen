package thousand.group.uylen.view_models.auth

import androidx.lifecycle.MutableLiveData
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.models.locale.EnterSingleInfo
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.profile.EditProfileViewModel
import thousand.group.uylen.view_models.registration.SecondRegistrationViewModel
import kotlin.reflect.KClass

class EnterSingleInfoViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetSingleInfoParams = MutableLiveData<EnterSingleInfo>()

    val ldClosePage = OnceLiveData<Unit>()
    val ldSetCurrentText = OnceLiveData<String>()

    private var anotherVmTagName: String? = null

    private var targetVm: KClass<out BaseViewModel>? = null

    private lateinit var enterInfo: EnterSingleInfo

    private var currentText: String? = null

    init {
        parseArgs()
        setInitialDatas()
    }

    fun onReadyBtnClicked(text: String) {
        targetVm?.apply {
            sendLocaleMessage(
                this,
                AppConstants.MessageKeysConstants.ENTER_VAL,
                Pair(enterInfo.type, text)
            )

            ldClosePage.call()
        }
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getString(AppConstants.BundleConstants.VM_TAG)?.apply {
                anotherVmTagName = this
            }
            getParcelable<EnterSingleInfo>(AppConstants.BundleConstants.ENTER_TYPE)?.apply {
                enterInfo = this
            }
            getString(AppConstants.BundleConstants.CURRENT_ENTER_TEXT)?.apply {
                currentText = this
            }
        }
    }

    private fun setInitialDatas() {
        targetVm = when (anotherVmTagName) {
            EditProfileViewModel::class.simpleName -> {
                EditProfileViewModel::class
            }
            else -> {
                SecondRegistrationViewModel::class
            }
        }

        ldSetSingleInfoParams.postValue(enterInfo)

        currentText?.apply {
            ldSetCurrentText.postValue(this)
        }
    }

}