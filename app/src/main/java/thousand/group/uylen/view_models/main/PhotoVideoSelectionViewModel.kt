package thousand.group.uylen.view_models.main

import android.net.Uri
import android.util.Log
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.chat.ChatMessagesViewModel
import thousand.group.uylen.view_models.home.NewPairViewModel
import thousand.group.uylen.view_models.profile.EditProfileViewModel
import thousand.group.uylen.view_models.registration.ThirdRegistrationViewModel
import java.io.File
import kotlin.reflect.KClass

class PhotoVideoSelectionViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldDismissDialog = OnceLiveData<Unit>()
    val ldOpenCameraForPhoto = OnceLiveData<Unit>()
    val ldOpenCameraForVideo = OnceLiveData<Unit>()
    val ldSetPhotoCameraOptions = OnceLiveData<File>()
    val ldOpenGallery = OnceLiveData<String>()

    private var photoUri: Uri? = null
    private var photoFile: File? = null

    private var anotherVmTagName: String? = null

    private var targetVm: KClass<out BaseViewModel>? = null

    private lateinit var selectedContentType: String

    init {
        parseParams()
        setTargetPoint()
    }

    fun onOpenGalleryBtnClicked() {
        var actionType = ""

        checkContent(
            {
                actionType = AppConstants.CameraActionConstants.imageType
            },
            {
                actionType = AppConstants.CameraActionConstants.videoType
            }
        )

        ldOpenGallery.postValue(actionType)
    }

    fun onGalleryResultSuccess(data: Uri?) {
        data?.apply {
            onLoadSuccess(this)
        }
    }

    fun onCameraResultSuccess(data: Uri?) {
        val dataUri = if (data != null) data else photoUri

        dataUri?.apply {
            onLoadSuccess(this)
        }
    }

    fun setCameraUri(uri: Uri) {
        photoUri = uri
    }

    fun onCameraBtnClicked() {
        checkContent({
            ldOpenCameraForPhoto.call()
        }, {
            ldOpenCameraForVideo.call()
        })
    }

    fun createAndOpenPhotoCamera(dirPictures: File) {
        createPhotoFile(dirPictures)?.apply {
            ldSetPhotoCameraOptions.postValue(this)
        }
    }

    private fun createPhotoFile(dirPictures: File): File? {
        photoFile = File(dirPictures.absolutePath + "/" + System.currentTimeMillis() + ".jpg")
        return photoFile
    }

    private fun onLoadSuccess(uri: Uri) {
        var key = ""

        checkContent({
            key = AppConstants.CameraActionConstants.ON_PHOTO_LOADED
        }, {
            key = AppConstants.CameraActionConstants.ON_VIDEO_LOADED
        })

        targetVm?.let {
            sendLocaleMessage(it, key, uri)
        }

        ldDismissDialog.call()
    }

    private fun parseParams() {
        paramsContainer?.apply {
            getString(AppConstants.BundleConstants.VM_TAG)?.apply {
                anotherVmTagName = this

                Log.i(vmTag, "anotherVmTagName: ${anotherVmTagName}")
            }
            getString(AppConstants.CameraActionConstants.CONTENT_TYPE)?.apply {
                selectedContentType = this

                Log.i(vmTag, "selectedContentType: ${selectedContentType}")
            }
        }
    }

    private fun checkContent(photoSelectListener: () -> Unit, videoSelectListener: () -> Unit) {
        if (selectedContentType in AppConstants.CameraActionConstants.MODE_PHOTO) {
            photoSelectListener.invoke()
        } else {
            videoSelectListener.invoke()
        }
    }

    private fun setTargetPoint() {
        targetVm = when (anotherVmTagName) {
            EditProfileViewModel::class.simpleName -> {
                EditProfileViewModel::class
            }
            ChatMessagesViewModel::class.simpleName -> {
                ChatMessagesViewModel::class
            }
            NewPairViewModel::class.simpleName -> {
                NewPairViewModel::class
            }
            else -> {
                ThirdRegistrationViewModel::class
            }
        }
    }
}