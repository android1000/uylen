package thousand.group.uylen.view_models.home

import android.net.Uri
import android.util.Log
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.MultipartBody
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.chat.ChatUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.BitmapHelper
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager

class NewPairViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val bitmapHelper: BitmapHelper,
    private val authUsecase: AuthUsecase,
    private val chatUsecase: ChatUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetInitialParams = OnceLiveData<ParamsContainer>()
    val ldOpenChatPage = OnceLiveData<Long>()
    val ldShowPhotoSelectDialog = OnceLiveData<Pair<String, String>>()

    private lateinit var user1: UserCardData
    private lateinit var user2: User

    init {
        setParcelableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.CameraActionConstants.ON_PHOTO_LOADED -> {
                    Log.i(vmTag, "ON_PHOTO_LOADED -> uri: ${message}")

                    createAndSetBitmapPart(
                        message as Uri
                    )
                }
            }
        }

        parseArgs()
        setInitialParams()
    }

    fun openPhotoVideoDialogFragment() {
        ldShowPhotoSelectDialog.postValue(
            Pair(
                vmTag,
                AppConstants.CameraActionConstants.MODE_PHOTO
            )
        )
    }

    fun onWriteMessageClicked() {
        doWork {
            chatUsecase.chatIsRead(user1.id)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    ldOpenChatPage.postValue(user1.id)
                }
        }
    }

    fun sendMessage(message: String) {
        if (message.trim().isEmpty()) {
            return
        }

        doWork {
            val receiverIdPart = RequestBodyHelper.getRequestBodyText(user1.id)
            val messagePart = RequestBodyHelper.getRequestBodyText(message.trim())

            chatUsecase.sendMessage(receiverIdPart, messagePart)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    ldOpenChatPage.postValue(user1.id)
                }
        }
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getParcelable<UserCardData>(AppConstants.BundleConstants.USER_1)?.apply {
                user1 = this
            }
            getParcelable<User>(AppConstants.BundleConstants.USER_2)?.apply {
                user2 = this
            }

            Log.i(vmTag, "parseArgs -> user1:${user1}")
            Log.i(vmTag, "parseArgs -> user2:${user2}")

        }
    }

    private fun setInitialParams() {
        val params = ParamsContainer()

        val userPhoto1 = RemoteMainConstants.SERVER_URL + user1.avatar
        val userPhoto2 = RemoteMainConstants.SERVER_URL + user2.avatar

        val matchText = String.format(
            resourceManager.getString(R.string.format_new_pair), user1.name
        )

        params.putString(AppConstants.BundleConstants.USER_PHOTO_1, userPhoto1)
        params.putString(AppConstants.BundleConstants.USER_PHOTO_2, userPhoto2)
        params.putString(AppConstants.BundleConstants.MATCH_TEXT, matchText)

        ldSetInitialParams.postValue(params)
    }

    private fun createAndSetBitmapPart(uri: Uri) {
        doWork {
            try {
                showProgressBar(true)

                bitmapHelper.getBitmapFromUri(
                    uri,
                    null,
                    null
                ) {
                    RequestBodyHelper.getMultipartData(RemoteConstants.FILE, it)?.apply {
                        sendBitmapMessage(this)
                    }

                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            } finally {
                showProgressBar(false)
            }
        }
    }

    private fun sendBitmapMessage(file: MultipartBody.Part) {
        doWork {
            val receiverIdPart = RequestBodyHelper.getRequestBodyText(user1.id)

            chatUsecase.sendMessage(receiverIdPart, file)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    ldOpenChatPage.postValue(user1.id)
                }
        }
    }

}