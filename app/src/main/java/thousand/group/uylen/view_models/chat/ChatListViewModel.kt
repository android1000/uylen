package thousand.group.uylen.view_models.chat

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonArray
import io.github.centrifugal.centrifuge.*
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.*
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.utils.GsonHelper
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.chat.ChatUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.Paginator
import thousand.group.uylen.utils.system.ResourceManager

class ChatListViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val chatUsecase: ChatUsecase,
    private val likesUsecase: LikesUsecase,
    private val utilUsecase: UtilUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetEmptyListParams = MutableLiveData<Boolean>()
    val ldShowBottomProgress = MutableLiveData<Boolean>()
    val ldSetMatchedUsersParams = MutableLiveData<ParamsContainer>()

    val ldSetChatList = OnceLiveData<MutableList<ChatListItem>>()
    val ldChatItemInserted = OnceLiveData<Pair<ChatListItem, Int>>()
    val ldChatItemRemoved = OnceLiveData<Int>()
    val ldSetChatListItem = OnceLiveData<Pair<ChatListItem, Int>>()
    val ldOpenChat = OnceLiveData<Triple<Long, ChatListItem, Int>>()
    val ldRemoveAndInsertChatItem = OnceLiveData<Triple<Int, Int, ChatListItem>>()

    private val chatsList = mutableListOf<ChatListItem>()
    private val usersList = mutableListOf<MatchedUser>()

    private val params = ParamsContainer()

    private val paginator = Paginator.Store<ChatListItem>()

    private var token = ""
    private var channelName = ""

    private var userId = authUsecase.getUserId()

    private var typeChat =
        RequestBodyHelper.getRequestBodyText(resourceManager.getString(R.string.field_chat))
    private var typeMatched =
        RequestBodyHelper.getRequestBodyText(resourceManager.getString(R.string.field_matched))

    private val eventListener = object : EventListener() {
        override fun onConnect(client: Client?, event: ConnectEvent?) {
            Log.i(vmTag, "onConnect -> event:${event}")
            super.onConnect(client, event)
        }

        override fun onDisconnect(client: Client?, event: DisconnectEvent?) {
            Log.i(vmTag, "onDisconnect -> event:${event}")
            super.onDisconnect(client, event)
        }
    }

    private val subscriptionListener = object : SubscriptionEventListener() {
        override fun onSubscribeSuccess(sub: Subscription?, event: SubscribeSuccessEvent?) {
            super.onSubscribeSuccess(sub, event)

            Log.i(vmTag, "onSubscribeSuccess -> event:${event}")
        }

        override fun onSubscribeError(sub: Subscription?, event: SubscribeErrorEvent?) {
            super.onSubscribeError(sub, event)

            Log.i(vmTag, "onSubscribeSuccess -> event:${event}")
        }

        override fun onPublish(sub: Subscription?, event: PublishEvent?) {
            super.onPublish(sub, event)

            val data = String(event!!.data, Charsets.UTF_8)

            parseSocketDataMessage(data)

            Log.i(vmTag, "subscriptionListener -> data:${data}")
        }
    }

    private var chatMessagesSubscription: Subscription? = null

    private val client = Client(
        RemoteMainConstants.SOCKET_URL,
        Options(),
        eventListener
    )

    init {
        generateSocketToken()

        checkToken(authUsecase) {
            paginator.render = {
                renderPaginationState(it)
            }

            doWork {
                paginator.sideEffects.consumeEach { effect ->
                    when (effect) {
                        is Paginator.SideEffect.LoadPage -> loadNewPage(effect.currentPage)

                        is Paginator.SideEffect.ErrorEvent -> {
                            parsePaginatorError(effect.error)
                        }
                    }
                }
            }

            refreshPage()
        }

    }

    override fun onCleared() {
        super.onCleared()
        unsubcribeChannel()
    }

    fun onItemClicked(chatItem: ChatListItem, pos: Int) {
        chatItem.receiver?.let { receiver ->
            doWork {
                val params = mutableMapOf<String, Long>()

                receiver.unread_messageIds.forEachIndexed { index, model ->
                    if (model.sender_id != userId) {
                        val key =
                            String.format(
                                resourceManager.getString(R.string.format_messages_id),
                                index
                            )

                        params.put(key, model.id)
                    }
                }

                chatUsecase.chatIsRead(receiver.id, params)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                        readNotification(typeChat)

                        chatItem.receiver?.unread_message = 0
                        chatItem.receiver?.unread_messageIds?.clear()

                        ldOpenChat.postValue(Triple(receiver.id, chatItem, pos))
                    }
            }
        }
    }

    fun onBottomReached() {
        loadNextPage()
    }

    fun onMatchedClicked() {
        readNotification(typeMatched)
    }

    private fun readNotification(type: RequestBody) {
        doWork {
            utilUsecase.updateNotification(type)
                .catch { it.printStackTrace() }
                .collect()
        }
    }

    fun getMatchedUsers() {
        doWork {
            likesUsecase.getMatchedUsers(1)
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    usersList.clear()
                    usersList.addAll(it.first)

                    parseMatchedUsers(it.second)
                }
        }
    }

    private fun parseMatchedUsers(total:Int) {
        doWork {
            when {
                total == 1 -> {
                    setMatchedUsersData(
                        true, false, false, false, false, false, total
                    )
                }
                total== 2 -> {
                    setMatchedUsersData(
                        true, true, false, false, false, false, total
                    )
                }
                total == 3 -> {
                    setMatchedUsersData(
                        true, true, true, false, false, false, total
                    )
                }
                total == 4 -> {
                    setMatchedUsersData(
                        true, true, true, false, false, false, total
                    )
                }
                usersList.size == 5 -> {
                    setMatchedUsersData(
                        true, true, true, true, false, false, total
                    )
                }
                total == 5 -> {
                    setMatchedUsersData(
                        true, true, true, true, true, false, total
                    )
                }
                total > 5 -> {
                    setMatchedUsersData(
                        true, true, true, true, true, true, total
                    )
                }
                else -> {
                    setMatchedUsersData(
                        false, false, false, false, false, false, total
                    )
                }
            }
        }
    }

    private fun setMatchedUsersData(
        ivVisible1: Boolean,
        ivVisible2: Boolean,
        ivVisible3: Boolean,
        ivVisible4: Boolean,
        ivVisible5: Boolean,
        sizeVisible: Boolean,
        size: Int
    ) {
        val text = when {
            size == 1 -> {
                String.format(
                    resourceManager.getString(R.string.format_users_count_1),
                    usersList.get(0).name
                )
            }
            size > 1 -> {
                String.format(
                    resourceManager.getString(R.string.format_users_count_3),
                    usersList.get(0).name,
                    size - 1
                )
            }
            else -> {
                resourceManager.getString(R.string.label_no_matched_users)
            }
        }

        params.clear()

        if (ivVisible1) {
            params.putString(
                AppConstants.BundleConstants.IV_LINK_1,
                parseMatcherUserAvatar(0)
            )
        }
        if (ivVisible2) {
            params.putString(
                AppConstants.BundleConstants.IV_LINK_2,
                parseMatcherUserAvatar(1)
            )
        }
        if (ivVisible3) {
            params.putString(
                AppConstants.BundleConstants.IV_LINK_3,
                parseMatcherUserAvatar(2)
            )
        }
        if (ivVisible4) {
            params.putString(
                AppConstants.BundleConstants.IV_LINK_4,
                parseMatcherUserAvatar(3)
            )
        }
        if (ivVisible5) {
            params.putString(
                AppConstants.BundleConstants.IV_LINK_5,
                parseMatcherUserAvatar(4)
            )
        }

        params.putBoolean(AppConstants.BundleConstants.IV_VISIBILITY_1, ivVisible1)
        params.putBoolean(AppConstants.BundleConstants.IV_VISIBILITY_2, ivVisible2)
        params.putBoolean(AppConstants.BundleConstants.IV_VISIBILITY_3, ivVisible3)
        params.putBoolean(AppConstants.BundleConstants.IV_VISIBILITY_4, ivVisible4)
        params.putBoolean(AppConstants.BundleConstants.IV_VISIBILITY_5, ivVisible5)
        params.putBoolean(AppConstants.BundleConstants.SIZE_VISIBILITY, sizeVisible)
        params.putString(AppConstants.BundleConstants.MATCHED_USERS_COUNT_TEXT, text)
        params.putString(AppConstants.BundleConstants.SIZE_TEXT, "+${size - 5}")

        ldSetMatchedUsersParams.postValue(params)
    }

    private fun parseMatcherUserAvatar(index: Int) =
        RemoteMainConstants.SERVER_URL + usersList.getOrNull(index)?.images?.getOrNull(0)?.images

    private fun readNotification() {

    }

    private fun refreshPage() = paginator.proceed(Paginator.Action.Refresh)

    private fun loadNextPage() = paginator.proceed(Paginator.Action.LoadMore)

    private suspend fun loadNewPage(page: Int) {
        doWork {
            chatUsecase.getChatList(page)
                .catch { e ->
                    paginator.proceed(Paginator.Action.PageError(e))
                }
                .collect {
                    paginator.proceed(Paginator.Action.NewPage(page, it))
                }
        }
    }

    private fun renderPaginationState(state: Paginator.State) {
        when (state) {
            is Paginator.State.Empty -> {
                Log.i(vmTag, "renderPaginationState -> state -> Empty")

                ldSetEmptyListParams.postValue(true)
                ldShowBottomProgress.postValue(false)

                showProgressBar(false)
            }
            is Paginator.State.EmptyProgress -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyProgress")

                ldSetEmptyListParams.postValue(false)
                showProgressBar(true)
            }
            is Paginator.State.EmptyError -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyError")

                parsePaginatorError(state.error)
            }
            is Paginator.State.Data<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Data")

                setDataItemsOnRenderView(state)
            }
            is Paginator.State.Refresh<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Refresh")
            }
            is Paginator.State.NewPageProgress<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> NewPageProgress")

                ldShowBottomProgress.postValue(true)
            }
            is Paginator.State.FullData<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> FullData")

                setDataItemsOnRenderView(state)
            }
        }
    }

    private fun parsePaginatorError(error: Throwable?) {
        error?.apply {
            showMessageError(
                ResErrorHelper.showThrowableMessage(
                    resourceManager,
                    vmTag,
                    error
                )
            )

            ldShowBottomProgress.postValue(false)
            ldSetEmptyListParams.postValue(true)
            showProgressBar(false)
        }
    }

    private fun setDataItemsOnRenderView(
        state: Paginator.State
    ) {
        Log.i(vmTag, "renderPaginationState -> state -> setDataItemsOnRenderView")

        val receivedList = when (state) {
            is Paginator.State.Data<*> -> state.data
            is Paginator.State.FullData<*> -> state.data
            else -> null
        }

        receivedList?.let { list ->

            chatsList.clear()
            chatsList.addAll(list as MutableList<ChatListItem>)

            ldSetChatList.postValue(chatsList)

            ldSetEmptyListParams.postValue(chatsList.isEmpty())
        }

        ldShowBottomProgress.postValue(false)
        showProgressBar(false)
    }

    private fun generateSocketToken() {
        doWork {
            chatUsecase.generateSocketToken()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    token = it
                    doSubscription()
                }
        }
    }

    private fun doSubscription() {
        try {
            client.setToken(token)

            channelName = String.format(
                RemoteMainConstants.CHAT_LIST_CHANNEL,
                authUsecase.getUserId()
            )

            chatMessagesSubscription = client.newSubscription(channelName, subscriptionListener)

            Log.i(
                vmTag,
                "doSubscription -> chatMessagesSubscription -> channel: ${chatMessagesSubscription?.channel}"
            )
            Log.i(vmTag, "doSubscription -> chatMessagesSubscription: ${chatMessagesSubscription}")

            chatMessagesSubscription?.subscribe()

            client.connect()

        } catch (ex: DuplicateSubscriptionException) {
            ex.printStackTrace()
        }
    }

    private fun unsubcribeChannel() {
        chatMessagesSubscription?.unsubscribe()
        chatMessagesSubscription?.apply {
            client.removeSubscription(this)
            client.disconnect()
        }
    }

    private fun parseSocketDataMessage(json: String) {
        doWork {
            try {
                val data = Gson().fromJson(json, JsonArray::class.java)
                val chatListItem = GsonHelper.getListFromJsonArray(
                    data,
                    ChatListItem::class.java
                ).get(0)

                var foundPos = -1
                var isMyMessage = false

                chatsList.forEachIndexed { index, item ->
                    if (item.receiver?.id == chatListItem.receiver?.id) {
                        foundPos = index
                        isMyMessage = chatListItem.last_message.sender != null
                        return@forEachIndexed
                    }
                }

                if (foundPos != -1 && chatsList.isNotEmpty()) {
                    chatsList.removeAt(foundPos)
                    chatsList.add(0, chatListItem)

                    ldSetChatList.postValue(chatsList)
                } else {
                    chatsList.add(0, chatListItem)

                    ldChatItemInserted.postValue(Pair(chatListItem, 0))
                }

                ldSetEmptyListParams.postValue(chatsList.isEmpty())
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }
}