package thousand.group.uylen.view_models.auth

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.profile.EditProfileViewModel
import thousand.group.uylen.view_models.registration.SecondRegistrationViewModel
import thousand.group.uylen.view_models.search.SearchViewModel
import kotlin.reflect.KClass

class InterestsViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val utilUsecase: UtilUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetEmptyList = MutableLiveData<Boolean>()

    val ldSetInterestList = OnceLiveData<MutableList<Interest>>()
    val ldSetItem = OnceLiveData<Pair<Interest, Int>>()

    private val interestList = mutableListOf<Interest>()
    private var argsInterestList = mutableListOf<Interest>()

    private var anotherVmTagName: String? = null

    private var targetVm: KClass<out BaseViewModel>? = null

    private var selectedItemsCount = 0

    init {
        parseArgs()
        setInitialDatas()

        createInterestList()

    }

    override fun onCleared() {
        targetVm?.apply {

            if (selectedItemsCount != 0) {
                sendLocaleMessage(
                    this,
                    AppConstants.MessageKeysConstants.SET_INTEREST_TEXT,
                    createSetText()
                )

                sendLocaleMessage(
                    this,
                    AppConstants.MessageKeysConstants.SET_INTEREST_LIST,
                    createSetList()
                )
            } else {
                sendLocaleCallback(
                    this,
                    AppConstants.MessageKeysConstants.CLEAR_INTERESTS
                )
            }
        }

        super.onCleared()
    }

    fun onItemClicked(model: Interest, pos: Int) {

        if (!model.isSelected && selectedItemsCount == 5) return

        interestList.get(pos).isSelected = !model.isSelected

        if (interestList.get(pos).isSelected) {
            selectedItemsCount += 1
        } else {
            selectedItemsCount -= 1
        }

        ldSetItem.postValue(Pair(interestList.get(pos), pos))
    }

    private fun createInterestList() {
        doWork {
            utilUsecase.getInterestsList()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.showThrowableMessage(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    interestList.clear()
                    interestList.addAll(it)

                    selectedItemsCount = argsInterestList.size

                    interestList.forEachIndexed { index, interest ->
                        argsInterestList.forEach {
                            if (interest.compareTo(it) == 0) {
                                interestList.set(index, it)
                                return@forEach
                            }
                        }
                    }


                    ldSetInterestList.postValue(interestList)
                    ldSetEmptyList.postValue(interestList.isEmpty())
                }
        }
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getString(AppConstants.BundleConstants.VM_TAG)?.apply {
                anotherVmTagName = this
            }
            getList<Interest>(AppConstants.BundleConstants.LIST)?.apply {
                argsInterestList.addAll(this)
            }
        }
    }

    private fun setInitialDatas() {
        targetVm = when (anotherVmTagName) {
            EditProfileViewModel::class.simpleName -> {
                EditProfileViewModel::class
            }
            SearchViewModel::class.simpleName -> {
                SearchViewModel::class
            }
            else -> {
                SecondRegistrationViewModel::class
            }
        }
    }

    private fun createSetText(): String {
        val sb = StringBuilder()

        interestList.forEachIndexed { index, interest ->
            if (interest.isSelected) {
                if (sb.isEmpty()) {
                    sb.append(interest.title)
                } else {
                    sb.append(", ${interest.title}")
                }
            }
        }

        return sb.toString()

    }

    private fun createSetList(): MutableList<Interest> {
        val list = mutableListOf<Interest>()

        interestList.forEachIndexed { index, interest ->
            if (interest.isSelected) {
                list.add(interest)
            }
        }

        return list

    }

}