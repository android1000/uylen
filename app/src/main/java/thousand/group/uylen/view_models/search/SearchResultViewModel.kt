package thousand.group.uylen.view_models.search

import android.app.appsearch.SearchResult
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.SearchUser
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.search_users.SearchUsersUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.helpers.BitmapHelper
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.Paginator
import thousand.group.uylen.utils.system.ResourceManager
import java.io.ByteArrayOutputStream

class SearchResultViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val utilUsecase: UtilUsecase,
    private val searchUsersUsecase: SearchUsersUsecase,
    private val bitmapHelper: BitmapHelper
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetListEmptyParams = MutableLiveData<Boolean>()
    val ldShowBottomProgress = MutableLiveData<Boolean>()

    val ldSetList = OnceLiveData<MutableList<SearchUser>>()
    val ldOpenDetailPage = OnceLiveData<Pair<Long, Boolean>>()

    private val paginator = Paginator.Store<SearchUser>()

    private val byteArrayOutputStream = ByteArrayOutputStream()

    private val searchList = mutableListOf<SearchUser>()

    private val params = mutableMapOf<String, RequestBody>()

    init {
        createParams()

        checkToken(authUsecase) {
            paginator.render = {
                renderPaginationState(it)
            }

            doWork {
                paginator.sideEffects.consumeEach { effect ->
                    when (effect) {
                        is Paginator.SideEffect.LoadPage -> loadNewPage(effect.currentPage)

                        is Paginator.SideEffect.ErrorEvent -> {
                            parsePaginatorError(effect.error)
                        }
                    }
                }
            }

            refreshPage()
        }
    }

    fun onBottomReached() {
        loadNextPage()
    }

    fun onItemClicked(model: SearchUser, pos: Int) {
        doWork {
            searchUsersUsecase.sendRecentlySearch(RequestBodyHelper.getRequestBodyText(model.id))
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    ldOpenDetailPage.postValue(Pair(model.id, model.chat))
                }
        }
    }

    private fun refreshPage() = paginator.proceed(Paginator.Action.Refresh)

    private fun loadNextPage() = paginator.proceed(Paginator.Action.LoadMore)

    private suspend fun loadNewPage(page: Int) {
        doWork {
            searchUsersUsecase.searchUsers(RequestBodyHelper.getRequestBodyText(page), params)
                .catch { e ->
                    paginator.proceed(Paginator.Action.PageError(e))
                }
                .collect {
                    paginator.proceed(Paginator.Action.NewPage(page, it))
                }
        }
    }

    private fun renderPaginationState(state: Paginator.State) {
        when (state) {
            is Paginator.State.Empty -> {
                Log.i(vmTag, "renderPaginationState -> state -> Empty")

                ldSetListEmptyParams.postValue(true)
                ldShowBottomProgress.postValue(false)

/*                sendLocaleMessage(
                    HomeViewModel::class,
                    AppConstants.MessageKeysConstants.SWIPE_REFRESH_MESSAGE, false
                )*/
                showProgressBar(false)
            }
            is Paginator.State.EmptyProgress -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyProgress")

                ldSetListEmptyParams.postValue(false)
                showProgressBar(true)
            }
            is Paginator.State.EmptyError -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyError")

                parsePaginatorError(state.error)
            }
            is Paginator.State.Data<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Data")

                setDataItemsOnRenderView(state)
            }
            is Paginator.State.Refresh<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Refresh")
/*
                sendLocaleMessage(
                    HomeViewModel::class,
                    AppConstants.MessageKeysConstants.SWIPE_REFRESH_MESSAGE, true
                )*/
            }
            is Paginator.State.NewPageProgress<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> NewPageProgress")

                ldShowBottomProgress.postValue(true)
            }
            is Paginator.State.FullData<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> FullData")

                setDataItemsOnRenderView(state)
            }
        }
    }

    private fun parsePaginatorError(error: Throwable?) {
        error?.apply {
            showMessageError(
                ResErrorHelper.showThrowableMessage(
                    resourceManager,
                    vmTag,
                    error
                )
            )

            ldShowBottomProgress.postValue(false)
            ldSetListEmptyParams.postValue(true)
            showProgressBar(false)

/*            sendLocaleMessage(
                HomeViewModel::class,
                AppConstants.MessageKeysConstants.SWIPE_REFRESH_MESSAGE,
                false
            )*/
        }
    }

    private fun setDataItemsOnRenderView(
        state: Paginator.State
    ) {
        Log.i(vmTag, "renderPaginationState -> state -> setDataItemsOnRenderView")

        val receivedList = when (state) {
            is Paginator.State.Data<*> -> state.data
            is Paginator.State.FullData<*> -> state.data
            else -> null
        }

        receivedList?.let { list ->

            searchList.clear()
            searchList.addAll(list as MutableList<SearchUser>)

/*            searchList.forEach { user ->
                bitmapHelper.getBitmapSync(
                    Uri.parse(
                        RemoteMainConstants.SERVER_URL + user.images.getOrNull(
                            0
                        )?.images
                    )
                ) {
                    it.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream)

                    user.avatarBitmap = it
                }
            }*/

            ldSetList.postValue(searchList)

            ldSetListEmptyParams.postValue(searchList.isEmpty())
        }

        ldShowBottomProgress.postValue(false)
        showProgressBar(false)
    }


    private fun createParams() {
        paramsContainer?.apply {
            params.putAll(RequestBodyHelper.getPartMap(this.getParams()))
        }
    }
}