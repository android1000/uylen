package thousand.group.uylen.view_models.profile

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.Tariff
import thousand.group.data.entities.remote.simple.TariffBanner
import thousand.group.data.entities.remote.simple.TariffPrice
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.locale.EnterSingleInfo
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.favourite.FavouriteViewModel
import thousand.group.uylen.view_models.registration.SecondRegistrationViewModel
import kotlin.reflect.KClass

class TarifsViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val likesUsecase: LikesUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetTariffsEmpty = MutableLiveData<Boolean>()
    val ldSetTarifDesc = MutableLiveData<Pair<String, String>>()

    val ldSetTariffPrices = OnceLiveData<MutableList<TariffPrice>>()
    val ldSetBannerList = OnceLiveData<MutableList<TariffBanner>>()
    val ldSetTariffPriceItem = OnceLiveData<Pair<TariffPrice, Int>>()
    val ldClosePage = OnceLiveData<Unit>()

    private var tariff: Tariff? = null
    private var tarifSelectedPosition = 0
    private var bannerSelectedPosition = 0

    private var anotherVmTagName: String? = null

    private var targetVm: KClass<out BaseViewModel>? = null

    init {
        parseArgs()

        targetVm = when (anotherVmTagName) {
            ProfileViewModel::class.simpleName -> {
                ProfileViewModel::class
            }
            else -> {
                FavouriteViewModel::class
            }
        }

        getTariff()
    }

    fun onViewPagerPositionChanged(pos: Int) {
        tariff?.apply {
            if(banner.isEmpty()) return@apply

            bannerSelectedPosition = pos

            val banner = this.banner.get(pos)

            ldSetTarifDesc.postValue(Pair(banner.title, banner.description))
        }
    }

    fun onPriceItemClicked(model: TariffPrice, pos: Int) {
        tariff?.apply {
            price.get(tarifSelectedPosition).isSelected = false

            ldSetTariffPriceItem.postValue(
                Pair(
                    price.get(tarifSelectedPosition),
                    tarifSelectedPosition
                )
            )

            doWorkAfterSomeTime(150) {
                tarifSelectedPosition = pos

                price.get(tarifSelectedPosition).isSelected = true

                ldSetTariffPriceItem.postValue(
                    Pair(
                        price.get(tarifSelectedPosition),
                        tarifSelectedPosition
                    )
                )
            }


        }
    }

    fun onBuyTarifClicked() {
        tariff?.let {
            doWork {
                val selPrice = it.price.get(tarifSelectedPosition)

                val activatedPart = RequestBodyHelper.getRequestBodyText(1)
                val endDatePart =
                    RequestBodyHelper.getRequestBodyText(selPrice.date)
                val priceIdPart = RequestBodyHelper.getRequestBodyText(selPrice.id)

                likesUsecase.buyTarif(it.id, activatedPart, endDatePart, priceIdPart)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                        showMessageSuccess(R.string.success_tarif_is_bought)

                        doWorkAfterSomeTime(1000) {
                            targetVm?.apply {

                                val updateTag = if (targetVm == ProfileViewModel::class) {
                                    AppConstants.MessageKeysConstants.UPDATE_PROFILE
                                } else {
                                    AppConstants.MessageKeysConstants.UPDATE_FAVOURITE
                                }

                                sendLocaleCallback(
                                    this,
                                    updateTag,
                                )

                                ldClosePage.call()
                            }
                        }
                    }
            }
        }
    }

    private fun getTariff() {
        doWork {
            likesUsecase.getTariff()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    tariff = it
                    parseTariff()
                }
        }
    }

    private fun parseTariff() {
        tariff?.apply {
            price.getOrNull(0)?.isSelected = true

            onViewPagerPositionChanged(0)

            ldSetTariffPrices.postValue(price)
            ldSetBannerList.postValue(banner)
            ldSetTariffsEmpty.postValue(price.isEmpty())
        }
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getString(AppConstants.BundleConstants.VM_TAG)?.apply {
                anotherVmTagName = this
            }
        }
    }

}