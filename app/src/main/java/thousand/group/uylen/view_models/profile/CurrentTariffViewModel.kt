package thousand.group.uylen.view_models.profile

import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import thousand.group.data.entities.remote.simple.MyTariff
import thousand.group.data.entities.remote.simple.Tariff
import thousand.group.data.entities.remote.simple.TariffPrice
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import timber.log.Timber

class CurrentTariffViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val profileUsecase: ProfileUsecase,
    private val likesUsecase: LikesUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetTarifParams = MutableLiveData<ParamsContainer>()
    val ldSetPeriodsAdapter = MutableLiveData<Pair<Long, String>>()

    val ldSetPeriodsList = OnceLiveData<MutableList<TariffPrice>>()
    val ldClosePage = OnceLiveData<Unit>()

    private var tariff: MyTariff? = null

    init {
        getMyTariff()
    }

    fun onCancelTariffClicked() {
        tariff?.tariff?.let {
            doWork {
                likesUsecase.cancelMyTariff(it.id)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                        showMessageSuccess(R.string.success_tariff_is_canceled)

                        doWorkAfterSomeTime(1000L) {
                            sendLocaleCallback(
                                ProfileViewModel::class,
                                AppConstants.MessageKeysConstants.UPDATE_PROFILE
                            )

                            ldClosePage.call()
                        }
                    }
            }
        }
    }

    private fun getMyTariff() {
        doWork {
            likesUsecase
                .getMyTariff()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch { it.printStackTrace() }
                .collect {
                    Log.i(vmTag, "getMyTariff -> tariff:${it}")

                    tariff = it

                    parseTariff()
                }
        }
    }

    private fun parseTariff() {
        tariff?.apply {
            val params = ParamsContainer()
            val photo = RemoteMainConstants.SERVER_URL + this.tariff?.banner?.getOrNull(0)?.image
            val selectedPriceID = if(this.price != null) this.price!!
                .id else 0

            params.putString(AppConstants.BundleConstants.NAME, this.tariff?.name.toString())
            params.putString(AppConstants.BundleConstants.PHOTO, photo)


            ldSetPeriodsAdapter.postValue(Pair(selectedPriceID, end_date))
            ldSetTarifParams.postValue(params)

            this.tariff?.price?.apply {
                ldSetPeriodsList.postValue(this)
            }

        }
    }
}