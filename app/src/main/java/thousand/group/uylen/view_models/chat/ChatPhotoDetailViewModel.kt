package thousand.group.uylen.view_models.chat

import androidx.lifecycle.MutableLiveData
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.chat.ChatUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.helpers.BitmapHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.ResourceManager

class ChatPhotoDetailViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val chatUsecase: ChatUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetImage = MutableLiveData<String>()

    private var address = ""

    init {
        parseArgs()

        ldSetImage.postValue(address)
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getString(AppConstants.BundleConstants.ADDRESS_IMAGE)?.apply {
                address = this
            }
        }
    }

}