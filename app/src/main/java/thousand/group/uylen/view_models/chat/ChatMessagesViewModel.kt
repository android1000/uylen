package thousand.group.uylen.view_models.chat

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import io.github.centrifugal.centrifuge.*
import io.github.centrifugal.centrifuge.EventListener
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.MultipartBody
import thousand.group.data.entities.remote.simple.ChatLocaleDateMessage
import thousand.group.data.entities.remote.simple.ChatMessage
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.chat.ChatUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.*
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.Paginator
import thousand.group.uylen.utils.system.ResourceManager
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.text.Charsets.UTF_8


class ChatMessagesViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val bitmapHelper: BitmapHelper,
    private val authUsecase: AuthUsecase,
    private val chatUsecase: ChatUsecase,
    private val profileUsecase: ProfileUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetEmptyListParams = MutableLiveData<Boolean>()
    val ldShowTopProgress = MutableLiveData<Boolean>()
    val ldSetUsersPair = MutableLiveData<ParamsContainer>()
    val ldSetAdapter = MutableLiveData<Pair<Long, Long>>()
    val ldSetBottomEnterVisibility = MutableLiveData<Boolean>()

    val ldClearMessage = OnceLiveData<Unit>()
    val ldSetChatList = OnceLiveData<MutableList<ChatLocaleDateMessage>>()
    val ldOpenDetailPage = OnceLiveData<Pair<Long, Int>>()
    val ldScrollDown = OnceLiveData<Unit>()
    val ldShowPhotoSelectDialog = OnceLiveData<Pair<String, String>>()
    val ldUpdateMessageItem = OnceLiveData<Pair<ChatLocaleDateMessage, Int>>()

    private var receiverId = 0L
    private var senderId = authUsecase.getUserId()

    private lateinit var receiverModel: UserCardData
    private var senderUserModel = authUsecase.getUserModel()

    private val messageList = mutableListOf<ChatLocaleDateMessage>()
    private val newMessagesList = mutableListOf<ChatLocaleDateMessage>()

    private val paginator = Paginator.Store<ChatLocaleDateMessage>()

    private var token = ""
    private var channelName = ""
    private var senderChannelName = ""
    private var readMessageChannelName = RemoteMainConstants.READ_MESSAGE_CHANNEL
    private var readMessageUserIdChannelName = ""

    private var messageIdKey =
        String.format(resourceManager.getString(R.string.format_messages_id), 0)

    private var isInternetActive = true

    private val eventListener = object : EventListener() {
        override fun onConnect(client: Client?, event: ConnectEvent?) {
            Log.i(vmTag, "onConnect -> event:${event}")
            super.onConnect(client, event)
        }

        override fun onDisconnect(client: Client?, event: DisconnectEvent?) {
            Log.i(vmTag, "onDisconnect -> event:${event}")
            super.onDisconnect(client, event)
        }
    }

    private val subscriptionListener = object : SubscriptionEventListener() {
        override fun onSubscribeSuccess(sub: Subscription?, event: SubscribeSuccessEvent?) {
            super.onSubscribeSuccess(sub, event)

            Log.i(vmTag, "onSubscribeSuccess -> event:${event}")
        }

        override fun onSubscribeError(sub: Subscription?, event: SubscribeErrorEvent?) {
            super.onSubscribeError(sub, event)

            Log.i(vmTag, "onSubscribeSuccess -> event:${event}")
        }

        override fun onPublish(sub: Subscription?, event: PublishEvent?) {
            super.onPublish(sub, event)

            val data = String(event!!.data, UTF_8)

            Log.i(vmTag, "sub channel: ${sub?.channel}")

            parseSocketDataMessage(data) {
                setMessageIsRead(it)
            }

            Log.i(vmTag, "subscriptionListener -> data:${data}")
        }
    }

    private val subscriptionListenerUserSelf = object : SubscriptionEventListener() {
        override fun onSubscribeSuccess(sub: Subscription?, event: SubscribeSuccessEvent?) {
            super.onSubscribeSuccess(sub, event)

            Log.i(vmTag, "onSubscribeSuccess -> event:${event}")
        }

        override fun onSubscribeError(sub: Subscription?, event: SubscribeErrorEvent?) {
            super.onSubscribeError(sub, event)

            Log.i(vmTag, "onSubscribeSuccess -> event:${event}")
        }

        override fun onPublish(sub: Subscription?, event: PublishEvent?) {
            super.onPublish(sub, event)

            val data = String(event!!.data, UTF_8)

            Log.i(vmTag, "sub channel: ${sub?.channel}")

            parseSocketDataMessage(data) {
            }

            Log.i(vmTag, "subscriptionListener -> data:${data}")
        }
    }

    private val subListenerReadMessage = object : SubscriptionEventListener() {
        override fun onSubscribeSuccess(sub: Subscription?, event: SubscribeSuccessEvent?) {
            super.onSubscribeSuccess(sub, event)

            Log.i(vmTag, "readMessageSubListener -> onSubscribeSuccess -> event:${event}")
        }

        override fun onSubscribeError(sub: Subscription?, event: SubscribeErrorEvent?) {
            super.onSubscribeError(sub, event)

            Log.i(vmTag, "readMessageSubListener -> onSubscribeSuccess -> event:${event}")
        }

        override fun onPublish(sub: Subscription?, event: PublishEvent?) {
            super.onPublish(sub, event)

            val data = String(event!!.data, UTF_8)

            Log.i(vmTag, "sub channel: ${sub?.channel}")

            Log.i(vmTag, "readMessageSubListener -> onPublish -> data:${data}")
        }
    }

    private val subListenerReadMessageUserId = object : SubscriptionEventListener() {
        override fun onSubscribeSuccess(sub: Subscription?, event: SubscribeSuccessEvent?) {
            super.onSubscribeSuccess(sub, event)

            Log.i(vmTag, "readMessageUserIdSubListener -> onSubscribeSuccess -> event:${event}")
        }

        override fun onSubscribeError(sub: Subscription?, event: SubscribeErrorEvent?) {
            super.onSubscribeError(sub, event)

            Log.i(vmTag, "readMessageUserIdSubListener -> onSubscribeSuccess -> event:${event}")
        }

        override fun onPublish(sub: Subscription?, event: PublishEvent?) {
            super.onPublish(sub, event)

            val data = String(event!!.data, UTF_8)

            Log.i(vmTag, "sub channel: ${sub?.channel}")

            Log.i(vmTag, "readMessageUserIdSubListener -> onPublish -> data:${data}")

            parseReadUserMessage(data)
        }
    }

    private var chatMessagesSubscription: Subscription? = null
    private var chatSenderSubscription: Subscription? = null
    private var chatReadMessageSub: Subscription? = null
    private var chatReadMessageUserIdSub: Subscription? = null

    private val client = Client(
        RemoteMainConstants.SOCKET_URL,
        Options(),
        eventListener
    )

    init {
        setParcelableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.CameraActionConstants.ON_PHOTO_LOADED -> {
                    Log.i(vmTag, "ON_PHOTO_LOADED -> uri: ${message}")

                    createAndSetBitmapPart(
                        message as Uri
                    )
                }
            }
        }

        parseArgs()

        ldSetAdapter.postValue(Pair(senderId, receiverId))

        generateSocketToken()
        getUsersPairParams()

        checkToken(authUsecase) {
            paginator.render = {
                renderPaginationState(it)
            }

            doWork {
                paginator.sideEffects.consumeEach { effect ->
                    when (effect) {
                        is Paginator.SideEffect.LoadPage -> loadNewPage(effect.currentPage)

                        is Paginator.SideEffect.ErrorEvent -> {
                            parsePaginatorError(effect.error)
                        }
                    }
                }
            }

            refreshPage()
        }

    }

    override fun onCleared() {
        unsubcribeChannel()

        super.onCleared()
    }

    fun openPhotoVideoDialogFragment() {
        ldShowPhotoSelectDialog.postValue(
            Pair(
                vmTag,
                AppConstants.CameraActionConstants.MODE_PHOTO
            )
        )
    }

    fun sendMessage(message: String) {
        if (message.trim().isEmpty() || !isInternetActive) {
            return
        }

        doWork {
            ldClearMessage.call()

            val receiverIdPart = RequestBodyHelper.getRequestBodyText(receiverId)
            val messagePart = RequestBodyHelper.getRequestBodyText(message.trim())

            chatUsecase.sendMessage(receiverIdPart, messagePart)
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect()
        }
    }

    fun onTopReached() {
        loadNextPage()
    }

    fun onReceiverAvatarClicked() {
        ldOpenDetailPage.postValue(Pair(receiverModel.id, 0))
    }

    fun onInternetSuccess() {
        this.isInternetActive = true
    }

    fun onInternetError() {
        this.isInternetActive = false
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getLong(AppConstants.BundleConstants.RECEIVER_ID)?.apply {
                receiverId = this
            }
        }
    }

    private fun createAndSetBitmapPart(uri: Uri) {
        doWork {
            try {
                showProgressBar(true)

                bitmapHelper.getBitmapFromUri(
                    uri,
                    null,
                    null
                ) {
                    RequestBodyHelper.getMultipartData(RemoteConstants.FILE, it)?.apply {
                        sendBitmapMessage(this)
                    }

                }

            } catch (ex: Exception) {
                ex.printStackTrace()
            } finally {
                showProgressBar(false)
            }
        }
    }

    private fun sendBitmapMessage(file: MultipartBody.Part) {
        doWork {
            val receiverIdPart = RequestBodyHelper.getRequestBodyText(receiverId)

            chatUsecase.sendMessage(receiverIdPart, file)
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect()
        }
    }

    private fun refreshPage() = paginator.proceed(Paginator.Action.Refresh)

    private fun loadNextPage() = paginator.proceed(Paginator.Action.LoadMore)

    private suspend fun loadNewPage(page: Int) {
        doWork {
            val pagePart = RequestBodyHelper.getRequestBodyText(page)
            val receiverIdPart = RequestBodyHelper.getRequestBodyText(receiverId)

            chatUsecase.getChatMessages(pagePart, receiverIdPart)
                .catch { e ->
                    paginator.proceed(Paginator.Action.PageError(e))
                }
                .collect {
                    paginator.proceed(Paginator.Action.NewPage(page, it))
                }
        }
    }

    private fun renderPaginationState(state: Paginator.State) {
        when (state) {
            is Paginator.State.Empty -> {
                Log.i(vmTag, "renderPaginationState -> state -> Empty")

                ldSetEmptyListParams.postValue(true)
                ldShowTopProgress.postValue(false)

                showProgressBar(false)
            }
            is Paginator.State.EmptyProgress -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyProgress")

                ldSetEmptyListParams.postValue(false)
                showProgressBar(true)
            }
            is Paginator.State.EmptyError -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyError")

                parsePaginatorError(state.error)
            }
            is Paginator.State.Data<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Data")

                setDataItemsOnRenderView(state)
            }
            is Paginator.State.Refresh<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Refresh")
            }
            is Paginator.State.NewPageProgress<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> NewPageProgress")

                ldShowTopProgress.postValue(true)
            }
            is Paginator.State.FullData<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> FullData")

                setDataItemsOnRenderView(state)
            }
        }
    }

    private fun parsePaginatorError(error: Throwable?) {
        error?.apply {
            showMessageError(
                ResErrorHelper.showThrowableMessage(
                    resourceManager,
                    vmTag,
                    error
                )
            )

            ldShowTopProgress.postValue(false)
            ldSetEmptyListParams.postValue(true)
            showProgressBar(false)
        }
    }

    private fun setDataItemsOnRenderView(
        state: Paginator.State
    ) {
        Log.i(vmTag, "renderPaginationState -> state -> setDataItemsOnRenderView")

        val receivedList = when (state) {
            is Paginator.State.Data<*> -> state.data
            is Paginator.State.FullData<*> -> state.data
            else -> null
        }

        val page = when (state) {
            is Paginator.State.Data<*> -> state.pageCount
            else -> null
        }

        receivedList?.let { list ->

            messageList.clear()

            messageList.addAll(newMessagesList)
            messageList.addAll(list as MutableList<ChatLocaleDateMessage>)

            ldSetChatList.postValue(messageList)

            ldSetEmptyListParams.postValue(messageList.isEmpty())

            if (page == 1) {
                ldScrollDown.call()
            }
        }

        ldShowTopProgress.postValue(false)
        showProgressBar(false)
    }

    private fun generateSocketToken() {
        doWork {
            chatUsecase.generateSocketToken()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    token = it
                    doSubscription()
                }
        }
    }

    private fun doSubscription() {
        try {
            client.setToken(token)

            channelName = String.format(
                RemoteMainConstants.CHAT_MESSAGES_CHANNEL,
                receiverId,
                authUsecase.getUserId()
            )

            senderChannelName = String.format(
                RemoteMainConstants.CHAT_SENDER_CHANNEL,
                senderId
            )

            readMessageUserIdChannelName = String.format(
                RemoteMainConstants.READ_MESSAGE_USER_ID_CHANNEL,
                authUsecase.getUserId()
            )

            chatMessagesSubscription = client.newSubscription(channelName, subscriptionListener)
            chatSenderSubscription =
                client.newSubscription(senderChannelName, subscriptionListenerUserSelf)
            chatReadMessageSub =
                client.newSubscription(readMessageChannelName, subListenerReadMessage)
            chatReadMessageUserIdSub =
                client.newSubscription(readMessageUserIdChannelName, subListenerReadMessageUserId)

            Log.i(
                vmTag,
                "doSubscription -> chatMessagesSubscription -> channel: ${chatMessagesSubscription?.channel}"
            )
            Log.i(
                vmTag,
                "doSubscription -> chatSenderSubscription: ${chatSenderSubscription?.channel}"
            )

/*            chatMessagesSubscription?.setRecoverable(true)
            chatSenderSubscription?.setRecoverable(true)*/

            chatMessagesSubscription?.subscribe()
            chatSenderSubscription?.subscribe()
            chatReadMessageSub?.subscribe()
            chatReadMessageUserIdSub?.subscribe()

            client.connect()

        } catch (ex: DuplicateSubscriptionException) {
            ex.printStackTrace()
        }
    }

    private fun unsubcribeChannel() {
        chatMessagesSubscription?.unsubscribe()
        chatSenderSubscription?.unsubscribe()
        chatReadMessageSub?.unsubscribe()
        chatReadMessageUserIdSub?.unsubscribe()

        chatMessagesSubscription?.apply {
            chatSenderSubscription?.let {
                chatReadMessageSub?.let { readMessageSub ->
                    chatReadMessageUserIdSub?.let { readMessageUserId ->
                        client.removeSubscription(this)
                        client.removeSubscription(it)
                        client.removeSubscription(readMessageSub)
                        client.removeSubscription(readMessageUserId)
                        client.disconnect()
                    }
                }
            }
        }
    }

    private fun getUsersPairParams() {
        doWork {
            profileUsecase.getUserById(receiverId)
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    receiverModel = it

                    ldSetBottomEnterVisibility.postValue(it.chat)

                    parseUsersPairParams()
                }
        }
    }

    private fun parseUsersPairParams() {
        val params = ParamsContainer()

        val pairText = String.format(
            resourceManager.getString(R.string.format_new_pair_chat),
            receiverModel.name
        )

        val datePairText = TimeAgo.covertTimeToText(
            DateParser.getCurrentTimeFormat(DateParser.formatStandartFormatTime),
            DateParser.formatStandartFormatTime
        ).toString()

        params.putString(
            AppConstants.BundleConstants.USER_PHOTO_1,
            RemoteMainConstants.SERVER_URL + receiverModel.avatar
        )
        params.putString(
            AppConstants.BundleConstants.USER_PHOTO_2,
            RemoteMainConstants.SERVER_URL + senderUserModel?.avatar
        )

        params.putString(AppConstants.BundleConstants.CHAT_PAIR_TEXT, pairText)
        params.putString(AppConstants.BundleConstants.CHAT_PAIR_TEXT_DATE, datePairText)
        params.putString(AppConstants.BundleConstants.NAME, receiverModel.name)

        ldSetUsersPair.postValue(params)
    }

    private fun parseSocketDataMessage(json: String, updateMessageRead: (id: Long) -> Unit) {
        doWork {
            Log.i(vmTag, "parseSocketDataMessage -> messages:${messageList}")

            val data = Gson().fromJson(json, JsonObject::class.java)
            val message = data.getAsJsonObject(RemoteConstants.MESSAGE)

            val dateStr = message.get(RemoteConstants.DATE_MESSAGE).asString

            val dateFormat = SimpleDateFormat(DateParser.format_YYYY_MM_dd)

            var firstDate: String? = null

            messageList.asReversed().forEachIndexed { index, chatLocaleDateMessage ->
                if (chatLocaleDateMessage.date != null) {
                    firstDate = chatLocaleDateMessage.date.toString()
                    return@forEachIndexed
                }
            }

            Log.i(
                vmTag,
                "parseSocketDataMessage -> secondDate:${dateStr}, firstDate:${firstDate}"
            )

            if (firstDate == null) {
                val item = ChatLocaleDateMessage(date = dateStr)

                messageList.add(0, item)
                newMessagesList.add(0, item)
            } else {
                if (!dateStr.equals(firstDate)) {
                    val item = ChatLocaleDateMessage(date = dateStr)

                    messageList.add(0, item)
                    newMessagesList.add(0, item)
                }
            }

            val id = message.get(RemoteConstants.ID).asLong
            val file = if (message.get(RemoteConstants.FILE).isJsonNull) null else message.get(
                RemoteConstants.FILE
            ).asString
            val reaction =
                if (message.get(RemoteConstants.REACTION).isJsonNull) null else message.get(
                    RemoteConstants.REACTION
                ).asString

            val messageText =
                if (message.get(RemoteConstants.MESSAGE).isJsonNull) null else message.get(
                    RemoteConstants.MESSAGE
                ).asString

            val itemMessage = ChatLocaleDateMessage(
                message = ChatMessage(
                    id = id,
                    sender_id = message.get(RemoteConstants.SENDER_ID).asLong,
                    receiver_id = message.get(RemoteConstants.RECEIVER_ID).asLong,
                    message = messageText.toString(),
                    is_read = 0,
                    created_at = message.get(RemoteConstants.CREATED_AT).asString,
                    file = file,
                    reaction = reaction
                )
            )

            messageList.add(
                0,
                itemMessage
            )
            newMessagesList.add(
                0, itemMessage
            )

            ldSetChatList.postValue(messageList)
            ldSetEmptyListParams.postValue(messageList.isEmpty())

            ldScrollDown.call()

            updateMessageRead.invoke(id)
        }
    }

    private fun setMessageIsRead(id: Long) {
        doWork {
            val params = mutableMapOf<String, Long>()

            params.put(messageIdKey, id)

            chatUsecase.chatIsRead(receiverId, params)
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect()
        }
    }

    private fun parseReadUserMessage(json: String) {
        doWork {
            val data = Gson().fromJson(json, JsonArray::class.java).get(0).asJsonArray

            if (data.size() == 1) {
                val id = data.get(0).asJsonObject.get(RemoteConstants.ID).asLong

                messageList.forEachIndexed { index, chatLocaleDateMessage ->
                    if (chatLocaleDateMessage.message != null) {
                        if (chatLocaleDateMessage.message!!.id == id) {
                            chatLocaleDateMessage.message!!.is_read = 1

                            ldUpdateMessageItem.postValue(Pair(chatLocaleDateMessage, index))
                        }
                    }
                }
            } else {
                messageList.forEachIndexed { index, chatLocaleDateMessage ->

                    data.forEach {
                        val id = it.asJsonObject.get(RemoteConstants.ID).asLong

                        if (chatLocaleDateMessage.message != null) {
                            if (chatLocaleDateMessage.message!!.id == id) {
                                chatLocaleDateMessage.message!!.is_read = 1

                                Log.i(
                                    vmTag,
                                    "parseReadUserMessage -> index: ${index}, model:${chatLocaleDateMessage}"
                                )

                            }
                        }
                    }
                }

                ldSetChatList.postValue(messageList)
            }


/*            data.get(0).asJsonArray.forEach {
                val id = it.asJsonObject.get(RemoteConstants.ID).asLong

                messageList.forEachIndexed { index, chatLocaleDateMessage ->
                    if (chatLocaleDateMessage.message != null) {
                        if (chatLocaleDateMessage.message!!.id == id) {
                            chatLocaleDateMessage.message!!.is_read = 1

                            ldUpdateMessageItem.postValue(Pair(chatLocaleDateMessage, index))

                            return@forEachIndexed
                        }
                    }
                }
            }*/


        }
    }
}