package thousand.group.uylen.view_models.profile

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.*
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.BitmapHelper
import thousand.group.uylen.utils.helpers.DateParser
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.locale.EnterSingleInfo
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.home.HomeViewModel
import java.util.*

class EditProfileViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val bitmapHelper: BitmapHelper,
    private val authUsecase: AuthUsecase,
    private val profileUsecase: ProfileUsecase,
    private val utilUsecase: UtilUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetNameAge = MutableLiveData<Pair<String, String>>()
    val ldSetInterests = MutableLiveData<String>()
    val ldSetSelectedCity = MutableLiveData<String>()
    val ldSetSelectedBirthdate = MutableLiveData<String>()
    val ldSetSelectedRu = MutableLiveData<String>()

    //    val ldSetSelectedGender = MutableLiveData<String>()
    val ldSetSelectedFamilyStatus = MutableLiveData<String>()
    val ldSetKalym = MutableLiveData<String>()
    val ldSetBadHabitsText = MutableLiveData<String>()
    val ldSetSelectedZodiac = MutableLiveData<String>()
    val ldSetWeight = MutableLiveData<String>()
    val ldSetHeight = MutableLiveData<String>()
    val ldSetKalymVisibility = MutableLiveData<Boolean>()

    val ldShowPhotoSelectDialog = OnceLiveData<Pair<String, String>>()
    val ldSetList = OnceLiveData<MutableList<UserPhoto?>>()
    val ldRequestPhotoPermission = OnceLiveData<Unit>()
    val ldSetItem = OnceLiveData<Pair<UserPhoto?, Int>>()
    val ldSetSimpleParams = OnceLiveData<Triple<String, String, String>>()
    val ldOpenInterestsPage = OnceLiveData<Pair<String, MutableList<Interest>>>()
    val ldOpenCityDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldGoBack = OnceLiveData<Unit>()
    val ldOpenRuDialog = OnceLiveData<ParamsContainer>()
    val ldSetRuParams = OnceLiveData<Triple<Int, Int, MutableList<String>>>()

    //    val ldOpenGenderDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenEnterSingleInfoPage = OnceLiveData<Triple<String, EnterSingleInfo, String>>()
    val ldOpenFamilyStatusDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldClosePage = OnceLiveData<Unit>()
    val ldClearRuPicker = OnceLiveData<Unit>()
    val ldOpenZodiacDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenBadHabitsDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenCropPage = OnceLiveData<Uri>()
    val ldOpenBirthdayDialog = OnceLiveData<Pair<Long, Triple<Int, Int, Int>>>()
    val ldOpenCropBitmapPage = OnceLiveData<Uri>()

    private val user = authUsecase.getUserModel()

    private val regPhotos = mutableListOf<UserPhoto?>()
    private val interestList = mutableListOf<Interest>()
    private val citiesList = mutableListOf<City>()
    private val citiesNamesList = mutableListOf<String>()
    private val zhuzList = mutableListOf<Zhuz>()
    private val zhuzNamesList = mutableListOf<String>()
    private val ruNamesList = mutableListOf<MutableList<String>>()

    private val genderList = mutableListOf<GeneralValue>()
    private val genderNamesList = mutableListOf<String>()

    private val familyStatusList = mutableListOf<GeneralValue>()
    private val familyNamesStatusList = mutableListOf<String>()

    private val zodiacList = mutableListOf<GeneralValue>()
    private val zodiacNameList = mutableListOf<String>()

    private val badHabitsList = mutableListOf<GeneralValue>()
    private val badHabitsNameList = mutableListOf<String>()

    private val params = mutableMapOf<String, RequestBody>()

    private var currentPhotoAddress: String? = null
    private var birthDate = ""
    private var birthDateFormatted = ""

    private var currentRegPhotoPos = 0
    private var selectedCityPos = 0
    private var selectedRegionPos = 0
    private var selectedZhuzPos = 0
    private var selectedRuPos = 0
    private var selectedGenderPos = 0
    private var selectedFamilyStatusPos = 0
    private var selectedZodiacPos = 0
    private var selectedBadHabitPos = 0

    private var birthYear = 0
    private var birthMonth = 0
    private var birthDay = 0

    init {
        Log.i(vmTag, "User: ${user}")

        setSerializableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.ENTER_VAL -> {
                    val params = message as Pair<Int, String>

                    parseEnterSingleValue(params.first, params.second)
                }
            }
        }
        setParcelableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.CameraActionConstants.ON_PHOTO_LOADED -> {
                    Log.i(vmTag, "ON_PHOTO_LOADED -> uri: ${message}")

                    createAndSetBitmapPart(
                        message as Uri
                    )
                }
                AppConstants.MessageKeysConstants.CROP_BITMAP -> {
                    (message as Bitmap).apply {
                        setBitmapToRegPhoto(this)
                    }
                }
            }
        }

        setStringMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.SET_INTEREST_TEXT -> {
                    ldSetInterests.postValue(message)
                }
            }
        }

        setListMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.SET_INTEREST_LIST -> {
                    Log.i(
                        "TestList",
                        "AppConstants.MessageKeysConstants.SET_INTEREST_LIST -> message: $message"
                    )

                    interestList.clear()
                    interestList.addAll(message as MutableList<Interest>)
                }
            }
        }

        setUnitMessageReceivedListener {
            when (it) {
                AppConstants.MessageKeysConstants.ON_BACK_PRESSED -> {
                    checkImagesCount {
                        ldGoBack.call()
                    }
                }
            }
        }

        Log.i(vmTag, "EditProfile -> user:${user}")

        fillPhotoList()
        ldSetList.postValue(regPhotos)

//        getGenderValues()
        getFamilyStatusValues()
        getZodiacValues()
        getBadHabitsValues()

        setNameAge()
        setUserDataParams()
        setInitInterest()
        setCityList()
        setInitBirthdate()
        setRuList()
        setInitKalym()
        setInitHeight()
        setInitWeight()
    }

    override fun onCleared() {
        sendLocaleCallback(
            ProfileViewModel::class,
            AppConstants.MessageKeysConstants.UPDATE_PROFILE
        )

        super.onCleared()
    }

    fun openBirthdayDialog() {
        val calendarCurrent = Calendar.getInstance()

        var currentYear = calendarCurrent.get(Calendar.YEAR)
        currentYear -= 18

        calendarCurrent.set(Calendar.YEAR, currentYear)

        if (birthDay == 0) {
            val calendar = Calendar.getInstance()

            birthYear = calendar.get(Calendar.YEAR)
            birthMonth = calendar.get(Calendar.MONTH)
            birthDay = calendar.get(Calendar.DAY_OF_MONTH)
        }

        ldOpenBirthdayDialog.postValue(
            Pair(
                calendarCurrent.timeInMillis,
                Triple(birthYear, birthMonth, birthDay)
            )
        )
    }

    fun openPhotoVideoDialogFragment() {
        ldShowPhotoSelectDialog.postValue(
            Pair(
                vmTag,
                AppConstants.CameraActionConstants.MODE_PHOTO
            )
        )
    }

    fun addItemPhoto(model: UserPhoto?, pos: Int) {
        currentRegPhotoPos = pos

        ldRequestPhotoPermission.call()
    }

    fun removeItemPhoto(model: UserPhoto?, pos: Int) {
        model?.let {
            doWork {
                profileUsecase.profileRemoveImage(it.id)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                        regPhotos.set(pos, null)

                        getProfile()

                        doWorkAfterSomeTime(200) {
                            researchAvatar()
                        }
                    }
            }
        }
    }

    fun selectCity(pos: Int) {
        selectedCityPos = pos

        ldSetSelectedCity.postValue(citiesList.get(selectedCityPos).name)
    }

    fun openCityDialog() {
        ldOpenCityDialog.postValue(
            Triple(
                selectedCityPos,
                citiesNamesList.size - 1,
                citiesNamesList
            )
        )
    }

    fun selectZodiac(pos: Int) {
        selectedZodiacPos = pos

        ldSetSelectedZodiac.postValue(zodiacNameList.get(selectedZodiacPos))
    }

    fun openZodiacDialog() {
        ldOpenZodiacDialog.postValue(
            Triple(
                selectedZodiacPos,
                zodiacNameList.size - 1,
                zodiacNameList
            )
        )
    }

    fun onOpenningDateSelected(year: Int, month: Int, dayOfMonth: Int) {

        birthDate =
            String.format(
                resourceManager.getString(R.string.format_date),
                year,
                month.inc(),
                dayOfMonth
            )

        birthYear = year
        birthMonth = month
        birthDay = dayOfMonth

        birthDateFormatted = DateParser.convertOneFormatToAnother(
            birthDate,
            DateParser.format_YYYY_MM_dd,
            DateParser.format_dd_LLLL_YYYY
        )

        ldSetSelectedBirthdate.postValue(birthDateFormatted)
    }

    fun selectZhuz(pos: Int) {
        ldClearRuPicker.call()

        if (ruNamesList.isEmpty() || ruNamesList.get(pos).isEmpty()) {
            return
        }

        selectedZhuzPos = pos
        selectedRuPos = 0

        val list = ruNamesList.get(selectedZhuzPos)

        ldSetRuParams.postValue(
            Triple(
                selectedRuPos,
                list.size - 1,
                list
            )
        )

        setSelectedZhuzParams()
    }

    fun selectRu(pos: Int) {
        selectedRuPos = pos

        setSelectedZhuzParams()
    }

    fun openZhuzDialog() {
        val params = ParamsContainer()

        val list = ruNamesList.get(selectedZhuzPos)

        params.putInt(AppConstants.BundleConstants.SELECTED_POS, selectedZhuzPos)
        params.putInt(AppConstants.BundleConstants.SELECTED_POS_2, selectedRuPos)
        params.putInt(AppConstants.BundleConstants.MAX_POS, zhuzList.size - 1)
        params.putInt(AppConstants.BundleConstants.MAX_POS_2, list.size - 1)
        params.putList(AppConstants.BundleConstants.PARENT_LIST, zhuzNamesList)

        if (list.isNotEmpty()) {
            params.putList(AppConstants.BundleConstants.CHILD_LIST, list)
        }

        ldOpenRuDialog.postValue(params)
    }

//    fun selectRegion(pos: Int) {
//        ldClearCityPicker.call()
//
//        Log.i(vmTag, "selectRegion -> citiesNamesList: ${citiesNamesList}")
//
//        citiesList.clear()
//        citiesList.addAll(regionsList.get(pos).cities)
//
//        if (citiesNamesList.isEmpty() || citiesNamesList.get(pos).isEmpty()) {
//            return
//        }
//
//        selectedRegionPos = pos
//        selectedCityPos = 0
//
//        val list = citiesNamesList.get(selectedRegionPos)
//
//        ldSetCityRegionParams.postValue(
//            Triple(
//                selectedCityPos,
//                list.size - 1,
//                list
//            )
//        )
//
//        setSelectedCityRegionParams()
//    }

    /*fun openRegionsDialog() {
        if (citiesList.isNotEmpty() && regionsNamesList.isNotEmpty()) {

            val params = ParamsContainer()

            val list = citiesNamesList.get(selectedRegionPos)

            val maxPos2 = if (list.isEmpty()) 0 else list.size - 1

            params.putInt(AppConstants.BundleConstants.SELECTED_POS, selectedRegionPos)
            params.putInt(AppConstants.BundleConstants.SELECTED_POS_2, selectedCityPos)
            params.putInt(AppConstants.BundleConstants.MAX_POS, regionsList.size - 1)
            params.putInt(AppConstants.BundleConstants.MAX_POS_2, maxPos2)
            params.putList(AppConstants.BundleConstants.PARENT_LIST, regionsNamesList)

            if (list.isNotEmpty()) {
                params.putList(AppConstants.BundleConstants.CHILD_LIST, list)
            }

            ldOpenCityRegionDialog.postValue(params)

            Log.i(vmTag, "openRegionsDialog -> params: ${params}")
        }
    }*/

    /*fun selectGender(pos: Int) {
        selectedGenderPos = pos
        ldSetSelectedGender.postValue(genderNamesList.get(pos))
        ldSetKalymVisibility.postValue(pos == 1)
    }

    fun openGenderDialog() {
        ldOpenGenderDialog.postValue(
            Triple(
                selectedGenderPos,
                genderNamesList.size - 1,
                genderNamesList
            )
        )
    }*/

    fun openKalymEditPage(kalym: String) {
        ldOpenEnterSingleInfoPage.postValue(
            Triple(
                vmTag,
                AppConstants.EnterSingleInfoTypes.ENTER_KALYM,
                kalym
            )
        )
    }

    fun selectBadHabit(pos: Int) {
        selectedBadHabitPos = pos

        ldSetBadHabitsText.postValue(badHabitsNameList.get(selectedBadHabitPos))
    }

    fun openBadHabitsDialog() {
        ldOpenBadHabitsDialog.postValue(
            Triple(
                selectedBadHabitPos,
                badHabitsNameList.size - 1,
                badHabitsNameList
            )
        )
    }

    fun openWeightEditPage(weight: String) {
        ldOpenEnterSingleInfoPage.postValue(
            Triple(
                vmTag,
                AppConstants.EnterSingleInfoTypes.ENTER_WEIGHT,
                weight
            )
        )
    }

    fun openHeightEditPage(height: String) {
        ldOpenEnterSingleInfoPage.postValue(
            Triple(
                vmTag,
                AppConstants.EnterSingleInfoTypes.ENTER_HEIGHT,
                height
            )
        )
    }

    fun selectFamilyStatus(pos: Int) {
        selectedFamilyStatusPos = pos

        val item = familyNamesStatusList.get(selectedFamilyStatusPos)

        ldSetSelectedFamilyStatus.postValue(item)
    }

    fun openFamilyStatusDialog() {
        val list = familyNamesStatusList

        ldOpenFamilyStatusDialog.postValue(
            Triple(
                selectedFamilyStatusPos,
                list.size - 1,
                list
            )
        )
    }

    fun openInterestsPage() {
        ldOpenInterestsPage.postValue(Pair(vmTag, interestList))
    }

    fun uploadImage(uri: Uri) {
        try {
            bitmapHelper.getBitmapFromUri(
                uri,
                null,
                null
            ) {
                setBitmapToRegPhoto(it)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun onSaveBtnClicked(
        name: String,
        about: String,
        salary: String,
        kalym: String,
        badHabits: String,
        weight: String,
        height: String,
        ruText:String
    ) {
        checkImagesCount {
            checkParams(name, salary, weight, height, badHabits, ruText) {
                createParams(
                    name, about, salary,
                    kalym,
                    weight,
                    height
                ) {
                    doWork {
                        profileUsecase.editProfile(params)
                            .onStart { showProgressBar(true) }
                            .onCompletion { showProgressBar(false) }
                            .catch {
                                showMessageError(
                                    ResErrorHelper.showThrowableMessage(
                                        resourceManager,
                                        vmTag,
                                        it
                                    )
                                )
                            }
                            .collect {
                                showMessageSuccess(R.string.success_edit_profile)

                                doWorkAfterSomeTime(1000L) {
                                    sendLocaleCallback(
                                        ProfileViewModel::class,
                                        AppConstants.MessageKeysConstants.UPDATE_PROFILE
                                    )

                                    ldClosePage.call()
                                }
                            }
                    }
                }
            }
        }
    }

    fun checkImagesCount(isSuccess: () -> Unit) {
        Log.i(vmTag, "checkImagesCount -> list: ${regPhotos}")

        if (regPhotos.count {
                it != null
            } < 2) {
            showMessageError(R.string.error_photos_size_2)
        } else {
            isSuccess.invoke()
        }
    }

    private fun fillPhotoList() {
        user?.apply {
            regPhotos.clear()
            regPhotos.addAll(this.images)

            regPhotos.getOrNull(0)?.avatar = 1

            if (regPhotos.size < 6) {
                val pos = regPhotos.size

                for (i in pos until 6) {
                    regPhotos.add(null)
                }
            }

        }
    }

    private fun createAndSetBitmapPart(uri: Uri) {
        doWork {
            try {
                showProgressBar(true)

//                ldOpenCropPage.postValue(uri)
                ldOpenCropBitmapPage.postValue(uri)

            } catch (ex: Exception) {
                ex.printStackTrace()
            } finally {
                showProgressBar(false)
            }
        }
    }

    private fun setBitmapToRegPhoto(bitmap: Bitmap) {
        val imageKey = String.format(RemoteConstants.IMAGE, currentRegPhotoPos)

        RequestBodyHelper.getMultipartData(imageKey, bitmap)?.let {
            doWork {
                profileUsecase.profileUploadImage(it)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {

//                        setUserImageAvatar(it)

                        regPhotos.set(currentRegPhotoPos, it)

                        getProfile()

                        doWorkAfterSomeTime(200) {
                            researchAvatar()
                        }
                    }

            }
        }
    }

    private fun setUserImageAvatar(userPhoto: UserPhoto) {
        if (currentRegPhotoPos == 0) {
            doWork {
                profileUsecase.setUserAvatar(userPhoto.id)
                    .catch {
                        it.printStackTrace()
                    }
                    .collect {
                        userPhoto.avatar = 1

                        regPhotos.set(currentRegPhotoPos, userPhoto)
                        ldSetItem.postValue(Pair(userPhoto, currentRegPhotoPos))
                    }
            }
        }
    }

    private fun setNameAge() {
        doWork {
            user?.let {
                ldSetNameAge.postValue(
                    Pair(
                        it.name,
                        getAge(it.details?.date_of_birth)
                    )
                )
            }
        }
    }

    private fun getAge(birthdateText: String?): String {
        if (birthdateText != null) {
            val birthdate =
                DateParser.stringToDate(birthdateText, DateParser.format_YYYY_MM_dd).time
            val currentDate = Date().time

            val diff = currentDate - birthdate
            val diffCount = Calendar.getInstance()
            diffCount.timeInMillis = diff

            val year = diffCount.get(Calendar.YEAR) - 1970

            return year.toString()
        } else {
            return ""
        }
    }

    private fun setUserDataParams() {
        user?.let {
            doWork {
                val salary = if (it.about?.salary == null) "0" else it.about?.salary.toString()
                val about = if (it.about?.text == null) "" else it.about?.text.toString()

                ldSetSimpleParams.postValue(
                    Triple(
                        it.name,
                        salary,
                        about
                    )
                )

            }
        }
    }

    private fun setInitInterest() {
        user?.let {
            doWork {
                val sb = StringBuilder()

                it.interests.forEachIndexed { index, interest ->
                    if (sb.isEmpty()) {
                        sb.append(interest.title)
                    } else {
                        sb.append(", ${interest.title}")
                    }
                }

                interestList.clear()
                interestList.addAll(it.interests)

                interestList.forEachIndexed { index, interest ->
                    interest.isSelected = true
                    interestList.set(index, interest)
                }

                ldSetInterests.postValue(sb.toString())
            }
        }
    }

    private fun setCityList() {
        authUsecase.getUserModel()?.let { user ->
            doWork {
                utilUsecase.getCitiesList()
                    .catch { it.printStackTrace() }
                    .collect {
                        citiesList.clear()
                        citiesNamesList.clear()

                        citiesList.addAll(it)

                        citiesList.forEach {
                            citiesNamesList.add(it.name)
                        }

                        setInitCityId()
                    }
            }
        }
    }

    private fun setInitCityId() {
        user?.details?.city_id?.let {
            citiesList.forEachIndexed { indexCity, city ->
                if (it.id == city.id) {
                    selectedCityPos = indexCity

                    ldSetSelectedCity.postValue(city.name)
                }
            }
        }
    }

    private fun setInitBirthdate() {
        user?.details?.date_of_birth?.let {
            doWork {
                birthDate = it

                birthDateFormatted = DateParser.convertOneFormatToAnother(
                    birthDate,
                    DateParser.format_YYYY_MM_dd,
                    DateParser.format_dd_LLLL_YYYY
                )

                ldSetSelectedBirthdate.postValue(birthDateFormatted)
            }
        }
    }

    private fun setRuList() {
        user?.let { user ->
            doWork {
                utilUsecase.getRuList()
                    .catch { it.printStackTrace() }
                    .collect {
                        zhuzList.clear()
                        zhuzList.addAll(it)

                        zhuzList.forEach {
                            zhuzNamesList.add(it.name)
                        }

                        zhuzList.forEachIndexed { index, zhuz ->
                            ruNamesList.add(mutableListOf())

                            zhuz.parents.forEach {
                                ruNamesList.get(index).add(it.name)
                            }
                        }

                        zhuzList.forEachIndexed { index, zhuz ->
                            zhuz.parents.forEachIndexed { indexRu, ru ->
                                if (ru.id == user.details?.gener_id?.id) {
                                    selectedZhuzPos = index
                                    selectedRuPos = indexRu
                                }
                            }
                        }

                        setSelectedZhuzParams()
                    }
            }
        }
    }

    private fun setSelectedZhuzParams() {
        val zhuzItem = zhuzNamesList.get(selectedZhuzPos)

        val ruItem = if (ruNamesList.isNotEmpty() && ruNamesList.get(selectedZhuzPos)
                .isNotEmpty()
        ) ruNamesList.get(selectedZhuzPos).get(selectedRuPos) else ""

        ldSetSelectedRu.postValue("$zhuzItem, $ruItem")
    }

/*    private fun setInitGender() {
        user?.details?.let {
            doWork {
                val pos =
                    if (it.sex == 9) 0 else 1

                selectGender(pos)
            }
        }
    }*/

    private fun parseEnterSingleValue(type: Int, text: String) {
        when (type) {
            AppConstants.EnterSingleInfoTypes.ENTER_KALYM.type -> ldSetKalym.postValue(text)
            AppConstants.EnterSingleInfoTypes.ENTER_BAD_HABITS.type -> ldSetBadHabitsText.postValue(
                text
            )
            AppConstants.EnterSingleInfoTypes.ENTER_WEIGHT.type -> ldSetWeight.postValue(text)
            AppConstants.EnterSingleInfoTypes.ENTER_HEIGHT.type -> ldSetHeight.postValue(text)
        }
    }

    private fun setInitFamilyStatus() {
        user?.details?.marital_status?.let {
            doWork {

                familyStatusList.forEachIndexed {index, it->
                    if(it.id == user.details?.marital_status_id){
                        selectedFamilyStatusPos = index
                    }
                }

                ldSetSelectedFamilyStatus.postValue(it)
            }
        }
    }

    private fun setInitKalym() {
        if (user?.details?.kalyn_male == null) {
            ldSetKalymVisibility.postValue(false)
        } else {
            doWork {
                val pos =
                    if (user.details?.sex == 9) 0 else 1

                ldSetKalymVisibility.postValue(pos == 1)
                ldSetKalym.postValue(user.details?.kalyn_male.toString())
            }
        }
    }

    private fun setInitBadHabits() {
        user?.details?.bad_habits?.let {
            doWork {
                badHabitsList.forEachIndexed {index, it->
                    if(it.id == user.details?.bad_habits_id){
                        selectedBadHabitPos = index
                    }
                }

                ldSetBadHabitsText.postValue(it)
            }
        }
    }

    private fun setInitZodiac() {
        user?.details?.zodiac?.let {
            doWork {

                zodiacList.forEachIndexed {index, it->
                    if(it.id == user.details?.zodiac_id){
                        selectedZodiacPos = index
                    }
                }

                ldSetSelectedZodiac.postValue(it)
            }
        }
    }

    private fun setInitHeight() {
        user?.details?.height?.let {
            doWork {
                ldSetHeight.postValue(it.toString())
            }
        }
    }

    private fun setInitWeight() {
        user?.details?.weight?.let {
            doWork {
                ldSetWeight.postValue(it.toString())
            }
        }
    }

    private fun checkParams(
        name: String,
        salary: String,
        weight: String,
        height: String,
        badHabits: String,
        ruText:String,
        isSuccess: () -> Unit
    ) {
        when {
            ruText.trim().isEmpty() -> showMessageError(R.string.error_select_ru)
            name.trim().isEmpty() -> showMessageError(R.string.error_empty_name)
            interestList.isEmpty() -> showMessageError(R.string.error_interests_empty)
//            salary.trim().isEmpty() -> showMessageError(R.string.error_empty_salary)
            birthDate.isEmpty() -> showMessageError(R.string.error_birthday)
//            weight.isEmpty() -> showMessageError(R.string.error_weight)
//            height.isEmpty() -> showMessageError(R.string.error_height)
            badHabits.isEmpty() -> showMessageError(R.string.error_bad_habits)
            else -> isSuccess.invoke()
        }
    }

    private fun createParams(
        name: String,
        about: String,
        salary: String,
        kalym: String,
        weight: String,
        height: String,
        isCreated: () -> Unit
    ) {
        params.clear()

        params.put(
            RemoteConstants.NAME,
            RequestBodyHelper.getRequestBodyText(name.trim())
        )

        params.put(
            RemoteConstants.ABOUT,
            RequestBodyHelper.getRequestBodyText(about.trim())
        )

        citiesList.getOrNull(selectedCityPos)?.apply {
            params.put(
                RemoteConstants.CITY_ID,
                RequestBodyHelper.getRequestBodyText(this.id)
            )
        }

        params.put(
            RemoteConstants.DATE_OF_BIRTH,
            RequestBodyHelper.getRequestBodyText(birthDate)
        )

        params.put(
            RemoteConstants.GENER_ID,
            RequestBodyHelper.getRequestBodyText(
                zhuzList
                    .get(selectedZhuzPos)
                    .parents
                    .get(
                        selectedRuPos
                    )
                    .id
            )
        )


        user?.details?.sex?.apply {
            params.put(
                RemoteConstants.SEX,
                RequestBodyHelper.getRequestBodyText(
                    this
                )
            )
        }

        params.put(
            RemoteConstants.MARITAL_STATUS,
            RequestBodyHelper.getRequestBodyText(
                familyStatusList.get(selectedFamilyStatusPos).id
            )
        )

        if (salary.isNotEmpty()) {
            params.put(
                RemoteConstants.SALARY,
                RequestBodyHelper.getRequestBodyText(salary)
            )
        }
        if (selectedGenderPos == 1 && kalym.trim().isNotEmpty()) {
            params.put(
                RemoteConstants.K_MAL,
                RequestBodyHelper.getRequestBodyText(
                    kalym.trim()
                )
            )
        }

        params.put(
            RemoteConstants.BAD_HABITS,
            RequestBodyHelper.getRequestBodyText(badHabitsList.get(selectedBadHabitPos).id)
        )

        params.put(
            RemoteConstants.ZODIAC,
            RequestBodyHelper.getRequestBodyText(
                zodiacList.get(selectedZodiacPos).id
            )
        )

        if (weight.isNotEmpty()) {
            params.put(
                RemoteConstants.WEIGHT,
                RequestBodyHelper.getRequestBodyText(
                    weight.trim()
                )
            )
        }

        if (height.isNotEmpty()) {
            params.put(
                RemoteConstants.HEIGHT,
                RequestBodyHelper.getRequestBodyText(
                    height.trim()
                )
            )
        }

        if (interestList.isNotEmpty()) {
            val interestFormat = resourceManager.getString(R.string.format_interests)

            interestList.forEachIndexed { index, item ->
                val key = String.format(interestFormat, index.inc())

                params.put(key, RequestBodyHelper.getRequestBodyText(item.id))
            }
        }

        isCreated.invoke()
    }

    /*private fun getGenderValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.SEX)
                .catch { it.printStackTrace() }
                .collect {
                    genderList.clear()
                    genderNamesList.clear()

                    genderList.addAll(it)

                    genderList.forEach {
                        genderNamesList.add(it.value)
                    }

                    Log.i(vmTag, "genderList:${genderList}")
                    Log.i(vmTag, "genderNamesList:${genderNamesList}")

                    setInitGender()
                }
        }
    }*/

    private fun getFamilyStatusValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.MARITAL_STATUS)
                .catch { it.printStackTrace() }
                .collect {
                    familyStatusList.clear()
                    familyNamesStatusList.clear()

                    familyStatusList.addAll(it)

                    familyStatusList.forEach {
                        familyNamesStatusList.add(it.value)
                    }

                    Log.i(vmTag, "familyStatusList:${familyStatusList}")
                    Log.i(vmTag, "familyNamesStatusList:${familyNamesStatusList}")

                    setInitFamilyStatus()
                }
        }
    }

    private fun getZodiacValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.ZODIAC)
                .catch { it.printStackTrace() }
                .collect {
                    zodiacList.clear()
                    zodiacNameList.clear()

                    zodiacList.addAll(it)

                    zodiacList.forEach {
                        zodiacNameList.add(it.value)
                    }

                    Log.i(vmTag, "zodiacList:${zodiacList}")
                    Log.i(vmTag, "zodiacNameList:${zodiacNameList}")

                    setInitZodiac()
                }
        }
    }

    private fun getBadHabitsValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.SMOKE)
                .catch { it.printStackTrace() }
                .collect {
                    badHabitsList.clear()
                    badHabitsNameList.clear()

                    badHabitsList.addAll(it)

                    badHabitsList.forEach {
                        badHabitsNameList.add(it.value)
                    }

                    Log.i(vmTag, "badHabitsList:${badHabitsList}")
                    Log.i(vmTag, "badHabitsNameList:${badHabitsNameList}")

                    setInitBadHabits()

                }
        }
    }

    private fun researchAvatar() {
        doWork {
            var count = 0

            regPhotos.forEach {
                if (it != null) {
                    if (count < 1) {
                        count += 1
                        it.avatar = 1
                    } else {
                        it.avatar = 0
                    }
                }
            }

            ldSetList.postValue(regPhotos)
        }
    }

    private fun getProfile() {
        doWork {
            profileUsecase.getProfile()
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    authUsecase.saveUserModel(it)
                    authUsecase.setUserId(it.id)
                }
        }
    }


}