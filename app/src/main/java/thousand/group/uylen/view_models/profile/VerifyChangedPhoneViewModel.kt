package thousand.group.uylen.view_models.profile

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.extensions.formatToPhoneNumber
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import java.util.concurrent.TimeUnit

class VerifyChangedPhoneViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val likesUsecase: LikesUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldTimerText = MutableLiveData<String>()
    val ldTimerTextEnabled = MutableLiveData<Boolean>()
    val ldSetPhoneText = MutableLiveData<String>()

    val ldStartTimer = OnceLiveData<Unit>()
    val ldStopTimer = OnceLiveData<Unit>()
    val ldClosePage = OnceLiveData<Unit>()

    private var phone = ""

    private var userId = authUsecase.getUserId()

    private var isDone = false

    private var timerText = resourceManager.getString(R.string.format_timer_send_code)
    private var timerTextPart = resourceManager.getString(R.string.label_resend_code)

    init {
        parseArgs()

        ldSetPhoneText.postValue(phone.formatToPhoneNumber())

        sendVerificationCode()
    }

    override fun onCleared() {
        super.onCleared()
        ldStopTimer.call()
    }

    fun onTimerFinished() {
        ldTimerText.postValue(timerTextPart)
        ldTimerTextEnabled.postValue(true)
    }

    fun onTimerTicker(millisUntilFinished: Long) {
        if (millisUntilFinished < 1000) {
            ldTimerText.postValue(timerTextPart)
            ldTimerTextEnabled.postValue(true)
        } else {
            ldTimerText.postValue(convertMillis(millisUntilFinished))
            ldTimerTextEnabled.postValue(false)
        }
    }

    fun onTimerStop() {
        ldTimerText.postValue(timerTextPart)
        ldTimerTextEnabled.postValue(true)
    }

    fun onCodeChanged(text: String) {
        if (text.trim().length == 4) {

            if (isDone) {
                isDone = false
                return
            }

            doWork {
                authUsecase.assertVerificationCode(
                    userId,
                    RequestBodyHelper.getRequestBodyText(text.trim())
                )
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                        isDone = true

                        sendLocaleCallback(
                            SettingsViewModel::class,
                            AppConstants.MessageKeysConstants.EDIT_PHONE
                        )
                        ldClosePage.call()
                    }
            }
        }
    }

    fun onSendCodeBtnClicked() {
        sendVerificationCode()
    }

    private fun convertMillis(millisUntilFinished: Long): String {
        val sec = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60
        val minutes = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60
        return String.format(
            timerText,
            minutes,
            sec
        )
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getString(AppConstants.BundleConstants.PHONE_NUMBER)?.apply {
                phone = this
            }
        }
    }

    private fun sendVerificationCode() {
        doWork {
            val phonePart = RequestBodyHelper.getRequestBodyText(phone)

            authUsecase.sendVerificationCodeToNewNumber(phonePart)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.showThrowableMessage(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    ldStartTimer.call()
                }
        }
    }
}