package thousand.group.uylen.view_models.auth

import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.responces.LoginResponse
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager

class LoginViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val utilUsecase: UtilUsecase,
    private val profileUsecase: ProfileUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldOpenRestorePasswordPage = OnceLiveData<Pair<String, User>>()
    val ldOpenMainActivity = OnceLiveData<Unit>()
    val ldReloadActivity = OnceLiveData<Unit>()
    val ldOpenTermPage = OnceLiveData<String>()

    val ldSetLanguageText = MutableLiveData<String>()

    private var phone: String? = null
    private var formattedPhone: String? = null

    private var password: String? = null

    private val params = mutableMapOf<String, RequestBody>()

    private var oneSignalUserID: String? = null

    init {
        setSerializableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.RESPONSE_LOCATION -> {
                    val location = message as Pair<Double, Double>

                    password?.apply {
                        login(this, location.first, location.second)
                    }
                }
            }
        }

        setUnitMessageReceivedListener { key ->
            when (key) {
                AppConstants.MessageKeysConstants.RESPONSE_LOCATION_ERROR -> {
                    showMessageError(R.string.error_lat_lng)
                }
            }
        }

        setLanguageText()
    }

    fun onLanguageTextClicked() {
        doWork {
            val language = profileUsecase.getLanguage()

            if (language.equals(AppConstants.Languages.kz.langCodeLocale)) {
                profileUsecase.setLanguage(AppConstants.Languages.ru.langCodeLocale)
                profileUsecase.setLanguageServer(AppConstants.Languages.ru.langCodeServer)
            } else {
                profileUsecase.setLanguage(AppConstants.Languages.kz.langCodeLocale)
                profileUsecase.setLanguageServer(AppConstants.Languages.kz.langCodeServer)
            }

            ldReloadActivity.call()
        }
    }

    fun openPrivacyPolicy() {
        ldOpenTermPage.postValue(AppConstants.PrivacyTermMode.PRIVACY_POLICY)
    }

    fun openTerms() {
        ldOpenTermPage.postValue(AppConstants.PrivacyTermMode.TERM_OF_USE)
    }

    fun checkPhone(
        maskFilled: Boolean,
        extractedValue: String,
        formatterValue: String
    ) {
        phone = if (maskFilled) extractedValue else null
        formattedPhone = formatterValue
    }

    fun openRestorePasswordPage() {
        if (phone != null) {
            sendRestoreCode()
        } else {
            showMessageError(R.string.error_phone_empty)
        }
    }

    fun login(password: String, lat: Double, longg: Double) {
        checkData(password) {
            doWork {
                createParams(password)

                authUsecase.login(params)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.showThrowableMessage(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                        sendLatLng(it, lat, longg)
                    }
            }
        }
    }

    fun setOneSignalUserId(id: String?) {
        this.oneSignalUserID = id

        Log.i(vmTag, "setOneSignalUserId -> oneSignalUserID: ${this.oneSignalUserID}")
    }

    fun checkLocation(password: String, checked: Boolean) {
        if (checked) {
            this.password = password

            sendLocaleMessage(
                AuthViewModel::class,
                AppConstants.MessageKeysConstants.REQUIRE_LOCATION,
                vmTag
            )
        } else {
            showMessageError(R.string.error_privacy_is_not_checked)
        }

    }

    private fun sendLatLng(loginRes: LoginResponse, lat: Double, longg: Double) {
        doWork {
            authUsecase.setAccessToken(loginRes.token)
            authUsecase.saveUserModel(loginRes.user)
            authUsecase.setUserId(loginRes.user.id)

            utilUsecase.updateLocation(
                RequestBodyHelper.getRequestBodyText(lat),
                RequestBodyHelper.getRequestBodyText(longg)
            )
                .catch { it.printStackTrace() }
                .collect {

                    ldOpenMainActivity.call()
                }
        }
    }

    private fun checkData(
        password: String,
        isSuccess: () -> Unit
    ) {
        when {
            phone.isNullOrEmpty() -> showMessageError(R.string.error_phone)
            password.trim().isEmpty() -> showMessageError(R.string.error_password)
            else -> isSuccess.invoke()
        }
    }

    private fun createParams(
        password: String
    ) {
        params.clear()

        params.put(RemoteConstants.PHONE, RequestBodyHelper.getRequestBodyText(phone))
        params.put(
            RemoteConstants.PASSWORD,
            RequestBodyHelper.getRequestBodyText(password)
        )
        params.put(
            RemoteConstants.APP_CLIENT_ID,
            RequestBodyHelper.getRequestBodyText(oneSignalUserID)
        )
    }

    private fun sendRestoreCode() {
        doWork {
            val phonePart = RequestBodyHelper.getRequestBodyText(phone)

            authUsecase.sendVerificationCode(phonePart)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.showThrowableMessage(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    phone?.apply {
                        ldOpenRestorePasswordPage.postValue(Pair(this, it))
                    }
                }
        }
    }

    private fun setLanguageText() {
        doWork {
            val lang = profileUsecase.getLanguage()

            if (AppConstants.Languages.kz.langCodeLocale.equals(lang)) {
                ldSetLanguageText.postValue(
                    resourceManager.getString(R.string.label_lang_kz)
                )
            } else {
                ldSetLanguageText.postValue(
                    resourceManager.getString(R.string.label_lang_ru)
                )
            }
        }
    }
}