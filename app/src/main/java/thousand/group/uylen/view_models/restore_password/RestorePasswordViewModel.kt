package thousand.group.uylen.view_models.restore_password

import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.utils.RemoteBuilder
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager

class RestorePasswordViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldOpenLoginPage = OnceLiveData<Unit>()

    private var phone = ""
    private var token = ""

    private val params = mutableMapOf<String, RequestBody>()

    init {
        parseArgs()
    }

    fun onReadyBtnClicked(password: String, repeatedPassword: String) {
        val pass = password.trim()
        val repeatedPass = repeatedPassword.trim()

        checkData(pass, repeatedPass) {
            createParams(pass)

            doWork {
                authUsecase.resetPassword("${RemoteBuilder.AUTH_PREFIX} $token", params)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.showThrowableMessage(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                        showMessageSuccess(R.string.success_reset_password)

                        doWorkAfterSomeTime(1000L) {
                            ldOpenLoginPage.call()
                        }
                    }
            }
        }

    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getString(AppConstants.BundleConstants.PHONE_NUMBER)?.apply {
                phone = this
            }
            getString(AppConstants.BundleConstants.TOKEN)?.apply {
                token = this
            }
        }
    }

    private fun checkData(
        pass: String,
        repeatedPass: String,
        isSuccess: () -> Unit
    ) {
        when {
            phone.isNullOrEmpty() -> showMessageError(R.string.error_phone)
            pass.isEmpty() || repeatedPass.isEmpty() || !pass.equals(repeatedPass) -> {
                showMessageError(R.string.error_passwords_invalid)
            }
            else -> isSuccess.invoke()
        }
    }

    private fun createParams(
        password: String
    ) {
        params.clear()

        params.put(RemoteConstants.PHONE, RequestBodyHelper.getRequestBodyText(phone))
        params.put(
            RemoteConstants.PASSWORD,
            RequestBodyHelper.getRequestBodyText(password)
        )
    }

}