package thousand.group.uylen.view_models.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.yuyakaido.android.cardstackview.Direction
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.CardUser
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.Paginator
import thousand.group.uylen.utils.system.ResourceManager

class HomeViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val likesUsecase: LikesUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetList = MutableLiveData<Pair<MutableList<UserCardData>, Int>>()
    val ldSetItem = MutableLiveData<Pair<UserCardData, Int>>()
    val ldSetListEmptyParams = MutableLiveData<Boolean>()
    val ldSetAdapter = MutableLiveData<Int>()

    val ldOpenDetailPage = OnceLiveData<Pair<UserCardData, Int>>()
    val ldOpenNewPairPage = OnceLiveData<Pair<UserCardData, User>>()
    val ldReplyCard = OnceLiveData<Unit>()
    val ldCancelCard = OnceLiveData<Unit>()
    val ldLikeCard = OnceLiveData<Unit>()
    val ldSuperLikeCard = OnceLiveData<Unit>()
    val ldCancelModeOnFabs = OnceLiveData<Unit>()
    val ldActivateSuperLikeFab = OnceLiveData<Unit>()
    val ldCancelSuperLikeFab = OnceLiveData<Unit>()
    val ldActivateCancelFab = OnceLiveData<Unit>()
    val ldCancelCancelFab = OnceLiveData<Unit>()
    val ldActivateLikeFab = OnceLiveData<Unit>()
    val ldCancelLikeFab = OnceLiveData<Unit>()
    val ldActivateRewindFab = OnceLiveData<Unit>()
    val ldACancelRewindFab = OnceLiveData<Unit>()
    val ldCancelAllActivation = OnceLiveData<Unit>()

    private var currentDirection: Direction? = null

    private val cardList = mutableListOf<UserCardData>()

    private var topPosition = 0

    private var cancelCardBlocked = false
    private var fromSignUpPage = false

    private var params = mutableMapOf<String, RequestBody>()

    private val paginator = Paginator.Store<UserCardData>()

    init {
        parseArgs()

        setUnitMessageReceivedListener { key ->
            when (key) {
                AppConstants.MessageKeysConstants.CARD_CANCEL -> {
                    cancelCard(topPosition)
                }
                AppConstants.MessageKeysConstants.CARD_LIKE -> {
                    likeCard(topPosition)
                }
                AppConstants.MessageKeysConstants.CARD_STAR -> {
                    superLikeCard(topPosition)
                }
                AppConstants.MessageKeysConstants.CARD_BAN_COMPLAIN -> {
                    cancelCardBlocked = true

                    cancelCard(topPosition)
                }
                AppConstants.MessageKeysConstants.HOME_PAGE_UPDATE -> {
                    refreshPage()
                }
            }
        }

        paginator.render = {
            renderPaginationState(it)
        }

        doWork {
            paginator.sideEffects.consumeEach { effect ->
                when (effect) {
                    is Paginator.SideEffect.LoadPage -> loadNewPage(effect.currentPage)

                    is Paginator.SideEffect.ErrorEvent -> {
                        parsePaginatorError(effect.error)
                    }
                }
            }
        }

        refreshPage()

    }

    fun parseArgs() {
        paramsContainer?.apply {
            getBoolean(AppConstants.BundleConstants.FROM_SIGN_UP_PAGE)?.apply {
                fromSignUpPage = this
            }
        }
    }

    fun replyCard(topPosition: Int) {
        Log.i(vmTag, "replyCard -> topPosition:$topPosition")
        Log.i(vmTag, "replyCard -> cardSize:$${cardList.size}")

        if (topPosition != 0) {

            doWork {
                likesUsecase.replyLikedUser(cardList.get(topPosition.dec()).id)
                    .catch { it.printStackTrace() }
                    .collect {

                        this@HomeViewModel.topPosition = topPosition.dec()

                        ldReplyCard.call()
                        ldActivateRewindFab.call()

                        ldSetListEmptyParams.postValue(topPosition.dec() == cardList.size)
                    }
            }
        }
    }

    fun cancelCard(topPosition: Int) {
        Log.i(vmTag, "cancelCard -> topPosition:$topPosition")
        Log.i(vmTag, "cancelCard -> List size:${cardList.size}")

        if (topPosition == cardList.size) return

        ldCancelCard.call()
        ldActivateCancelFab.call()
        currentDirection = Direction.Left
    }

    fun likeCard(topPosition: Int) {
        Log.i(vmTag, "likeCard -> topPosition:$topPosition")
        if (topPosition == cardList.size) return

        ldLikeCard.call()
        ldActivateLikeFab.call()
        currentDirection = Direction.Right
    }

    fun superLikeCard(topPosition: Int) {
        Log.i(vmTag, "superLikeCard -> topPosition:$topPosition")

        if (topPosition == cardList.size) return

        ldSuperLikeCard.call()
        ldActivateSuperLikeFab.call()
        currentDirection = Direction.Top
    }

    fun onLeftPlaneClicked(model: UserCardData, pos: Int) {
        model.activePhotoLinkPos =
            if (model.activePhotoLinkPos == 0) 0 else model.activePhotoLinkPos.dec()

        cardList.set(pos, model)

        ldSetItem.postValue(Pair(model, pos))
    }

    fun onRightPlaneClicked(model: UserCardData, pos: Int) {
        model.activePhotoLinkPos =
            if (model.activePhotoLinkPos == model.images.size - 1) model.images.size - 1 else model.activePhotoLinkPos.inc()

        cardList.set(pos, model)

        ldSetItem.postValue(Pair(model, pos))
    }

    fun onBottomPlaneClicked(model: UserCardData, pos: Int) {
        topPosition = pos
        ldOpenDetailPage.postValue(Pair(model, pos))
    }

    fun parseCardSwipeDirection(direction: Direction) {

        doWork {

            currentDirection?.apply {
                if (!this.equals(direction)) {
                    ldCancelAllActivation.call()
                }

            }

            when (direction) {
                Direction.Top -> {
                    ldActivateSuperLikeFab.call()
                    ldCancelLikeFab.call()
                    ldCancelCancelFab.call()

                    currentDirection = Direction.Top
                }
                Direction.Left -> {
                    ldCancelSuperLikeFab.call()
                    ldCancelLikeFab.call()
                    ldActivateCancelFab.call()
                    currentDirection = Direction.Left
                }
                Direction.Right -> {
                    ldCancelSuperLikeFab.call()
                    ldActivateLikeFab.call()
                    ldCancelCancelFab.call()
                    currentDirection = Direction.Right
                }
                else -> {
                    ldCancelAllActivation.call()
                    currentDirection = null
                }
            }
        }
    }

    fun paginateOnCardSwiped(topPosition: Int) {
        if (topPosition == cardList.size - 3) {
            this.topPosition = topPosition
            loadNextPage()
        }
    }

    fun onCardSwipe(topPosition: Int, direction: Direction) {
        Log.i(vmTag, "onCardSwipe -> topPOs:${topPosition}, direction:${direction}")

        this.topPosition = topPosition

        when (direction) {
            Direction.Top -> {
                superLikeUser(topPosition)
            }
            Direction.Left -> {
                dislikeUser(topPosition)
            }
            Direction.Right -> {
                likeUser(topPosition)
            }
        }
    }

    fun setAdapter() {
        ldSetAdapter.postValue(topPosition)
    }

    private fun dislikeUser(topPosition: Int) {
        if (cancelCardBlocked) {
            cancelCardBlocked = false

            ldSetListEmptyParams.postValue(topPosition == cardList.size)
        } else {
            doWork {
                params.clear()

                params.put(
                    RemoteConstants.TO_USER_ID,
                    RequestBodyHelper.getRequestBodyText(cardList.get(topPosition.dec()).id)
                )
                params.put(
                    RemoteConstants.STATUS,
                    RequestBodyHelper.getRequestBodyText(2)
                )

                likesUsecase.setLikeStatus(params)
                    .onStart { showProgressBar(topPosition == cardList.size) }
                    .onCompletion { showProgressBar(false) }
                    .catch { it.printStackTrace() }
                    .collect {
                        ldSetListEmptyParams.postValue(topPosition == cardList.size)
                    }
            }
        }
    }

    private fun likeUser(topPosition: Int) {
        doWork {
            val user = cardList.get(topPosition.dec())
            params.clear()

            params.put(
                RemoteConstants.TO_USER_ID,
                RequestBodyHelper.getRequestBodyText(cardList.get(topPosition.dec()).id)
            )
            params.put(
                RemoteConstants.STATUS,
                RequestBodyHelper.getRequestBodyText(1)
            )

            likesUsecase.setLikeStatus(params)
                .onStart { showProgressBar(topPosition == cardList.size) }
                .onCompletion { showProgressBar(false) }
                .catch { it.printStackTrace() }
                .collect {
                    if (it.matched == 1) {
                        authUsecase.getUserModel()?.apply {
                            ldOpenNewPairPage.postValue(Pair(user, this))
                        }
                    }

                    ldSetListEmptyParams.postValue(topPosition == cardList.size)
                }
        }
    }

    private fun superLikeUser(topPosition: Int) {
        doWork {
            val user = cardList.get(topPosition.dec())
            params.clear()

            params.put(
                RemoteConstants.TO_USER_ID,
                RequestBodyHelper.getRequestBodyText(user.id)
            )
            params.put(
                RemoteConstants.SUPER_LIKE,
                RequestBodyHelper.getRequestBodyText(1)
            )

            likesUsecase.setLikeStatus(params)
                .onStart { showProgressBar(topPosition == cardList.size) }
                .onCompletion { showProgressBar(false) }
                .catch { it.printStackTrace() }
                .collect {
                    if (it.matched == 1) {
                        authUsecase.getUserModel()?.apply {
                            ldOpenNewPairPage.postValue(Pair(user, this))
                        }
                    }

                    ldSetListEmptyParams.postValue(topPosition == cardList.size)
                }
        }
    }

    private fun refreshPage() = paginator.proceed(Paginator.Action.Refresh)

    private fun loadNextPage() = paginator.proceed(Paginator.Action.LoadMore)

    private suspend fun loadNewPage(page: Int) {
        doWork {
            likesUsecase.getLikeUsers(page)
                .catch { e ->
                    paginator.proceed(Paginator.Action.PageError(e))
                }
                .collect {
                    paginator.proceed(Paginator.Action.NewPage(page, it))
                }
        }
    }

    private fun renderPaginationState(state: Paginator.State) {
        when (state) {
            is Paginator.State.Empty -> {
                Log.i(vmTag, "renderPaginationState -> state -> Empty")

                ldSetListEmptyParams.postValue(true)
//                ldShowBottomProgress.postValue(false)
/*
                sendLocaleMessage(
                    HomeViewModel::class,
                    AppConstants.MessageKeysConstants.SWIPE_REFRESH_MESSAGE, false
                )*/
                showProgressBar(false)
            }
            is Paginator.State.EmptyProgress -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyProgress")

                ldSetListEmptyParams.postValue(false)
                showProgressBar(true)
            }
            is Paginator.State.EmptyError -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyError")

                parsePaginatorError(state.error)
            }
            is Paginator.State.Data<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Data")

                setDataItemsOnRenderView(state)
            }
            is Paginator.State.Refresh<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Refresh")

/*                sendLocaleMessage(
                    HomeViewModel::class,
                    AppConstants.MessageKeysConstants.SWIPE_REFRESH_MESSAGE, true
                )*/
            }
            is Paginator.State.NewPageProgress<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> NewPageProgress")

//                ldShowBottomProgress.postValue(true)
            }
            is Paginator.State.FullData<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> FullData")

                setDataItemsOnRenderView(state)
            }
        }
    }

    private fun parsePaginatorError(error: Throwable?) {
        error?.apply {
            showMessageError(
                ResErrorHelper.showThrowableMessage(
                    resourceManager,
                    vmTag,
                    error
                )
            )

//            ldShowBottomProgress.postValue(false)
            ldSetListEmptyParams.postValue(true)
            showProgressBar(false)

/*            sendLocaleMessage(
                HomeViewModel::class,
                AppConstants.MessageKeysConstants.SWIPE_REFRESH_MESSAGE,
                false
            )*/
        }
    }

    private fun setDataItemsOnRenderView(
        state: Paginator.State
    ) {
        Log.i(vmTag, "renderPaginationState -> state -> setDataItemsOnRenderView")

        val receivedList = when (state) {
            is Paginator.State.Data<*> -> state.data
            is Paginator.State.FullData<*> -> state.data
            else -> null
        }

        receivedList?.let { list ->

            cardList.clear()
            cardList.addAll(list as MutableList<UserCardData>)

            if (fromSignUpPage) {
                val cardItem = cardList.get(0)
                cardItem.hintMode = 1

                cardList.set(0, cardItem)
                fromSignUpPage = false
            }

            ldSetList.postValue(Pair(cardList, topPosition))
            ldSetListEmptyParams.postValue(cardList.isEmpty())
        }

//        ldShowBottomProgress.postValue(false)
        showProgressBar(false)
        /*sendLocaleMessage(
            HomeViewModel::class,
            AppConstants.MessageKeysConstants.SWIPE_REFRESH_MESSAGE, false
        )*/
    }

}