package thousand.group.uylen.view_models.profile

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import thousand.group.data.entities.remote.simple.Tariff
import thousand.group.data.entities.remote.simple.TariffBanner
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.DateParser
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.home.HomeViewModel
import timber.log.Timber
import java.util.*

class ProfileViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val profileUsecase: ProfileUsecase,
    private val likesUsecase: LikesUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetProfileData = MutableLiveData<ParamsContainer>()
    val ldSetTariffVisibleParams = MutableLiveData<Boolean>()
    val ldSetTarifExistParams = MutableLiveData<ParamsContainer>()
    val ldSetTarifDesc = MutableLiveData<Pair<String, String>>()

    val ldSetBannerList = OnceLiveData<MutableList<TariffBanner>>()

    private var user: User? = null
    private var tariff: Tariff? = null

    private var bannerSelectedPosition = 0

    init {
        setUnitMessageReceivedListener {
            when (it) {
                AppConstants.MessageKeysConstants.UPDATE_PROFILE -> {
                    getProfile()

                    sendLocaleCallback(
                        HomeViewModel::class,
                        AppConstants.MessageKeysConstants.HOME_PAGE_UPDATE
                    )
                }

            }
        }

        getProfile()
    }

    fun onViewPagerPositionChanged(pos: Int) {
        tariff?.apply {
            bannerSelectedPosition = pos

            if (this.banner.isNotEmpty()) {
                val banner = this.banner.get(pos)

                ldSetTarifDesc.postValue(Pair(banner.title, banner.description))
            }
        }
    }

    private fun getProfile() {
        doWork {
            profileUsecase.getProfile()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    authUsecase.saveUserModel(it)
                    authUsecase.setUserId(it.id)

                    user = it

                    parseProfileData()
                }
        }
    }

    private fun parseProfileData() {
        user?.apply {
            val params = ParamsContainer()
            val image = RemoteMainConstants.SERVER_URL + avatar
            val ageCity = String.format(
                resourceManager.getString(R.string.format_age_city),
                getAge(details?.date_of_birth),
                details?.city_id?.name
            )

            params.putString(AppConstants.BundleConstants.PHOTO, image)
            params.putString(AppConstants.BundleConstants.NAME, name)
            params.putString(AppConstants.BundleConstants.AGE, ageCity)

            ldSetProfileData.postValue(params)

        }
    }

    private fun getAge(birthdateText: String?): String {
        if (birthdateText != null) {
            val birthdate =
                DateParser.stringToDate(birthdateText, DateParser.format_YYYY_MM_dd).time
            val currentDate = Date().time

            val diff = currentDate - birthdate
            val diffCount = Calendar.getInstance()
            diffCount.timeInMillis = diff

            val year = diffCount.get(Calendar.YEAR) - 1970

            return year.toString()
        } else {
            return ""
        }
    }

   /* private fun getMyTariff() {
        doWork {
            likesUsecase
                .getMyTariff()
                .catch { it.printStackTrace() }
                .collect {
                    Timber.i("getMyTariff -> tariff:${it}")

                    val tarifIsNotNull = it != null

                    ldSetTariffVisibleParams.postValue(tarifIsNotNull)

                    tariff = it?.tariff

                    if (tarifIsNotNull) {
                        parseTarifNotNullParams()
                    } else {
                        parseTarifNullParams()
                    }
                }
        }
    }*/

    private fun parseTarifNotNullParams() {
        tariff?.apply {
            ldSetBannerList.postValue(this.banner)

            onViewPagerPositionChanged(0)
        }
    }

    private fun parseTarifNullParams() {
    }

}