package thousand.group.uylen.view_models.favourite

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.LikedMeUser
import thousand.group.data.entities.remote.simple.SearchUser
import thousand.group.data.entities.remote.simple.TariffUsersList
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.search_users.SearchUsersUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.BitmapHelper
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.Paginator
import thousand.group.uylen.utils.system.ResourceManager
import java.io.ByteArrayOutputStream

class FavouriteViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val searchUsersUsecase: SearchUsersUsecase,
    private val utilUsecase: UtilUsecase,
    private val bitmapHelper: BitmapHelper
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetEmptyListParams = MutableLiveData<Boolean>()
    val ldSetAdapter = MutableLiveData<Unit>()
    val ldSetRefreshState = MutableLiveData<Boolean>()
    val ldShowBottomProgress = MutableLiveData<Boolean>()

    val ldSetUsersList = OnceLiveData<MutableList<LikedMeUser>>()
    val ldOpenDetailPage = OnceLiveData<Pair<Long, Int>>()
    val ldDeleteItem = OnceLiveData<Int>()

    private var firstActive = true

    private var typeLike =
        RequestBodyHelper.getRequestBodyText(resourceManager.getString(R.string.field_like))

    private val paginator = Paginator.Store<LikedMeUser>()

    init {
        setIntMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.DELETE_WHO_LIKES_ITEM -> {
                    refreshPage()
                }
            }
        }

        setUnitMessageReceivedListener {
            when (it) {
                AppConstants.MessageKeysConstants.UPDATE_FAVOURITE -> {
                    refreshPage()
                }
            }
        }

        checkToken(authUsecase) {
            paginator.render = {
                renderPaginationState(it)
            }

            doWork {
                paginator.sideEffects.consumeEach { effect ->
                    when (effect) {
                        is Paginator.SideEffect.LoadPage -> loadNewPage(effect.currentPage)

                        is Paginator.SideEffect.ErrorEvent -> {
                            parsePaginatorError(effect.error)
                        }
                    }
                }
            }

            refreshPage()
        }

    }

    fun onBottomReached() {
        loadNextPage()
    }

    fun onUserItemClicked(user: LikedMeUser, pos: Int) {
        ldOpenDetailPage.postValue(Pair(user.id, pos))
    }

    fun setStatusReadNotification() {
        readNotification(typeLike)
    }

    fun onRefreshSwiped() {
        refreshPage()
    }

    private fun readNotification(type: RequestBody) {
        doWork {
            utilUsecase.updateNotification(type)
                .catch { it.printStackTrace() }
                .collect()
        }
    }

    private fun refreshPage() = paginator.proceed(Paginator.Action.Refresh)

    private fun loadNextPage() = paginator.proceed(Paginator.Action.LoadMore)

    private suspend fun loadNewPage(page: Int) {
        doWork {
            searchUsersUsecase.getWhoLikesMe(page)
                .catch { e ->
                    paginator.proceed(Paginator.Action.PageError(e))
                }
                .collect {
                    paginator.proceed(Paginator.Action.NewPage(page, it.likes))
                }
        }
    }

    private fun renderPaginationState(state: Paginator.State) {
        when (state) {
            is Paginator.State.Empty -> {
                Log.i(vmTag, "renderPaginationState -> state -> Empty")

                ldSetEmptyListParams.postValue(true)
                ldShowBottomProgress.postValue(false)

                ldSetRefreshState.postValue(false)
                showProgressBar(false)
            }
            is Paginator.State.EmptyProgress -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyProgress")

                ldSetEmptyListParams.postValue(false)
                showProgressBar(true)
            }
            is Paginator.State.EmptyError -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyError")

                parsePaginatorError(state.error)
            }
            is Paginator.State.Data<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Data")

                setDataItemsOnRenderView(state)
            }
            is Paginator.State.Refresh<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Refresh")

                ldSetRefreshState.postValue(true)
            }
            is Paginator.State.NewPageProgress<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> NewPageProgress")

                ldShowBottomProgress.postValue(true)
            }
            is Paginator.State.FullData<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> FullData")

                setDataItemsOnRenderView(state)
            }
        }
    }

    private fun parsePaginatorError(error: Throwable?) {
        error?.apply {
            showMessageError(
                ResErrorHelper.showThrowableMessage(
                    resourceManager,
                    vmTag,
                    error
                )
            )

            ldShowBottomProgress.postValue(false)
            ldSetEmptyListParams.postValue(true)
            ldSetRefreshState.postValue(false)

            showProgressBar(false)
        }
    }

    private fun setDataItemsOnRenderView(
        state: Paginator.State
    ) {
        Log.i(vmTag, "renderPaginationState -> state -> setDataItemsOnRenderView")

        val receivedList = when (state) {
            is Paginator.State.Data<*> -> state.data
            is Paginator.State.FullData<*> -> state.data
            else -> null
        }

        receivedList?.let { list ->

            ldSetUsersList.postValue(list as MutableList<LikedMeUser>)

            ldSetEmptyListParams.postValue(list.isEmpty())
        }

        ldShowBottomProgress.postValue(false)
        ldSetRefreshState.postValue(false)
        showProgressBar(false)
    }


}