package thousand.group.uylen.view_models.home

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.Reaction
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.DateParser
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import java.text.DecimalFormat
import java.util.*

class HomeDetailViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val likesUsecase: LikesUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetCurrentImage = MutableLiveData<Triple<String, Int, Int>>()
    val ldSetTextParams = MutableLiveData<ParamsContainer>()
    val ldSetEmptyList = MutableLiveData<Boolean>()

    val ldSetInterestList = OnceLiveData<MutableList<Interest>>()
    val ldSetReactionsList = OnceLiveData<MutableList<Reaction>>()
    val ldClosePage = OnceLiveData<Unit>()
    val ldShowComplainReasonMenu = OnceLiveData<Unit>()

    private lateinit var user: UserCardData
    private var selPos = 0

    private val formatter = DecimalFormat("#,###")

    private val reactionsList = mutableListOf<Reaction>()

    init {
        parseArgs()
        setReactionsList()
        setInterestsList()
        setPhotos()
        setTextParams()
    }

    fun onLeftPlaneClicked() {
        user.activePhotoLinkPos =
            if (user.activePhotoLinkPos == 0) 0 else user.activePhotoLinkPos.dec()

        setPhotos()
    }

    fun onRightPlaneClicked() {
        user.activePhotoLinkPos =
            if (user.activePhotoLinkPos == user.images.size - 1) user.images.size - 1 else user.activePhotoLinkPos.inc()

        setPhotos()
    }

    fun onReactionClicked(model: Reaction, pos: Int) {
        doWork {
            val userIdPart = RequestBodyHelper.getRequestBodyText(user.id)
            val reactionIdPart = RequestBodyHelper.getRequestBodyText(model.id)

            likesUsecase.sendReaction(userIdPart, reactionIdPart)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    showMessageSuccess(R.string.success_rating_send)
                }
        }
    }

    fun onCancelBtnClicked() {
        sendBackResponse(AppConstants.MessageKeysConstants.CARD_CANCEL)
    }

    fun onLikeBtnClicked() {
        sendBackResponse(AppConstants.MessageKeysConstants.CARD_LIKE)
    }

    fun onStarBtnClicked() {
        sendBackResponse(AppConstants.MessageKeysConstants.CARD_STAR)
    }

    fun onComplainMenuItemSelected(pos: Int) {
        //0-Заблокировать
        //1-Пожаловаться

        if (pos == 0) {
            doWork {
                likesUsecase.banAccount(RequestBodyHelper.getRequestBodyText(user.id))
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                        sendBackResponse(AppConstants.MessageKeysConstants.CARD_BAN_COMPLAIN)
                    }
            }
        } else {
            ldShowComplainReasonMenu.call()
        }
    }

    fun onComplainReasonMenuItemClicked(pos: Int) {
        doWork {
            likesUsecase.complainAccount(RequestBodyHelper.getRequestBodyText(user.id))
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    sendBackResponse(AppConstants.MessageKeysConstants.CARD_BAN_COMPLAIN)
                }
        }
    }

    private fun sendBackResponse(key: String) {
        sendLocaleCallback(
            HomeViewModel::class,
            key
        )

        ldClosePage.call()
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getParcelable<UserCardData>(AppConstants.BundleConstants.USER)?.apply {
                user = this
            }
            getInt(AppConstants.BundleConstants.SELECTED_POS)?.apply {
                selPos = this
            }
        }
    }

    private fun setPhotos() {
        doWork {
            val currentImage =
                RemoteMainConstants.SERVER_URL + if (user.images.isNotEmpty()) user.images.get(user.activePhotoLinkPos).images else null

            ldSetCurrentImage.postValue(
                Triple(
                    currentImage,
                    user.images.size,
                    user.activePhotoLinkPos
                )
            )
        }
    }

    private fun setTextParams() {
        doWork {
            val params = ParamsContainer()
            val age = getAge(user.details?.date_of_birth)
            val city = user.details?.city_id?.name.toString()
            val gender =
                if (user.details?.sex == 9) resourceManager.getString(
                    R.string.label_male
                ) else resourceManager.getString(R.string.label_female)

            val birthdateText = DateParser.convertOneFormatToAnother(
                user.details?.date_of_birth,
                DateParser.format_YYYY_MM_dd,
                DateParser.format_dd_LLLL_YYYY
            )

            val salary =
                if (user.about?.salary != null && user.about?.salary?.toLongOrNull() != 0L) getFormattedAmount(
                    user.about!!.salary!!.toLong()
                ) else getEmptySymbol()

            params.putString(
                AppConstants.BundleConstants.SALARY,
                salary
            )

            val ruName =
                if (user.details?.gener_id != null)
                    user.details?.gener_id?.name.toString()
                else getEmptySymbol()

            val maritalStatus =
                if (!user.details?.marital_status.isNullOrEmpty()) user.details?.marital_status.toString() else getEmptySymbol()

            val weightStatus = if (user.details?.weight != null && user.details?.weight != 0)
                String.format(
                    resourceManager.getString(R.string.format_weight_kg),
                    user.details?.weight
                ) else getEmptySymbol()

            val heightStatus = if (user.details?.height != null && user.details?.height != 0)
                String.format(
                    resourceManager.getString(R.string.format_height_sm),
                    user.details?.height
                ) else getEmptySymbol()

            val zodiacStatus =
                if (!user.details?.zodiac.isNullOrEmpty()) user.details?.zodiac.toString() else getEmptySymbol()

            val badHabitsStatus =
                if (user.details?.bad_habits != null) user.details?.bad_habits.toString() else getEmptySymbol()

            val kalynMalStatus =
                if (user.details?.kalyn_male != null && user.details?.kalyn_male != 0L) getFormattedAmount(
                    user.details?.kalyn_male!!
                ) else getEmptySymbol()

            params.putString(AppConstants.BundleConstants.NAME, "${user.name}, $age")
            params.putString(AppConstants.BundleConstants.ABOUT, user.about?.text.toString())
            params.putString(AppConstants.BundleConstants.CITY, city)
            params.putString(
                AppConstants.BundleConstants.RU,
                ruName
            )
            params.putString(
                AppConstants.BundleConstants.FAMILY_STATUS,
                maritalStatus
            )
            params.putString(
                AppConstants.BundleConstants.GENDER,
                gender
            )
            params.putString(
                AppConstants.BundleConstants.BIRTHDATE,
                String.format(resourceManager.getString(R.string.format_birthdate), birthdateText)
            )
            params.putString(
                AppConstants.BundleConstants.WEIGHT,
                weightStatus
            )
            params.putString(
                AppConstants.BundleConstants.HEIGHT,
                heightStatus
            )
            params.putString(
                AppConstants.BundleConstants.ZODIAC,
                zodiacStatus
            )
            params.putString(
                AppConstants.BundleConstants.BAD_HABITS,
                badHabitsStatus
            )
            params.putString(
                AppConstants.BundleConstants.KALYM,
                kalynMalStatus
            )
            params.putBoolean(
                AppConstants.BundleConstants.KALYM_VISIBILITY,
                user.details?.sex == 10
            )

            ldSetTextParams.postValue(params)
        }
    }

    private fun getFormattedAmount(sum: Long): String {
        val sumStr =
            formatter.format(sum)
                .replace(",", " ")

        return String.format(
            resourceManager.getString(R.string.format_price),
            sumStr
        )
    }

    private fun setInterestsList() {
        doWork {
            ldSetInterestList.postValue(user.interests)
            ldSetEmptyList.postValue(user.interests.isEmpty())
        }
    }

    private fun getAge(birthdateText: String?): String {
        if (birthdateText != null) {
            val birthdate =
                DateParser.stringToDate(birthdateText, DateParser.format_YYYY_MM_dd).time
            val currentDate = Date().time

            val diff = currentDate - birthdate
            val diffCount = Calendar.getInstance()
            diffCount.timeInMillis = diff

            val year = diffCount.get(Calendar.YEAR) - 1970

            return year.toString()
        } else {
            return ""
        }
    }

    private fun setReactionsList() {
        doWork {
            likesUsecase.getReactions()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    reactionsList.clear()
                    reactionsList.addAll(it)

                    ldSetReactionsList.postValue(reactionsList)
                }
        }
    }

    private fun getEmptySymbol() = resourceManager.getString(
        R.string.field_empty_symbol
    )
}