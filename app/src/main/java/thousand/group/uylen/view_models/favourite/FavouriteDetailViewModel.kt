package thousand.group.uylen.view_models.favourite

import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.Reaction
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.domain.usecase.search_users.SearchUsersUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.DateParser
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.home.HomeViewModel
import java.text.DecimalFormat
import java.util.*


class FavouriteDetailViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val likesUsecase: LikesUsecase,
    private val searchUsersUsecase: SearchUsersUsecase,
    private val profileUsecase: ProfileUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetCurrentImage = MutableLiveData<Triple<String, Int, Int>>()
    val ldSetTextParams = MutableLiveData<ParamsContainer>()
    val ldSetEmptyList = MutableLiveData<Boolean>()
    val ldShowBtns = MutableLiveData<Boolean>()

    val ldSetInterestList = OnceLiveData<MutableList<Interest>>()
    val ldSetReactionsList = OnceLiveData<MutableList<Reaction>>()
    val ldClosePage = OnceLiveData<Unit>()
    val ldOpenNewPairPage = OnceLiveData<Pair<UserCardData, User>>()
    val ldOpenChatListPage = OnceLiveData<Unit>()
    val ldShowComplainReasonMenu = OnceLiveData<Unit>()

    private var userId = 0L
    private var selPos = 0

    private var showBtn = true

    private var params = mutableMapOf<String, RequestBody>()

    private var user: UserCardData? = null

    private val formatter = DecimalFormat("#,###")

    private val reactionsList = mutableListOf<Reaction>()

    init {
        parseArgs()

        getUser()
    }

    fun onLeftPlaneClicked() {
        user?.apply {
            activePhotoLinkPos =
                if (activePhotoLinkPos == 0) 0 else activePhotoLinkPos.dec()

            setPhotos()
        }
    }

    fun onRightPlaneClicked() {
        user?.apply {
            activePhotoLinkPos =
                if (activePhotoLinkPos == images.size - 1) images.size - 1 else activePhotoLinkPos.inc()

            setPhotos()
        }
    }

    fun onReactionClicked(model: Reaction, pos: Int) {
        doWork {
            val userIdPart = RequestBodyHelper.getRequestBodyText(user?.id)
            val reactionIdPart = RequestBodyHelper.getRequestBodyText(model.id)

            likesUsecase.sendReaction(userIdPart, reactionIdPart)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    showMessageSuccess(R.string.success_rating_send)
                }
        }
    }

    fun onComplainMenuItemSelected(pos: Int) {
        //0-Заблокировать
        //1-Пожаловаться

        if (pos == 0) {
            doWork {
                likesUsecase.banAccount(RequestBodyHelper.getRequestBodyText(user?.id))
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch {
                        showMessageError(
                            ResErrorHelper.parseErrorByStatusCode(
                                resourceManager,
                                vmTag,
                                it
                            )
                        )
                    }
                    .collect {
                    }
            }
        } else {
            ldShowComplainReasonMenu.call()
        }
    }

    fun onComplainReasonMenuItemClicked(pos: Int) {
        doWork {
            likesUsecase.complainAccount(RequestBodyHelper.getRequestBodyText(user?.id))
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                }
        }
    }

    fun onCancelBtnClicked() {
        user?.let { user ->
            doWork {
                params.clear()

                params.put(
                    RemoteConstants.TO_USER_ID,
                    RequestBodyHelper.getRequestBodyText(userId)
                )
                params.put(
                    RemoteConstants.STATUS,
                    RequestBodyHelper.getRequestBodyText(2)
                )

                likesUsecase.setLikeStatus(params)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch { it.printStackTrace() }
                    .collect {
                        sendBackResponse()
                    }
            }
        }
    }

    fun onLikeBtnClicked() {
        user?.let { user ->
            doWork {
                params.clear()

                params.put(
                    RemoteConstants.TO_USER_ID,
                    RequestBodyHelper.getRequestBodyText(userId)
                )
                params.put(
                    RemoteConstants.STATUS,
                    RequestBodyHelper.getRequestBodyText(1)
                )

                likesUsecase.setLikeStatus(params)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch { it.printStackTrace() }
                    .collect {
                        if (it.matched == 1) {
                            authUsecase.getUserModel()?.apply {
                                ldOpenNewPairPage.postValue(Pair(user, this))
                            }
                        }

//                        ldOpenChatListPage.call()
                    }
            }
        }
    }

    fun onSuperLikeBtnClicked() {
        user?.let { user ->

            doWork {
                params.clear()

                params.put(
                    RemoteConstants.TO_USER_ID,
                    RequestBodyHelper.getRequestBodyText(userId)
                )
                params.put(
                    RemoteConstants.SUPER_LIKE,
                    RequestBodyHelper.getRequestBodyText(1)
                )

                likesUsecase.setLikeStatus(params)
                    .onStart { showProgressBar(true) }
                    .onCompletion { showProgressBar(false) }
                    .catch { it.printStackTrace() }
                    .collect {
                        if (it.matched == 1) {
                            authUsecase.getUserModel()?.apply {
                                ldOpenNewPairPage.postValue(Pair(user, this))
                            }
                        }
                    }
            }

        }
    }

    private fun getUser() {
        doWork {
            profileUsecase.getUserById(userId)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    user = it

                    if (showBtn) {
                        authUsecase.getUserModel()?.let { me ->
                            user?.details?.sex?.let { cardGen ->
                                val gender = me.details?.sex

                                ldShowBtns.postValue(!it.chat && gender != cardGen)
                            }
                        }
                    } else {
                        ldShowBtns.postValue(showBtn)
                    }


                    setReactionsList()
                    setInterestsList()
                    setPhotos()
                    setTextParams()
                }
        }
    }

    private fun sendBackResponse() {
        sendLocaleMessage(
            FavouriteViewModel::class,
            AppConstants.MessageKeysConstants.DELETE_WHO_LIKES_ITEM,
            selPos
        )

        ldClosePage.call()
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getLong(AppConstants.BundleConstants.ID)?.apply {
                userId = this
            }
            getInt(AppConstants.BundleConstants.SELECTED_POS)?.apply {
                selPos = this
            }
            getBoolean(AppConstants.BundleConstants.SHOW_BTN)?.apply {
                showBtn = this
            }
        }
    }

    private fun setPhotos() {
        doWork {
            user?.apply {
                val currentImage =
                    RemoteMainConstants.SERVER_URL + if (images.isNotEmpty()) images.get(
                        activePhotoLinkPos
                    ).images else null

                ldSetCurrentImage.postValue(
                    Triple(
                        currentImage,
                        images.size,
                        activePhotoLinkPos
                    )
                )
            }
        }
    }

    private fun setTextParams() {
        doWork {
            user?.apply {
                val params = ParamsContainer()
                val age = getAge(details?.date_of_birth)
                val city = user?.details?.city_id?.name.toString()

                val gender =
                    if (details?.sex == 9) resourceManager.getString(
                        R.string.label_male
                    ) else resourceManager.getString(R.string.label_female)

                val birthdateText = DateParser.convertOneFormatToAnother(
                    details?.date_of_birth,
                    DateParser.format_YYYY_MM_dd,
                    DateParser.format_dd_LLLL_YYYY
                )

                val ruName =
                    if (user?.details?.gener_id != null)
                        user?.details?.gener_id?.name.toString()
                    else getEmptySymbol()

                val maritalStatus =
                    if (!user?.details?.marital_status.isNullOrEmpty()) user?.details?.marital_status.toString() else getEmptySymbol()

                val weightStatus = if (user?.details?.weight != null && user?.details?.weight != 0)
                    String.format(
                        resourceManager.getString(R.string.format_weight_kg),
                        user?.details?.weight
                    ) else getEmptySymbol()

                val heightStatus = if (user?.details?.height != null && user?.details?.height != 0)
                    String.format(
                        resourceManager.getString(R.string.format_height_sm),
                        user?.details?.height
                    ) else getEmptySymbol()

                val zodiacStatus =
                    if (!user?.details?.zodiac.isNullOrEmpty()) user?.details?.zodiac.toString() else getEmptySymbol()

                val badHabitsStatus =
                    if (user?.details?.bad_habits != null) user?.details?.bad_habits.toString() else getEmptySymbol()

                val kalynMalStatus =
                    if (user?.details?.kalyn_male != null && user?.details?.kalyn_male != 0L) getFormattedAmount(
                        details?.kalyn_male!!
                    ) else getEmptySymbol()

                val salary =
                    if (user?.about?.salary != null && user?.about?.salary?.toLongOrNull() != 0L) getFormattedAmount(
                        user?.about!!.salary!!.toLong()
                    ) else getEmptySymbol()

                params.putString(
                    AppConstants.BundleConstants.SALARY,
                    salary
                )

                params.putString(AppConstants.BundleConstants.NAME, "${user?.name}, $age")
//                params.putString(AppConstants.BundleConstants.AGE, age)
                params.putString(AppConstants.BundleConstants.ABOUT, about?.text.toString())
                params.putString(AppConstants.BundleConstants.CITY, city)
                params.putString(
                    AppConstants.BundleConstants.RU,
                    user?.details?.gener_id?.name.toString()
                )
                params.putString(
                    AppConstants.BundleConstants.STATUS,
                    details?.me_status.toString()
                )
                params.putString(
                    AppConstants.BundleConstants.FAMILY_STATUS,
                    details?.marital_status.toString()
                )
                params.putString(
                    AppConstants.BundleConstants.GENDER,
                    gender
                )
                params.putString(
                    AppConstants.BundleConstants.BIRTHDATE,
                    String.format(
                        resourceManager.getString(R.string.format_birthdate),
                        birthdateText
                    )
                )
                params.putString(
                    AppConstants.BundleConstants.WEIGHT,
                    String.format(
                        resourceManager.getString(R.string.format_weight_kg),
                        if (user?.details?.weight != null) user?.details?.weight else 0
                    )
                )
                params.putString(
                    AppConstants.BundleConstants.HEIGHT,
                    String.format(
                        resourceManager.getString(R.string.format_height_sm),
                        if (user?.details?.height != null) user?.details?.height else 0
                    )
                )
                params.putString(
                    AppConstants.BundleConstants.ZODIAC,
                    user?.details?.zodiac.toString()
                )
                params.putString(
                    AppConstants.BundleConstants.BAD_HABITS,
                    user?.details?.bad_habits.toString()
                )
                params.putString(
                    AppConstants.BundleConstants.KALYM,
                    kalynMalStatus
                )
                params.putBoolean(
                    AppConstants.BundleConstants.KALYM_VISIBILITY,
                    user?.details?.sex == 10
                )

                params.putString(
                    AppConstants.BundleConstants.RU,
                    ruName
                )
                params.putString(
                    AppConstants.BundleConstants.FAMILY_STATUS,
                    maritalStatus
                )
                params.putString(
                    AppConstants.BundleConstants.GENDER,
                    gender
                )
                params.putString(
                    AppConstants.BundleConstants.BIRTHDATE,
                    String.format(
                        resourceManager.getString(R.string.format_birthdate),
                        birthdateText
                    )
                )
                params.putString(
                    AppConstants.BundleConstants.WEIGHT,
                    weightStatus
                )
                params.putString(
                    AppConstants.BundleConstants.HEIGHT,
                    heightStatus
                )
                params.putString(
                    AppConstants.BundleConstants.ZODIAC,
                    zodiacStatus
                )
                params.putString(
                    AppConstants.BundleConstants.BAD_HABITS,
                    badHabitsStatus
                )
                params.putString(
                    AppConstants.BundleConstants.KALYM,
                    kalynMalStatus
                )
                params.putBoolean(
                    AppConstants.BundleConstants.KALYM_VISIBILITY,
                    user?.details?.sex == 10
                )

                ldSetTextParams.postValue(params)

            }
        }
    }

    private fun getFormattedAmount(sum: Long): String {
        val sumStr =
            formatter.format(sum)
                .replace(",", " ")

        return String.format(
            resourceManager.getString(R.string.format_price),
            sumStr
        )
    }

    private fun setInterestsList() {
        doWork {
            user?.apply {
                ldSetInterestList.postValue(interests)
                ldSetEmptyList.postValue(interests.isEmpty())
            }
        }
    }

    private fun getAge(birthdateText: String?): String {
        if (birthdateText != null) {
            val birthdate =
                DateParser.stringToDate(birthdateText, DateParser.format_YYYY_MM_dd).time
            val currentDate = Date().time

            val diff = currentDate - birthdate
            val diffCount = Calendar.getInstance()
            diffCount.timeInMillis = diff

            val year = diffCount.get(Calendar.YEAR) - 1970

            return year.toString()
        } else {
            return ""
        }
    }

    private fun setReactionsList() {
        doWork {
            likesUsecase.getReactions()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    reactionsList.clear()
                    reactionsList.addAll(it)

                    ldSetReactionsList.postValue(reactionsList)
                }
        }
    }

    private fun getEmptySymbol() = resourceManager.getString(
        R.string.field_empty_symbol
    )

}