package thousand.group.uylen.view_models.chat

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import thousand.group.data.entities.remote.simple.ChatListItem
import thousand.group.data.entities.remote.simple.MatchedUser
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.chat.ChatUsecase
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.helpers.BitmapHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.Paginator
import thousand.group.uylen.utils.system.ResourceManager
import java.io.ByteArrayOutputStream

class MutualSympathyViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val likesUsecase: LikesUsecase,
    private val chatUsecase: ChatUsecase,
    private val bitmapHelper: BitmapHelper
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetEmptyListParams = MutableLiveData<Boolean>()
    val ldShowSwipeRefresh = MutableLiveData<Boolean>()
    val ldShowBottomProgress = MutableLiveData<Boolean>()

    val ldSetUsersList = OnceLiveData<MutableList<MatchedUser>>()
    val ldOpenChatPage = OnceLiveData<Long>()

    private val paginator = Paginator.Store<MatchedUser>()

    private val usersList = mutableListOf<MatchedUser>()

    private var firstActive = true

    init {
        paginator.render = {
            renderPaginationState(it)
        }

        doWork {
            paginator.sideEffects.consumeEach { effect ->
                when (effect) {
                    is Paginator.SideEffect.LoadPage -> loadNewPage(effect.currentPage)

                    is Paginator.SideEffect.ErrorEvent -> {
                        parsePaginatorError(effect.error)
                    }
                }
            }
        }

        refreshList()
    }

    fun onUserItemClicked(user: MatchedUser, pos: Int) {
        doWork {
            chatUsecase.chatIsRead(user.id)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    ldOpenChatPage.postValue(user.id)
                }
        }
    }

    fun refreshList() {
//        getMatchedUsers()
        refreshPage()
    }

    fun onBottomReached() {
        loadNextPage()
    }

    private fun refreshPage() = paginator.proceed(Paginator.Action.Refresh)

    private fun loadNextPage() = paginator.proceed(Paginator.Action.LoadMore)

    private suspend fun loadNewPage(page: Int) {
        doWork {
            likesUsecase.getMatchedUsers(page)
                .catch { e ->
                    paginator.proceed(Paginator.Action.PageError(e))
                }
                .collect {
                    paginator.proceed(Paginator.Action.NewPage(page, it.first))
                }
        }
    }

    private fun renderPaginationState(state: Paginator.State) {
        when (state) {
            is Paginator.State.Empty -> {
                Log.i(vmTag, "renderPaginationState -> state -> Empty")

                ldSetEmptyListParams.postValue(true)
                ldShowBottomProgress.postValue(false)

                ldShowSwipeRefresh.postValue(false)
                showProgressBar(false)
            }
            is Paginator.State.EmptyProgress -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyProgress")

                ldSetEmptyListParams.postValue(false)
                showProgressBar(true)
            }
            is Paginator.State.EmptyError -> {
                Log.i(vmTag, "renderPaginationState -> state -> EmptyError")

                parsePaginatorError(state.error)
            }
            is Paginator.State.Data<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Data")

                setDataItemsOnRenderView(state)
            }
            is Paginator.State.Refresh<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> Refresh")

                ldShowSwipeRefresh.postValue(true)
            }
            is Paginator.State.NewPageProgress<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> NewPageProgress")

                ldShowBottomProgress.postValue(true)
            }
            is Paginator.State.FullData<*> -> {
                Log.i(vmTag, "renderPaginationState -> state -> FullData")

                setDataItemsOnRenderView(state)
            }
        }
    }

    private fun parsePaginatorError(error: Throwable?) {
        error?.apply {
            showMessageError(
                ResErrorHelper.showThrowableMessage(
                    resourceManager,
                    vmTag,
                    error
                )
            )

            ldShowBottomProgress.postValue(false)
            ldSetEmptyListParams.postValue(true)
            showProgressBar(false)
        }
    }

    private fun setDataItemsOnRenderView(
        state: Paginator.State
    ) {
        Log.i(vmTag, "renderPaginationState -> state -> setDataItemsOnRenderView")

        val receivedList = when (state) {
            is Paginator.State.Data<*> -> state.data
            is Paginator.State.FullData<*> -> state.data
            else -> null
        }

        receivedList?.let { list ->

            usersList.clear()
            usersList.addAll(list as MutableList<MatchedUser>)

            ldSetUsersList.postValue(usersList)

            ldSetEmptyListParams.postValue(usersList.isEmpty())
        }

        ldShowBottomProgress.postValue(false)
        showProgressBar(false)
        ldShowSwipeRefresh.postValue(false)
    }

    /*private fun getMatchedUsers() {
        doWork {
            likesUsecase.getMatchedUsers()
                .onStart {
                    if (firstActive) showProgressBar(true)
                    else ldShowSwipeRefresh.postValue(
                        true
                    )
                }
                .onCompletion {
                    showProgressBar(false)
                    ldShowSwipeRefresh.postValue(false)
                }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    firstActive = false

                    usersList.clear()
                    usersList.addAll(it)

*//*                    usersList.forEach { user ->
                        bitmapHelper.getBitmapSync(
                            Uri.parse(
                                RemoteMainConstants.SERVER_URL + user.images.getOrNull(
                                    0
                                )?.images
                            )
                        ) {
                            it.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream)

                            user.avatarBitmap = it
                        }
                    }*//*

                    ldSetUsersList.postValue(usersList)
                    ldSetEmptyListParams.postValue(usersList.isEmpty())
                }
        }
    }*/

}