package thousand.group.uylen.view_models.registration

import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.entities.remote.responces.LoginResponse
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.utils.RemoteBuilder.AUTH_PREFIX
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.helpers.BitmapHelper
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.locale.RegistrationPhoto
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.auth.AuthViewModel
import java.lang.Exception

class ThirdRegistrationViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val bitmapHelper: BitmapHelper,
    private val authUsecase: AuthUsecase,
    private val profileUsecase: ProfileUsecase,
    private val utilUsecase: UtilUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetCrossVisibility = MutableLiveData<Boolean>()

    val ldShowPhotoSelectDialog = OnceLiveData<Pair<String, String>>()
    val ldSetList = OnceLiveData<MutableList<RegistrationPhoto>>()
    val ldRequestPhotoPermission = OnceLiveData<Unit>()
    val ldSetItem = OnceLiveData<Pair<RegistrationPhoto, Int>>()
    val ldOpenLoginPage = OnceLiveData<Unit>()
    val ldOpenCropPage = OnceLiveData<Uri>()
    val ldOpenMainActivity = OnceLiveData<Unit>()
    val ldOpenRegistrationPage = OnceLiveData<Unit>()

    private val regPhotos = mutableListOf<RegistrationPhoto>()

    private var currentRegPhoto: RegistrationPhoto? = null
    private var currentRegPhotoPos: Int = 0

    private lateinit var user: User

    private lateinit var token: String
    private var oneSignalUserID: String? = null

    private lateinit var location: Pair<Double, Double>

    init {
        setParcelableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.CameraActionConstants.ON_PHOTO_LOADED -> {
                    Log.i(vmTag, "ON_PHOTO_LOADED -> uri: ${message}")

                    createAndSetBitmapPart(
                        message as Uri
                    )
                }
                AppConstants.MessageKeysConstants.CROP_BITMAP -> {
                    (message as Bitmap).apply {
                        setBitmapToRegPhoto(this)
                    }
                }
            }
        }

        setSerializableMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.RESPONSE_LOCATION -> {
                    if (!::location.isInitialized) {
                        location = message as Pair<Double, Double>
                    }
                }
            }
        }

        setUnitMessageReceivedListener { key ->
            when (key) {
                AppConstants.MessageKeysConstants.RESPONSE_LOCATION_ERROR -> {
                    showMessageError(R.string.error_lat_lng)
                }
            }
        }

        parseArgs()

        ldSetCrossVisibility.postValue(authUsecase.tokenTempIsNotEmpty())

        sendLocationRequest()

        authUsecase.setTempAccessToken(token)
        authUsecase.saveTempUserModel(user)

        fillPhotoList()
        ldSetList.postValue(regPhotos)
    }

    fun openPhotoVideoDialogFragment() {
        ldShowPhotoSelectDialog.postValue(
            Pair(
                vmTag,
                AppConstants.CameraActionConstants.MODE_PHOTO
            )
        )
    }

    fun addItemPhoto(model: RegistrationPhoto, pos: Int) {
        currentRegPhoto = model
        currentRegPhotoPos = pos

        ldRequestPhotoPermission.call()
    }

    fun removeItemPhoto(model: RegistrationPhoto, pos: Int) {
        model.photo = null

        ldSetItem.postValue(Pair(model, pos))
    }

    fun onCrossIconClicked() {
        authUsecase.deleteTempToken()
        authUsecase.deleteTempUserModel()

        ldOpenRegistrationPage.call()
    }

    fun onCompleteBtnClicked(aboutText: String) {
        checkParams(aboutText) {
            doWork {
                createParams(aboutText) { part, partList ->
                    sendAboutPart(part) {
                        sendImagesPart(partList) {
                            showMessageSuccess(R.string.success_registration)

                            authUsecase.setAccessToken(token)

                            authUsecase.deleteTempToken()
                            authUsecase.deleteTempUserModel()

                            getProfile()

                        }
                    }
                }
            }
        }
    }

    fun uploadImage(uri: Uri) {
        try {
            bitmapHelper.getBitmapFromUri(
                uri,
                null,
                null
            ) {
                setBitmapToRegPhoto(it)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun setOneSignalUserId(id: String?) {
        this.oneSignalUserID = id

        Log.i(vmTag, "setOneSignalUserId -> oneSignalUserID: ${this.oneSignalUserID}")
    }

    private fun fillPhotoList() {
        for (i in 0 until 6) {
            regPhotos.add(RegistrationPhoto())
        }
    }

    private fun createAndSetBitmapPart(uri: Uri) {
        doWork {
            try {
                showProgressBar(true)
/*
                bitmapHelper.getBitmapFromUri(
                    uri,
                    null,
                    null
                ) {
                    setBitmapToRegPhoto(it)
                }*/

                ldOpenCropPage.postValue(uri)

            } catch (ex: Exception) {
                ex.printStackTrace()
            } finally {
                showProgressBar(false)
            }
        }
    }

    private fun setBitmapToRegPhoto(it: Bitmap) {
        currentRegPhoto?.apply {
            photo = it

            ldSetItem.postValue(Pair(this, currentRegPhotoPos))
        }
    }

    private fun setUserImageAvatar(id: Long) {
        doWork {
            profileUsecase.setUserAvatar(id)
                .catch {
                    it.printStackTrace()
                }
                .collect()
        }
    }

    private fun sendLatLng(lat: Double, longg: Double) {
        doWork {
            utilUsecase.updateLocation(
                RequestBodyHelper.getRequestBodyText(lat),
                RequestBodyHelper.getRequestBodyText(longg)
            )
                .catch { it.printStackTrace() }
                .collect()
        }
    }

    private fun sendLocationRequest() {
        doWorkAfterSomeTime(1000) {
            if (authUsecase.tokenTempIsNotEmpty()) {
                sendLocaleMessage(
                    AuthViewModel::class,
                    AppConstants.MessageKeysConstants.REQUIRE_LOCATION,
                    vmTag
                )
            }
        }
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getParcelable<User>(AppConstants.BundleConstants.USER)?.apply {
                user = this
            }
            getString(AppConstants.BundleConstants.TOKEN)?.apply {
                token = this
            }
            getSerializable(AppConstants.BundleConstants.LOCATION)?.apply {
                location = this as Pair<Double, Double>
            }
        }
    }

    private fun checkParams(aboutText: String, isSuccess: () -> Unit) {
        when {
            aboutText.trim().isEmpty() -> {
                showMessageError(R.string.error_empty_about)
            }
            regPhotos.count { it.photo != null } < 2 -> {
                showMessageError(R.string.error_photos_size_2)
            }
            else -> isSuccess.invoke()
        }
    }

    private fun createParams(
        aboutText: String,
        isCreated: (part: RequestBody, partList: MutableList<MultipartBody.Part>) -> Unit
    ) {
        val aboutPart = RequestBodyHelper.getRequestBodyText(aboutText.trim())
        val parts = mutableListOf<MultipartBody.Part>()
        val imageKey = resourceManager.getString(R.string.format_image_position)

        regPhotos.forEachIndexed { index, registrationPhoto ->
            registrationPhoto.photo?.apply {
                val imageKeyPos = String.format(imageKey, index)
                val imagePart = RequestBodyHelper.getMultipartData(imageKeyPos, this)

                imagePart?.apply {
                    parts.add(this)
                }
            }
        }


        isCreated.invoke(aboutPart, parts)
    }

    private fun sendAboutPart(part: RequestBody, isSuccess: () -> Unit) {
        doWork {
            authUsecase.editAbout("$AUTH_PREFIX $token", part)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.showThrowableMessage(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    isSuccess.invoke()
                }
        }
    }

    private fun sendImagesPart(partList: MutableList<MultipartBody.Part>, isSuccess: () -> Unit) {
        doWork {
            authUsecase.fillPhoto("$AUTH_PREFIX $token", partList)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.showThrowableMessage(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    isSuccess.invoke()

                    setUserImageAvatar(it.get(0).id)
                }
        }
    }

    private fun getProfile() {
        doWork {
            profileUsecase.getProfile()
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    authUsecase.saveUserModel(it)
                    authUsecase.setUserId(it.id)

                    sendLatLng(location.first, location.second)

                    doWorkAfterSomeTime(1000L) {
                        ldOpenMainActivity.call()
                    }

                }
        }
    }
}