package thousand.group.uylen.view_models.profile

import androidx.lifecycle.MutableLiveData
import thousand.group.data.remote.constants.Endpoints
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager

class PrivacyTermsViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val profileUsecase: ProfileUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetWebViewContent = OnceLiveData<String>()
    val ldSetTitle = MutableLiveData<String>()

    private var termsType = ""

    init {
        parseArgs()
        setTitle()
        setContent()
    }

    private fun parseArgs() {
        paramsContainer?.apply {
            getString(AppConstants.BundleConstants.TERMS_TYPE)?.apply {
                termsType = this
            }
        }
    }

    private fun setContent() {
        val content = when (termsType) {
            AppConstants.PrivacyTermMode.TERM_OF_USE -> {
                Endpoints.GET_TERMS_OF_USE
            }
            else -> {
                Endpoints.GET_PRIVACY_POLICY
            }
        }

        ldSetWebViewContent.postValue(content)
    }

    private fun setTitle() {
        val title = when (termsType) {
            AppConstants.PrivacyTermMode.TERM_OF_USE -> {
                resourceManager.getString(R.string.label_terms_of_use)
            }
            else -> {
                resourceManager.getString(R.string.label_privacy_policy)
            }
        }

        ldSetTitle.postValue(title)
    }


}