package thousand.group.uylen.view_models.search

import android.util.Log
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import thousand.group.data.entities.remote.simple.*
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.search_users.SearchUsersUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.uylen.R
import thousand.group.uylen.utils.base.BaseViewModel
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.extensions.clear
import thousand.group.uylen.utils.helpers.ResErrorHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.OnceLiveData
import thousand.group.uylen.utils.system.ResourceManager

class SearchViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val authUsecase: AuthUsecase,
    private val utilUsecase: UtilUsecase,
    private val searchUsersUsecase: SearchUsersUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetAgeText = MutableLiveData<String>()
    val ldSetSalaryText = MutableLiveData<String>()
    val ldSetFilterActivityVisible = MutableLiveData<Boolean>()

    //    val ldSetSelectedCity = MutableLiveData<String>()
    val ldSetEmptyRecentUsersList = MutableLiveData<Boolean>()
    val ldSetSelectedFamilyStatus = MutableLiveData<String>()
    val ldSetInterests = MutableLiveData<String>()
    val ldSetSelectedRu = MutableLiveData<String>()
    val ldSetSelectedZodiac = MutableLiveData<String>()
    val ldSetDistanceText = MutableLiveData<String>()
    val ldSetKalymVisibility = MutableLiveData<Boolean>()
    val ldSetRefreshState = MutableLiveData<Boolean>()

    //    val ldOpenCityDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldSetRecentSearchList = OnceLiveData<MutableList<UserCardData>>()
    val ldRemoveRecentUser = OnceLiveData<Int>()
    val ldOpenSearchResultPage = OnceLiveData<ParamsContainer>()
    val ldOpenFamilyStatusDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenInterestsPage = OnceLiveData<Pair<String, MutableList<Interest>>>()
    val ldOpenRuDialog = OnceLiveData<ParamsContainer>()
    val ldSetRuParams = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldClearRuPicker = OnceLiveData<Unit>()
    val ldOpenZodiacDialog = OnceLiveData<Triple<Int, Int, MutableList<String>>>()
    val ldOpenDetailPage = OnceLiveData<Pair<Long, Boolean>>()
    val ldClearEditTexts = OnceLiveData<Unit>()
    val ldClearKalymEditText = OnceLiveData<Unit>()
    val ldSetDistanceProgress = OnceLiveData<Int>()

    private var filterActive = true

    private var ageMinVal = 18
    private var ageMaxVal = 69

    //    private var selectedCityPos = 0
    private var selectedFamilyStatusPos = 0
    private var selectedZhuzPos = 0
    private var selectedRuPos = 0
    private var selectedZodiacPos = 0
    private var currentDistance = 160

    /*    private val citiesList = mutableListOf<City>()
        private val citiesNamesList = mutableListOf<String>()*/
    private val recentUserList = mutableListOf<UserCardData>()

    private val genderList = mutableListOf<GeneralValue>()
    private val genderNamesList = mutableListOf<String>()

    private val familyStatusList = mutableListOf<GeneralValue>()
    private val familyNamesStatusList = mutableListOf<String>()

    private val zodiacList = mutableListOf<GeneralValue>()
    private val zodiacNameList = mutableListOf<String>()

    private val interestList = mutableListOf<Interest>()

    private val zhuzList = mutableListOf<Zhuz>()
    private val zhuzNamesList = mutableListOf<String>()
    private val ruNamesList = mutableListOf<MutableList<String>>()

    private val ageRangeText = resourceManager.getString(R.string.format_age_range)
    private val salaryRangeText = resourceManager.getString(R.string.format_salary_range)
    private val noSelectedText = resourceManager.getString(R.string.label_no_selected)

    init {
        setListMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.SET_INTEREST_LIST -> {
                    Log.i(
                        "TestList",
                        "AppConstants.MessageKeysConstants.SET_INTEREST_LIST -> message: $message"
                    )

                    interestList.clear()
                    interestList.addAll(message as MutableList<Interest>)
                }
            }
        }

        setStringMessageReceivedListener { key, message ->
            when (key) {
                AppConstants.MessageKeysConstants.SET_INTEREST_TEXT -> {
                    ldSetInterests.postValue(message)
                }
            }
        }

        setUnitMessageReceivedListener {
            when (it) {
                AppConstants.MessageKeysConstants.CLEAR_INTERESTS -> {
                    interestList.clear()
                    ldSetInterests.postValue(noSelectedText)
                }
            }
        }

//        setCityList()
        getRecentSearchUsers()
        setRuList()
        onDistanceRangeChanged(160)
        getZodiacValues()
        getFamilyStatusValues()

        ldSetDistanceProgress.postValue(160)

        ldSetSelectedFamilyStatus.postValue(noSelectedText)
        ldSetInterests.postValue(noSelectedText)
        ldSetSelectedRu.postValue(noSelectedText)
//        ldSetSelectedCity.postValue(noSelectedText)
        ldSetSelectedZodiac.postValue(noSelectedText)

    }

    fun setDistanceProgress() {
        ldSetDistanceProgress.postValue(currentDistance)
    }

    fun setKalymVisibility() {
        authUsecase.getUserModel()?.details?.apply {
            ldSetKalymVisibility.postValue(this.sex == 9 && filterActive)

            if (ldSetKalymVisibility.value == false) {
                ldClearKalymEditText.call()
            }

        }
    }

    fun onRecentUserItemClicked(model: UserCardData, pos: Int) {
        ldOpenDetailPage.postValue(Pair(model.id, model.chat))
    }

    fun onSearchActionClicked(
        searchText: String
    ) {
        if (searchText.trim().isEmpty()) {
            showMessageError(R.string.error_empty_search_text)
            return
        }

        val params = ParamsContainer()

        params.putString(RemoteConstants.NAME, searchText.trim())

        ldOpenSearchResultPage.postValue(params)
    }

    fun onFilterUseBtnClicked(
        searchText: String,
        ruName: String,
        familyStatus: String,
        zodiac: String,
        salaryMin: String,
        salaryMax: String,
        kalymMin: String,
        kalymMax: String,
        heightMin: String,
        heightMax: String,
        weightMin: String,
        weightMax: String,
        isGpsOn: Boolean = true
    ) {
        if (!isGpsOn) {
            showMessageError(R.string.error_lat_lng)
            return
        }

        val params = ParamsContainer()

        if (searchText.trim().isNotEmpty()) {
            params.putString(RemoteConstants.NAME, searchText.trim())
        }

        if (filterActive) {
            params.putInt(resourceManager.getString(R.string.field_age_1), ageMinVal)
            params.putInt(resourceManager.getString(R.string.field_age_2), ageMaxVal)

            /*if (selectedCityPos != 0) {
                citiesList.getOrNull(selectedCityPos.dec())?.apply {
                    params.putLong(
                        RemoteConstants.CITY_ID,
                        this.id
                    )
                }
            }*/

            if (!ruName.trim().equals(noSelectedText)) {
                params.putLong(
                    RemoteConstants.GENER_ID,
                    zhuzList
                        .get(selectedZhuzPos.dec())
                        .parents
                        .get(
                            selectedRuPos
                        )
                        .id
                )
            }

            if (!familyStatus.trim().equals(noSelectedText)) {
                params.putInt(
                    RemoteConstants.MARITAL_STATUS,
                    familyStatusList.get(selectedFamilyStatusPos.dec()).id
                )
            }

            if (!interestList.equals(noSelectedText)) {
                val interestFormat = resourceManager.getString(R.string.format_interest_id)

                interestList.forEachIndexed { index, item ->
                    val key = String.format(interestFormat, index.inc())

                    params.putLong(key, item.id)
                }
            }

            if (!zodiac.equals(noSelectedText)) {
                params.putInt(RemoteConstants.ZODIAC, zodiacList.get(selectedZodiacPos.dec()).id)
            }

            if (salaryMin.trim().isNotEmpty()
                && salaryMin.trim().isDigitsOnly()
                && !salaryMin.startsWith("0")
            ) {

                val salaryFormat = resourceManager.getString(R.string.format_salary)
                val salaryFirstKey = String.format(salaryFormat, 1)

                params.putInt(salaryFirstKey, salaryMin.toInt())
            }

            if (salaryMax.trim().isNotEmpty()
                && salaryMax.trim().isDigitsOnly()
                && !salaryMax.startsWith("0")
            ) {

                val salaryFormat = resourceManager.getString(R.string.format_salary)
                val salarySecondKey = String.format(salaryFormat, 2)

                params.putInt(salarySecondKey, salaryMax.toInt())
            }

            if (kalymMin.trim().isNotEmpty()
                && kalymMin.trim().isDigitsOnly()
                && !kalymMin.startsWith("0")
            ) {
                val kalymMinInt = kalymMin.toInt()

                val kalymFormat = resourceManager.getString(R.string.format_k_mal)
                val kalymFirstKey = String.format(kalymFormat, 1)

                params.putInt(kalymFirstKey, kalymMinInt)
            }

            if (kalymMax.trim().isNotEmpty()
                && kalymMax.trim().isDigitsOnly()
                && !kalymMax.startsWith("0")
            ) {
                val kalymMaxInt = kalymMax.toInt()

                val kalymFormat = resourceManager.getString(R.string.format_k_mal)
                val kalymSecondKey = String.format(kalymFormat, 2)

                params.putInt(kalymSecondKey, kalymMaxInt)
            }

            if (heightMin.trim().isNotEmpty()
                && heightMin.trim().isDigitsOnly()
                && !heightMin.startsWith("0")
            ) {
                val heightMinInt = heightMin.toInt()

                val heightFormat = resourceManager.getString(R.string.format_height)
                val heighFirstKey = String.format(heightFormat, 1)

                params.putInt(heighFirstKey, heightMinInt)
            }

            if (heightMax.trim().isNotEmpty()
                && heightMax.trim().isDigitsOnly()
                && !heightMax.startsWith("0")
            ) {
                val heightMaxInt = heightMax.toInt()

                val heightFormat = resourceManager.getString(R.string.format_height)
                val heighSecondKey = String.format(heightFormat, 2)

                params.putInt(heighSecondKey, heightMaxInt)
            }

            if (weightMin.trim().isNotEmpty()
                && weightMin.trim().isDigitsOnly()
                && !weightMin.startsWith("0")
            ) {
                val weightMinInt = weightMin.toInt()

                val weightFormat = resourceManager.getString(R.string.format_weight)
                val weightFirstKey = String.format(weightFormat, 1)

                params.putInt(weightFirstKey, weightMinInt)
            }

            if (weightMax.trim().isNotEmpty()
                && weightMax.trim().isDigitsOnly()
                && !weightMax.startsWith("0")
            ) {
                val weightMaxInt = weightMax.toInt()

                val weightFormat = resourceManager.getString(R.string.format_weight)
                val weightSecondKey = String.format(weightFormat, 2)

                params.putInt(weightSecondKey, weightMaxInt)
            }

            params.putInt(RemoteConstants.DISTANCE, currentDistance)
        }

        if (!params.isNotEmpty()) {
            showMessageError(R.string.error_check_search_params)
            return
        }

        ldOpenSearchResultPage.postValue(params)
    }

    fun onDeleteItemClicked(model: UserCardData, pos: Int) {
        doWork {
            searchUsersUsecase.deleteRecentUser(model.id)
                .onStart { showProgressBar(true) }
                .onCompletion { showProgressBar(false) }
                .catch {
                    showMessageError(
                        ResErrorHelper.showThrowableMessage(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    recentUserList.removeAt(pos)
                    ldRemoveRecentUser.postValue(pos)
                    ldSetEmptyRecentUsersList.postValue(recentUserList.isEmpty())
                }
        }
    }

    fun clearFilter() {

        //clear family status
        ldSetSelectedFamilyStatus.postValue(resourceManager.getString(R.string.label_no_selected))
        selectedFamilyStatusPos = 0

        //Clear interest list
        ldSetInterests.postValue(resourceManager.getString(R.string.label_no_selected))
        interestList.clear()

        //Clear Ru
        ldSetSelectedRu.postValue(resourceManager.getString(R.string.label_no_selected))
        selectedZhuzPos = 0
        selectedRuPos = 0

        //Clear City
/*
        ldSetSelectedCity.postValue(resourceManager.getString(R.string.label_no_selected))
        selectedCityPos = 0
*/

        //Clear zodiac
        ldSetSelectedZodiac.postValue(resourceManager.getString(R.string.label_no_selected))
        selectedZodiacPos = 0

        //clear age
        onRangeChanged(18, 69)

        //clear EditTexts
        ldClearEditTexts.call()
    }

    fun onFilterActivityClicked() {
        filterActive = !filterActive

        ldSetFilterActivityVisible.postValue(filterActive)
    }

    fun onRangeChanged(minAge: Int, maxAge: Int) {
        this.ageMinVal = minAge
        this.ageMaxVal = maxAge

        val ageText = String.format(
            ageRangeText,
            ageMinVal,
            ageMaxVal
        )

        ldSetAgeText.postValue(ageText)
    }

    fun selectZodiac(pos: Int) {
        selectedZodiacPos = pos

        ldSetSelectedZodiac.postValue(zodiacNameList.get(selectedZodiacPos))
    }

    fun openZodiacDialog() {
        ldOpenZodiacDialog.postValue(
            Triple(
                selectedZodiacPos,
                zodiacNameList.size - 1,
                zodiacNameList
            )
        )
    }

    fun getRecentSearchUsers() {
        doWork {
            searchUsersUsecase.getRecentlySearch()
                .catch {
                    showMessageError(
                        ResErrorHelper.parseErrorByStatusCode(
                            resourceManager,
                            vmTag,
                            it
                        )
                    )
                }
                .collect {
                    recentUserList.clear()
                    recentUserList.addAll(it)

                    ldSetRecentSearchList.postValue(it)
                    ldSetEmptyRecentUsersList.postValue(it.isEmpty())
                }
        }
    }

    fun selectFamilyStatus(pos: Int) {
        selectedFamilyStatusPos = pos

        val item = familyNamesStatusList.get(selectedFamilyStatusPos)

        ldSetSelectedFamilyStatus.postValue(item)
    }

    fun openFamilyStatusDialog() {
        val list = familyNamesStatusList

        ldOpenFamilyStatusDialog.postValue(
            Triple(
                selectedFamilyStatusPos,
                list.size - 1,
                list
            )
        )
    }

    fun openInterestsPage() {
        ldOpenInterestsPage.postValue(Pair(vmTag, interestList))
    }


    fun selectZhuz(pos: Int) {
        if (pos != 0) {
            ldClearRuPicker.call()

            if (ruNamesList.isEmpty() || ruNamesList.get(pos).isEmpty()) return

            selectedZhuzPos = pos
            selectedRuPos = 0

            val list = ruNamesList.get(selectedZhuzPos)

            ldSetRuParams.postValue(
                Triple(
                    selectedRuPos,
                    list.size - 1,
                    list
                )
            )

            setSelectedZhuzParams()
        } else {
            ldClearRuPicker.call()

            selectedZhuzPos = pos
            selectedRuPos = 0

            val list = ruNamesList.get(selectedZhuzPos)

            ldSetRuParams.postValue(
                Triple(
                    selectedRuPos,
                    list.size - 1,
                    list
                )
            )

            ldSetSelectedRu.postValue(noSelectedText)
        }
    }

    fun selectRu(pos: Int) {
        if (selectedZhuzPos != 0) {
            selectedRuPos = pos

            setSelectedZhuzParams()
        }
    }


    fun openZhuzDialog() {
        val params = ParamsContainer()

        val list = ruNamesList.get(selectedZhuzPos)

        params.putInt(AppConstants.BundleConstants.SELECTED_POS, selectedZhuzPos)
        params.putInt(AppConstants.BundleConstants.SELECTED_POS_2, selectedRuPos)
        params.putInt(AppConstants.BundleConstants.MAX_POS, zhuzNamesList.size - 1)
        params.putInt(AppConstants.BundleConstants.MAX_POS_2, list.size - 1)
        params.putList(AppConstants.BundleConstants.PARENT_LIST, zhuzNamesList)
        params.putList(AppConstants.BundleConstants.CHILD_LIST, list)

        ldOpenRuDialog.postValue(params)
    }

/*    fun selectCity(pos: Int) {
//        selectedCityPos = pos

        setSelectedCityParams()
    }*/

    /* fun openCitiesDialog() {
         if (citiesList.isNotEmpty()) {
             val params = ParamsContainer()

             ldOpenCityDialog.postValue(
                 Triple(
                     selectedCityPos,
                     citiesNamesList.size - 1,
                     citiesNamesList
                 )
             )

             Log.i(vmTag, "openCitiesDialog -> params: ${params}")
         }
     }*/

    fun onDistanceRangeChanged(distance: Int) {
        currentDistance = distance

        ldSetDistanceText.postValue(
            String.format(resourceManager.getString(R.string.format_distance_km), distance)
        )
    }

/*    fun onRefreshSwiped() {
        selectedFamilyStatusPos = 0
        interestList.clear()
        selectedZhuzPos = 0
        selectedRuPos = 0
        selectedCityPos = -1
        selectedZodiacPos = 0
        onDistanceRangeChanged(2)

        ldSetSelectedFamilyStatus.clear()
        ldSetInterests.clear()
        ldSetSelectedRu.clear()
        ldSetSelectedCity.clear()
        ldSetSelectedZodiac.clear()

        onRangeChanged(16, 69)
    }*/

    /* private fun setSelectedCityParams() {
         citiesNamesList.get(selectedCityPos)

         ldSetSelectedCity.postValue(citiesNamesList.get(selectedCityPos))
     }
 */
    /*private fun setCityList() {
        authUsecase.getUserModel()?.let { user ->
            doWork {
                utilUsecase.getCitiesList()
                    .catch { it.printStackTrace() }
                    .collect {
                        citiesList.clear()
                        citiesNamesList.clear()

                        citiesNamesList.add(noSelectedText)

                        citiesList.addAll(it)

                        citiesList.forEach {
                            citiesNamesList.add(it.name)
                        }
                    }
            }
        }
    }*/

    private fun setSelectedZhuzParams() {
        val zhuzItem = zhuzNamesList.get(selectedZhuzPos)

        val ruItem = if (ruNamesList.isNotEmpty() && ruNamesList.get(selectedZhuzPos)
                .isNotEmpty()
        ) ruNamesList.get(selectedZhuzPos).get(selectedRuPos) else ""

        ldSetSelectedRu.postValue("$zhuzItem, $ruItem")
    }

    private fun setRuList() {
        doWork {
            utilUsecase.getRuList()
                .catch { it.printStackTrace() }
                .collect {
                    zhuzList.clear()
                    zhuzList.addAll(it)

                    Log.i(vmTag, "setRuList -> success-> api ru list: $it")
                    Log.i(vmTag, "setRuList -> success-> ru list: $zhuzList")

                    zhuzNamesList.add(noSelectedText)
                    ruNamesList.add(mutableListOf(noSelectedText))

                    zhuzList.forEach {
                        zhuzNamesList.add(it.name)
                    }

                    zhuzList.forEachIndexed { index, zhuz ->
                        ruNamesList.add(mutableListOf())

                        zhuz.parents.forEach {
                            ruNamesList.get(index.inc()).add(it.name)
                        }
                    }

                }
        }
    }

    private fun getFamilyStatusValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.MARITAL_STATUS)
                .catch { it.printStackTrace() }
                .collect {
                    familyStatusList.clear()
                    familyNamesStatusList.clear()

                    familyStatusList.addAll(it)

                    familyNamesStatusList.add(noSelectedText)

                    familyStatusList.forEach {
                        familyNamesStatusList.add(it.value)
                    }

                }
        }
    }

    private fun getZodiacValues() {
        doWork {
            utilUsecase.getGeneralValues(RemoteConstants.ZODIAC)
                .catch { it.printStackTrace() }
                .collect {
                    zodiacList.clear()
                    zodiacNameList.clear()

                    zodiacList.addAll(it)

                    zodiacNameList.add(noSelectedText)

                    zodiacList.forEach {
                        zodiacNameList.add(it.value)
                    }

                }
        }
    }

}