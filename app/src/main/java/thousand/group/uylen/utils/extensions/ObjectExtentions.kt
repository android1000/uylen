package thousand.group.uylen.utils.extensions

import kotlin.reflect.full.allSupertypes

internal fun Any?.isMutableList(): Boolean {
    if (this != null) {
        return this::class.allSupertypes.any { it.toString() == "kotlin.collections.MutableList<E>" }
    } else {
        return false
    }
}
