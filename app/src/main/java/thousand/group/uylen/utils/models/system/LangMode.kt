package thousand.group.uylen.utils.models.system

import android.os.Parcelable
import androidx.annotation.DrawableRes
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LangMode(
    var langCodeServer: String,
    var langCodeLocale: String,
    var isSelected: Boolean = false
) : Parcelable