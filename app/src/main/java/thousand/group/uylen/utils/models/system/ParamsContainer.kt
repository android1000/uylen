package thousand.group.uylen.utils.models.system

import android.os.Bundle
import android.os.Parcelable
import okhttp3.RequestBody
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.helpers.RequestBodyHelper
import java.io.Serializable


class ParamsContainer() : Serializable {

    companion object {
        fun createParamsContainer(bundle: Bundle): ParamsContainer {
            val paramsContainer = ParamsContainer()

            if (!bundle.isEmpty) {
                bundle.keySet().forEachIndexed { index, key ->
                    val value = bundle.get(key)

                    when (value) {
                        is Int -> {
                            paramsContainer.putInt(key, value)
                        }
                        is Long -> {
                            paramsContainer.putLong(key, value)
                        }
                        is Float -> {
                            paramsContainer.putFloat(key, value)
                        }
                        is Double -> {
                            paramsContainer.putDouble(key, value)
                        }
                        is String -> {
                            paramsContainer.putString(key, value)
                        }
                        is Boolean -> {
                            paramsContainer.putBoolean(key, value)
                        }
                        is Parcelable -> {
                            paramsContainer.putParcelable(key, value)
                        }
                        is Serializable -> {
                            paramsContainer.putSerializable(key, value)
                        }
                        is ArrayList<*> -> {
                            paramsContainer.putList(key, value)
                        }
                    }
                }
            }
            return paramsContainer
        }
    }

    private val paramsMap = mutableMapOf<String, Any>()

    fun putInt(key: String, value: Int) = paramsMap.put(key, value)

    fun getInt(key: String) = paramsMap.get(key) as Int?

    fun putLong(key: String, value: Long) = paramsMap.put(key, value)

    fun getLong(key: String) = paramsMap.get(key) as Long?

    fun putFloat(key: String, value: Float) = paramsMap.put(key, value)

    fun getFloat(key: String) = paramsMap.get(key) as Float?

    fun putDouble(key: String, value: Double) = paramsMap.put(key, value)

    fun getDouble(key: String) = paramsMap.get(key) as Double?

    fun putString(key: String, value: String) = paramsMap.put(key, value)

    fun getString(key: String) = paramsMap.get(key) as String?

    fun putBoolean(key: String, value: Boolean) = paramsMap.put(key, value)

    fun getBoolean(key: String) = paramsMap.get(key) as Boolean?

    fun putParcelable(key: String, value: Parcelable) = paramsMap.put(key, value)

    fun <T : Parcelable> getParcelable(key: String) = paramsMap.get(key) as T?

    fun putSerializable(key: String, value: Serializable) = paramsMap.put(key, value)

    fun getSerializable(key: String) = paramsMap.get(key) as Serializable?

    fun putList(key: String, list: ArrayList<*>) = paramsMap.put(key, list)

    fun putList(key: String, list: MutableList<*>) = putList(key, ArrayList(list))

    fun <T> getList(key: String) = paramsMap.get(key) as MutableList<T>?

    fun clear() {
        paramsMap.clear()
    }

    fun isExist(key: String) = paramsMap.get(key) != null

    fun isNotEmpty() = paramsMap.isNotEmpty()

    fun getParams() = paramsMap

    override fun toString(): String {
        val paramsSB = StringBuilder("ParamsContainer -> ")

        paramsMap.forEach {
            paramsSB.append("\nkey: ${it.key}, value: ${it.value}")
        }

        return paramsSB.toString()
    }

}