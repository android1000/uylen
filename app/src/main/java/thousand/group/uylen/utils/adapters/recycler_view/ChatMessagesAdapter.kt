package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.ChatListItem
import thousand.group.data.entities.remote.simple.ChatLocaleDateMessage
import thousand.group.data.entities.remote.simple.ChatMessage
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemChatClockBinding
import thousand.group.uylen.databinding.ItemChatLeftUserBinding
import thousand.group.uylen.databinding.ItemChatRightUserBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.extensions.setVisibilityState
import thousand.group.uylen.utils.helpers.DateParser

class ChatMessagesAdapter(
    val senderId: Long,
    val receiverId: Long,
    val imageClickedListener: (imagePath: String) -> Unit
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val LEFT_MESSAGE = 0
        const val RIGHT_MESSAGE = 1
        const val DATE_TEXT = 2
    }

    private var dataList = mutableListOf<ChatLocaleDateMessage>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun getItemViewType(position: Int) = when {
        getItem(position).message?.sender_id == senderId -> RIGHT_MESSAGE
        getItem(position).message?.sender_id == receiverId -> LEFT_MESSAGE
        else -> DATE_TEXT
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = when (viewType) {
            LEFT_MESSAGE -> ItemChatLeftUserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            RIGHT_MESSAGE -> ItemChatRightUserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
            else -> ItemChatClockBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        }

        return when (binding) {
            is ItemChatLeftUserBinding -> LeftViewHolder(binding, binding.root.resources)
            is ItemChatRightUserBinding -> RightViewHolder(binding, binding.root.resources)
            else -> {
                val parsedBinding = binding as ItemChatClockBinding
                ClockViewHolder(parsedBinding, parsedBinding.root.resources)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LeftViewHolder -> holder.bind(getItem(position), position)
            is RightViewHolder -> holder.bind(getItem(position), position)
            is ClockViewHolder -> holder.bind(getItem(position), position)
        }
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<ChatLocaleDateMessage>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: ChatLocaleDateMessage, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    inner class LeftViewHolder(
        private val binding: ItemChatLeftUserBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(model: ChatLocaleDateMessage, pos: Int) {
            binding.root.tag = model

            val fileIsNotNull = model.message?.file != null
            val reactionIsNotNull = model.message?.reaction != null
            val messageIsNotNull = model.message?.message != null

            Log.i(adapterTag, "Left -> pos:${pos}, model:${model}")
            Log.i(adapterTag, "Left -> fileIsNotNull: ${fileIsNotNull}")
            Log.i(adapterTag, "Left -> reactionIsNotNull: ${reactionIsNotNull}")
            Log.i(adapterTag, "Left -> messageIsNotNull: ${messageIsNotNull}")

            binding.ctlMessageText.setVisibilityState(messageIsNotNull)
            binding.ctlMessageFile.setVisibilityState(fileIsNotNull || reactionIsNotNull)

            binding.ivMessage.setVisibilityState(fileIsNotNull)
            binding.ivReaction.setVisibilityState(reactionIsNotNull)
            binding.tvReaction.setVisibilityState(reactionIsNotNull)

            if (fileIsNotNull) {
                binding.ctlMessageText.setVisibilityState(false)

                Picasso.get()
                    .load(RemoteMainConstants.SERVER_URL + model.message?.file)
                    .error(R.drawable.ic_error)
                    .fit()
                    .centerCrop()
                    .into(binding.ivMessage)

                binding.tvTimeFile.setText(model.message?.created_at)

                binding.ctlMessageFile.setSafelyClickListener {
                    imageClickedListener.invoke(RemoteMainConstants.SERVER_URL + model.message?.file)
                }

            } else if (reactionIsNotNull) {
                binding.ctlMessageText.setVisibilityState(false)

                GlideToVectorYou
                    .init()
                    .with(binding.root.context)
                    .load(
                        Uri.parse(RemoteMainConstants.SERVER_URL + model.message?.reaction),
                        binding.ivReaction
                    )

                binding.tvTimeFile.setText(model.message?.created_at)
                binding.tvReaction.setVisibilityState(false)

            } else {
                binding.tvMessage.setText(model.message?.message)

                binding.tvTimeMessage.setText(model.message?.created_at)
            }

        }
    }

    inner class RightViewHolder(
        private val binding: ItemChatRightUserBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(model: ChatLocaleDateMessage, pos: Int) {
            binding.root.tag = model

            val fileIsNotNull = model.message?.file != null
            val reactionIsNotNull = model.message?.reaction != null
            val messageIsNotNull = model.message?.message != null

            val messageIsRead =
                if (model.message?.is_read == 1) R.drawable.ic_message_read else R.drawable.ic_message_unread

            Log.i(adapterTag, "Right -> pos:${pos}, model:${model}")
            Log.i(adapterTag, "Right -> fileIsNotNull: ${fileIsNotNull}")
            Log.i(adapterTag, "Right -> reactionIsNotNull: ${reactionIsNotNull}")
            Log.i(adapterTag, "Right -> messageIsNotNull: ${messageIsNotNull}")

            binding.ctlMessageText.setVisibilityState(messageIsNotNull)
            binding.ctlMessageFile.setVisibilityState(fileIsNotNull || reactionIsNotNull)

            binding.ivMessage.setVisibilityState(fileIsNotNull)
            binding.ivReaction.setVisibilityState(reactionIsNotNull)
            binding.tvReaction.setVisibilityState(reactionIsNotNull)

            if (fileIsNotNull) {
                binding.ctlMessageText.setVisibilityState(false)

                Picasso.get()
                    .load(RemoteMainConstants.SERVER_URL + model.message?.file)
                    .error(R.drawable.ic_error)
                    .fit()
                    .centerCrop()
                    .into(binding.ivMessage)

                binding.tvTimeFile.setText(model.message?.created_at)

                binding.tvTimeFile.setCompoundDrawablesWithIntrinsicBounds(0, 0, messageIsRead, 0);

                binding.ctlMessageFile.setSafelyClickListener {
                    imageClickedListener.invoke(RemoteMainConstants.SERVER_URL + model.message?.file)
                }
            } else if (reactionIsNotNull) {
                binding.ctlMessageText.setVisibilityState(false)

                GlideToVectorYou
                    .init()
                    .with(binding.root.context)
                    .load(
                        Uri.parse(RemoteMainConstants.SERVER_URL + model.message?.reaction),
                        binding.ivReaction
                    )

                binding.tvTimeFile.setText(model.message?.created_at)
                binding.tvTimeFile.setCompoundDrawablesWithIntrinsicBounds(0, 0, messageIsRead, 0);

                binding.tvReaction.setVisibilityState(false)
            } else {
                binding.tvMessage.setText(model.message?.message)

                binding.tvTimeMessage.setText(model.message?.created_at)

                binding.tvTimeMessage.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    messageIsRead,
                    0
                );
            }
        }
    }

    inner class ClockViewHolder(
        private val binding: ItemChatClockBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(model: ChatLocaleDateMessage, pos: Int) {
            binding.root.tag = model

            val dateFormatted = DateParser.convertOneFormatToAnother(
                model.date,
                DateParser.format_YYYY_MM_dd,
                DateParser.format_dd_LLLL_YYYY
            )

            binding.tvTimeAgo.setText(dateFormatted)
        }
    }
}