package thousand.group.uylen.utils.custom_views

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.doOnNextLayout
import thousand.group.uylen.R
import thousand.group.uylen.databinding.CustomTitleTextArrowBinding
import thousand.group.uylen.databinding.CustomTitleTextBinding

class CustomTitleText : ConstraintLayout {
    private val TAG = this::class.simpleName

    private var titleText: String? = null
    private var simpleText: String? = null
    private var isBoldTitle: Boolean = false

    private val binding =
        CustomTitleTextBinding.inflate(LayoutInflater.from(context), this, true)

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        Log.i("TestState", "init")

        val attrArray =
            context.obtainStyledAttributes(attrs, R.styleable.CustomTitleTextArrow)

        try {
            titleText = attrArray.getString(R.styleable.CustomTitleTextArrow_title_text)
            simpleText = attrArray.getString(R.styleable.CustomTitleTextArrow_text)
            isBoldTitle =
                attrArray.getBoolean(R.styleable.CustomTitleTextArrow_is_bold_title, false)
        } finally {
            attrArray.recycle()
        }

        doOnNextLayout {
            simpleText?.apply {
                binding.tvVal.setText(this)
            }
            titleText?.apply {
                binding.tvTitle.setText(this)
            }

            if (isBoldTitle) {
                binding.tvTitle.setTypeface(ResourcesCompat.getFont(context, R.font.nunito_bold))
                binding.tvVal.gravity = Gravity.END
            }
        }
    }

    fun getText() = binding.tvVal.text.toString()

    fun setText(text: String) = binding.tvVal.setText(text)
    fun setText(@StringRes text: Int) = binding.tvVal.setText(text)

}