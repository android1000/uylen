package thousand.group.uylen.utils.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

internal fun FragmentManager.removeFragment(tag: String) {
    this.findFragmentByTag(tag)?.let {
        this.beginTransaction()
            .remove(it)
            .commit()
        this.popBackStack()
    }
}

internal fun FragmentManager.removeFragmentByMainTag(className: String) {
    this.fragments.forEach {
        it.tag?.apply {
            if (className in this) {
                removeFragment(this)
                return@forEach
            }
        }
    }
}

internal fun FragmentManager.addFragment(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.beginTransaction()
        .disallowAddToBackStack()
        .add(containerViewId, fragment, tag)
        .commit()
}


internal fun FragmentManager.replaceFragment(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.beginTransaction()
        .disallowAddToBackStack()
        .replace(containerViewId, fragment, tag)
        .commit()
}

internal fun FragmentManager.replaceFragmentWithBackStack(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.beginTransaction()
        .replace(containerViewId, fragment, tag)
        .addToBackStack(tag)
        .commit()
}

internal fun FragmentManager.addFragmentWithBackStack(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.beginTransaction()
        .add(containerViewId, fragment, tag)
        .addToBackStack(tag)
        .commit()
}


inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) =
    beginTransaction().func().commit()

internal fun FragmentManager.clearBackStack() {
    if (this.backStackEntryCount > 0) {
        this.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
}

internal fun FragmentManager.clearAndReplaceFragment(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.clearBackStack()
    this.beginTransaction()
        .disallowAddToBackStack()
        .replace(containerViewId, fragment, tag)
        .commit()
}

internal fun FragmentManager.replaceFragmentWithCheck(
    containerViewId: Int,
    newFragment: Fragment,
    tag: String
) {
    val transaction = this.beginTransaction()
    val oldFragment = findFragmentByTag(tag)

    if (oldFragment == null) {
        transaction.replace(containerViewId, newFragment, tag)
        transaction.addToBackStack(null)
    } else {
        transaction.replace(containerViewId, oldFragment, tag)
        transaction.addToBackStack(null)
    }

    transaction.commit()
}

internal fun FragmentManager.getCallerFragment(): String? = fragments.last().tag

internal fun FragmentManager.findFragment(tag: String): Fragment? = findFragmentByTag(tag)

internal fun Fragment.getSupportFragmentManager(): FragmentManager {
    return requireActivity().supportFragmentManager
}

internal fun Fragment.goBack() {
    requireActivity().onBackPressed()
}