package thousand.group.uylen.utils.extensions

import android.view.View
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import thousand.group.uylen.utils.system.SingleClickListener

fun View.isVisible() = this.visibility == View.VISIBLE

fun View.setVisibilityState(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.setSafelyClickListener(clickListener: (View) -> Unit) =
    setOnClickListener(object : SingleClickListener(500) {
        override fun performClick(v: View) {
            clickListener.invoke(v)
        }
    })


