package thousand.group.uylen.utils.models.locale

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Problem(
    val cause: String?,
    var anotherCause: String? = null,
    var isSelected: Boolean
) : Parcelable