package thousand.group.uylen.utils.helpers

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class TimeAgo {

    companion object {

        fun covertTimeToText(dataDate: String?, inputFormat: String): String? {
            var convTime: String? = null
            val suffix = "назад"
            try {
                val dateFormat =
                    SimpleDateFormat(inputFormat, Locale.getDefault())
                dateFormat.setTimeZone(TimeZone.getDefault());

                val pasTime: Date = dateFormat.parse(dataDate)
                val nowTime = Date()

                val dateDiff: Long = nowTime.time - pasTime.time
                val second: Long = TimeUnit.MILLISECONDS.toSeconds(dateDiff)
                val minute: Long = TimeUnit.MILLISECONDS.toMinutes(dateDiff)
                val hour: Long = TimeUnit.MILLISECONDS.toHours(dateDiff)
                val day: Long = TimeUnit.MILLISECONDS.toDays(dateDiff)

                when {

                    second < 60 -> {

                        convTime = if (second < 5) {
                            "сейчас"
                        } else {
                            "$second секунд $suffix"
                        }

                    }
                    minute < 60 -> {
                        convTime = "$minute минут $suffix"
                    }
                    hour < 24 -> {
                        convTime = "$hour час $suffix"
                    }
                    day >= 7 -> {
                        convTime = when {
                            day > 360 -> {
                                (day / 360).toString() + " год " + suffix
                            }
                            day > 30 -> {
                                (day / 30).toString() + " месяц " + suffix
                            }
                            else -> {
                                (day / 7).toString() + " неделя " + suffix
                            }
                        }
                    }
                    day < 7 -> {

                        convTime = if (day == 1.toLong()) {
                            "$day день $suffix"
                        } else {
                            "$day дня $suffix"
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("ConvTimeE", "${e.message}")
            }

            return convTime
        }

    }
}