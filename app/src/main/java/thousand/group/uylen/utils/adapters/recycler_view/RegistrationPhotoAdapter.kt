package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemInterestBinding
import thousand.group.uylen.databinding.ItemRegImagesBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.extensions.setVisibilityState
import thousand.group.uylen.utils.models.locale.RegistrationPhoto
import thousand.group.uylen.utils.models.simple.Interest

class RegistrationPhotoAdapter(
    val onAddPhotoClickListener: (model: RegistrationPhoto, pos: Int) -> Unit,
    val onRemovePhotoClickListener: (model: RegistrationPhoto, pos: Int) -> Unit
) : RecyclerView.Adapter<RegistrationPhotoAdapter.ViewHolder>() {

    private var dataList = mutableListOf<RegistrationPhoto>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemRegImagesBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources, parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<RegistrationPhoto>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: RegistrationPhoto, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    inner class ViewHolder(
        private val binding: ItemRegImagesBinding,
        private val resources: Resources,
        private val parentt: ViewGroup
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: RegistrationPhoto, pos: Int) {
            binding.root.tag = model

            with(binding.root) {
                if (model.photo != null) {
                    binding.ivPhoto.setImageBitmap(model.photo)

                    binding.ivAddPhoto.setVisibilityState(false)
                    binding.ivRemovePhoto.setVisibilityState(true)

                    binding.ivAvatar.setVisibilityState(pos == 0)
                } else {
                    binding.ivPhoto.setImageResource(R.color.colorGrey100)

                    binding.ivAddPhoto.setVisibilityState(true)
                    binding.ivRemovePhoto.setVisibilityState(false)

                    binding.ivAvatar.setVisibilityState(false)
                }

                binding.ivAddPhoto.setSafelyClickListener {
                    onAddPhotoClickListener.invoke(model, pos)
                }

                binding.ivRemovePhoto.setSafelyClickListener {
                    onRemovePhotoClickListener.invoke(model, pos)
                }

            }
        }
    }
}