package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isEmpty
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.CardUser
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.entities.remote.simple.UserPhoto
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemHomeCardsBinding
import thousand.group.uylen.databinding.ItemInterestBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.extensions.setVisibilityState
import thousand.group.uylen.utils.helpers.DateParser
import thousand.group.uylen.utils.models.simple.Interest
import java.util.*
import java.util.concurrent.TimeUnit

class HomeCardAdapter(
    val onLeftPlaneClicked: (model: UserCardData, position: Int) -> Unit,
    val onRightPlaneClicked: (model: UserCardData, position: Int) -> Unit,
    val onBottomPlaneClicked: (model: UserCardData, position: Int) -> Unit
) : RecyclerView.Adapter<HomeCardAdapter.ViewHolder>() {

    private var dataList = mutableListOf<UserCardData>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemHomeCardsBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<UserCardData>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: UserCardData, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    inner class ViewHolder(
        private val binding: ItemHomeCardsBinding,
        private val resources: Resources
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: UserCardData, pos: Int) {
            binding.root.tag = model

            val userAge = getAge(model.details?.date_of_birth)
            val currentImage =
                if (model.images.isNotEmpty()) model.images.get(model.activePhotoLinkPos).images else null

            binding.tvUsersName.setText("${model.name}, $userAge")
            binding.tvDesc.setText(model.about?.text)

/*            Picasso.get()
                .load(RemoteMainConstants.SERVER_URL + currentImage)
                .error(R.drawable.ic_error_avatar)
                .into(binding.ivAvatar)*/

            Glide.with(binding.root.context)
                .load(RemoteMainConstants.SERVER_URL + currentImage)
                .error(R.drawable.ic_error_avatar)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.20f)
                .centerCrop()
                .into(binding.ivAvatar)

            showHints(model.hintMode)

            updateOrCreateProgressPositions(model.images, model.activePhotoLinkPos)

            binding.flLeftPlane.setSafelyClickListener {
                onLeftPlaneClicked.invoke(model, pos)
            }

            binding.flRightPlane.setSafelyClickListener {
                onRightPlaneClicked.invoke(model, pos)
            }

            binding.flBottomPlane.setSafelyClickListener {
                onBottomPlaneClicked.invoke(model, pos)
            }

            binding.ivInfo.setSafelyClickListener {
                model.hintMode = 1
                showHints(1)
            }

            binding.tvCenterHint.setSafelyClickListener {
                model.hintMode = 2
                showHints(2)
            }
            binding.tvLeftHint.setSafelyClickListener {
                model.hintMode = 0
                showHints(0)
            }
            binding.tvRightHint.setSafelyClickListener {
                model.hintMode = 0
                showHints(0)
            }
            binding.tvBottomHint.setSafelyClickListener {
                model.hintMode = 0
                showHints(0)
            }
        }

        private fun showHints(mode: Int) {
            when (mode) {
                1 -> {
                    binding.ctlHint.setVisibilityState(true)
                    binding.tvCenterHint.setVisibilityState(true)
                    binding.tvLeftHint.setVisibilityState(false)
                    binding.tvRightHint.setVisibilityState(false)
                    binding.tvBottomHint.setVisibilityState(false)
                }
                2 -> {
                    binding.ctlHint.setVisibilityState(true)
                    binding.tvCenterHint.setVisibilityState(false)
                    binding.tvLeftHint.setVisibilityState(true)
                    binding.tvRightHint.setVisibilityState(true)
                    binding.tvBottomHint.setVisibilityState(true)
                }
                else -> {
                    binding.ctlHint.setVisibilityState(false)
                }
            }
        }

        private fun updateOrCreateProgressPositions(list: MutableList<UserPhoto>, currentPos: Int) {
            binding.llProgress.removeAllViews()

            list.forEachIndexed { index, s ->
                val frameProgress = FrameLayout(binding.root.context)
                val backgroundColor =
                    if (index == currentPos) R.color.colorWhite else R.color.colorWhiteOp32

                frameProgress.setBackgroundResource(backgroundColor)
                frameProgress.layoutParams =
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        1f
                    )

                val marginLp = frameProgress.layoutParams as ViewGroup.MarginLayoutParams
                marginLp.leftMargin =
                    if (index != 0) resources.getDimensionPixelSize(R.dimen.progressMargin) else 0

                frameProgress.layoutParams = marginLp

                binding.llProgress.addView(frameProgress)
            }

        }

        private fun getAge(birthdateText: String?): String {
            if (birthdateText != null) {
                val birthdate =
                    DateParser.stringToDate(birthdateText, DateParser.format_YYYY_MM_dd).time
                val currentDate = Date().time

                val diff = currentDate - birthdate
                val diffCount = Calendar.getInstance()
                diffCount.timeInMillis = diff

                val year = diffCount.get(Calendar.YEAR) - 1970

                return year.toString()
            } else {
                return ""
            }

        }
    }

}