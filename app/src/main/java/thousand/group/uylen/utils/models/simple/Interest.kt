package thousand.group.uylen.utils.models.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Interest(
    val text: String,
    var isSelected: Boolean = false
) : Parcelable