package thousand.group.uylen.utils.di

import android.content.Context
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import thousand.group.uylen.utils.base.SharedViewModel
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.view_models.auth.AuthViewModel
import thousand.group.uylen.view_models.auth.EnterSingleInfoViewModel
import thousand.group.uylen.view_models.auth.InterestsViewModel
import thousand.group.uylen.view_models.auth.LoginViewModel
import thousand.group.uylen.view_models.chat.ChatListViewModel
import thousand.group.uylen.view_models.chat.ChatPhotoDetailViewModel
import thousand.group.uylen.view_models.chat.ChatMessagesViewModel
import thousand.group.uylen.view_models.chat.MutualSympathyViewModel
import thousand.group.uylen.view_models.favourite.FavouriteDetailViewModel
import thousand.group.uylen.view_models.favourite.FavouriteViewModel
import thousand.group.uylen.view_models.home.HomeDetailViewModel
import thousand.group.uylen.view_models.home.HomeViewModel
import thousand.group.uylen.view_models.home.NewPairViewModel
import thousand.group.uylen.view_models.main.CropBitmapViewModel
import thousand.group.uylen.view_models.main.MainViewModel
import thousand.group.uylen.view_models.main.PhotoVideoSelectionViewModel
import thousand.group.uylen.view_models.profile.*
import thousand.group.uylen.view_models.registration.FirstRegistrationViewModel
import thousand.group.uylen.view_models.registration.SecondRegistrationViewModel
import thousand.group.uylen.view_models.registration.ThirdRegistrationViewModel
import thousand.group.uylen.view_models.registration.VerifyRegistrationViewModel
import thousand.group.uylen.view_models.restore_password.RestorePasswordViewModel
import thousand.group.uylen.view_models.restore_password.SendRestoreCodeViewModel
import thousand.group.uylen.view_models.search.SearchDetailViewModel
import thousand.group.uylen.view_models.search.SearchResultViewModel
import thousand.group.uylen.view_models.search.SearchViewModel

val vmModule = module {
    viewModel { SharedViewModel() }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        AuthViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        LoginViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        FirstRegistrationViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        SecondRegistrationViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        ThirdRegistrationViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get {
                parametersOf(
                    context
                )
            },
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        SendRestoreCodeViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        RestorePasswordViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        EnterSingleInfoViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        InterestsViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        PhotoVideoSelectionViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        CropBitmapViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get {
                parametersOf(
                    context
                )
            }
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        MainViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        HomeViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        HomeDetailViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        NewPairViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get {
                parametersOf(
                    context
                )
            },
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        ChatListViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        ChatMessagesViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get {
                parametersOf(
                    context
                )
            },
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        ChatPhotoDetailViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        MutualSympathyViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get(),
            get {
                parametersOf(
                    context
                )
            }
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        FavouriteViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get(),
            get {
                parametersOf(
                    context
                )
            }
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        FavouriteDetailViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        EditProfileViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get {
                parametersOf(
                    context
                )
            },
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        ProfileViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        PrivacyTermsViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        SettingsViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        TarifsViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        CurrentTariffViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        SearchViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        SearchResultViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get(),
            get {
                parametersOf(
                    context
                )
            }
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        SearchDetailViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get(),
            get(),
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        VerifyRegistrationViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        VerifyChangedPhoneViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get(),
            get()
        )
    }

}