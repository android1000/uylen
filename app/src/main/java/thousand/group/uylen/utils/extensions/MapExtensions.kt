package thousand.group.uylen.utils.extensions

import java.io.Serializable

internal fun <K, V> MutableMap<K, V>.toHashMap() = HashMap(this)

internal fun <K, V> Serializable.toMutableMap(): MutableMap<K, V> {
    val hashMap = this as HashMap<K, V>
    return hashMap.toMutableMap()
}

internal fun <K, V> HashMap<K, V>.toMutableMap(): MutableMap<K, V> {
    val mutableMap = mutableMapOf<K, V>()

    for ((k, v) in this) {
        mutableMap.put(k, v)
    }

    return mutableMap
}