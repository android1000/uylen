package thousand.group.uylen.utils

sealed class MessageStatuses {
    internal object ON_SUCCESS : MessageStatuses()
    internal object ON_ERROR : MessageStatuses()
}