package thousand.group.uylen.utils.helpers

import android.util.Log
import androidx.annotation.ColorRes
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException

data class AuthFragmentHelper(
    var title: String = "",
    var isLightStatusBar: Boolean = false,
    var isTabsVisible: Boolean = false,
    var tabsPosition: Int? = null
) {
    companion object {

        internal val TAG = AuthFragmentHelper::class.java.simpleName

        fun getJsonFragmentTag(fragmentHelper: AuthFragmentHelper): String =
            Gson().toJson(fragmentHelper).toString()

        fun isLightStatusBar(fragmentTag: String?): Boolean {
            Log.i(MainFragmentHelper.TAG, "isLightStatusBar -> $fragmentTag")
            return try {
                if (fragmentTag != null)
                    Gson().fromJson(fragmentTag, MainFragmentHelper::class.java).isLightStatusBar
                else
                    false
            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
                true
            }
        }

        fun isTabsVisible(fragmentTag: String?): Boolean {
            Log.i(MainFragmentHelper.TAG, "isLightStatusBar -> $fragmentTag")
            return try {
                if (fragmentTag != null)
                    Gson().fromJson(fragmentTag, AuthFragmentHelper::class.java).isTabsVisible
                else
                    false
            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
                true
            }
        }

        fun getTabsPosition(fragmentTag: String?): Int? {
            Log.i(MainFragmentHelper.TAG, "getBottomNavPos -> $fragmentTag")

            return try {
                if (fragmentTag != null) {
                    Gson().fromJson(fragmentTag, AuthFragmentHelper::class.java).tabsPosition
                } else {
                    null
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                null
            }
        }

    }
}