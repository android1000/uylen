package thousand.group.uylen.global_utils.extentions

import android.os.Bundle
import android.os.Parcelable
import thousand.group.data.storage.utils.GsonHelper

internal fun <T : Parcelable> Bundle.putMutableParcelableList(key: String, list: MutableList<T>) {
    this.putParcelableArrayList(key, ArrayList(list))
}


