package thousand.group.uylen.utils.base

import android.os.Parcelable
import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.isMutableList
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.view_models.main.MainViewModel
import java.io.Serializable
import kotlin.coroutines.CoroutineContext
import kotlin.reflect.KClass

abstract class BaseViewModel(
    open val resourceManager: ResourceManager,
    open val sharedViewModel: SharedViewModel,
    open var paramsContainer: ParamsContainer? = null
) : ViewModel() {

    var vmTag: String = this.javaClass.simpleName

    private var viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Main + viewModelJob)

    private var isActive = true

    private lateinit var stringMessageListener: (key: String, message: String) -> Unit
    private lateinit var intMessageListener: (key: String, message: Int) -> Unit
    private lateinit var booleanMessageListener: (key: String, message: Boolean) -> Unit
    private lateinit var parcelableMessageListener: (key: String, message: Parcelable) -> Unit
    private lateinit var serializableMessageListener: (key: String, message: Serializable) -> Unit
    private lateinit var listMessageListener: (key: String, message: MutableList<*>) -> Unit
    private lateinit var unitMessageListener: (key: String) -> Unit

    override fun onCleared() {
        super.onCleared()
        isActive = false
        viewModelJob.cancel()
    }

    fun showMessageSuccess(message: String) = sharedViewModel.showMessageSuccess(message)

    fun showMessageSuccess(@StringRes message: Int) = sharedViewModel.showMessageSuccess(message)

    fun showMessageError(message: String) = sharedViewModel.showMessageError(message)

    fun showMessageError(@StringRes message: Int) = sharedViewModel.showMessageError(message)

    fun showMessageToast(message: String) = sharedViewModel.showMessageToast(message)

    fun showMessageToast(@StringRes message: Int) = sharedViewModel.showMessageToast(message)

    fun showProgressBar(show: Boolean) = sharedViewModel.showProgressBar(show)

    fun openKeyBoard() = sharedViewModel.openKeyBoard()

    fun closeKeyBoard() = sharedViewModel.closeKeyBoard()

    fun <T, ViewModelType : BaseViewModel> sendLocaleMessage(
        tag: KClass<ViewModelType>,
        key: String,
        message: T
    ) {
        Log.i("TestLocale", "tag: ${tag.simpleName}, key:${key}, message:${message}")

        tag.simpleName?.apply {
            sharedViewModel.sendLocaleMessage(this, key, message)
        }

    }

    fun <ViewModelType : BaseViewModel> sendLocaleCallback(
        tag: KClass<ViewModelType>,
        key: String
    ) {
        Log.i("TestLocale", "tag: ${tag.simpleName}")

        tag.simpleName?.apply {
            sharedViewModel.sendLocaleMessage(this, key, null)
        }
    }

    // Do work in IO
    fun <P> doWork(doOnAsyncBlock: suspend CoroutineScope.() -> P) {
        doCoroutineWork(doOnAsyncBlock, viewModelScope, IO)
    }

    // Do work in Default
    fun <P> doWorkInDefault(doOnAsyncBlock: suspend CoroutineScope.() -> P) {
        doCoroutineWork(doOnAsyncBlock, viewModelScope, Default)
    }

    // Do work in Main
    // doWorkInMainThread {...}
    fun <P> doWorkInMainThread(doOnAsyncBlock: suspend CoroutineScope.() -> P) {
        doCoroutineWork(doOnAsyncBlock, viewModelScope, Main)
    }

    // Do work in IO repeately
    // doRepeatWork(1000) {...}
    // then we need to stop it calling stopRepeatWork()
    fun <P> doRepeatWork(delayl: Long, doOnAsyncBlock: suspend CoroutineScope.() -> P) {
        isActive = true
        viewModelScope.launch {
            while (this@BaseViewModel.isActive) {
                withContext(IO) {
                    doOnAsyncBlock.invoke(this)
                }
                if (this@BaseViewModel.isActive) {
                    delay(delayl)
                }
            }
        }
    }

    fun <P> doWorkAfterSomeTime(delayl: Long, doOnAsyncBlock: suspend CoroutineScope.() -> P) {
        viewModelScope.launch {
            delay(delayl)

            withContext(Default) {
                doOnAsyncBlock.invoke(this)
            }
        }
    }

    fun stopRepeatWork() {
        isActive = false
    }

    fun <T> parseReceivedMessage(
        tag: String,
        key: String,
        message: T?,
        liveData: MutableLiveData<Triple<String, String, T>>? = null,
        unitLiveData: MutableLiveData<Pair<String, String>>? = null
    ) {
        if (!tag.equals(this::class.simpleName)) {
            return
        }

        liveData?.postValue(null)
        unitLiveData?.postValue(null)


        if (message.isMutableList()) {
            Log.i("TestList", "BaseViewModel-> tag: ${vmTag}, key:${key}, message:${message}")

            if (::listMessageListener.isInitialized) {
                listMessageListener.invoke(key, message as MutableList<*>)
            }
        } else {
            when (message) {
                is String -> {
                    if (::stringMessageListener.isInitialized) {
                        stringMessageListener.invoke(key, message)
                    }
                }
                is Int -> {
                    if (::intMessageListener.isInitialized) {
                        intMessageListener.invoke(key, message)
                    }
                }
                is Boolean -> {
                    if (::booleanMessageListener.isInitialized) {
                        booleanMessageListener.invoke(key, message)
                    }
                }
                is Parcelable -> {
                    if (::parcelableMessageListener.isInitialized) {
                        parcelableMessageListener.invoke(key, message)
                    }
                }
                is Serializable -> {
                    if (::serializableMessageListener.isInitialized) {
                        serializableMessageListener.invoke(key, message)
                    }
                }
                else -> {
                    if (::unitMessageListener.isInitialized) {
                        unitMessageListener.invoke(key)
                    }
                }
            }
        }
    }

    /*protected fun doIfTokenDoesNotExpired(error: Throwable, onTokenNotExpired: () -> Unit) {
        if (error is HttpException && error.code() == 401) {
            sendLocaleCallback(
                MainViewModel::class,
                AppConstants.MessageKeysConstants.USER_EXIT
            )
        } else {
            onTokenNotExpired.invoke()
        }

    }*/

/*    protected fun checkToken(authUsecase: AuthUsecase, onTokenExist: () -> Unit) = doWork {
        if (authUsecase.tokenIsNotEmpty()) {
            onTokenExist.invoke()
        } else {
            sendLocaleCallback(
                MainViewModel::class,
                AppConstants.MessageKeysConstants.NEED_AUTH
            )
        }
    }*/

    protected fun setStringMessageReceivedListener(listener: (key: String, message: String) -> Unit) {
        this.stringMessageListener = listener
    }

    protected fun setIntMessageReceivedListener(listener: (key: String, message: Int) -> Unit) {
        this.intMessageListener = listener
    }

    protected fun setBooleanMessageReceivedListener(listener: (key: String, message: Boolean) -> Unit) {
        this.booleanMessageListener = listener
    }

    protected fun setParcelableMessageReceivedListener(listener: (key: String, message: Parcelable) -> Unit) {
        this.parcelableMessageListener = listener
    }

    protected fun setSerializableMessageReceivedListener(listener: (key: String, message: Serializable) -> Unit) {
        this.serializableMessageListener = listener
    }

    protected fun setListMessageReceivedListener(listener: (key: String, message: MutableList<*>) -> Unit) {
        this.listMessageListener = listener
    }

    protected fun setUnitMessageReceivedListener(listener: (key: String) -> Unit) {
        this.unitMessageListener = listener
    }

    protected fun checkToken(authUsecase: AuthUsecase, onTokenExist: () -> Unit) = doWork {
        if (authUsecase.tokenIsNotEmpty()) {
            onTokenExist.invoke()
        } else {
            sendLocaleCallback(
                MainViewModel::class,
                AppConstants.MessageKeysConstants.NEED_AUTH
            )
        }
    }

    private inline fun <P> doCoroutineWork(
        crossinline doOnAsyncBlock: suspend CoroutineScope.() -> P,
        coroutineScope: CoroutineScope,
        context: CoroutineContext
    ) {
        coroutineScope.launch {
            withContext(context) {
                doOnAsyncBlock.invoke(this)
            }
        }
    }
}
