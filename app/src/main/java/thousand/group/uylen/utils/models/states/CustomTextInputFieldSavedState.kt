package thousand.group.uylen.utils.models.states

import android.os.Parcel
import android.os.Parcelable
import android.view.View

class CustomTextInputFieldSavedState : View.BaseSavedState {
    var text: String? = null
    var hint: String? = null
    var errorText: String? = null

    var focus = 0
    var errorStateActivated = 0
    var inputType = 0

    constructor(superState: Parcelable?) : super(superState) {}

    constructor(parcel: Parcel) : super(parcel) {
        text = parcel.readString()
        hint = parcel.readString()
        errorText = parcel.readString()
        focus = parcel.readInt()
        errorStateActivated = parcel.readInt()
        inputType = parcel.readInt()
    }

    override fun writeToParcel(out: Parcel, flags: Int) {
        super.writeToParcel(out, flags)
        out.writeString(text)
        out.writeString(hint)
        out.writeString(errorText)
        out.writeInt(focus)
        out.writeInt(errorStateActivated)
        out.writeInt(inputType)
    }

    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<CustomTextInputFieldSavedState?> {
            override fun createFromParcel(`in`: Parcel): CustomTextInputFieldSavedState? {
                return CustomTextInputFieldSavedState(`in`)
            }

            override fun newArray(size: Int): Array<CustomTextInputFieldSavedState?> {
                return arrayOfNulls(size)
            }
        }
    }
}