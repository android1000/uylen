package thousand.group.uylen.utils.system

import android.content.Context
import android.os.Environment
import java.io.File

class FileUtils(val context: Context) {

    fun getCompressedFilesDirectory() =
        File(context.getExternalFilesDir(Environment.DIRECTORY_MOVIES)?.absolutePath + "/${context.packageName}/compressed_videos").absolutePath
}