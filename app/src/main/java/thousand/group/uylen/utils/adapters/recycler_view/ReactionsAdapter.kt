package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.Reaction
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemInterestBinding
import thousand.group.uylen.databinding.ItemReactionBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener

class ReactionsAdapter(
    val onItemClicked: (reaction: Reaction, pos: Int) -> Unit
) :
    RecyclerView.Adapter<ReactionsAdapter.ViewHolder>() {

    private var dataList = mutableListOf<Reaction>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemReactionBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<Reaction>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: Reaction, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    inner class ViewHolder(
        private val binding: ItemReactionBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: Reaction, pos: Int) {
            binding.root.tag = model

            GlideToVectorYou
                .init()
                .with(binding.root.context)
                .load(Uri.parse(RemoteMainConstants.SERVER_URL + model.image), binding.root)

            binding.root.setSafelyClickListener {
                onItemClicked.invoke(model, pos)
            }
        }
    }
}