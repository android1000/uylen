package thousand.group.uylen.utils.decorations

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class HorizontalGridListItemDecoration(
    private var spaceHorizontal: Int,
    private var spaceVertical: Int,
    private var spanCount: Int
) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        val size = parent.childCount
        val row = position % spanCount

        with(outRect) {
//            left = if (column == 0) 0 else space / 2
//            right = if (column == spanCount.dec()) 0 else space / 2
//            top = if (position < spanCount) 0 else space

            left = if (position < spanCount) 0 else spaceHorizontal / 2
            right = if (position < size.dec() - spanCount) spaceHorizontal / 2 else 0
            top = if (row == 0) 0 else spaceVertical
        }
    }
}