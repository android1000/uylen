package thousand.group.uylen.utils.helpers

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

object PastTenseTimeData {
    fun covertTimeToText(dataDate: String?, inputFormat: String): String? {
        var convTime: String? = null

        try {
            val dateFormat =
                SimpleDateFormat(inputFormat, Locale.getDefault())
            dateFormat.setTimeZone(TimeZone.getDefault());

            val pasTime: Date = dateFormat.parse(dataDate)
            val nowTime = Date()

            val dateDiff: Long = nowTime.time - pasTime.time
            val day: Long = TimeUnit.MILLISECONDS.toDays(dateDiff)

            Log.i("DateDiff", "day: ${day}")

            when {
                day <= 1L -> {
                    convTime = DateParser.formatDate(pasTime, DateParser.formatSIMPLE_TIME)
                }
                day > 1L && day < 8L->{
                    convTime = DateParser.formatDate(pasTime, DateParser.format_SHORT_DAY_NAME)
                }
                else->{
                    convTime = DateParser.formatDate(pasTime, DateParser.format_DD_MM_YYYY_With_Dots)
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            Log.e("ConvTimeE", "${ex.message}")
        }

        return convTime
    }
}