package  thousand.group.uylen.utils.constants

import android.text.InputType
import android.view.inputmethod.EditorInfo
import thousand.group.uylen.R
import thousand.group.uylen.utils.models.locale.EnterSingleInfo
import thousand.group.uylen.utils.models.system.LangMode
import java.util.regex.Pattern


object AppConstants {
    object CameraActionConstants {
        const val GALLERY_PHOTO_REQUEST_CODE = 10
        const val CAMERA_PHOTO_REQUEST_CODE = 11
        const val DELETE_PHOTO_REQUEST_CODE = 12

        const val ON_PHOTO_LOADED = "ON_PHOTO_LOADED"
        const val ON_VIDEO_LOADED = "ON_VIDEO_LOADED"

        const val MODE_PHOTO = "MODE_PHOTO"
        const val MODE_VIDEO = "MODE_VIDEO"
        const val CONTENT_TYPE = "CONTENT_TYPE"

        const val imageType = "image/*"
        const val videoType = "video/*"

        const val PHOTO = "photo"
        const val VIDEO = "video"
    }

    object VideoLinkConstants {
        val MP4 = "mp4"
        val FLV = "flv"
        val M4A = "m4a"
        val GP_3 = "3gp"
        val MKV = "mkv"
        val VIDEO = "video"
    }

    object AudioLinkConstants {
        val MP3 = "mp3"
        val OGG = "ogg"
        val AUDIO = "audio"
    }

    object PhotoLinkConstants {
        val JPG = "jpg"
        val PNG = "png"
        val GIF = "gif"
        val PHOTO = "photo"
    }

    object MessageKeysConstants {
        const val ENTER_VAL = "ENTER_VAL"
        const val CLEAR_INTERESTS = "CLEAR_INTERESTS"
        const val SET_INTEREST_TEXT = "SET_INTEREST_TEXT"
        const val SET_INTEREST_LIST = "SET_INTEREST_LIST"
        const val NEED_AUTH = "NEED_AUTH"
        const val CARD_CANCEL = "CARD_CANCEL"
        const val CARD_LIKE = "CARD_LIKE"
        const val CARD_STAR = "CARD_STAR"
        const val CARD_BAN_COMPLAIN = "CARD_BAN_COMPLAIN"
        const val DELETE_WHO_LIKES_ITEM = "DELETE_WHO_LIKES_ITEM"
        const val UPDATE_PROFILE = "UPDATE_PROFILE"
        const val ACTIVATE_SOCKET = "ACTIVATE_SOCKET"
        const val INACTIVATE_SOCKET = "INACTIVATE_SOCKET"
        const val SUBCRIBE_CHAT_MESSAGE = "SUBCRIBE_CHAT_MESSAGE"
        const val UNSUBCRIBE_CHAT_MESSAGE = "UNSUBCRIBE_CHAT_MESSAGE"
        const val CHAT_LIST_PAGE_IS_ACTIVE = "CHAT_LIST_PAGE_IS_ACTIVE"
        const val FAVOURITE_PAGE_IS_ACTIVE = "FAVOURITE_PAGE_IS_ACTIVE"
        const val UPDATE_FAVOURITE = "UPDATE_FAVOURITE"
        const val REQUIRE_LOCATION = "REQUIRE_LOCATION"
        const val RESPONSE_LOCATION = "RESPONSE_LOCATION"
        const val RESPONSE_LOCATION_ERROR = "RESPONSE_LOCATION_ERROR"
        const val EDIT_PHONE = "EDIT_PHONE"
        const val ON_BACK_PRESSED = "ON_BACK_PRESSED"
        const val HOME_PAGE_UPDATE = "HOME_PAGE_UPDATE"
        const val CROP_BITMAP = "CROP_BITMAP"
    }

    object BundleConstants {
        const val PARAMS_CONTAINER = "PARAMS_CONTAINER"
        const val PHONE_NUMBER = "PHONE_NUMBER"
        const val ID = "ID"
        const val URI = "URI"
        const val BITMAP = "BITMAP"
        const val SALARY = "SALARY"
        const val SELECTED_POS = "SELECTED_POS"
        const val SELECTED_POS_2 = "SELECTED_POS_2"
        const val MAX_POS = "MAX_POS"
        const val MAX_POS_2 = "MAX_POS_2"
        const val PARENT_LIST = "PARENT_LIST"
        const val CHILD_LIST = "CHILD_LIST"
        const val ENTER_TYPE = "ENTER_TYPE"
        const val CURRENT_ENTER_TEXT = "CURRENT_ENTER_TEXT"
        const val VM_TAG = "VM_TAG"
        const val USER = "USER"
        const val TOKEN = "TOKEN"
        const val USER_1 = "USER_1"
        const val USER_2 = "USER_2"
        const val USER_PHOTO_1 = "USER_PHOTO_1"
        const val USER_PHOTO_2 = "USER_PHOTO_2"
        const val MATCH_TEXT = "MATCH_TEXT"
        const val NAME = "NAME"
        const val AGE = "AGE"
        const val ABOUT = "ABOUT"
        const val GENER_ID = "GENER_ID"
        const val CITY = "CITY"
        const val RU = "RU"
        const val STATUS = "STATUS"
        const val SHOW_BTN = "SHOW_BTN"
        const val SHOW_LIKE_BTN = "SHOW_LIKE_BTN"
        const val PHOTO = "PHOTO"
        const val LIST = "LIST"
        const val IV_VISIBILITY_1 = "IV_VISIBILITY_1"
        const val IV_VISIBILITY_2 = "IV_VISIBILITY_2"
        const val IV_VISIBILITY_3 = "IV_VISIBILITY_3"
        const val IV_VISIBILITY_4 = "IV_VISIBILITY_4"
        const val IV_VISIBILITY_5 = "IV_VISIBILITY_5"
        const val IV_LINK_1 = "IV_LINK_1"
        const val IV_LINK_2 = "IV_LINK_2"
        const val IV_LINK_3 = "IV_LINK_3"
        const val IV_LINK_4 = "IV_LINK_4"
        const val IV_LINK_5 = "IV_LINK_5"
        const val SIZE_VISIBILITY = "SIZE_VISIBILITY"
        const val SIZE_TEXT = "SIZE_TEXT"
        const val MATCHED_USERS_COUNT_TEXT = "MATCHED_USERS_COUNT_TEXT"
        const val CHAT_ID = "CHAT_ID"
        const val RECEIVER_ID = "RECEIVER_ID"
        const val CHAT_PAIR_TEXT = "CHAT_PAIR_TEXT"
        const val CHAT_PAIR_TEXT_DATE = "CHAT_PAIR_TEXT_DATE"
        const val ADDRESS_IMAGE = "ADDRESS_IMAGE"
        const val PUSH_OBJECT = "PUSH_OBJECT"
        const val RELOAD_ACTIVITY = "RELOAD_ACTIVITY"
        const val FAMILY_STATUS = "FAMILY_STATUS"
        const val GENDER = "GENDER"
        const val BIRTHDATE = "BIRTHDATE"
        const val LOCATION = "LOCATION"
        const val WEIGHT = "WEIGHT"
        const val HEIGHT = "HEIGHT"
        const val ZODIAC = "ZODIAC"
        const val BAD_HABITS = "BAD_HABITS"
        const val KALYM = "KALYM"
        const val KALYM_VISIBILITY = "KALYM_VISIBILITY"
        const val TERMS_TYPE = "TERMS_TYPE"
        const val OPEN_REGISTRATION_PAGE = "OPEN_REGISTRATION_PAGE"
        const val FROM_SIGN_UP_PAGE = "FROM_SIGN_UP_PAGE"
    }

    object SharedActionConstants {
    }

    object ScreenOrientationModes {
        const val ORIENTATION_UNDEFINED = 0
        const val ORIENTATION_PORTRAIT = 1
        const val ORIENTATION_LANDSCAPE = 2
    }

    object EnterSingleInfoTypes {
        val ENTER_STATUS = EnterSingleInfo(
            type = 1,
            title = R.string.label_status_title,
            desc = R.string.label_status_desc,
            hint = R.string.hint_status,
            inputType = InputType.TYPE_CLASS_TEXT
        )

        val ENTER_INCOME = EnterSingleInfo(
            type = 2,
            title = R.string.label_income_title,
            desc = R.string.label_income_desc,
            hint = R.string.hint_income,
            inputType = InputType.TYPE_CLASS_NUMBER
        )


        val ENTER_PHONE = EnterSingleInfo(
            type = 3,
            title = R.string.label_phone_number,
            desc = R.string.label_phone_number_desc,
            hint = R.string.hint_phone,
            inputType = InputType.TYPE_CLASS_NUMBER
        )

        val ENTER_EMAIL = EnterSingleInfo(
            type = 4,
            title = R.string.label_email,
            desc = R.string.label_email_desc,
            hint = R.string.hint_email,
            inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        )

        val ENTER_KALYM = EnterSingleInfo(
            type = 5,
            title = R.string.label_kalym,
            desc = R.string.label_kalym_desc,
            hint = R.string.hint_kalym,
            inputType = InputType.TYPE_CLASS_NUMBER
        )

        val ENTER_BAD_HABITS = EnterSingleInfo(
            type = 6,
            title = R.string.label_bad_habits,
            desc = R.string.label_bad_habits_desc,
            hint = R.string.hint_bad_habits,
            inputType = InputType.TYPE_CLASS_TEXT
        )

        val ENTER_WEIGHT = EnterSingleInfo(
            type = 7,
            title = R.string.label_weight,
            desc = R.string.label_weight_desc,
            hint = R.string.hint_enter_weight,
            inputType = InputType.TYPE_CLASS_NUMBER
        )

        val ENTER_HEIGHT = EnterSingleInfo(
            type = 8,
            title = R.string.label_height,
            desc = R.string.label_height,
            hint = R.string.hint_enter_height,
            inputType = InputType.TYPE_CLASS_NUMBER
        )

    }

    object Languages {
        val ru = LangMode(
            langCodeServer = "ru_KZ",
            langCodeLocale = "ru"
        )
        val kz = LangMode(
            langCodeServer = "kk_KZ",
            langCodeLocale = "kk"
        )
    }

    object PrivacyTermMode {
        val TERM_OF_USE = "TERM_OF_USE"
        val PRIVACY_POLICY = "PRIVACY_POLICY"
    }

    object ErrorFields {
        const val PHONE = "PHONE"
        const val EMAIL = "EMAIL"
        const val FULLNAME = "FULLNAME"
        const val CITY = "CITY"
        const val SCHOOL = "SCHOOL"
        const val PASSWORD = "PASSWORD"
    }

    object PasswordQualityLevels {
        /*
        ^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$
        ^                 # start-of-string
        (?=.*[0-9])       # a digit must occur at least once
        (?=.*[a-z])       # a lower case letter must occur at least once
        (?=.*[A-Z])       # an upper case letter must occur at least once
        (?=.*[@#$%^&+=])  # a special character must occur at least once you can replace with your special characters
        (?=\\S+$)          # no whitespace allowed in the entire string
        .{4,}             # anything, at least six places though
        $                 # end-of-string
        */
        val LEVEL_LOW = Pattern.compile("^(?=\\S+$)(?=.*[a-z]).{8,}$")
        val LEVEL_MIDDLE = Pattern.compile("^(?=\\S+$)(?=.*[a-z])(?=.*[0-9]).{8,}$")
        val LEVEL_HARD = Pattern.compile("^(?=\\S+$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$")

    }
}