package thousand.group.uylen.utils.custom_views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.addTextChangedListener
import thousand.group.uylen.R
import thousand.group.uylen.databinding.CustomEnterCodeViewBinding
import thousand.group.uylen.utils.helpers.ExecHelper

class EnterCodeView : ConstraintLayout {

    var cvTag: String = this.javaClass.simpleName

    private val binding =
        CustomEnterCodeViewBinding.inflate(LayoutInflater.from(context), this, true)

    private var editList = mutableListOf<EditText>()

    private var text = ""
    private var filled = false

    private lateinit var onFillListener: (value: String, filled: Boolean) -> Unit

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        val attrArray = context.obtainStyledAttributes(attrs, R.styleable.EnterCodeView)

        try {
            text = attrArray.getString(R.styleable.EnterCodeView_text)!!
        } catch (ex: Exception) {
            attrArray.recycle()
        }

        editList.add(binding.edit1)
        editList.add(binding.edit2)
        editList.add(binding.edit3)
        editList.add(binding.edit4)

        editList.forEachIndexed { index, it ->
            it.addTextChangedListener {
                if (it.toString().trim().isNotEmpty()) {
                    if (index < (editList.size - 1)) {
                        editList.get(index + 1).requestFocus()
                    }
                } else {
                    if (index > 0) {
                        editList.get(index - 1).requestFocus()
                    }
                }

                ExecHelper().execute {
                    checkEdits()
                }
            }
        }


        setText(text)

    }

    fun setText(text: String) {
        if (editList.isNotEmpty()) {
            clearEditText()
            for (i in 0 until text.length) {
                editList.get(i).setText(text.get(i).toString())
            }
            editList.get(0).requestFocus()
        }
    }

    private fun clearEditText() {
        editList.forEach {
            it.clearFocus()
            it.setText("")
        }
    }

    private fun checkEdits() {
        text = ""

        editList.forEach {
            text += it.text.toString().trim()
        }

        filled = (text.length == editList.size)

        onFillListener.invoke(text, filled)
    }

    fun setOnFilledListener(onFillListener: (value: String, filled: Boolean) -> Unit) {
        this.onFillListener = onFillListener
    }


}