package thousand.group.uylen.utils.base

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.viewbinding.ViewBinding
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import thousand.group.uylen.R
import thousand.group.uylen.utils.constants.AppConstants
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.utils.models.system.ParamsContainer.Companion.createParamsContainer
import kotlin.reflect.KClass

abstract class BaseDialogFragment<ViewBindingType : ViewBinding, ViewModelType : BaseViewModel>(val clazz: KClass<ViewModelType>) :
    DialogFragment() {

    var fragmentTag: String = this.javaClass.simpleName

    protected lateinit var binding: ViewBindingType

    protected val sharedViewModell: SharedViewModel by sharedViewModel()

    protected lateinit var viewModel: ViewModelType

    private var connManager: ConnectivityManager? = null
    private var internetAccess = false

    private val networkRequest = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .build()

    private val networkListener = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            internetSuccess()
            internetAccess = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            internetError()
            internetAccess = false
        }

        override fun onUnavailable() {
            super.onUnavailable()
            internetError()
            internetAccess = false
        }
    }


    override fun onStart() {
        super.onStart()

        dialog?.apply {
            window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = getViewModel(clazz) {
            Log.i("koinViewModel", "createing in Fragment viewModel -> $clazz")
            parametersOf(requireContext(), sharedViewModell, parseBundle())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        initBinding()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addConnectionCallback()

        initView(savedInstanceState)
        initLiveData()
        initController()

    }

    override fun onDestroy() {
        super.onDestroy()

        viewModelStore.clear()
    }

    @SuppressLint("MissingPermission")
    protected fun addConnectionCallback() {
        connManager =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connManager?.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                registerDefaultNetworkCallback(networkListener)
            } else {
                registerNetworkCallback(
                    networkRequest,
                    networkListener
                )
            }
        }
    }

    protected fun isInternetActive() = internetAccess

    protected fun doOnTimeFinished(allTime: Long, periodTime: Long, onFinished: () -> Unit) {
        object : CountDownTimer(allTime, periodTime) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                onFinished.invoke()
            }

        }.start()
    }

    protected fun doIfInternetConnected(onInternetConnected: () -> Unit) {
        if (isInternetActive()) {
            onInternetConnected.invoke()
        } else {
            sharedViewModell.showMessageError(R.string.error_internet_connection)
        }
    }

    private fun parseBundle(): ParamsContainer? {
        var paramsContainer: ParamsContainer? = null

        arguments?.let { bundle ->
            val pc = bundle.getSerializable(AppConstants.BundleConstants.PARAMS_CONTAINER)

            if (pc != null) {
                paramsContainer = pc as ParamsContainer
            } else {
                paramsContainer = createParamsContainer(bundle)
            }
        }

        return paramsContainer
    }

    protected fun setStandartLiveDataCallback() {
        observeLiveData(sharedViewModell.ldSendMessageStr) {
            viewModel.parseReceivedMessage(
                it.first,
                it.second,
                it.third,
                liveData = sharedViewModell.ldSendMessageStr
            )
        }
        observeLiveData(sharedViewModell.ldSendMessageInt) {
            viewModel.parseReceivedMessage(
                it.first,
                it.second,
                it.third,
                liveData = sharedViewModell.ldSendMessageInt
            )
        }
        observeLiveData(sharedViewModell.ldSendMessageBoolean) {
            viewModel.parseReceivedMessage(
                it.first,
                it.second,
                it.third,
                liveData = sharedViewModell.ldSendMessageBoolean
            )
        }
        observeLiveData(sharedViewModell.ldSendMessageParcelable) {
            viewModel.parseReceivedMessage(
                it.first,
                it.second,
                it.third,
                liveData = sharedViewModell.ldSendMessageParcelable
            )
        }
        observeLiveData(sharedViewModell.ldSendMessageSerializable) {
            viewModel.parseReceivedMessage(
                it.first,
                it.second,
                it.third,
                liveData = sharedViewModell.ldSendMessageSerializable
            )
        }
        observeLiveData(sharedViewModell.ldSendMessageMutableList) {
            viewModel.parseReceivedMessage(
                it.first,
                it.second,
                it.third,
                liveData = sharedViewModell.ldSendMessageMutableList
            )
        }
        observeLiveData(sharedViewModell.ldSendMessageCallback) {
            viewModel.parseReceivedMessage(
                it.first,
                it.second,
                message = null,
                unitLiveData = sharedViewModell.ldSendMessageCallback
            )

        }
    }

    private fun initBinding() {
        this.binding = getBindingObject()
    }

    abstract fun getBindingObject(): ViewBindingType

    abstract fun internetSuccess()

    abstract fun internetError()

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initLiveData()

    abstract fun initController()
}
