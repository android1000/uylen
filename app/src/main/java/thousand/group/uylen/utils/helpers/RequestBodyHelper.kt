package thousand.group.uylen.utils.helpers

import android.graphics.Bitmap
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

object RequestBodyHelper {
    internal fun <T> getRequestBodyText(item: T): RequestBody = item.toString()
        .toRequestBody("text/plain".toMediaTypeOrNull())

    internal fun getRequestBodyImage(item: File): RequestBody = item
        .asRequestBody("image/*".toMediaTypeOrNull())

    internal fun getRequestBodyImage(item: ByteArray): RequestBody = item
        .toRequestBody("image/*".toMediaTypeOrNull(), 0, item.size)

    internal fun getMultipartData(name: String, file: File?): MultipartBody.Part? {
        file?.apply {
            val filePart = getRequestBodyImage(this)

            return MultipartBody.Part.createFormData(
                name,
                this.name,
                filePart
            )
        }
        return null
    }

    internal fun getMultipartData(
        name: String,
        file_name: String,
        filePart: RequestBody?
    ): MultipartBody.Part? {
        filePart?.apply {
            return MultipartBody.Part.createFormData(
                name,
                file_name,
                filePart
            )
        }
        return null
    }

    internal fun getMultipartData(
        name: String,
        bitmap: Bitmap?
    ): MultipartBody.Part? {

        bitmap?.let {
            return BitmapHelper.getFileDataFromBitmap(name, it)
        }

        return null
    }

    fun getPartMap(params: MutableMap<String, Any>): MutableMap<String, RequestBody> {
        val parts = mutableMapOf<String, RequestBody>()

        for ((key, value) in params) {
            parts.put(key, getRequestBodyText(value))
        }

        return parts
    }


}