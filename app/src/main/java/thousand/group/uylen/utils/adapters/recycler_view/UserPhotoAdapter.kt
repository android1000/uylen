package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.UserPhoto
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemRegImagesBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.extensions.setVisibilityState
import thousand.group.uylen.utils.models.locale.RegistrationPhoto

class UserPhotoAdapter(
    val onAddPhotoClickListener: (model: UserPhoto?, pos: Int) -> Unit,
    val onRemovePhotoClickListener: (model: UserPhoto?, pos: Int) -> Unit
) : RecyclerView.Adapter<UserPhotoAdapter.ViewHolder>() {

    private var dataList = mutableListOf<UserPhoto?>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemRegImagesBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<UserPhoto?>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: UserPhoto?, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    inner class ViewHolder(
        private val binding: ItemRegImagesBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: UserPhoto?, pos: Int) {
            binding.root.tag = model

            with(binding.root) {
                if (model != null) {
                    Picasso.get()
                        .load(RemoteMainConstants.SERVER_URL + model.images)
                        .error(R.drawable.ic_error)
                        .into(binding.ivPhoto)

                    binding.ivAddPhoto.setVisibilityState(false)
                    binding.ivRemovePhoto.setVisibilityState(true)

                    binding.ivAvatar.setVisibilityState(model.avatar == 1)

                } else {
                    binding.ivPhoto.setImageResource(R.color.colorGrey100)

                    binding.ivAddPhoto.setVisibilityState(true)
                    binding.ivRemovePhoto.setVisibilityState(false)

                    binding.ivAvatar.setVisibilityState(false)
                }

                binding.ivAddPhoto.setSafelyClickListener {
                    onAddPhotoClickListener.invoke(model, pos)
                }

                binding.ivRemovePhoto.setSafelyClickListener {
                    onRemovePhotoClickListener.invoke(model, pos)
                }

            }
        }
    }
}