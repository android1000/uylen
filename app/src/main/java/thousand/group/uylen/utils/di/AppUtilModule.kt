package thousand.group.uylen.utils.di

import android.content.Context
import org.koin.dsl.module
import thousand.group.uylen.utils.helpers.*
import thousand.group.uylen.utils.system.ResourceManager
import thousand.group.uylen.utils.system.FileUtils

val appUtilModule = module {
    single { ExecHelper() }

    factory { (context: Context) -> ResourceManager(context) }
    factory { (context: Context) -> MessageStatusHelper(context, get()) }
    factory { (context: Context) -> ProgressBarHelper(context, get()) }
    factory { (context: Context) -> LocaleHelper(context, get()) }
    factory { (context: Context) -> FileUtils(context) }
    factory { (context: Context) -> BitmapHelper.withContext(context) }
}