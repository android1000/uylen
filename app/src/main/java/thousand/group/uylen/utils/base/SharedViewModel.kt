package thousand.group.uylen.utils.base

import android.os.Parcelable
import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import thousand.group.uylen.utils.extensions.call
import thousand.group.uylen.utils.extensions.isMutableList
import thousand.group.uylen.utils.system.OnceLiveData
import java.io.Serializable

class SharedViewModel : ViewModel() {

    val ldSuccessStr = OnceLiveData<String>()
    val ldSuccessInt = OnceLiveData<Int>()
    val ldErrorStr = OnceLiveData<String>()
    val ldErrorInt = OnceLiveData<Int>()
    val ldProgressShow = OnceLiveData<Unit>()
    val ldProgressHide = OnceLiveData<Unit>()
    val ldToastStr = OnceLiveData<String>()
    val ldToastInt = OnceLiveData<Int>()
    val ldOpenKeyboard = OnceLiveData<Unit>()
    val ldCloseKeyboard = OnceLiveData<Unit>()
    val ldSetFullScreen = OnceLiveData<Unit>()
    val ldCancelFullScreen = OnceLiveData<String>()

    val ldSendMessageInt = MutableLiveData<Triple<String, String, Int>>()
    val ldSendMessageStr = MutableLiveData<Triple<String, String, String>>()
    val ldSendMessageParcelable = MutableLiveData<Triple<String, String, Parcelable>>()
    val ldSendMessageBoolean = MutableLiveData<Triple<String, String, Boolean>>()
    val ldSendMessageSerializable =
        MutableLiveData<Triple<String, String, Serializable>>()
    val ldSendMessageMutableList = MutableLiveData<Triple<String, String, MutableList<*>>>()
    val ldSendMessageCallback = MutableLiveData<Pair<String, String>>()

    fun showMessageSuccess(message: String) = ldSuccessStr.postValue(message)

    fun showMessageSuccess(@StringRes message: Int) = ldSuccessInt.postValue(message)

    fun showMessageError(message: String) = ldErrorStr.postValue(message)

    fun showMessageError(@StringRes message: Int) = ldErrorInt.postValue(message)

    fun showMessageToast(message: String) = ldToastStr.postValue(message)

    fun showMessageToast(@StringRes message: Int) = ldToastInt.postValue(message)

    fun showProgressBar(show: Boolean) {
        if (show) {
            ldProgressShow.call()
        } else {
            ldProgressHide.call()
        }
    }

    fun openKeyBoard() = ldOpenKeyboard.call()

    fun closeKeyBoard() = ldCloseKeyboard.call()

    fun <T> sendLocaleMessage(
        tag: String,
        key: String,
        message: T?
    ) {
        if (message.isMutableList()) {
            Log.i("TestList", "key:${key}, message:${message}")

            ldSendMessageMutableList.postValue(Triple(tag, key, message as MutableList<*>))
        } else {
            when (message) {
                is Parcelable -> {
                    ldSendMessageParcelable.postValue(Triple(tag, key, message))
                }
                is String -> {
                    ldSendMessageStr.postValue(Triple(tag, key, message))
                }
                is Boolean -> {
                    ldSendMessageBoolean.postValue(Triple(tag, key, message))
                }
                is Int -> {
                    ldSendMessageInt.postValue(Triple(tag, key, message))
                }
                is Serializable -> {
                    ldSendMessageSerializable.postValue(Triple(tag, key, message))
                }
                null -> {
                    Log.i("TestLocale", "send Locale Callback")

                    ldSendMessageCallback.postValue(Pair(tag, key))
                }
            }
        }
    }

    fun setFullScreen() {
        ldSetFullScreen.call()
    }

    fun cancelFullScreen(tag: String) {
        ldCancelFullScreen.postValue(tag)
    }

}