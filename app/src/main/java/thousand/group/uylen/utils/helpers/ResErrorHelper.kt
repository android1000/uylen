package thousand.group.uylen.utils.helpers

import android.util.Log
import com.google.gson.JsonParser
import retrofit2.HttpException
import thousand.group.data.storage.utils.GsonHelper
import thousand.group.uylen.utils.system.ResourceManager

object ResErrorHelper {
    internal fun showThrowableMessage(
        resourceManager: ResourceManager,
        tag: String,
        throwable: Throwable
    ): String = when (throwable) {
        is HttpException -> {
            val body = throwable.response()?.errorBody()
            var mess = ""

            body?.string()?.apply {
                val message = GsonHelper.getExceptionMessage(this)
                Log.i(tag, message)
                Log.i(tag, "")
                mess = message
            }

            mess
        }
        else -> {
            val error = throwable.localizedMessage.toString()
            Log.i(tag, error)
            error
        }
    }

    suspend fun parseErrorByStatusCode(
        resourceManager: ResourceManager,
        tag: String,
        throwable: Throwable
    ): String = when (throwable) {
        is HttpException -> {
            val body = throwable.response()?.errorBody()?.string()
            val bodyJson = JsonParser().parse(body).asJsonObject
            val statusCode = throwable.response()?.code()
            var error = ""

            body?.let { body ->
                when (statusCode) {
                    400, 401, 429, 403, 500 -> {
                        val message = bodyJson.get("message").asString
                        error = message
                    }
                    422 -> {
                        if (bodyJson.has("errors")) {
                            val errors = bodyJson.getAsJsonObject("errors")
                            val firstKey = errors.entrySet().toMutableList().get(0).value.asString

                            error = firstKey
                        } else {
                            val message = bodyJson.get("message").asString
                            error = message
                        }

                    }
                }
            }

            error
        }
        else -> {
            val error = throwable.localizedMessage.toString()
            Log.i(tag, error)

            error
        }
    }
}