package thousand.group.uylen.utils.helpers

import android.util.Log
import androidx.annotation.ColorRes
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException

data class MainFragmentHelper(
    var title: String = "",
    var navBarItemPos: Int? = null,
    var isShowBottomNavBar: Boolean = false,
    var isLightStatusBar: Boolean = false
) {
    companion object {

        internal val TAG = MainFragmentHelper::class.java.simpleName

        fun getJsonFragmentTag(fragmentHelper: MainFragmentHelper): String =
            Gson().toJson(fragmentHelper).toString()

        fun getNavBarItemPos(fragmentTag: String?): Int? {
            Log.i(TAG, "getBottomNavPos -> $fragmentTag")

            return try {
                if (fragmentTag != null) {
                    Gson().fromJson(fragmentTag, MainFragmentHelper::class.java).navBarItemPos
                } else {
                    null
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                null
            }
        }

        fun isShowBottomNavBar(fragmentTag: String?): Boolean {
            Log.i(TAG, "isShowBottomNavBar -> $fragmentTag")
            return try {
                if (fragmentTag != null)
                    Gson().fromJson(fragmentTag, MainFragmentHelper::class.java).isShowBottomNavBar
                else
                    false
            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
                true
            }
        }

        fun isLightStatusBar(fragmentTag: String?): Boolean {
            Log.i(TAG, "isLightStatusBar -> $fragmentTag")
            return try {
                if (fragmentTag != null)
                    Gson().fromJson(fragmentTag, MainFragmentHelper::class.java).isLightStatusBar
                else
                    false
            } catch (e: JsonSyntaxException) {
                e.printStackTrace()
                true
            }
        }

    }
}