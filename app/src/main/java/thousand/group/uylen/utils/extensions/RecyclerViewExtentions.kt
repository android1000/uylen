package thousand.group.uylen.utils.extensions

import androidx.recyclerview.widget.RecyclerView

fun RecyclerView.clearItemDecorations() {
    while (this.itemDecorationCount > 0) {
        this.removeItemDecorationAt(0)
    }
}