package thousand.group.uylen.utils.models.locale

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegistrationPhoto(
    var photo: Bitmap? = null
) : Parcelable