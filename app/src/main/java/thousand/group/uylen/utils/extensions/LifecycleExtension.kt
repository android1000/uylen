package thousand.group.uylen.utils.extensions

import androidx.lifecycle.*

inline fun <T> LifecycleOwner.observeLiveData(
    data: LiveData<T>,
    crossinline onChanged: (T) -> Unit
) {
    data.removeObservers(this)
    data.observe(this, Observer {
        it?.let { value -> onChanged(value) }
    })
}

inline fun <reified T> MutableLiveData<T>.call() {
    when (T::class) {
        Unit::class -> {
            postValue(Unit as T)
        }
    }
}

inline fun <reified T> MutableLiveData<T>.clear() {
    postValue(null)
}

fun <A, B> zipLiveData(a: LiveData<A>, b: LiveData<B>): LiveData<Pair<A, B>> {
    return MediatorLiveData<Pair<A, B>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        fun update() {
            val localLastA = lastA
            val localLastB = lastB

            if (localLastA != null && localLastB != null) {
                this.value = Pair(localLastA, localLastB)
            }
        }

        addSource(a) {
            lastA = it
            update()
        }
        addSource(b) {
            lastB = it
            update()
        }
    }
}

