package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.TariffBanner
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemTariffBannerBinding

class TariffBannerAdapter(
) : RecyclerView.Adapter<TariffBannerAdapter.ViewHolder>() {

    private val dataList = mutableListOf<TariffBanner>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemTariffBannerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<TariffBanner>) {
        this.dataList.clear()
        this.dataList.addAll(dataList)
        notifyDataSetChanged()
    }

    fun getItem(position: Int) = dataList.get(position)

    inner class ViewHolder(
        private val binding: ItemTariffBannerBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: TariffBanner, pos: Int) {
            binding.root.tag = model

            Picasso
                .get()
                .load(RemoteMainConstants.SERVER_URL + model.image)
                .error(R.drawable.ic_error)
                .into(binding.ivBanner)

        }
    }

}