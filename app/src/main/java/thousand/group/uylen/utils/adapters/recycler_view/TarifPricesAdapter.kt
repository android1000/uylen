package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import thousand.group.data.entities.remote.simple.TariffPrice
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemTarifPriceBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.extensions.setVisibilityState
import java.text.DecimalFormat

class TarifPricesAdapter(
    val onItemClicked: (model: TariffPrice, pos: Int) -> Unit
) : RecyclerView.Adapter<TarifPricesAdapter.ViewHolder>() {

    private var dataList = mutableListOf<TariffPrice>()
    private var adapterTag: String = this.javaClass.simpleName

    private val formatter = DecimalFormat("#,###")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemTarifPriceBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<TariffPrice>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: TariffPrice, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    inner class ViewHolder(
        private val binding: ItemTarifPriceBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {

        private val popularType = resources.getString(R.string.field_popular)
        private val bestType = resources.getString(R.string.field_best_price)

        fun bind(model: TariffPrice, pos: Int) {
            binding.root.tag = model

            val monthSuffix = when {
                model.date == 1 -> R.string.label_month_1
                model.date > 1 && model.date < 5 -> R.string.label_month_2
                else -> R.string.label_month_3
            }

            val price =
                formatter.format(model.price)
                    .replace(",", " ")

            val priceFormatted = String.format(
                binding.root.resources.getString(R.string.format_price),
                price
            )

            binding.tvPrice.setText(priceFormatted)
            binding.tvMonthCount.setText(model.date.toString())
            binding.tvMonthField.setText(resources.getString(monthSuffix))

            when (model.price_type) {
                null -> {
                    binding.tvPopular.setVisibilityState(false)
                }
                popularType -> {
                    binding.tvPopular.setVisibilityState(true)
                    binding.tvPopular.setText(R.string.label_popular)
                }
                bestType -> {
                    binding.tvPopular.setVisibilityState(true)
                    binding.tvPopular.setText(R.string.label_best_price)
                }
            }

            if (model.isSelected) {
                binding.tvPopular.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.colorWhite
                    )
                )
                binding.tvPopular.setBackgroundResource(R.drawable.back_tarif_best)

                binding.tvPrice.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.colorGrey600
                    )
                )
                binding.tvMonthCount.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.colorGrey600
                    )
                )
                binding.tvMonthField.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.colorGrey600
                    )
                )

                binding.root.setBackgroundResource(R.color.colorWhite)

            } else {
                binding.tvPopular.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.colorGrey600
                    )
                )
                binding.tvPopular.setBackgroundResource(R.drawable.back_tarif_popular)

                binding.tvPrice.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.colorWhite
                    )
                )
                binding.tvMonthCount.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.colorWhite
                    )
                )
                binding.tvMonthField.setTextColor(
                    ContextCompat.getColor(
                        binding.root.context,
                        R.color.colorWhite
                    )
                )

                binding.root.setBackgroundResource(R.color.colorBlackOp24)
            }

            binding.root.setSafelyClickListener {
                onItemClicked.invoke(model, pos)
            }
        }
    }
}
