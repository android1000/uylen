package thousand.group.uylen.utils.helpers

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*

object DateParser {

    const val format_YYYY_MM_dd = "yyyy-MM-dd"
    const val format_DD_MM_YYYY_With_Dots = "dd.MM.yyyy"
    const val format_dd_LLLL = "dd LLLL"
    const val format_dd_LLLL_YYYY = "dd LLLL, yyyy"
    const val format_dd_LLLL_YYYY_HH_MM = "dd LLLL, yyyy HH:mm"
    const val format_dd_LL_YYYY_HH_MM = "dd LL, yyyy HH:mm"
    const val format_YYY_MM_HH_MM_SS = "yyyy-MM-dd HH:mm:ss"
    const val format_YYY_MM_HH_MM = "yyyy-MM-dd HH:mm"
    const val format_HH_MM = "HH:mm"
    const val format_dd = "dd"
    const val format_HH = "HH"
    const val format_MM = "MM"
    const val format_SHORT_DAY_NAME = "EE"
    const val format_dd_LLLL_YYYY_EEEE = "dd LLLL, yyyy, EEEEE"
    const val format_FULL_DAY_NAME = "EEEE"
    const val formatStandartFormatTime = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val formatStandartFormatTime2 = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'"
    const val formatMonthName = ""
    const val formatSIMPLE_TIME = "HH:mm"

    fun stringToDate(dateStr: String, format: String): Date {
        val simpleDateFormat = SimpleDateFormat(format, Locale.US)
        return simpleDateFormat.parse(dateStr)
    }

    fun formatToFullMonthNameCreate(dateStr: String): String {
        val parser = SimpleDateFormat(format_YYYY_MM_dd, Locale.getDefault())
        val formatter = SimpleDateFormat(format_dd_LLLL, Locale.getDefault())
        val output = formatter.format(parser.parse(dateStr))

        return output
    }

    fun convertOneFormatToAnother(
        dateStr: String?,
        fromFormat: String,
        toFormat: String
    ): String {

        dateStr?.apply {
            val parser = SimpleDateFormat(fromFormat, Locale.getDefault())
            val formatter = SimpleDateFormat(toFormat, Locale.getDefault())
            val output = formatter.format(parser.parse(dateStr))

            return output
        }
        return ""
    }

    fun getCurrentTimeFormat(toFormat: String): String {
        val time = Calendar.getInstance().time
        val formatter = SimpleDateFormat(toFormat, Locale.getDefault())
        return formatter.format(time)
    }

    fun formatToFullMonthNameWithYearCreate(dateStr: String): String {
        val parser = SimpleDateFormat(format_YYYY_MM_dd, Locale.getDefault())
        val formatter = SimpleDateFormat(format_dd_LLLL_YYYY, Locale.getDefault())
        val output = formatter.format(parser.parse(dateStr))

        return output
    }

    fun formatToDateWithDots(dateStr: String): String {
        val parser = SimpleDateFormat(format_YYYY_MM_dd, Locale.getDefault())
        val formatter = SimpleDateFormat(format_DD_MM_YYYY_With_Dots, Locale.getDefault())
        val output = formatter.format(parser.parse(dateStr))

        return output
    }

    fun formatDate(date: Date, toFormat: String): String {
        val simpleDateFormat = SimpleDateFormat(toFormat, Locale.getDefault())
        return simpleDateFormat.format(date)
    }

    fun isCurrentDate(dateStrVal: String): Boolean {
        val dateFormat = SimpleDateFormat(format_YYY_MM_HH_MM_SS)
        val date = dateFormat.parse(dateStrVal)

        return if (date != null) DateUtils.isToday(date.time) else false
    }

}