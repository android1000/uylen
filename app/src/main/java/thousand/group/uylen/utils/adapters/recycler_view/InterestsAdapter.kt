package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemInterestBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener

class InterestsAdapter(
    val onItemClickListener: (model: Interest, pos: Int) -> Unit,
) : RecyclerView.Adapter<InterestsAdapter.ViewHolder>() {

    private var dataList = mutableListOf<Interest>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemInterestBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<Interest>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: Interest, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    inner class ViewHolder(
        private val binding: ItemInterestBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: Interest, pos: Int) {
            binding.root.tag = model

            with(binding.root) {
                setText(model.title)

                if (model.isSelected) {
                    setTextColor(ContextCompat.getColor(context, R.color.colorWhite))
                    setBackgroundResource(R.drawable.back_interest_item_active)
                } else {
                    setTextColor(ContextCompat.getColor(context, R.color.colorGrey500))
                    setBackgroundResource(R.drawable.back_interest_item_disactive)
                }

                setSafelyClickListener {
                    onItemClickListener.invoke(model, pos)
                }

            }
        }
    }
}