package thousand.group.uylen.utils.models.locale

import android.os.Parcelable
import android.view.inputmethod.EditorInfo
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EnterSingleInfo(
    val type: Int,
    val title: Int,
    val desc: Int,
    val hint: Int,
    val inputType:Int
) : Parcelable