package thousand.group.uylen.utils.custom_views

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.doOnNextLayout
import thousand.group.uylen.R
import thousand.group.uylen.databinding.CustomTitleEditTextBinding
import thousand.group.uylen.utils.models.locale.SavedState

class CustomTitleEditText : ConstraintLayout {
    private val TAG = this::class.simpleName

    init {
        isSaveEnabled = true
    }

    private var inputText: String? = null
    private var titleText: String? = null
    private var hintText: String? = null

    private var editInputType = 0
    private var linesCount = 0

    private val binding =
        CustomTitleEditTextBinding.inflate(LayoutInflater.from(context), this, true)

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        Log.i("TestState", "init")

        val attrArray = context.obtainStyledAttributes(attrs, R.styleable.CustomTitleEditText)

        try {
            inputText = attrArray.getString(R.styleable.CustomTitleEditText_text)
            hintText = attrArray.getString(R.styleable.CustomTitleEditText_hint_text)
            titleText = attrArray.getString(R.styleable.CustomTitleEditText_title_text)
            linesCount = attrArray.getInt(R.styleable.CustomTitleEditText_android_maxLines, 0)

            editInputType = attrArray.getInt(
                R.styleable.CustomTitleEditText_android_inputType,
                EditorInfo.TYPE_NULL
            )
        } finally {
            attrArray.recycle()
        }

        if (editInputType != EditorInfo.TYPE_NULL) {
            binding.etInput.inputType = editInputType
        }

        if (linesCount != 0) {
            binding.etInput.maxLines = linesCount
        }

        inputText?.apply {
            binding.etInput.setText(this)
        }
        hintText?.apply {
            binding.etInput.hint = this
        }

        titleText?.apply {
            binding.tvTitle.setText(this)
        }
    }

    fun getText() = binding.etInput.text.toString().trim()

    fun setText(text: String) = binding.etInput.setText(text)
    fun setText(@StringRes text: Int) = binding.etInput.setText(text)

    fun getEditText() = binding.etInput

    override fun onSaveInstanceState(): Parcelable? {
        Log.i("TestState", "onSaveInstanceState")

        val superState = super.onSaveInstanceState()

        val myState = SavedState(superState)
        myState.text = getText()

        return myState
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        Log.i("TestState", "onRestoreInstanceState -> state: $state")

        val savedState = state as SavedState
        super.onRestoreInstanceState(savedState.getSuperState())

        inputText = savedState.text

        Log.i("TestState", "onRestoreInstanceState -> inputText: $inputText")

        doOnNextLayout {
            binding.etInput.let {
                it.setText(inputText)

                if (!inputText.isNullOrEmpty()) {
                    it.setSelection(inputText!!.length - 1)
                }
            }
        }
    }

}