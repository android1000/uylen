package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemRecentUserBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener

class RecentSearchUsersAdapter(
    val onItemClickListener: (model: UserCardData, pos: Int) -> Unit,
    val onDeleteClickListener: (model: UserCardData, pos: Int) -> Unit
) :
    RecyclerView.Adapter<RecentSearchUsersAdapter.ViewHolder>() {

    private var dataList = mutableListOf<UserCardData>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemRecentUserBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<UserCardData>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: UserCardData, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    fun deleteItem(position: Int) {
        dataList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, dataList.size)
    }

    inner class ViewHolder(
        private val binding: ItemRecentUserBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: UserCardData, pos: Int) {
            binding.root.tag = model

            Picasso.get()
                .load(RemoteMainConstants.SERVER_URL + model.avatar)
                .error(R.drawable.ic_error)
                .into(binding.ivAvatar)

            binding.tvName.setText(model.name)

            binding.ivClose.setSafelyClickListener {
                onDeleteClickListener.invoke(model, pos)
            }

            binding.root.setSafelyClickListener {
                onItemClickListener.invoke(model, pos)
            }
        }
    }
}