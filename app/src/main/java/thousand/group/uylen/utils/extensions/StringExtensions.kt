package thousand.group.uylen.utils.extensions

import android.graphics.Paint
import android.telephony.PhoneNumberUtils
import android.text.TextUtils
import thousand.group.uylen.utils.constants.AppConstants.AudioLinkConstants.AUDIO
import thousand.group.uylen.utils.constants.AppConstants.AudioLinkConstants.MP3
import thousand.group.uylen.utils.constants.AppConstants.AudioLinkConstants.OGG
import thousand.group.uylen.utils.constants.AppConstants.PhotoLinkConstants.GIF
import thousand.group.uylen.utils.constants.AppConstants.PhotoLinkConstants.JPG
import thousand.group.uylen.utils.constants.AppConstants.PhotoLinkConstants.PHOTO
import thousand.group.uylen.utils.constants.AppConstants.PhotoLinkConstants.PNG
import thousand.group.uylen.utils.constants.AppConstants.VideoLinkConstants.FLV
import thousand.group.uylen.utils.constants.AppConstants.VideoLinkConstants.GP_3
import thousand.group.uylen.utils.constants.AppConstants.VideoLinkConstants.M4A
import thousand.group.uylen.utils.constants.AppConstants.VideoLinkConstants.MKV
import thousand.group.uylen.utils.constants.AppConstants.VideoLinkConstants.MP4
import thousand.group.uylen.utils.constants.AppConstants.VideoLinkConstants.VIDEO
import java.util.regex.Pattern


private const val YOU_TUBE_ID_PATTERN =
    "^(?:(?:\\\\w*.?://)?\\\\w*.?\\\\w*-?.?\\\\w*/(?:embed|e|v|watch|.*/)?\\\\??(?:feature=\\\\w*\\\\.?\\\\w*)?&?(?:v=)?/?)([\\\\w\\\\d_-]+).*"


fun String.getYouTubeVideoId(): String? {
    val matcher = Pattern.compile(YOU_TUBE_ID_PATTERN).matcher(this)
    matcher.find()

    return matcher.group(1)
}

fun String.extractYTId(): String? {
    var vId: String? = null
    val pattern = Pattern.compile(
        "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
        Pattern.CASE_INSENSITIVE
    )
    val matcher = pattern.matcher(this)
    if (matcher.matches()) {
        vId = matcher.group(1)
    }
    return vId
}

fun String.isYouTubeValidLink(): Boolean {
    val pattern = "^(http(s)?://)?((w){3}.)?youtu(be|.be)?(\\.com)?/.+"
    return  this.isNotEmpty() && this.matches(pattern.toRegex())
}

fun String.getVideoId(): String? {
    val reg =
        "(?:youtube(?:-nocookie)?.com/(?:[^/\\n\\s]+/\\S+/|(?:v|e(?:mbed)?)/|\\S*?[?&]v=)|youtu\\.be/)([a-zA-Z0-9_-]{11})"
    val pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(this)
    return if (matcher.find()) matcher.group(1) else null
}

fun String.isEmailValid(): Boolean {
    return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.getFileTypeFromURL(url: String): String {
    val splitedArray = url.split("\\.").toTypedArray()
    val lastValueOfArray = splitedArray[splitedArray.size - 1]
    return when (lastValueOfArray) {
        MP4, FLV, M4A, GP_3, MKV -> VIDEO
        MP3, OGG -> AUDIO
        JPG, PNG, GIF -> PHOTO
        else -> ""
    }
}

fun String.isVideoUrl() = getFileTypeFromURL(this).equals(VIDEO)

fun String.formatToPhoneNumber(): String {
    return "+7" + PhoneNumberUtils.formatNumber(this, "KZ")
}

fun String.getWidthOfString(textSize: Float): Float {
    val paint = Paint()
    paint.textSize = textSize

    return paint.measureText(this, 0, this.length)

}