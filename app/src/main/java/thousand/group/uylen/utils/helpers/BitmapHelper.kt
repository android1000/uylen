package thousand.group.uylen.utils.helpers

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import okhttp3.MultipartBody
import java.io.ByteArrayOutputStream

@SuppressLint("LongLogTag")
class BitmapHelper {
    companion object {
        fun withContext(context: Context) = BitmapHelper(context)

        fun getFileDataFromBitmap(fieldName: String, bitmap: Bitmap): MultipartBody.Part? {
            val byteArrayOutputStream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream)

            val filePart =
                RequestBodyHelper.getRequestBodyImage(byteArrayOutputStream.toByteArray())

            return RequestBodyHelper.getMultipartData(
                fieldName,
                "${System.currentTimeMillis()}.jpg",
                filePart
            )
        }
    }

    private var contextt: Context? = null

    constructor(context: Context) {
        this.contextt = context
    }

    fun getBitmapFromUrl(
        url: String,
        image_width: Int?,
        image_height: Int?,
        imageLoadedListener: (bitmap: Bitmap) -> Unit
    ) {
        contextt?.apply {

            if (image_width == null || image_height == null) {
                Glide.with(this)
                    .asBitmap()
                    .load(url)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) {}

                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            imageLoadedListener.invoke(resource)
                        }
                    })
            } else {
                Glide.with(this)
                    .asBitmap()
                    .load(url)
                    .apply(RequestOptions().override(image_width, image_height))
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) {}

                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            imageLoadedListener.invoke(resource)
                        }
                    })
            }
        }
    }

    fun getBitmapFromUri(
        uri: Uri,
        image_width: Int?,
        image_height: Int?,
        imageLoadedListener: (bitmap: Bitmap) -> Unit
    ) {
        contextt?.apply {
            if (image_width == null || image_height == null) {
                Glide.with(this)
                    .asBitmap()
                    .load(uri)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) {}

                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            imageLoadedListener.invoke(resource)
                        }
                    })
            } else {
                Glide.with(this)
                    .asBitmap()
                    .load(uri)
                    .apply(RequestOptions().override(image_width, image_height))
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadCleared(placeholder: Drawable?) {}

                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: Transition<in Bitmap>?
                        ) {
                            imageLoadedListener.invoke(resource)
                        }
                    })
            }

        }
    }

    fun getBitmapSync(uri: Uri, imageLoadedListener: (bitmap: Bitmap) -> Unit) {
        contextt?.apply {
            val futureTarget = Glide.with(this)
                .asBitmap()
                .load(uri)
                .submit()

            val bitmap = futureTarget.get()

            imageLoadedListener.invoke(bitmap)
        }
    }
}