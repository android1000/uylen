package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.TariffPrice
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemInterestBinding
import thousand.group.uylen.databinding.ItemPeriodsBinding
import thousand.group.uylen.utils.extensions.setVisibilityState
import thousand.group.uylen.utils.helpers.DateParser
import java.text.DecimalFormat

class CurrentPeriodAdapter(val priceId: Long, val endDate: String) :
    RecyclerView.Adapter<CurrentPeriodAdapter.ViewHolder>() {

    private var dataList = mutableListOf<TariffPrice>()
    private var adapterTag: String = this.javaClass.simpleName

    private val formatter = DecimalFormat("#,###")

    private val refreshDate = DateParser.convertOneFormatToAnother(
        endDate,
        DateParser.format_YYYY_MM_dd,
        DateParser.format_dd_LLLL_YYYY
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPeriodsBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<TariffPrice>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: TariffPrice, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    inner class ViewHolder(
        private val binding: ItemPeriodsBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: TariffPrice, pos: Int) {
            binding.root.tag = model

            val monthSuffix = when {
                model.date == 1 -> R.string.label_month_1
                model.date > 1 && model.date < 5 -> R.string.label_month_2
                else -> R.string.label_month_3
            }

            val price =
                formatter.format(model.price)
                    .replace(",", " ")

            val priceFormatted = String.format(
                binding.root.resources.getString(R.string.format_price),
                price
            )

            if (model.id == priceId){
                binding.tvMonthCount.setTypeface(binding.tvMonthCount.typeface, Typeface.BOLD)
                binding.txtDescProfile.setText(refreshDate)
                binding.txtDescProfile.setVisibilityState(true)
                binding.ivCheckedMark.setVisibilityState(true)
            }

                binding.tvMonthCount.setText("${model.date} ${resources.getString(monthSuffix)}")
            binding.tvPrice.setText(priceFormatted)

            binding.flBottom.visibility =
                if (pos != dataList.size.dec()) View.VISIBLE else View.INVISIBLE
        }
    }
}