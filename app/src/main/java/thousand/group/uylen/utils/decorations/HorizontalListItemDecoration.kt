package thousand.group.uylen.utils.decorations

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class HorizontalListItemDecoration(private var space: Int, private var size: Int) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {

        if (parent.getChildAdapterPosition(view) != 0 || parent.getChildAdapterPosition(view) != size - 1) {
            outRect.left = space / 2
            outRect.right = space / 2
        } else {
            outRect.setEmpty()
        }
    }
}