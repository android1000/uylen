package thousand.group.uylen.utils.custom_views

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.doOnNextLayout
import thousand.group.uylen.R
import thousand.group.uylen.databinding.CustomTitleTextArrowBinding
import thousand.group.uylen.utils.extensions.setVisibilityState

class CustomTitleTextArrow : ConstraintLayout {
    private val TAG = this::class.simpleName

    private var titleText: String? = null
    private var simpleText: String? = null
    private var isBoldTitle: Boolean = false
    private var arrowEnabled: Boolean = true
    private var showBottomLine: Boolean = true
    private var hintText: String? = null

    private val binding =
        CustomTitleTextArrowBinding.inflate(LayoutInflater.from(context), this, true)

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {
        Log.i("TestState", "init")

        val attrArray =
            context.obtainStyledAttributes(attrs, R.styleable.CustomTitleTextArrow)

        try {
            titleText = attrArray.getString(R.styleable.CustomTitleTextArrow_title_text)
            hintText = attrArray.getString(R.styleable.CustomTitleTextArrow_hint_text)
            simpleText = attrArray.getString(R.styleable.CustomTitleTextArrow_text)
            isBoldTitle =
                attrArray.getBoolean(R.styleable.CustomTitleTextArrow_is_bold_title, false)
            arrowEnabled =
                attrArray.getBoolean(R.styleable.CustomTitleTextArrow_arrow_enabled, true)
            showBottomLine =
                attrArray.getBoolean(R.styleable.CustomTitleTextArrow_show_bottom_line, true)
        } finally {
            attrArray.recycle()
        }

        Log.i(TAG, "titleText: ${titleText}")
        Log.i(TAG, "hintText: ${hintText}")

        doOnNextLayout {
            checkHintTextEmpty()

            simpleText?.apply {
                binding.tvVal.setText(this)
            }

            hintText?.apply {
                binding.tvHint.setText(this)
            }

            titleText?.apply {
                binding.tvTitle.setText(this)
            }

            binding.ivLeftArrow.setVisibilityState(arrowEnabled)

            if (!showBottomLine) {
                binding.root.setBackgroundResource(R.color.colorWhite)
            }

            if (isBoldTitle) {
                binding.tvTitle.setTypeface(ResourcesCompat.getFont(context, R.font.nunito_bold))
                binding.tvVal.gravity = Gravity.END
            }
        }
    }

    fun getText() = binding.tvVal.text.toString()

    fun setText(text: String) {
        binding.tvVal.setText(text)
        simpleText = text

        checkHintTextEmpty()
    }

    fun setText(@StringRes text: Int) {
        binding.tvVal.setText(text)
        simpleText = resources.getString(text)

        checkHintTextEmpty()
    }

    private fun checkHintTextEmpty() {
        val check = simpleText.isNullOrEmpty()

        binding.tvHint.setVisibilityState(check)
        binding.tvVal.setVisibilityState(!check)
    }

}