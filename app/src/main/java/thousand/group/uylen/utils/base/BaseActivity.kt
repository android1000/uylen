package thousand.group.uylen.utils.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.*
import android.view.View.*
import android.view.WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewbinding.ViewBinding
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import thousand.group.data.entities.remote.simple.ChatMessage
import thousand.group.uylen.R
import thousand.group.uylen.utils.extensions.observeLiveData
import thousand.group.uylen.utils.helpers.LocaleHelper
import thousand.group.uylen.utils.helpers.MessageStatusHelper
import thousand.group.uylen.utils.helpers.ProgressBarHelper
import thousand.group.uylen.utils.models.system.ParamsContainer
import thousand.group.uylen.views.chat.ChatMessagesFragment
import thousand.group.uylen.views.home.NewPairFragment
import kotlin.reflect.KClass

abstract class BaseActivity<ViewBindingType : ViewBinding, ViewModelType : BaseViewModel>(val clazz: KClass<ViewModelType>) :
    AppCompatActivity() {

    var activityTag: String = this.javaClass.simpleName

    private var startFlag = false
    private var canParseMessages = false
    private var isIvMessageViewClicked = false

    protected lateinit var binding: ViewBindingType

    protected val sharedViewModel: SharedViewModel by viewModel()

    protected lateinit var viewModel: ViewModelType

    private val messageHelper: MessageStatusHelper by inject {
        parametersOf(this)
    }
    private val progressHelper: ProgressBarHelper by inject {
        parametersOf(this)
    }

    private var connManager: ConnectivityManager? = null
    private var internetAccess = false

    private val networkRequest = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .build()

    private val networkListener = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            internetSuccess()
            internetAccess = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            internetError()
            internetAccess = false
        }

        override fun onUnavailable() {
            super.onUnavailable()
            internetError()
            internetAccess = false
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            if (startFlag) {
                when (supportFragmentManager.findFragmentById(R.id.fl_fragment_container)) {
                    is ChatMessagesFragment -> {

                    }
                    else -> {
                        val imm =
                            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                    }
                }
            } else {
                startFlag = true
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = getViewModel(clazz) {
            Log.i("koinViewModel", "createing in Fragment viewModel -> $clazz")
            parametersOf(this, sharedViewModel, parseIntent())
        }

        initBinding()
        setContentView(binding.root)

//        setTransparentStatusBar()

        setStandartLiveDataCallback()
        addConnectionCallback()
        registerFragmentLifecycle()

        initIntent(intent)
        initView(savedInstanceState)
        initLiveData()
        initController()

        startFlag = true

    }

    override fun onDestroy() {
        Log.i("TestDestroy", "onDestroy")

        viewModelStore.clear()
        super.onDestroy()
    }

    override fun onPause() {
        super.onPause()

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        initIntent(intent)
    }

    override fun attachBaseContext(newBase: Context?) {
        newBase?.apply {
            try {
                val localeHelper: LocaleHelper by inject {
                    parametersOf(this)
                }

                super.attachBaseContext(localeHelper.wrap())
            } catch (e: ClassCastException) {
                e.printStackTrace()
            }
        }
    }

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {

        overrideConfiguration?.let {
            val uiMode = it.uiMode
            it.setTo(baseContext.resources.configuration)
            it.uiMode = uiMode
        }

        super.applyOverrideConfiguration(overrideConfiguration)
    }

    protected fun closeKeyboard() {
        Log.i(activityTag, "close Keyboard")

        currentFocus?.apply {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(windowToken, 0)
        }
    }

    protected fun openKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    protected fun setStatusBarParams(lightStatusBar: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            /*  WindowCompat.setDecorFitsSystemWindows(window, false)
              window.statusBarColor = Color.TRANSPARENT*/

            val lightStatusBarApp = if (lightStatusBar) APPEARANCE_LIGHT_STATUS_BARS else 0

            window.insetsController?.setSystemBarsAppearance(
                lightStatusBarApp,
                lightStatusBarApp
            )
        } else {
/*            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)

            window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
            )*/

            window.decorView.systemUiVisibility =
                if (lightStatusBar) {
                    /*SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or*/ SYSTEM_UI_FLAG_LAYOUT_STABLE or SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
                } else {
                    /*SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or */SYSTEM_UI_FLAG_LAYOUT_STABLE
                }
        }

    }

    protected fun isInternetActive() = internetAccess

    protected fun setStandartLiveDataCallback() {

        viewModel.let {
            observeLiveData(sharedViewModel.ldSuccessStr) {
                messageHelper.showDialogMessageSuccess(it)
            }
            observeLiveData(sharedViewModel.ldSuccessInt) {
                messageHelper.showDialogMessageSuccess(it)
            }
            observeLiveData(sharedViewModel.ldErrorStr) {
                messageHelper.showDialogMessageError(it)
            }
            observeLiveData(sharedViewModel.ldErrorInt) {
                messageHelper.showDialogMessageError(it)
            }
            observeLiveData(sharedViewModel.ldProgressShow) {
                progressHelper.showProgress()
            }
            observeLiveData(sharedViewModel.ldProgressHide) {
                doOnTimeFinished(100, 100) {
                    progressHelper.hideProgress()
                }
            }
            observeLiveData(sharedViewModel.ldToastStr) {
                Toast.makeText(this@BaseActivity, it, Toast.LENGTH_SHORT).show()
            }
            observeLiveData(sharedViewModel.ldToastInt) {
                Toast.makeText(this@BaseActivity, it!!, Toast.LENGTH_SHORT).show()
            }
            observeLiveData(sharedViewModel.ldOpenKeyboard) {
                openKeyboard()
            }
            observeLiveData(sharedViewModel.ldCloseKeyboard) {
                closeKeyboard()
            }
            observeLiveData(sharedViewModel.ldSendMessageStr) {
                viewModel.parseReceivedMessage(
                    it.first,
                    it.second,
                    it.third,
                    liveData = sharedViewModel.ldSendMessageStr
                )
            }
            observeLiveData(sharedViewModel.ldSendMessageInt) {
                viewModel.parseReceivedMessage(
                    it.first,
                    it.second,
                    it.third,
                    liveData = sharedViewModel.ldSendMessageInt
                )
            }
            observeLiveData(sharedViewModel.ldSendMessageBoolean) {
                viewModel.parseReceivedMessage(
                    it.first,
                    it.second,
                    it.third,
                    liveData = sharedViewModel.ldSendMessageBoolean
                )
            }
            observeLiveData(sharedViewModel.ldSendMessageParcelable) {
                viewModel.parseReceivedMessage(
                    it.first,
                    it.second,
                    it.third,
                    liveData = sharedViewModel.ldSendMessageParcelable
                )
            }
            observeLiveData(sharedViewModel.ldSendMessageSerializable) {
                viewModel.parseReceivedMessage(
                    it.first,
                    it.second,
                    it.third,
                    liveData = sharedViewModel.ldSendMessageSerializable
                )
            }
            observeLiveData(sharedViewModel.ldSendMessageMutableList) {
                viewModel.parseReceivedMessage(
                    it.first,
                    it.second,
                    it.third,
                    liveData = sharedViewModel.ldSendMessageMutableList
                )
            }
            observeLiveData(sharedViewModel.ldSendMessageCallback) {
                viewModel.parseReceivedMessage(
                    it.first,
                    it.second,
                    message = null,
                    unitLiveData = sharedViewModel.ldSendMessageCallback
                )
            }
        }
    }


    @SuppressLint("MissingPermission")
    protected fun addConnectionCallback() {
        connManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connManager?.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                registerDefaultNetworkCallback(networkListener)
            } else {
                registerNetworkCallback(
                    networkRequest,
                    networkListener
                )
            }
        }
    }

    protected fun doOnTimeFinished(allTime: Long, periodTime: Long, onFinished: () -> Unit) {
        object : CountDownTimer(allTime, periodTime) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                onFinished.invoke()
            }

        }.start()
    }

    protected fun doIfInternetConnected(onInternetConnected: () -> Unit) {
        if (isInternetActive()) {
            onInternetConnected.invoke()
        } else {
            sharedViewModel.showMessageError(R.string.error_internet_connection)
        }
    }

    private fun registerFragmentLifecycle() {
        supportFragmentManager.registerFragmentLifecycleCallbacks(
            object : FragmentManager.FragmentLifecycleCallbacks() {

                override fun onFragmentStarted(fm: FragmentManager, f: Fragment) {
                    super.onFragmentStarted(fm, f)

                    f.tag?.apply {
                        Log.i(activityTag, this)
                        if (isJSONValid(this)) {
                            fragmentLifeCycleController(this)
                        }
                    }
                }

                override fun onFragmentPaused(fm: FragmentManager, f: Fragment) {
                    super.onFragmentPaused(fm, f)

                    if (!fm.fragments.isNullOrEmpty()) {
                        val tag = fm.fragments[fm.fragments.size - 1].tag

                        tag?.apply {
                            Log.i(activityTag, this)

                            if (isJSONValid(this)) {
                                fragmentLifeCycleController(this)
                            }
                        }
                    }
                }

                override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
                    super.onFragmentResumed(fm, f)

                    if (!fm.fragments.isNullOrEmpty()) {
                        val tag = fm.fragments[fm.fragments.size - 1].tag

                        tag?.apply {
                            Log.i(activityTag, this)

                            if (isJSONValid(this)) {
                                fragmentLifeCycleController(this)
                            }
                        }
                    }
                }
            },
            true
        )
    }

    private fun parseIntent(): ParamsContainer? {
        var paramsContainer: ParamsContainer? = null

        intent.extras?.let { bundle ->
            paramsContainer = ParamsContainer.createParamsContainer(bundle)
        }

        return paramsContainer
    }

    protected fun parseIntent(newIntent: Intent): ParamsContainer? {
        var paramsContainer: ParamsContainer? = null

        newIntent.extras?.let { bundle ->
            paramsContainer = ParamsContainer.createParamsContainer(bundle)
        }

        return paramsContainer
    }


    private fun isJSONValid(test: String): Boolean {
        try {
            JSONObject(test);
        } catch (ex: JSONException) {
            try {
                JSONArray(test);
            } catch (ex1: JSONException) {
                return false;
            }
        }
        return true;
    }

    private fun initBinding() {
        this.binding = getBindingObject()
    }

    abstract fun getBindingObject(): ViewBindingType

    abstract fun internetSuccess()

    abstract fun internetError()

    abstract fun initIntent(intent: Intent?)

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initLiveData()

    abstract fun initController()

    abstract fun fragmentLifeCycleController(fragmentTag: String)

}