package thousand.group.uylen.utils.system

import androidx.fragment.app.Fragment

interface FragmentTransactionFutures {
    fun replaceFragment(
        containerViewId: Int,
        newFragment: Fragment,
        tag: String
    )

    fun removeAndReplaceWithAnotherFragment(
        containerViewId: Int,
        newFragment: Fragment,
        newTag: String,
        oldTag: String
    )

    fun goBackFragment(containerViewId: Int)

    fun getFragmentMap():MutableMap<String, Fragment>

}