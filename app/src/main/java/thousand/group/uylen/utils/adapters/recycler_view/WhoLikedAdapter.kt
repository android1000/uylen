package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.BlurTransformation
import thousand.group.data.entities.remote.simple.LikedMeUser
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.uylen.R
import thousand.group.uylen.databinding.FavoriteItemBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.extensions.setVisibilityState

class WhoLikedAdapter(
    val onItemClicked: (reaction: LikedMeUser, pos: Int) -> Unit
) :
    RecyclerView.Adapter<WhoLikedAdapter.ViewHolder>() {

    private var dataList = mutableListOf<LikedMeUser>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FavoriteItemBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<LikedMeUser>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: LikedMeUser, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun getItem(position: Int) = dataList[position]

    fun deleteItem(position: Int) {
        dataList.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, dataList.size)
    }

    inner class ViewHolder(
        private val binding: FavoriteItemBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(model: LikedMeUser, pos: Int) {
            binding.root.tag = model

            binding.tvName.setVisibilityState(true)

//            if (isBought) {
/*            model.avatarBitmap?.apply {
                binding.imgFavItem.setImageBitmap(this)
            }*/

            Glide.with(binding.root.context)
                .load(RemoteMainConstants.SERVER_URL + model.avatar)
                .error(R.drawable.ic_error_avatar)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.20f)
                .centerCrop()
                .into(binding.imgFavItem)

            binding.tvName.setText(model.name)

            binding.root.setSafelyClickListener {
                onItemClicked.invoke(model, pos)
            }
            /*  } else {
                  Picasso.get()
                      .load(RemoteMainConstants.SERVER_URL + model.avatar)
                      .error(R.drawable.ic_error_avatar)
                      .transform(BlurTransformation(binding.root.context, 90, 1))
                      .into(binding.imgFavItem)
              }*/
        }
    }
}