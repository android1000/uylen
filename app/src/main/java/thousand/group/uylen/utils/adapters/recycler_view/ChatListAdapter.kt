package thousand.group.uylen.utils.adapters.recycler_view

import android.content.res.Resources
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import thousand.group.data.entities.remote.simple.ChatListItem
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.uylen.R
import thousand.group.uylen.databinding.ItemChatListBinding
import thousand.group.uylen.utils.extensions.setSafelyClickListener
import thousand.group.uylen.utils.extensions.setVisibilityState
import thousand.group.uylen.utils.helpers.DateParser
import thousand.group.uylen.utils.helpers.PastTenseTimeData
import thousand.group.uylen.utils.helpers.TimeAgo

class ChatListAdapter(
    val itemClickListener: (chatItem: ChatListItem, pos: Int) -> Unit
) :
    RecyclerView.Adapter<ChatListAdapter.ViewHolder>() {

    private var dataList = mutableListOf<ChatListItem>()
    private var adapterTag: String = this.javaClass.simpleName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemChatListBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, binding.root.resources)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), position)
    }

    override fun getItemCount() = dataList.size

    fun setData(dataList: MutableList<ChatListItem>) {
        this.dataList.clear()
        this.dataList.addAll(dataList.toMutableList())

        notifyDataSetChanged()
    }

    fun setItem(model: ChatListItem, pos: Int) {
        this.dataList.set(pos, model)
        notifyItemChanged(pos)
    }

    fun addItem(model: ChatListItem, pos: Int) {
        this.dataList.add(pos, model)
        notifyItemInserted(pos)
    }

    fun removeItem(pos: Int) {
        if (dataList.isNotEmpty()) {
            this.dataList.removeAt(pos)
            notifyItemRemoved(pos)
        }
    }

    fun getItem(position: Int) = dataList[position]

    inner class ViewHolder(
        private val binding: ItemChatListBinding,
        private val resources: Resources
    ) :
        RecyclerView.ViewHolder(binding.root) {

        private val myLastMessage = resources.getString(R.string.format_my_message)

        fun bind(model: ChatListItem, pos: Int) {
            Log.i(adapterTag, "bind -> model:${model}, pos:${pos}")

            binding.root.tag = model

            val avatar =
                RemoteMainConstants.SERVER_URL + model.receiver?.images?.getOrNull(0)?.images

            val lastMessage = if (model.last_message.sender?.message != null) {
                String.format(myLastMessage, model.last_message.sender?.message.toString())
            } else if (model.last_message.receiver?.message != null) {
                model.last_message.receiver?.message.toString()
            } else if (model.last_message.sender?.reaction != null) {
                resources.getString(R.string.label_my_reaction)
            } else if (model.last_message.receiver?.reaction != null) {
                resources.getString(R.string.label_another_reaction)
            } else if (model.last_message.sender?.file != null) {
                String.format(myLastMessage, resources.getString(R.string.label_photo))
            } else {
                resources.getString(R.string.label_photo)
            }

            val lastMessageDate = if (model.last_message.sender != null) {
                model.last_message.sender?.created_at
            } else {
                model.last_message.receiver?.created_at
            }

            val lastMessageData =
                PastTenseTimeData.covertTimeToText(
                    lastMessageDate,
                    DateParser.format_YYY_MM_HH_MM_SS
                )

            Picasso.get()
                .load(avatar)
                .error(R.drawable.ic_error_avatar)
                .into(binding.ivAvatar)

            binding.tvName.setText(model.receiver?.name)
            binding.tvMessage.setText(lastMessage)
            binding.tvDate.setText(lastMessageData)

            if (model.receiver?.unread_messageIds?.size != 0 && model.receiver?.unread_messageIds?.size != null && model.last_message.sender == null) {
                binding.tvNewMessageCount.setText(model.receiver?.unread_messageIds?.size.toString())
                binding.tvNewMessageCount.setVisibilityState(true)
                binding.ivStatusIndicator.setVisibilityState(true)
            } else {
                binding.tvNewMessageCount.setVisibilityState(false)
                binding.ivStatusIndicator.setVisibilityState(false)
            }

            binding.root.setSafelyClickListener {
                itemClickListener.invoke(model, pos)
            }

        }
    }
}