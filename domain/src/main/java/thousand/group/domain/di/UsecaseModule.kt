package thousand.group.domain.di

import org.koin.dsl.module
import thousand.group.domain.usecase.auth.AuthUsecase
import thousand.group.domain.usecase.auth.AuthUsecaseImpl
import thousand.group.domain.usecase.chat.ChatUsecase
import thousand.group.domain.usecase.chat.ChatUsecaseImpl
import thousand.group.domain.usecase.likes.LikesUsecase
import thousand.group.domain.usecase.likes.LikesUsecaseImpl
import thousand.group.domain.usecase.profile.ProfileUsecase
import thousand.group.domain.usecase.profile.ProfileUsecaseImpl
import thousand.group.domain.usecase.search_users.SearchUserUsecaseImpl
import thousand.group.domain.usecase.search_users.SearchUsersUsecase
import thousand.group.domain.usecase.util.UtilUsecase
import thousand.group.domain.usecase.util.UtilUsecaseImpl

val usecaseModule = module {
    single<AuthUsecase> { AuthUsecaseImpl(get(), get()) }
    single<UtilUsecase> { UtilUsecaseImpl(get(), get()) }
    single<LikesUsecase> { LikesUsecaseImpl(get(), get()) }
    single<SearchUsersUsecase> { SearchUserUsecaseImpl(get(), get()) }
    single<ProfileUsecase> { ProfileUsecaseImpl(get(), get()) }
    single<ChatUsecase> { ChatUsecaseImpl(get(), get()) }
}