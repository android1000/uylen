package thousand.group.data.di

import org.koin.dsl.module
import thousand.group.domain.repositories.db.DatabaseRepository
import thousand.group.domain.repositories.db.DatabaseRepositoryImpl
import thousand.group.domain.repositories.remote.auth.AuthRepository
import thousand.group.domain.repositories.remote.auth.AuthRepositoryImpl
import thousand.group.domain.repositories.remote.chat.ChatRepository
import thousand.group.domain.repositories.remote.chat.ChatRepositoryImpl
import thousand.group.domain.repositories.remote.likes.LikesRepository
import thousand.group.domain.repositories.remote.likes.LikesRepositoryImpl
import thousand.group.domain.repositories.remote.profile.ProfileRepository
import thousand.group.domain.repositories.remote.profile.ProfileRepositoryImpl
import thousand.group.domain.repositories.remote.search_users.SearchUserRepository
import thousand.group.domain.repositories.remote.search_users.SearchUsersRepositoryImpl
import thousand.group.domain.repositories.remote.util.UtilRepository
import thousand.group.domain.repositories.remote.util.UtilRepositoryImpl

val repModule = module {
    //remote
    single<AuthRepository> { AuthRepositoryImpl(get()) }
    single<UtilRepository> { UtilRepositoryImpl(get()) }
    single<LikesRepository> { LikesRepositoryImpl(get()) }
    single<SearchUserRepository> { SearchUsersRepositoryImpl(get()) }
    single<ProfileRepository> { ProfileRepositoryImpl(get()) }
    single<ChatRepository> { ChatRepositoryImpl(get()) }

    //database
    single<DatabaseRepository> { DatabaseRepositoryImpl(get()) }
}