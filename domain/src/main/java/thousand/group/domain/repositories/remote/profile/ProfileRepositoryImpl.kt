package thousand.group.domain.repositories.remote.profile

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.remote.service.RemoteService
import thousand.group.domain.repositories.remote.likes.LikesRepository


class ProfileRepositoryImpl(private val remoteService: RemoteService) : ProfileRepository {

    override suspend fun getUserById(id: Long) = flow {
        val result = remoteService.getUserById(id)
        emit(result)
    }

    override suspend fun getProfile() = flow {
        val result = remoteService.getProfile()
        emit(result)
    }

    override suspend fun profileUploadImage(image: MultipartBody.Part) = flow {
        val result = remoteService.profileUploadImage(image)
        emit(result)
    }

    override suspend fun profileRemoveImage(id: Long) = flow {
        val result = remoteService.profileRemoveImage(id)
        emit(result)
    }

    override suspend fun editProfile(params: MutableMap<String, RequestBody>) = flow {
        val result = remoteService.editProfile(params)
        emit(result)
    }

    override suspend fun editShowMeGender(sex: RequestBody) = flow {
        val result = remoteService.editShowMeGender(sex)
        emit(result)
    }

    override suspend fun editShowMyAnketa(status: RequestBody) = flow {
        val result = remoteService.editShowMyAnketa(status)
        emit(result)
    }

    override suspend fun setDistance(distance: RequestBody) = flow {
        val result = remoteService.setDistance(distance)
        emit(result)
    }

    override suspend fun getPushState() = flow {
        val result = remoteService.getPushState()
        emit(result)
    }

    override suspend fun changePushState(status: RequestBody) = flow {
        val result = remoteService.changePushState(status)
        emit(result)
    }

    override suspend fun setUserAvatar(id: Long) = flow {
        val result = remoteService.setUserAvatar(id)
        emit(result)
    }

    override suspend fun deleteAccount() = flow {
        val result = remoteService.deleteAccount()
        emit(result)
    }

}