package thousand.group.domain.repositories.remote.chat

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.GET
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.QueryMap
import thousand.group.data.remote.constants.Endpoints
import thousand.group.data.remote.constants.RemoteConstants

interface ChatRepository {
    suspend fun generateSocketToken(): Flow<JsonObject>

    suspend fun getChatList(page: Int): Flow<JsonObject>

    suspend fun sendMessage(
        receiverId: RequestBody,
        message: RequestBody
    ): Flow<Unit>

    suspend fun sendMessage(
        receiverId: RequestBody,
        file: MultipartBody.Part
    ): Flow<Unit>

    suspend fun getChatMessages(
        page: RequestBody,
        receiverId: RequestBody
    ): Flow<JsonObject>

    suspend fun chatIsRead(id: Long): Flow<Unit>

    suspend fun chatIsRead(
        id: Long,
        params: MutableMap<String, Long>
    ): Flow<Unit>

    suspend fun chatMessageIsRead(id: Long): Flow<Unit>
}