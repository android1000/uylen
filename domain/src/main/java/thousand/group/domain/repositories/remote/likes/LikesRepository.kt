package thousand.group.domain.repositories.remote.likes

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody
import retrofit2.http.*
import thousand.group.data.remote.constants.Endpoints
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants

interface LikesRepository {
    suspend fun getLikeUsers(page: Int): Flow<JsonObject>

    suspend fun setLikeStatus(params: MutableMap<String, RequestBody>): Flow<JsonObject>

    suspend fun replyLikedUser(prevId: Long): Flow<Unit>

    suspend fun getReactions(): Flow<JsonObject>

    suspend fun sendReaction(
        userId: RequestBody,
        reactionId: RequestBody
    ): Flow<Unit>

    suspend fun getTariff(): Flow<JsonObject>

    suspend fun buyTarif(
        id: Long,
        activated: RequestBody,
        endDate: RequestBody,
        priceId:RequestBody
    ): Flow<Unit>

    suspend fun getMyTariff(): Flow<JsonObject>

    suspend fun cancelMyTariff(id:Long): Flow<Unit>

    suspend fun getMatchedUsers(page: Int): Flow<JsonObject>

    suspend fun banAccount(userId: RequestBody): Flow<Unit>

    suspend fun complainAccount(userId: RequestBody): Flow<Unit>

}