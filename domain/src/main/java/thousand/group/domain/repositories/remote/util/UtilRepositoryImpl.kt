package thousand.group.domain.repositories.remote.util

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.RequestBody
import thousand.group.data.remote.service.RemoteService
import thousand.group.domain.repositories.remote.auth.AuthRepository


class UtilRepositoryImpl(private val remoteService: RemoteService) : UtilRepository {
    override suspend fun getInterestsList() = flow {
        val result = remoteService.getInterestsList()
        emit(result)
    }

    override suspend fun getCitiesList() = flow {
        val result = remoteService.getCitiesList()
        emit(result)
    }

    override suspend fun getRuList() = flow {
        val result = remoteService.getRuList()
        emit(result)
    }

    override suspend fun updateLocation(lat: RequestBody, longg: RequestBody) = flow {
        val result = remoteService.updateLocation(lat, longg)
        emit(result)
    }

    override suspend fun getRegions() = flow {
        val result = remoteService.getRegions()
        emit(result)
    }

    override suspend fun updateNotification(type: RequestBody) = flow {
        val result = remoteService.updateNotification(type)
        emit(result)
    }

    override suspend fun getLastNotification() = flow {
        val result = remoteService.getLastNotification()
        emit(result)
    }

    override suspend fun sendDeviceToken(appClientId: RequestBody) = flow {
        val result = remoteService.sendDeviceToken(appClientId)
        emit(result)
    }

    override suspend fun getGeneralValues(type: String) = flow {
        val result = remoteService.getGeneralValues(type)
        emit(result)
    }

}