package thousand.group.domain.repositories.remote.profile

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import thousand.group.data.remote.constants.Endpoints
import thousand.group.data.remote.constants.RemoteConstants

interface ProfileRepository {
    suspend fun getUserById(id: Long): Flow<JsonObject>

    suspend fun getProfile(): Flow<JsonObject>

    suspend fun profileUploadImage(image: MultipartBody.Part): Flow<JsonObject>

    suspend fun profileRemoveImage(id: Long): Flow<Unit>

    suspend fun editProfile(params: MutableMap<String, RequestBody>): Flow<JsonObject>

    suspend fun editShowMeGender(sex: RequestBody): Flow<Unit>

    suspend fun editShowMyAnketa(status: RequestBody): Flow<Unit>

    suspend fun setDistance(distance: RequestBody): Flow<Unit>

    suspend fun getPushState(): Flow<JsonObject>

    suspend fun changePushState(status: RequestBody): Flow<JsonObject>

    suspend fun setUserAvatar(id:Long):Flow<Unit>

    suspend fun deleteAccount(): Flow<Unit>
}