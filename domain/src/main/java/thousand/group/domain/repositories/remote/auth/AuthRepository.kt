package thousand.group.domain.repositories.remote.auth

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import thousand.group.data.entities.remote.responces.DataResponse
import thousand.group.data.entities.remote.responces.LoginResponse
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.remote.constants.Endpoints
import thousand.group.data.remote.constants.RemoteConstants

interface AuthRepository {
    suspend fun login(params: MutableMap<String, RequestBody>): Flow<JsonObject>

    suspend fun register(params: MutableMap<String, RequestBody>): Flow<DataResponse<User>>

    suspend fun sendVerificationCode(
        phone: RequestBody
    ): Flow<JsonObject>

    suspend fun sendVerificationCodeToNewNumber(
        phone: RequestBody
    ): Flow<Unit>

    suspend fun assertVerificationCode(
        id: Long,
        code: RequestBody
    ): Flow<JsonObject>

    suspend fun editPhone(
        phone: RequestBody,
        token: RequestBody
    ): Flow<Unit>

    suspend fun resetPassword(
        token: String,
        params: MutableMap<String, RequestBody>
    ): Flow<JsonObject>

    suspend fun fillAnketa(
        token: String,
        params: MutableMap<String, RequestBody>
    ): Flow<Unit>

    suspend fun fillPhoto(
        token: String,
        partList: MutableList<MultipartBody.Part>
    ): Flow<JsonObject>

    suspend fun editAbout(
        token: String,
        about: RequestBody
    ): Flow<Unit>

    suspend fun logout(appClientId: RequestBody): Flow<Unit>

}