package thousand.group.domain.repositories.remote.chat

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.remote.service.RemoteService
import thousand.group.domain.repositories.remote.likes.LikesRepository


class ChatRepositoryImpl(private val remoteService: RemoteService) : ChatRepository {

    override suspend fun generateSocketToken() = flow {
        val result = remoteService.generateSocketToken()
        emit(result)
    }

    override suspend fun getChatList(page: Int) = flow {
        val result = remoteService.getChatList(page)
        emit(result)
    }

    override suspend fun sendMessage(
        receiverId: RequestBody,
        message: RequestBody
    ) = flow {
        val result = remoteService.sendMessage(
            receiverId,
            message
        )
        emit(result)
    }

    override suspend fun sendMessage(
        receiverId: RequestBody,
        file: MultipartBody.Part
    ) = flow {
        val result = remoteService.sendMessage(
            receiverId,
            file
        )
        emit(result)
    }

    override suspend fun getChatMessages(page: RequestBody, receiverId: RequestBody) = flow {
        val result = remoteService.getChatMessages(page, receiverId)
        emit(result)
    }

    override suspend fun chatIsRead(id: Long) = flow {
        val result = remoteService.chatIsRead(id)
        emit(result)
    }

    override suspend fun chatIsRead(id: Long, params: MutableMap<String, Long>) = flow {
        val result = remoteService.chatIsRead(id, params)
        emit(result)
    }

    override suspend fun chatMessageIsRead(id: Long) = flow {
        val result = remoteService.chatMessageIsRead(id)
        emit(result)
    }


}