package thousand.group.domain.repositories.remote.auth

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.remote.service.RemoteService

class AuthRepositoryImpl(private val remoteService: RemoteService) : AuthRepository {

    override suspend fun login(params: MutableMap<String, RequestBody>) = flow {
        val result = remoteService.login(params)
        emit(result)
    }

    override suspend fun register(params: MutableMap<String, RequestBody>) = flow {
        val result = remoteService.register(params)
        emit(result)
    }

    override suspend fun sendVerificationCode(phone: RequestBody) = flow {
        val result = remoteService.sendVerificationCode(phone)
        emit(result)
    }

    override suspend fun sendVerificationCodeToNewNumber(phone: RequestBody) = flow {
        val result = remoteService.sendVerificationCodeToNewNumber(phone)
        emit(result)
    }

    override suspend fun assertVerificationCode(id: Long, code: RequestBody) = flow {
        val result = remoteService.assertVerificationCode(id, code)
        emit(result)
    }

    override suspend fun editPhone(phone: RequestBody, token: RequestBody) = flow {
        val result = remoteService.editPhone(phone, token)
        emit(result)
    }

    override suspend fun resetPassword(token: String, params: MutableMap<String, RequestBody>) =
        flow {
            val result = remoteService.resetPassword(token, params)
            emit(result)
        }

    override suspend fun fillAnketa(token: String, params: MutableMap<String, RequestBody>) = flow {
        val result = remoteService.fillAnketa(token, params)
        emit(result)
    }

    override suspend fun fillPhoto(token: String, partList: MutableList<MultipartBody.Part>) =
        flow {
            val result = remoteService.fillUserPhoto(token, partList)
            emit(result)
        }

    override suspend fun editAbout(token: String, about: RequestBody) = flow {
        val result = remoteService.editAbout(token, about)
        emit(result)
    }

    override suspend fun logout(appClientId: RequestBody) = flow {
        val result = remoteService.logout(appClientId)
        emit(result)
    }

}