package thousand.group.domain.repositories.remote.likes

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.RequestBody
import retrofit2.http.Query
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.remote.service.RemoteService

class LikesRepositoryImpl(private val remoteService: RemoteService) : LikesRepository {

    override suspend fun getLikeUsers(page: Int) = flow {
        val result = remoteService.getLikeUsers(page)
        emit(result)
    }

    override suspend fun setLikeStatus(params: MutableMap<String, RequestBody>) = flow {
        val result = remoteService.setLikeStatus(params)
        emit(result)
    }

    override suspend fun replyLikedUser(prevId: Long) = flow {
        val result = remoteService.replyLikedUser(prevId)
        emit(result)
    }

    override suspend fun getReactions() = flow {
        val result = remoteService.getReactions()
        emit(result)
    }

    override suspend fun sendReaction(userId: RequestBody, reactionId: RequestBody) = flow {
        val result = remoteService.sendReaction(userId, reactionId)
        emit(result)
    }

    override suspend fun getTariff() = flow {
        val result = remoteService.getTariff()
        emit(result)
    }

    override suspend fun buyTarif(
        id: Long,
        activated: RequestBody,
        endDate: RequestBody,
        priceId: RequestBody
    ) = flow {
        val result = remoteService.buyTarif(id, activated, endDate, priceId)
        emit(result)
    }

    override suspend fun getMyTariff() = flow {
        val result = remoteService.getMyTariff()
        emit(result)
    }

    override suspend fun cancelMyTariff(id: Long) = flow {
        val result = remoteService.cancelMyTariff(id)
        emit(result)
    }

    override suspend fun getMatchedUsers(page: Int) = flow {
        val result = remoteService.getMatchedUsers(page)
        emit(result)
    }

    override suspend fun banAccount(userId: RequestBody) = flow {
        val result = remoteService.banAccount(userId)
        emit(result)
    }

    override suspend fun complainAccount(userId: RequestBody) = flow {
        val result = remoteService.complainAccount(userId)
        emit(result)
    }


}