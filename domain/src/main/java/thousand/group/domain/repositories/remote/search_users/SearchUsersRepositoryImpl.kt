package thousand.group.domain.repositories.remote.search_users

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.RequestBody
import thousand.group.data.remote.service.RemoteService


class SearchUsersRepositoryImpl(private val remoteService: RemoteService) : SearchUserRepository {

    override suspend fun getWhoLikesMe(page: Int) = flow {
        val result = remoteService.getWhoLikesMe(page)
        emit(result)
    }

    override suspend fun getRecentlySearch() = flow {
        val result = remoteService.getRecentlySearch()
        emit(result)
    }

    override suspend fun sendRecentlySearch(userId: RequestBody) = flow {
        val result = remoteService.sendRecentlySearch(userId)
        emit(result)
    }

    override suspend fun searchUsers(page: RequestBody, params: MutableMap<String, RequestBody>) =
        flow {
            val result = remoteService.searchUsers(page, params)
            emit(result)
        }

    override suspend fun deleteRecentUser(id: Long) = flow {
        val result = remoteService.deleteRecentUser(id)
        emit(result)
    }


}