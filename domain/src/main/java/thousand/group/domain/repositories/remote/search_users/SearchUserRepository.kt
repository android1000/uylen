package thousand.group.domain.repositories.remote.search_users

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody
import retrofit2.http.*
import thousand.group.data.remote.constants.Endpoints
import thousand.group.data.remote.constants.RemoteConstants

interface SearchUserRepository {
    suspend fun getWhoLikesMe(page: Int): Flow<JsonObject>

    suspend fun getRecentlySearch(): Flow<JsonObject>

    suspend fun sendRecentlySearch(userId: RequestBody): Flow<Unit>

    suspend fun searchUsers(
        page: RequestBody,
        params: MutableMap<String, RequestBody>
    ): Flow<JsonObject>

    suspend fun deleteRecentUser(id: Long): Flow<Unit>
}