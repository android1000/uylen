package thousand.group.domain.repositories.remote.util

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody
import retrofit2.http.GET
import retrofit2.http.Part
import retrofit2.http.Query
import thousand.group.data.remote.constants.Endpoints
import thousand.group.data.remote.constants.RemoteConstants

interface UtilRepository {
    suspend fun getInterestsList(): Flow<JsonObject>

    suspend fun getCitiesList(): Flow<JsonObject>

    suspend fun getRuList(): Flow<JsonObject>

    suspend fun updateLocation(
        lat: RequestBody,
        longg: RequestBody
    ): Flow<Unit>

    suspend fun getRegions(): Flow<JsonObject>

    suspend fun updateNotification(type: RequestBody): Flow<Unit>

    suspend fun getLastNotification(): Flow<Unit>

    suspend fun sendDeviceToken(appClientId: RequestBody): Flow<Unit>

    suspend fun getGeneralValues(type: String): Flow<JsonObject>
}