package thousand.group.domain.utils

import android.telephony.PhoneNumberUtils
import android.text.TextUtils
import java.util.*
import java.util.regex.Pattern

fun String.getYouTubeVideoId(): String {
    val firstIndex = this.indexOf("?v=") + 3
    val secondIndex = this.indexOf("&")
    return this.substring(firstIndex, secondIndex)
}

fun String.isEmailValid(): Boolean {
    return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.formatToPhoneNumber(): String {
    return PhoneNumberUtils.formatNumber(this, "KZ")
}