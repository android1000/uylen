package thousand.group.domain.usecase.search_users

import kotlinx.coroutines.flow.map
import okhttp3.RequestBody
import thousand.group.data.storage.utils.LocaleStorage
import thousand.group.domain.mappers.LikesMapper
import thousand.group.domain.mappers.SearchUsersMapper
import thousand.group.domain.repositories.remote.search_users.SearchUserRepository

class SearchUserUsecaseImpl(
    val repository: SearchUserRepository,
    val storage: LocaleStorage
) : SearchUsersUsecase {
    override suspend fun getWhoLikesMe(page: Int) = repository.getWhoLikesMe(page)
        .map {
            SearchUsersMapper.mapToUsersWhoLikedList(it)
        }

    override suspend fun getRecentlySearch() = repository.getRecentlySearch()
        .map {
            LikesMapper.mapJsonToLikesList(it)
        }

    override suspend fun sendRecentlySearch(userId: RequestBody) =
        repository.sendRecentlySearch(userId)

    override suspend fun searchUsers(page: RequestBody, params: MutableMap<String, RequestBody>) =
        repository.searchUsers(page, params)
            .map {
                SearchUsersMapper.mapJsonToSearchUsersList(it)
            }

    override suspend fun deleteRecentUser(id: Long) = repository.deleteRecentUser(id)

}