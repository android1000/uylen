package thousand.group.domain.usecase.profile

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.storage.utils.LocaleStorage
import thousand.group.domain.mappers.ProfileMapper
import thousand.group.domain.repositories.remote.profile.ProfileRepository

class ProfileUsecaseImpl(
    val repository: ProfileRepository,
    val storage: LocaleStorage
) : ProfileUsecase {

    override suspend fun getUserById(id: Long) = repository.getUserById(id)
        .map {
            ProfileMapper.mapJsonToUserCard(it)
        }

    override suspend fun getProfile() = repository.getProfile()
        .map {
            ProfileMapper.mapJsonToUser(it)
        }

    override suspend fun profileUploadImage(image: MultipartBody.Part) =
        repository.profileUploadImage(image)
            .map {
                ProfileMapper.mapJsontoUserPhoto(it)
            }

    override suspend fun profileRemoveImage(id: Long) = repository.profileRemoveImage(id)

    override suspend fun editProfile(params: MutableMap<String, RequestBody>) =
        repository.editProfile(params)
            .map {
                ProfileMapper.mapJsonToUserData(it)
            }

    override suspend fun editShowMeGender(sex: RequestBody) = repository.editShowMeGender(sex)

    override suspend fun editShowMyAnketa(status: RequestBody) = repository.editShowMyAnketa(status)

    override suspend fun setDistance(distance: RequestBody) = repository.setDistance(distance)

    override suspend fun getPushState() = repository.getPushState()
        .map {
            ProfileMapper.mapJsonToPushFlag(it)
        }

    override suspend fun changePushState(status: RequestBody) = repository.changePushState(status)
        .map {
            ProfileMapper.mapJsonToSettingsPushFlag(it)
        }

    override suspend fun setUserAvatar(id: Long) = repository.setUserAvatar(id)

    override suspend fun deleteAccount() = repository.deleteAccount()

    override fun setLanguage(lang: String) = storage.setLanguage(lang)

    override fun getLanguage() = storage.getLanguage()

    override fun setLanguageServer(lang: String) = storage.setLanguageServer(lang)

    override fun getLanguageServer() = storage.getLanguageServer()


}