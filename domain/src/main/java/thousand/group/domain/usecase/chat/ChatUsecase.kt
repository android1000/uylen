package thousand.group.domain.usecase.chat

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.ChatListItem
import thousand.group.data.entities.remote.simple.ChatLocaleDateMessage
import thousand.group.data.entities.remote.simple.ChatMessage

interface ChatUsecase {
    suspend fun generateSocketToken(): Flow<String>

    suspend fun getChatList(page: Int): Flow<MutableList<ChatListItem>>

    suspend fun sendMessage(
        receiverId: RequestBody,
        message: RequestBody
    ): Flow<Unit>

    suspend fun sendMessage(
        receiverId: RequestBody,
        file: MultipartBody.Part
    ): Flow<Unit>

    suspend fun getChatMessages(
        page: RequestBody,
        receiverId: RequestBody
    ): Flow<MutableList<ChatLocaleDateMessage>>

    suspend fun chatIsRead(id: Long): Flow<Unit>

    suspend fun chatIsRead(
        id: Long,
        params: MutableMap<String, Long>
    ): Flow<Unit>

    suspend fun chatMessageIsRead(id: Long): Flow<Unit>
}