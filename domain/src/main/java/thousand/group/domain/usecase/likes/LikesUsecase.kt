package thousand.group.domain.usecase.likes

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.*
import thousand.group.domain.mappers.LikesMapper

interface LikesUsecase {
    suspend fun getLikeUsers(page: Int): Flow<MutableList<UserCardData>>

    suspend fun setLikeStatus(params: MutableMap<String, RequestBody>): Flow<LikeParams>

    suspend fun replyLikedUser(prevId: Long): Flow<Unit>

    suspend fun getReactions(): Flow<MutableList<Reaction>>

    suspend fun sendReaction(
        userId: RequestBody,
        reactionId: RequestBody
    ): Flow<Unit>

    suspend fun getTariff(): Flow<Tariff>

    suspend fun buyTarif(
        id: Long,
        activated: RequestBody,
        endDate: RequestBody,
        priceId: RequestBody
    ): Flow<Unit>

    suspend fun getMyTariff(): Flow<MyTariff?>

    suspend fun cancelMyTariff(id: Long): Flow<Unit>

    suspend fun getMatchedUsers(page: Int): Flow<Pair<MutableList<MatchedUser>, Int>>

    suspend fun banAccount(userId: RequestBody): Flow<Unit>

    suspend fun complainAccount(userId: RequestBody): Flow<Unit>

}