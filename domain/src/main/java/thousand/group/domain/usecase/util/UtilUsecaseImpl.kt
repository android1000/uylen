package thousand.group.domain.usecase.util

import android.util.Log
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.City
import thousand.group.data.entities.remote.simple.GeneralValue
import thousand.group.data.entities.remote.simple.Region
import thousand.group.data.entities.remote.simple.Zhuz
import thousand.group.data.storage.utils.LocaleStorage
import thousand.group.domain.mappers.AuthMapper
import thousand.group.domain.mappers.UtilMapper
import thousand.group.domain.repositories.remote.util.UtilRepository

class UtilUsecaseImpl(
    val utilRepository: UtilRepository,
    val storage: LocaleStorage
) : UtilUsecase {

    override suspend fun getInterestsList() =
        utilRepository.getInterestsList()
            .map {
                AuthMapper.mapJsonToInterestList(it)
            }

    override suspend fun getCitiesList() = utilRepository.getCitiesList()
        .map {
            UtilMapper.mapJsonToCityList(it)
        }
        .map {
            it.sortBy { it.name }
            it
        }

    override suspend fun getRuList() = utilRepository.getRuList()
        .map {
            AuthMapper.mapJsonToRuList(it)
        }
        .map {
            Log.i("UtilUsecase", "getRuList -> ruList:${it}")
            Log.i("UtilUsecase", "getRuList -> size:${it.size}")

            it.sortBy { model -> model.name }

            it.forEach { zhuz ->
                zhuz.parents.sortBy { ru -> ru.name }
            }

            Log.i("UtilUsecase", "getRuList -> sorted ruList:${it}")
            Log.i("UtilUsecase", "getRuList -> size:${it.size}")

            it
        }

    override suspend fun updateLocation(lat: RequestBody, longg: RequestBody) =
        utilRepository.updateLocation(lat, longg)

    override suspend fun getRegions() = utilRepository.getRegions()
        .map {
            UtilMapper.mapJsonToRegionList(it)
        }

    override suspend fun updateNotification(type: RequestBody) =
        utilRepository.updateNotification(type)

    override suspend fun getLastNotification() = utilRepository.getLastNotification()

    override suspend fun sendDeviceToken(appClientId: RequestBody) =
        utilRepository.sendDeviceToken(appClientId)

    override suspend fun getGeneralValues(type: String) = utilRepository.getGeneralValues(type)
        .map {
            UtilMapper.mapJsonToGeneralValuesList(it)
        }
        .map {
            it.sortBy { it.value }
            it
        }
}