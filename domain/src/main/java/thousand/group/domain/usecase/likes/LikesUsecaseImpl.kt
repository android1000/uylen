package thousand.group.domain.usecase.likes

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.Tariff
import thousand.group.data.storage.utils.LocaleStorage
import thousand.group.domain.mappers.LikesMapper
import thousand.group.domain.repositories.remote.likes.LikesRepository

class LikesUsecaseImpl(
    val repository: LikesRepository,
    val storage: LocaleStorage
) : LikesUsecase {

    override suspend fun getLikeUsers(page: Int) = repository.getLikeUsers(page)
        .map {
            LikesMapper.mapJsonToLikesList(it)
        }

    override suspend fun setLikeStatus(params: MutableMap<String, RequestBody>) =
        repository.setLikeStatus(params)
            .map {
                LikesMapper.mapJsonToLikeUsers(it)
            }

    override suspend fun replyLikedUser(prevId: Long) = repository.replyLikedUser(prevId)

    override suspend fun getReactions() =
        repository.getReactions()
            .map {
                LikesMapper.mapJsonToReactionsList(it)
            }

    override suspend fun sendReaction(userId: RequestBody, reactionId: RequestBody) =
        repository.sendReaction(userId, reactionId)

    override suspend fun getTariff() = repository.getTariff()
        .map {
            LikesMapper.mapJsonToTariff(it)
        }

    override suspend fun buyTarif(
        id: Long, activated: RequestBody, endDate: RequestBody,
        priceId: RequestBody
    ) =
        repository.buyTarif(id, activated, endDate, priceId)

    override suspend fun getMyTariff() = repository.getMyTariff()
        .map {
            LikesMapper.mapJsonToDataTariff(it)
        }

    override suspend fun cancelMyTariff(id: Long) = repository.cancelMyTariff(id)

    override suspend fun getMatchedUsers(page: Int) = repository.getMatchedUsers(page)
        .map {
            LikesMapper.mapJsonToMatchedList(it)
        }

    override suspend fun banAccount(userId: RequestBody) = repository.banAccount(userId)

    override suspend fun complainAccount(userId: RequestBody) = repository.complainAccount(userId)

}