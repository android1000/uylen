package thousand.group.domain.usecase.auth

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.entities.remote.responces.DataResponse
import thousand.group.data.entities.remote.responces.LoginResponse
import thousand.group.data.entities.remote.simple.Interest
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserPhoto

interface AuthUsecase {

    //Remote
    suspend fun login(params: MutableMap<String, RequestBody>): Flow<LoginResponse>

    suspend fun register(params: MutableMap<String, RequestBody>): Flow<DataResponse<User>>

    suspend fun sendVerificationCode(
        phone: RequestBody
    ): Flow<User>

    suspend fun sendVerificationCodeToNewNumber(
        phone: RequestBody
    ): Flow<Unit>

    suspend fun assertVerificationCode(
        id: Long,
        code: RequestBody
    ): Flow<String>

    suspend fun editPhone(
        phone: RequestBody,
        token: RequestBody
    ): Flow<Unit>

    suspend fun resetPassword(
        token: String,
        params: MutableMap<String, RequestBody>
    ): Flow<JsonObject>

    suspend fun fillAnketa(
        token: String,
        params: MutableMap<String, RequestBody>
    ): Flow<Unit>

    suspend fun fillPhoto(
        token: String,
        partList: MutableList<MultipartBody.Part>
    ): Flow<MutableList<UserPhoto>>

    suspend fun editAbout(
        token: String,
        about: RequestBody
    ): Flow<Unit>

    suspend fun logout(appClientId: RequestBody): Flow<Unit>

    //LocaleStorage
    fun setAccessTokenType(type: String)

    fun getAccessTokenType(): String

    fun setUserId(id: Long)

    fun getUserId(): Long

    fun setAccessToken(accessToken: String)

    fun getAccessToken(): String

    fun tokenIsNotEmpty(): Boolean

    fun setLanguage(lang: String)

    fun getLanguage(): String

    fun setSound(sound: Boolean)

    fun getSound(): Boolean

    fun setPush(push: Boolean)

    fun getPush(): Boolean

    fun saveUserModel(user: User)

    fun deleteUserModel()

    fun getUserModel(): User?

    fun isStartPageVisible(): Boolean

    fun setStartPageVisible(visible: Boolean)

    fun setTempAccessToken(accessToken: String)

    fun getTempAccessToken(): String

    fun tokenTempIsNotEmpty(): Boolean

    fun deleteTempToken()

    fun saveTempUserModel(user: User)

    fun deleteTempUserModel()

    fun getTempUserModel(): User?


}