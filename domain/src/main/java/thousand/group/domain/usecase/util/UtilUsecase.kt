package thousand.group.domain.usecase.util

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody
import retrofit2.http.Part
import thousand.group.data.entities.remote.simple.*
import thousand.group.data.remote.constants.RemoteConstants

interface UtilUsecase {

    suspend fun getInterestsList(): Flow<MutableList<Interest>>

    suspend fun getCitiesList(): Flow<MutableList<City>>

    suspend fun getRuList(): Flow<MutableList<Zhuz>>

    suspend fun updateLocation(
        lat: RequestBody,
        longg: RequestBody
    ): Flow<Unit>

    suspend fun getRegions(): Flow<MutableList<Region>>

    suspend fun updateNotification(type: RequestBody): Flow<Unit>

    suspend fun getLastNotification(): Flow<Unit>

    suspend fun sendDeviceToken(appClientId: RequestBody): Flow<Unit>

    suspend fun getGeneralValues(type: String): Flow<MutableList<GeneralValue>>
}