package thousand.group.domain.usecase.search_users

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.SearchUser
import thousand.group.data.entities.remote.simple.TariffUsersList
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData

interface SearchUsersUsecase {
    suspend fun getWhoLikesMe(page: Int): Flow<TariffUsersList>

    suspend fun getRecentlySearch(): Flow<MutableList<UserCardData>>

    suspend fun sendRecentlySearch(userId: RequestBody): Flow<Unit>

    suspend fun searchUsers(
        page: RequestBody,
        params: MutableMap<String, RequestBody>
    ): Flow<MutableList<SearchUser>>

    suspend fun deleteRecentUser(id: Long): Flow<Unit>
}