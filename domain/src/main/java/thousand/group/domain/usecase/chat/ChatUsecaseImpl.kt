package thousand.group.domain.usecase.chat

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.ChatMessage
import thousand.group.data.storage.utils.LocaleStorage
import thousand.group.domain.mappers.ChatMapper
import thousand.group.domain.repositories.remote.auth.AuthRepository
import thousand.group.domain.repositories.remote.chat.ChatRepository
import thousand.group.domain.usecase.auth.AuthUsecase

class ChatUsecaseImpl(
    val repository: ChatRepository,
    val storage: LocaleStorage
) : ChatUsecase {

    override suspend fun generateSocketToken() =
        repository.generateSocketToken()
            .map {
                ChatMapper.mapJsonToToken(it)
            }

    override suspend fun getChatList(page: Int) = repository.getChatList(page)
        .map {
            ChatMapper.mapJsonToChatList(it)
        }

    override suspend fun sendMessage(
        receiverId: RequestBody, message: RequestBody
    ) =
        repository.sendMessage(receiverId, message)

    override suspend fun sendMessage(
        receiverId: RequestBody,
        file: MultipartBody.Part
    ) = repository.sendMessage(receiverId, file)

    override suspend fun getChatMessages(page: RequestBody, receiverId: RequestBody) =
        repository.getChatMessages(page, receiverId)
            .map {
                ChatMapper.mapJsonToChatDateMessage(it)
            }

    override suspend fun chatIsRead(id: Long) = repository.chatIsRead(id)

    override suspend fun chatIsRead(
        id: Long,
        params: MutableMap<String, Long>
    )= repository.chatIsRead(id, params)

    override suspend fun chatMessageIsRead(id: Long) = repository.chatMessageIsRead(id)

}