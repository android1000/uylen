package thousand.group.domain.usecase.profile

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.entities.remote.simple.UserPhoto
import thousand.group.data.remote.constants.Endpoints
import thousand.group.data.remote.constants.RemoteConstants

interface ProfileUsecase {

    suspend fun getUserById(id: Long): Flow<UserCardData>

    suspend fun getProfile(): Flow<User>

    suspend fun profileUploadImage(image: MultipartBody.Part): Flow<UserPhoto>

    suspend fun profileRemoveImage(id: Long): Flow<Unit>

    suspend fun editProfile( params:MutableMap<String, RequestBody>):Flow<User>

    suspend fun editShowMeGender(sex: RequestBody): Flow<Unit>

    suspend fun editShowMyAnketa(status: RequestBody): Flow<Unit>

    suspend fun setDistance(distance:RequestBody):Flow<Unit>

    suspend fun getPushState(): Flow<Boolean>

    suspend fun changePushState(status: RequestBody): Flow<Boolean>

    suspend fun setUserAvatar(id:Long):Flow<Unit>

    suspend fun deleteAccount(): Flow<Unit>

    fun setLanguage(lang: String)

    fun getLanguage(): String

    fun setLanguageServer(lang: String)

    fun getLanguageServer(): String


}