package thousand.group.domain.usecase.auth

import com.google.gson.JsonObject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import okhttp3.MultipartBody
import okhttp3.RequestBody
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.storage.utils.LocaleStorage
import thousand.group.domain.mappers.AuthMapper
import thousand.group.domain.repositories.remote.auth.AuthRepository

class AuthUsecaseImpl(
    val authRepository: AuthRepository,
    val storage: LocaleStorage
) : AuthUsecase {

    //Remote
    override suspend fun login(params: MutableMap<String, RequestBody>) =
        authRepository.login(params)
            .map {
                AuthMapper.mapJsonDataToLoginResponse(it)
            }

    override suspend fun register(params: MutableMap<String, RequestBody>) =
        authRepository.register(params)


    override suspend fun sendVerificationCode(phone: RequestBody) =
        authRepository.sendVerificationCode(phone)
            .map {
                AuthMapper.mapJsonToUser(it)
            }

    override suspend fun sendVerificationCodeToNewNumber(phone: RequestBody) =
        authRepository.sendVerificationCodeToNewNumber(phone)

    override suspend fun assertVerificationCode(id: Long, code: RequestBody) =
        authRepository.assertVerificationCode(id, code)
            .map {
                AuthMapper.mapJsonToToken(it)
            }

    override suspend fun editPhone(phone: RequestBody, token: RequestBody) =
        authRepository.editPhone(phone, token)

    override suspend fun resetPassword(token: String, params: MutableMap<String, RequestBody>) =
        authRepository.resetPassword(token, params)


    override suspend fun fillAnketa(token: String, params: MutableMap<String, RequestBody>) =
        authRepository.fillAnketa(token, params)

    override suspend fun fillPhoto(token: String, partList: MutableList<MultipartBody.Part>) =
        authRepository.fillPhoto(token, partList)
            .map {
                AuthMapper.mapUserPhotoList(it)
            }

    override suspend fun editAbout(token: String, about: RequestBody) =
        authRepository.editAbout(token, about)

    override suspend fun logout(appClientId: RequestBody) = authRepository.logout(appClientId)

    //LocaleStorage
    override fun setAccessTokenType(type: String) = storage.setAccessTokenType(type)

    override fun getAccessTokenType() = storage.getAccessTokenType()

    override fun setUserId(id: Long) = storage.setUserId(id)

    override fun getUserId() = storage.getUserId()

    override fun setAccessToken(accessToken: String) = storage.setAccessToken(accessToken)

    override fun getAccessToken() = storage.getAccessToken()

    override fun tokenIsNotEmpty() = storage.tokenIsNotEmpty()

    override fun setLanguage(lang: String) = storage.setLanguage(lang)

    override fun getLanguage() = storage.getLanguage()

    override fun setSound(sound: Boolean) = storage.setSound(sound)

    override fun getSound() = storage.getSound()

    override fun setPush(push: Boolean) = storage.setPush(push)

    override fun getPush() = storage.getPush()

    override fun saveUserModel(user: User) = storage.saveUserModel(user)

    override fun deleteUserModel() = storage.deleteUserModel()

    override fun getUserModel(): User? = storage.getUserModel()

    override fun isStartPageVisible(): Boolean = storage.isStartPageVisible()

    override fun setStartPageVisible(visible: Boolean) = storage.setStartPageVisible(visible)

    override fun setTempAccessToken(accessToken: String) = storage.setTempAccessToken(accessToken)

    override fun getTempAccessToken() = storage.getTempAccessToken()

    override fun tokenTempIsNotEmpty() = storage.tokenTempIsNotEmpty()

    override fun deleteTempToken() = storage.deleteTempToken()

    override fun saveTempUserModel(user: User) = storage.saveTempUserModel(user)

    override fun deleteTempUserModel() = storage.deleteTempUserModel()

    override fun getTempUserModel() = storage.getTempUserModel()

}