package thousand.group.domain.mappers

import com.google.gson.JsonObject
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.entities.remote.simple.UserPhoto
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.utils.GsonHelper

object ProfileMapper {

    fun mapJsonToUserCard(json: JsonObject) = GsonHelper.getObjectFromJson(
        json.getAsJsonObject(RemoteConstants.USER),
        UserCardData::class.java
    )

    fun mapJsonToUser(json: JsonObject) = GsonHelper.getObjectFromJson(
        json.getAsJsonObject(RemoteConstants.USER),
        User::class.java
    )

    fun mapJsonToUserData(json: JsonObject) = GsonHelper.getObjectFromJson(
        json.getAsJsonObject(RemoteMainConstants.DATA),
        User::class.java
    )

    fun mapJsontoUserPhoto(json: JsonObject) = GsonHelper.getObjectFromJson(
        json.getAsJsonArray(RemoteMainConstants.DATA).get(0).asJsonObject,
        UserPhoto::class.java
    )

    fun mapJsonToPushFlag(json: JsonObject) =
        json.getAsJsonObject(RemoteConstants.INFO).get(RemoteConstants.STATUS).asBoolean

    fun mapJsonToSettingsPushFlag(json: JsonObject) = json.get(RemoteConstants.STATUS).asInt == 1
}