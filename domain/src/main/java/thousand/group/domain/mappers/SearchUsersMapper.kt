package thousand.group.domain.mappers

import com.google.gson.JsonObject
import thousand.group.data.entities.remote.simple.*
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.utils.GsonHelper

object SearchUsersMapper {
    fun mapToUsersWhoLikedList(json: JsonObject) = GsonHelper.getObjectFromJson(
        json.getAsJsonObject(RemoteMainConstants.DATA),
        TariffUsersList::class.java
    )

    fun mapJsonToSearchUsersList(jsonObject: JsonObject): MutableList<SearchUser> {
        val data = jsonObject.getAsJsonArray(RemoteMainConstants.DATA)

        val parsedList = GsonHelper.getListFromJsonArray(data, SearchUser::class.java)
        return parsedList
    }

}