package thousand.group.domain.mappers

import com.google.gson.JsonObject
import thousand.group.data.entities.remote.simple.ChatListItem
import thousand.group.data.entities.remote.simple.ChatLocaleDateMessage
import thousand.group.data.entities.remote.simple.ChatMessage
import thousand.group.data.entities.remote.simple.UserCardData
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.utils.GsonHelper

object ChatMapper {
    fun mapJsonToToken(json: JsonObject) = json.get(RemoteConstants.TOKEN).asString

    fun mapJsonToChatList(jsonObject: JsonObject): MutableList<ChatListItem> {
        val data =
            jsonObject.getAsJsonArray(RemoteMainConstants.DATA)
        val parsedList = GsonHelper.getListFromJsonArray(data, ChatListItem::class.java)
        return parsedList
    }

    fun mapJsonToChatDateMessage(json: JsonObject): MutableList<ChatLocaleDateMessage> {
        val data = json.getAsJsonArray(RemoteMainConstants.DATA)
        val list = mutableListOf<ChatLocaleDateMessage>()

        data.forEachIndexed { index, jsonElement ->
            val date = jsonElement.asJsonObject.get(RemoteConstants.DATE).asString

            jsonElement.asJsonObject.getAsJsonArray(RemoteConstants.MESSAGES)
                .forEachIndexed { indexChild, jsonElementChild ->

                    val message = GsonHelper.getObjectFromJson(
                        jsonElementChild.asJsonObject,
                        ChatMessage::class.java
                    )

                    list.add(ChatLocaleDateMessage(message = message))
                }

            list.add(ChatLocaleDateMessage(date = date, message = null))

        }

        return list
    }

}