package thousand.group.domain.mappers

import com.google.gson.JsonObject
import thousand.group.data.entities.remote.simple.City
import thousand.group.data.entities.remote.simple.GeneralValue
import thousand.group.data.entities.remote.simple.Region
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.utils.GsonHelper

object UtilMapper {

    fun mapJsonToCityList(json: JsonObject) = GsonHelper.getListFromJsonArray(
        json.getAsJsonArray(RemoteMainConstants.DATA),
        City::class.java
    )

    fun mapJsonToRegionList(json: JsonObject) = GsonHelper.getListFromJsonArray(
        json.getAsJsonArray(RemoteMainConstants.DATA),
        Region::class.java
    )

    fun mapJsonToGeneralValuesList(json: JsonObject) = GsonHelper.getListFromJsonArray(
        json.getAsJsonArray(RemoteMainConstants.DATA),
        GeneralValue::class.java
    )
}