package thousand.group.domain.mappers

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import thousand.group.data.entities.remote.responces.LoginResponse
import thousand.group.data.entities.remote.simple.*
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.utils.GsonHelper

object AuthMapper {
    fun mapJsonDataToLoginResponse(json: JsonObject): LoginResponse {
        return GsonHelper.getObjectFromJson(
            json,
            LoginResponse::class.java
        )
    }

    fun mapJsonToUser(json: JsonObject): User {
        return GsonHelper.getObjectFromJson(
            json.getAsJsonObject(RemoteMainConstants.DATA),
            User::class.java
        )
    }

    fun mapJsonToToken(json: JsonObject) = json.get(RemoteConstants.TOKEN).asString

    fun mapJsonToInterestList(json: JsonObject) = GsonHelper.getListFromJsonArray(
        json.getAsJsonArray(RemoteMainConstants.DATA),
        Interest::class.java
    )

    fun mapJsonToRuList(json: JsonObject) = GsonHelper.getListFromJsonArray(
        json.getAsJsonArray(RemoteMainConstants.DATA),
        Zhuz::class.java
    )

    fun mapUserPhotoList(json: JsonObject) = GsonHelper.getListFromJsonArray(
        json.getAsJsonArray(RemoteMainConstants.DATA),
        UserPhoto::class.java
    )

}