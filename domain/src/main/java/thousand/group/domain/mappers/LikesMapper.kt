package thousand.group.domain.mappers

import com.google.gson.JsonObject
import thousand.group.data.entities.remote.simple.*
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.utils.GsonHelper

object LikesMapper {
    fun mapJsonToLikesList(jsonObject: JsonObject): MutableList<UserCardData> {
        val data =
            jsonObject.getAsJsonArray(RemoteMainConstants.DATA)
        val parsedList = GsonHelper.getListFromJsonArray(data, UserCardData::class.java)
        return parsedList
    }

    fun mapJsonToLikeUsers(json: JsonObject) = GsonHelper.getObjectFromJson(
        json.getAsJsonObject(RemoteMainConstants.DATA),
        LikeParams::class.java
    )

    fun mapJsonToReactionsList(json: JsonObject): MutableList<Reaction> {
        val data =
            json.getAsJsonArray(RemoteMainConstants.DATA)
        val parsedList = GsonHelper.getListFromJsonArray(data, Reaction::class.java)
        return parsedList
    }

    fun mapJsonToTariff(json: JsonObject) = GsonHelper.getObjectFromJson(
        json.getAsJsonObject(RemoteMainConstants.DATA),
        Tariff::class.java
    )

    fun mapJsonToDataTariff(json: JsonObject): MyTariff? {
        val tariff =
            json.getAsJsonObject(RemoteMainConstants.DATA)

        if (!tariff.get(RemoteConstants.TARIFF).isJsonNull) {
            return GsonHelper.getObjectFromJson(
                tariff.asJsonObject,
                MyTariff::class.java
            )
        } else {
            return null
        }
    }

    fun mapJsonToMatchedList(json: JsonObject): Pair<MutableList<MatchedUser>, Int> {
        val data =
            json.getAsJsonArray(RemoteMainConstants.DATA)
        val total = json.get(RemoteConstants.TOTAL).asInt

        val parsedList = GsonHelper.getListFromJsonArray(data, MatchedUser::class.java)
        return Pair(parsedList, total)
    }
}