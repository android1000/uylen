package thousand.group.data.storage.utils

import android.content.Context
import android.content.ContextWrapper
import com.pixplicity.easyprefs.library.Prefs

object CreateLocaleStoragePrefs {
    fun initLocalePrefs(context: Context) {
        Prefs.Builder()
            .setContext(context)
            .setMode(ContextWrapper.MODE_PRIVATE)
            .setPrefsName(context.packageName)
            .setUseDefaultSharedPreference(true)
            .build()
    }
}