package thousand.group.data.storage.utils

import android.util.Log
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

object GsonHelper {
    fun <T> getJsonFromObject(model: T): String {
        return Gson().toJson(model)
    }

    fun <T> getObjectFromJson(jsonObject: JsonObject, clazz: Class<T>): T {
        return Gson().fromJson(jsonObject, clazz)
    }

    fun <T> getObjectFromJson(jsonElement: JsonElement?, type: Type): T {
        return Gson().fromJson(jsonElement, type)
    }

    fun <T> getObjectFromString(modelString: String, clazz: Class<T>): T {
        return Gson().fromJson(modelString, clazz)
    }

    fun <T> getListFromJsonArray(model: JsonArray, clazz: Class<T>): MutableList<T> {
        val gson = Gson()
        val type = TypeToken.getParameterized(MutableList::class.java, clazz).type
        val list = gson.fromJson<MutableList<T>>(model, type)
        return list
    }

    fun <T> getListFromJsonArray(strList: String, clazz: Class<T>): MutableList<T> {
        val gson = Gson()
        val type = TypeToken.getParameterized(MutableList::class.java, clazz).type
        val list = gson.fromJson<MutableList<T>>(strList, type)
        return list
    }

    fun getExceptionMessage(modelString: String): String {
        return JsonParser().parse(modelString).asJsonObject.get("message").asString
    }

    fun getParsedExceptionMessage(unparsedMessage: String): String {
        val jsonObject = JsonObject()
        val key = "String"
        jsonObject.addProperty(key, unparsedMessage)
        return jsonObject.get(key).asString
    }

    fun turnListToJsonString(list: MutableList<*>) = Gson().toJson(list)

}