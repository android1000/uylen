package thousand.group.data.storage.utils

import thousand.group.data.entities.remote.simple.User

interface LocaleStorage {

    fun setAccessToken(accessToken: String)

    fun getAccessToken(): String

    fun tokenIsNotEmpty(): Boolean

    fun setTempAccessToken(accessToken: String)

    fun getTempAccessToken(): String

    fun tokenTempIsNotEmpty(): Boolean

    fun deleteTempToken()

    fun setLanguage(lang: String)

    fun getLanguage(): String

    fun setLanguageServer(lang: String)

    fun getLanguageServer(): String

    fun setSound(sound: Boolean)

    fun getSound(): Boolean

    fun setPush(push: Boolean)

    fun getPush(): Boolean

    fun isAbleToShowPushCount(): Boolean

    fun setAbiityToShowPushCount(show: Boolean)

    fun setFirstTimeLaunched(firstTimeLaunched: Boolean)

    fun isFirstTimeLaunched(): Boolean

    fun setPushReceived(received: Boolean)

    fun isPushReceived(): Boolean

    fun isStartPageVisible(): Boolean

    fun setStartPageVisible(visible: Boolean)

    fun setAccessTokenType(type: String)

    fun getAccessTokenType(): String

    fun setUserId(id: Long)

    fun getUserId(): Long

    fun saveUserModel(user: User)

    fun deleteUserModel()

    fun getUserModel(): User?

    fun saveTempUserModel(user: User)

    fun deleteTempUserModel()

    fun getTempUserModel(): User?

    fun getInitialMode(): Boolean

    fun setInitialMode(mode: Boolean)

    fun setPassword(password: String)

    fun getPassword(): String

    fun deletePassword()

    fun setTeacherMode(isTeacher: Boolean)

    fun isTeacherMode(): Boolean

    fun setCardHintsVisible(isVisible: Boolean)

    fun isCardHintsVisible(): Boolean
}