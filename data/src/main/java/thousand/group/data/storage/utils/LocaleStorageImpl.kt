package thousand.group.data.storage.utils

import com.pixplicity.easyprefs.library.Prefs
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.storage.constants.StorageConstants.PREF_ACCESS_TOKEN
import thousand.group.data.storage.constants.StorageConstants.PREF_ACCESS_TOKEN_TYPE
import thousand.group.data.storage.constants.StorageConstants.PREF_FIRST_LAUNCHED
import thousand.group.data.storage.constants.StorageConstants.PREF_INITIAL_MODE
import thousand.group.data.storage.constants.StorageConstants.PREF_IS_START_PAGE_VISIBLE
import thousand.group.data.storage.constants.StorageConstants.PREF_IS_TEACHER
import thousand.group.data.storage.constants.StorageConstants.PREF_LANG_CODE
import thousand.group.data.storage.constants.StorageConstants.PREF_LANG_CODE_SERVER
import thousand.group.data.storage.constants.StorageConstants.PREF_LANG_DEFAULT_VAL
import thousand.group.data.storage.constants.StorageConstants.PREF_LANG_DEFAULT_VAL_SERVER
import thousand.group.data.storage.constants.StorageConstants.PREF_NO_VAL
import thousand.group.data.storage.constants.StorageConstants.PREF_PASSWORD
import thousand.group.data.storage.constants.StorageConstants.PREF_PERM_AUDIO
import thousand.group.data.storage.constants.StorageConstants.PREF_PERM_PUSH
import thousand.group.data.storage.constants.StorageConstants.PREF_PUSH_RECEIVED
import thousand.group.data.storage.constants.StorageConstants.PREF_SAVED_USER_MODEL
import thousand.group.data.storage.constants.StorageConstants.PREF_SHOW_HINTS
import thousand.group.data.storage.constants.StorageConstants.PREF_SHOW_PUSH_COUNT
import thousand.group.data.storage.constants.StorageConstants.PREF_TEMP_TOKEN
import thousand.group.data.storage.constants.StorageConstants.PREF_TEMP_USER
import thousand.group.data.storage.constants.StorageConstants.PREF_USER_ID

class LocaleStorageImpl : LocaleStorage {

    override fun setAccessToken(accessToken: String) =
        Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    override fun getAccessToken(): String = Prefs.getString(PREF_ACCESS_TOKEN, PREF_NO_VAL)

    override fun tokenIsNotEmpty() = !getAccessToken().equals(PREF_NO_VAL)

    override fun setTempAccessToken(accessToken: String) =
        Prefs.putString(PREF_TEMP_TOKEN, accessToken)

    override fun getTempAccessToken() = Prefs.getString(PREF_TEMP_TOKEN, PREF_NO_VAL)

    override fun tokenTempIsNotEmpty() = !getTempAccessToken().equals(PREF_NO_VAL)

    override fun deleteTempToken() = Prefs.remove(PREF_TEMP_TOKEN)

    override fun setLanguage(lang: String) = Prefs.putString(PREF_LANG_CODE, lang)

    override fun getLanguage(): String = Prefs.getString(PREF_LANG_CODE, PREF_LANG_DEFAULT_VAL)

    override fun setLanguageServer(lang: String) = Prefs.putString(PREF_LANG_CODE_SERVER, lang)

    override fun getLanguageServer() =
        Prefs.getString(PREF_LANG_CODE_SERVER, PREF_LANG_DEFAULT_VAL_SERVER)

    override fun setSound(sound: Boolean) = Prefs.putBoolean(PREF_PERM_AUDIO, sound)

    override fun getSound(): Boolean = Prefs.getBoolean(PREF_PERM_AUDIO, true)

    override fun setPush(push: Boolean) = Prefs.putBoolean(PREF_PERM_PUSH, push)

    override fun getPush(): Boolean = Prefs.getBoolean(PREF_PERM_PUSH, true)

    override fun isAbleToShowPushCount(): Boolean = Prefs.getBoolean(PREF_SHOW_PUSH_COUNT, true)

    override fun setAbiityToShowPushCount(show: Boolean) =
        Prefs.putBoolean(PREF_SHOW_PUSH_COUNT, show)

    override fun setFirstTimeLaunched(firstTimeLaunched: Boolean) =
        Prefs.putBoolean(PREF_FIRST_LAUNCHED, firstTimeLaunched)

    override fun isFirstTimeLaunched(): Boolean = Prefs.getBoolean(PREF_FIRST_LAUNCHED, true)

    override fun setPushReceived(received: Boolean) = Prefs.putBoolean(PREF_PUSH_RECEIVED, received)

    override fun isPushReceived(): Boolean = Prefs.getBoolean(PREF_PUSH_RECEIVED, false)

    override fun isStartPageVisible() =
        Prefs.getBoolean(PREF_IS_START_PAGE_VISIBLE, false)

    override fun setStartPageVisible(visible: Boolean) =
        Prefs.putBoolean(PREF_IS_START_PAGE_VISIBLE, visible)

    override fun setAccessTokenType(type: String) = Prefs.putString(PREF_ACCESS_TOKEN_TYPE, type)

    override fun getAccessTokenType() = Prefs.getString(PREF_ACCESS_TOKEN_TYPE, PREF_NO_VAL)

    override fun setUserId(id: Long) = Prefs.putLong(PREF_USER_ID, id)

    override fun getUserId() = Prefs.getLong(PREF_USER_ID, 0L)

    override fun saveUserModel(user: User) =
        Prefs.putString(PREF_SAVED_USER_MODEL, GsonHelper.getJsonFromObject(user))

    override fun deleteUserModel() = Prefs.remove(PREF_SAVED_USER_MODEL)

    override fun getUserModel(): User? {
        val modelJson = Prefs.getString(PREF_SAVED_USER_MODEL, null)

        modelJson?.apply {
            return GsonHelper.getObjectFromString(this, User::class.java)
        }

        return null
    }

    override fun saveTempUserModel(user: User) =
        Prefs.putString(PREF_TEMP_USER, GsonHelper.getJsonFromObject(user))

    override fun deleteTempUserModel() = Prefs.remove(PREF_SAVED_USER_MODEL)

    override fun getTempUserModel(): User? {
        val modelJson = Prefs.getString(PREF_TEMP_USER, null)

        modelJson?.apply {
            return GsonHelper.getObjectFromString(this, User::class.java)
        }

        return null
    }

    override fun getInitialMode() = Prefs.getBoolean(PREF_INITIAL_MODE, false)

    override fun setInitialMode(mode: Boolean) = Prefs.putBoolean(PREF_INITIAL_MODE, mode)

    override fun setPassword(password: String) = Prefs.putString(PREF_PASSWORD, password)

    override fun getPassword() = Prefs.getString(PREF_PASSWORD, PREF_NO_VAL)

    override fun deletePassword() = Prefs.remove(PREF_PASSWORD)

    override fun setTeacherMode(isTeacher: Boolean) = Prefs.putBoolean(PREF_IS_TEACHER, isTeacher)

    override fun isTeacherMode() = Prefs.getBoolean(PREF_IS_TEACHER, false)

    override fun setCardHintsVisible(isVisible: Boolean) =
        Prefs.putBoolean(PREF_SHOW_HINTS, isVisible)

    override fun isCardHintsVisible() = Prefs.getBoolean(PREF_SHOW_HINTS, false)

}