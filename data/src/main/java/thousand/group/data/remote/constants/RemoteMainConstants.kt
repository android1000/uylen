package thousand.group.data.remote.constants

object RemoteMainConstants {
    const val SERVER_URL = "http://185.116.193.160/"
    const val SOCKET_URL = "ws://185.116.193.160:8000/connection/websocket?format=protobuf"
    const val CHAT_MESSAGES_CHANNEL = "dialogs:dialog#%d,%d"
    const val CHAT_LIST_CHANNEL = "listOfChat_%s"
    const val CHAT_SENDER_CHANNEL = "sender_%d"
    const val NOTIFICATION_SIGNAL_CHANNEL = "signal_%d"
    const val READ_MESSAGE_CHANNEL = "read_message"
    const val READ_MESSAGE_USER_ID_CHANNEL = "read_message_%d"
    const val API = "api/"
    const val V_1 = ""
    const val CONTENT_TYPE = "Content-Type"
    const val CONTENT_TYPE_VALUE = "application/json; charset=utf-8"
    const val DATA = "data"
    const val PAGE = "page"
    const val LIMIT = "limit"
    const val SEARCH = "search"
    const val LIMIT_VAL = 10
    const val ACCEPT_TAG = "Accept"
    const val APP_JSON = "application/json"
    const val MESSAGE = "message"
    const val RESULTS = "results"
    const val TESTS_RESULTS = "testsResults"
    const val TEST_RESULTS = "testResults"
    const val LIKES = "likes"
    const val AUTHORIZATION = "Authorization"
}
