package thousand.group.data.remote.utils

import okhttp3.OkHttpClient
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.storage.constants.StorageConstants.PREF_LANG_DEFAULT_VAL
import thousand.group.data.storage.constants.StorageConstants.PREF_LANG_DEFAULT_VAL_SERVER
import thousand.group.data.storage.constants.StorageConstants.PREF_NO_VAL
import thousand.group.data.storage.utils.LocaleStorage


object RemoteBuilder {

    const val AUTH_HEADER = "Authorization"
    const val LANG_HEADER = "Accept-Language"
    const val X_REQUEST_WITH = "X-Requested-With"
    const val X_REQUEST_WITH_VALUE = "XMLHttpRequest"
    const val AUTH_PREFIX = "Bearer"

    fun createOkHttpClient(localeStorage: LocaleStorage): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()

        okHttpClientBuilder.addInterceptor { chain ->
            val request = chain.request()
            val url = request.url.newBuilder()
            val newRequest = request.newBuilder()
                .url(url.build())

            if (!localeStorage.getAccessToken().equals(PREF_NO_VAL))
                newRequest.addHeader(
                    AUTH_HEADER,
                    "$AUTH_PREFIX ${localeStorage.getAccessToken()}"
                )

            newRequest.addHeader(
                RemoteMainConstants.ACCEPT_TAG,
                RemoteMainConstants.APP_JSON
            )

            newRequest.addHeader(
                LANG_HEADER,
                localeStorage.getLanguageServer()
            )

            chain.proceed(newRequest.build())

        }

        okHttpClientBuilder.addNetworkInterceptor { chain ->
            val request = chain.request()
            val originalResponse = chain.proceed(request)
            val body = originalResponse.body?.bytes()
            val newBody =
                ImageHelper.processImage(body).toResponseBody(originalResponse.body?.contentType())

            return@addNetworkInterceptor originalResponse.newBuilder().body(newBody).build()
        }

        val loggingInterceptor =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        okHttpClientBuilder.addInterceptor(loggingInterceptor)

        return okHttpClientBuilder.build()
    }


    inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(T::class.java)
    }

}