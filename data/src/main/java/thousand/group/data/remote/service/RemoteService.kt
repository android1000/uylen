package thousand.group.data.remote.service

import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import thousand.group.data.entities.remote.responces.DataResponse
import thousand.group.data.entities.remote.simple.User
import thousand.group.data.remote.constants.Endpoints
import thousand.group.data.remote.constants.RemoteConstants
import thousand.group.data.remote.constants.RemoteMainConstants
import thousand.group.data.remote.utils.RemoteBuilder

interface RemoteService {
    //Auth
    @Multipart
    @POST(Endpoints.LOGIN)
    suspend fun login(
        @PartMap params: MutableMap<String, RequestBody>
    ): JsonObject

    @Multipart
    @POST(Endpoints.REGISTER)
    suspend fun register(
        @PartMap params: MutableMap<String, RequestBody>
    ): DataResponse<User>

    @Multipart
    @POST(Endpoints.SEND_VERIFICATION_CODE)
    suspend fun sendVerificationCode(
        @Part(RemoteConstants.PHONE) phone: RequestBody
    ): JsonObject

    @Multipart
    @POST(Endpoints.SEND_CODE_TO_NEW_NUMBER)
    suspend fun sendVerificationCodeToNewNumber(
        @Part(RemoteConstants.PHONE) phone: RequestBody
    )

    @Multipart
    @POST(Endpoints.ASSERT_VERITIFICATION_CODE)
    suspend fun assertVerificationCode(
        @Path(RemoteConstants.ID) id: Long,
        @Part(RemoteConstants.CODE) code: RequestBody
    ): JsonObject

    @Multipart
    @POST(Endpoints.EDIT_PHONE)
    suspend fun editPhone(
        @Part(RemoteConstants.PHONE) phone: RequestBody,
        @Part(RemoteConstants.TOKEN) token: RequestBody
    )

    @Multipart
    @POST(Endpoints.RESET_PASSWORD)
    suspend fun resetPassword(
        @Header(RemoteBuilder.AUTH_HEADER) token: String,
        @PartMap params: MutableMap<String, RequestBody>
    ): JsonObject

    @Multipart
    @POST(Endpoints.FILL_ANKETA)
    suspend fun fillAnketa(
        @Header(RemoteMainConstants.AUTHORIZATION) token: String,
        @PartMap params: MutableMap<String, RequestBody>
    )

    @GET(Endpoints.GET_INTERESTS_LIST)
    suspend fun getInterestsList(): JsonObject

    @GET(Endpoints.GET_CITIES_LIST)
    suspend fun getCitiesList(): JsonObject

    @GET(Endpoints.GET_RU_LIST)
    suspend fun getRuList(): JsonObject

    @Multipart
    @POST(Endpoints.FILL_USER_PHOTO)
    suspend fun fillUserPhoto(
        @Header(RemoteMainConstants.AUTHORIZATION) token: String,
        @Part partList: MutableList<MultipartBody.Part>
    ): JsonObject

    @Multipart
    @POST(Endpoints.EDIT_ABOUT)
    suspend fun editAbout(
        @Header(RemoteMainConstants.AUTHORIZATION) token: String,
        @Part(RemoteConstants.ABOUT) about: RequestBody
    )

    @GET(Endpoints.GET_LIKE_USERS)
    suspend fun getLikeUsers(@Query(RemoteMainConstants.PAGE) page: Int): JsonObject

    @Multipart
    @POST(Endpoints.SET_LIKE_STATUS)
    suspend fun setLikeStatus(@PartMap params: MutableMap<String, RequestBody>): JsonObject

    @GET(Endpoints.REPLY_LIKED_USER)
    suspend fun replyLikedUser(@Query(RemoteConstants.PREV_ID) prevId: Long)

    @GET(Endpoints.GET_REACTIONS)
    suspend fun getReactions(): JsonObject

    @Multipart
    @POST(Endpoints.GET_REACTIONS)
    suspend fun sendReaction(
        @Part(RemoteConstants.USER_ID) userId: RequestBody,
        @Part(RemoteConstants.REACTION_ID) reactionId: RequestBody
    )

    @GET(Endpoints.GET_WHO_LIKES_ME)
    suspend fun getWhoLikesMe(@Query(RemoteMainConstants.PAGE) page: Int): JsonObject

    @GET(Endpoints.GET_USER_BY_ID)
    suspend fun getUserById(@Path(RemoteConstants.ID) id: Long): JsonObject

    @GET(Endpoints.GET_TARIF)
    suspend fun getTariff(): JsonObject

    @Multipart
    @POST(Endpoints.BUY_TARIF)
    suspend fun buyTarif(
        @Path(RemoteConstants.ID) id: Long,
        @Part(RemoteConstants.ACTIVATED) activated: RequestBody,
        @Part(RemoteConstants.END_DATE) endDate: RequestBody,
        @Part(RemoteConstants.PRICE_ID) priceId: RequestBody
    )

    @GET(Endpoints.RECENTLY_SEARCH)
    suspend fun getRecentlySearch(): JsonObject

    @Multipart
    @POST(Endpoints.RECENTLY_SEARCH)
    suspend fun sendRecentlySearch(@Part(RemoteConstants.USER_ID) userId: RequestBody)

    @Multipart
    @POST(Endpoints.SEARCH)
    suspend fun searchUsers(
        @Part(RemoteMainConstants.PAGE) page: RequestBody,
        @PartMap params: MutableMap<String, RequestBody>
    ): JsonObject

    @DELETE(Endpoints.DELETE_RECENT_USER)
    suspend fun deleteRecentUser(@Path(RemoteConstants.ID) id: Long)

    @GET(Endpoints.GET_PROFILE)
    suspend fun getProfile(): JsonObject

    @GET(Endpoints.GET_MY_TARIFF)
    suspend fun getMyTariff(): JsonObject

    @GET(Endpoints.CANCEL_MY_TARIFF)
    suspend fun cancelMyTariff(@Path(RemoteConstants.ID) id: Long)

    @Multipart
    @POST(Endpoints.PROFILE_UPLOAD_IMAGE)
    suspend fun profileUploadImage(@Part image: MultipartBody.Part): JsonObject

    @GET(Endpoints.PROFILE_REMOVE_IMAGE)
    suspend fun profileRemoveImage(@Path(RemoteConstants.ID) id: Long)

    @Multipart
    @POST(Endpoints.EDIT_PROFILE)
    suspend fun editProfile(@PartMap params: MutableMap<String, RequestBody>): JsonObject

    @Multipart
    @POST(Endpoints.EDIT_SHOW_ME_GENDER)
    suspend fun editShowMeGender(@Part(RemoteConstants.SEX) sex: RequestBody)

    @Multipart
    @POST(Endpoints.EDIT_SHOW_MY_ANKETA)
    suspend fun editShowMyAnketa(@Part(RemoteConstants.STATUS) status: RequestBody)

    @GET(Endpoints.GET_MATCHED_USERS)
    suspend fun getMatchedUsers(@Query(RemoteMainConstants.PAGE) page: Int): JsonObject

    @POST(Endpoints.GENERATE_SOCKET_TOKEN)
    suspend fun generateSocketToken(): JsonObject

    @GET(Endpoints.GET_CHAT_LIST)
    suspend fun getChatList(@Query(RemoteMainConstants.PAGE) page: Int): JsonObject

    @Multipart
    @POST(Endpoints.SEND_MESSAGE)
    suspend fun sendMessage(
        @Part(RemoteConstants.RECEIVER_ID) receiverId: RequestBody,
        @Part(RemoteConstants.MESSAGE) message: RequestBody
    )

    @Multipart
    @POST(Endpoints.SEND_MESSAGE)
    suspend fun sendMessage(
        @Part(RemoteConstants.RECEIVER_ID) receiverId: RequestBody,
        @Part file: MultipartBody.Part
    )

    @Multipart
    @POST(Endpoints.GET_CHAT_MESSAGES)
    suspend fun getChatMessages(
        @Part(RemoteMainConstants.PAGE) page: RequestBody,
        @Part(RemoteConstants.RECEIVER_ID) receiverId: RequestBody
    ): JsonObject

    @GET(Endpoints.CHAT_IS_READ)
    suspend fun chatIsRead(@Path(RemoteConstants.ID) id: Long)

    @GET(Endpoints.CHAT_IS_READ)
    suspend fun chatIsRead(
        @Path(RemoteConstants.ID) id: Long,
        @QueryMap params: MutableMap<String, Long>
    )

    @Multipart
    @POST(Endpoints.LOGOUT)
    suspend fun logout(@Part(RemoteConstants.APP_CLIENT_ID) appClientId: RequestBody)

    @Multipart
    @POST(Endpoints.UPDATE_LOCATION)
    suspend fun updateLocation(
        @Part(RemoteConstants.LAT) lat: RequestBody,
        @Part(RemoteConstants.LONG) longg: RequestBody
    )

    @Multipart
    @POST(Endpoints.SET_DISTANCE)
    suspend fun setDistance(
        @Part(RemoteConstants.DISTANCE) distance: RequestBody
    )

    @GET(Endpoints.GET_REGIONS)
    suspend fun getRegions(): JsonObject

    @GET(Endpoints.CHAT_MESSAGE_IS_READ)
    suspend fun chatMessageIsRead(@Path(RemoteConstants.ID) id: Long)

    @Multipart
    @POST(Endpoints.UPDATE_NOTIFICATION)
    suspend fun updateNotification(@Part(RemoteConstants.TYPE) type: RequestBody)

    @GET(Endpoints.GET_LAST_NOTIFICATION)
    suspend fun getLastNotification()

    @GET(Endpoints.GET_PUSH_STATE)
    suspend fun getPushState(): JsonObject

    @Multipart
    @POST(Endpoints.CHANGE_PUSH_STATE)
    suspend fun changePushState(
        @Part(RemoteConstants.STATUS) status: RequestBody
    ): JsonObject

    @POST(Endpoints.SET_AVATAR)
    suspend fun setUserAvatar(@Path(RemoteConstants.ID) id: Long)

    @Multipart
    @POST(Endpoints.SEND_DEVICE_TOKEN)
    suspend fun sendDeviceToken(@Part(RemoteConstants.APP_CLIENT_ID) appClientId: RequestBody)

    @Multipart
    @POST(Endpoints.BAN_ACCOUNT)
    suspend fun banAccount(@Part(RemoteConstants.USER_ID) userId: RequestBody)

    @POST(Endpoints.DELETE_ACCOUNT)
    suspend fun deleteAccount()

    @Multipart
    @POST(Endpoints.COMPLAIN_ACCOUNT)
    suspend fun complainAccount(@Part(RemoteConstants.USER_ID) userId: RequestBody)

    @GET(Endpoints.GENERAL_VALUES)
    suspend fun getGeneralValues(@Query(RemoteConstants.TYPE) type: String): JsonObject

}