package thousand.group.data.remote.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.ByteArrayOutputStream;

public class ImageHelper {

    public static byte[] processImage(byte[] originalImg) {
        int orientation = Exif.getOrientation(originalImg);
        if (orientation != 0) {
            Bitmap bmp = BitmapFactory.decodeByteArray(originalImg, 0, originalImg.length);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            rotateImage(orientation, bmp).compress(Bitmap.CompressFormat.PNG, 100, stream);

            return stream.toByteArray();
        }
        return originalImg;
    }

    private static Bitmap rotateImage(int angle, Bitmap bitmapSrc) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(bitmapSrc, 0, 0,
                bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);
    }
}