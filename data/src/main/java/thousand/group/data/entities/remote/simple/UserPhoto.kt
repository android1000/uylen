package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserPhoto(
    val id:Long,
    val images:String,
    var avatar:Int
):Parcelable