package thousand.group.data.entities.remote.simple

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MatchedUser(
    val id: Long,
    val name: String,
    val email: String?,
    val phone: String?,
    val avatar: String?,
    var avatarBitmap:Bitmap?,
    val email_verified_at: String?,
    val phone_verified: String?,
    val sex: Int,
    val images: MutableList<UserPhoto>,
    val questionnaire_status: String,
    val role: String,
    val ban_status: Int,
    val created_at: String?,
    val updated_at: String?
) : Parcelable