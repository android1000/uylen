package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Ru(
    val id:Long,
    val name:String,
    val parent_id:Long,
    val zhuz:String,
    val deleted_at:String?,
    val created_at:String?,
    val updated_at:String?,
    val zhuz_id:Long?
):Parcelable