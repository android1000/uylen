package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AboutUser(
    val id: Long,
    val text: String,
    val salary: String?
) : Parcelable