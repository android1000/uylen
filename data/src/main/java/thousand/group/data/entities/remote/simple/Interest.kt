package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Interest(
    val id: Long,
    val title: String,
    var isSelected: Boolean = false
) : Parcelable, Comparable<Interest> {

    override fun compareTo(other: Interest): Int {
        return this.id.compareTo(other.id)
    }

}