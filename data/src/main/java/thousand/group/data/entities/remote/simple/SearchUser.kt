package thousand.group.data.entities.remote.simple

import android.graphics.Bitmap
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchUser(
    val id: Long,
    val name: String,
    val avatar:String?,
    var avatarBitmap: Bitmap?,
    val images: MutableList<UserPhoto>,
    val date_of_birth: String,
    val chat: Boolean
) : Parcelable