package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TariffBanner(
    val title: String,
    val description: String,
    val image: String
) : Parcelable
