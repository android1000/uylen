package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserLocation(
    val latitude:Double,
    val longitude:Double,
    val distance:Int
) : Parcelable