package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MyTariffPrice(
    val id: Long,
    val tariff_id: Long,
    val month: Int,
    val price: Int,
    val created_at: String?,
    val updated_at: String?,
    val price_type: String?
) : Parcelable