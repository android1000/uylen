package thousand.group.data.entities.remote.responces

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import thousand.group.data.entities.remote.simple.User

@Parcelize
data class LoginResponse(
    val message: String,
    val user: User,
    val token: String
) : Parcelable