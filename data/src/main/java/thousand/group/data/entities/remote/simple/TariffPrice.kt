package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TariffPrice(
    val id: Long,
    val date: Int,
    val price: Int,
    val price_type: String?,
    var isSelected:Boolean = false
) : Parcelable