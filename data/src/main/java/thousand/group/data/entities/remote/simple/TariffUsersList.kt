package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TariffUsersList(
    val likes: MutableList<LikedMeUser>,
    val tariff: MyTariff?
) : Parcelable