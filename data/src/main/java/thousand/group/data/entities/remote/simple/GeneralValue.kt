package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GeneralValue(
    val id: Int,
    val value: String,
    val type: String,
    val lang: String,
    val created_at: String,
    val updated_at: String
) : Parcelable