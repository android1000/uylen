package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserDetail(
    val id:Long,
    val city_id:City?,
    val date_of_birth:String,
    val gener_id:Ru,
    val sex:Int,
    val me_status:String,
    val marital_status:String,
    val marital_status_id:Int,
    val kalyn_male: Long?,
    val bad_habits: String?,
    val bad_habits_id:Int?,
    val zodiac: String?,
    val zodiac_id:Int?,
    val height: Int?,
    val weight: Int?
):Parcelable