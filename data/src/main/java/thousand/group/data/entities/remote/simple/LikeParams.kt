package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LikeParams(
    val id: Long,
    val to_user_id: Long,
    val from_user_id: Long,
    val matched: Int,
    val created_at: String,
    val updated_at: String,
    val status: Int
) : Parcelable