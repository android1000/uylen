package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Zhuz(
    val id: Long,
    val name: String,
    val parents: MutableList<Ru>
) : Parcelable