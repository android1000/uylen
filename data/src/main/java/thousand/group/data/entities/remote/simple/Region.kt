package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Region(
    val id: Long,
    val name: String,
    val cities: MutableList<City>
) : Parcelable