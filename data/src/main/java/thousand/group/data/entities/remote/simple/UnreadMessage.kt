package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UnreadMessage(
    val id: Long,
    val sender_id: Long,
    val receiver_id: Long,
    val message: String,
    val is_read: Int,
    val created_at: String?,
    val updated_at: String?,
    val conversation_id: Long?,
    val date_message: String?,
    val file: String?,
    val reaction_id: String?
) : Parcelable