package thousand.group.data.entities.remote.responces

data class DataResponse<out T>(
    val token: String?,
    val message: String?,
    val data: T?
)