package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NewDataNotification(
    val chat: Boolean,
    val like: Boolean,
    val matched: Boolean
) : Parcelable