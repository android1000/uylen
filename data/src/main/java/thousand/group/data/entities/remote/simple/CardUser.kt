package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CardUser(
    val name: String,
    val age: String,
    val desc: String,
    var activePhotoLinkPos: Int = 0,
    val photoLinkList: MutableList<String>
) : Parcelable