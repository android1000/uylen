package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserReaction(
    val id:Long,
    val from_user_id:ShortUser
) : Parcelable