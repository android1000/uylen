package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChatListItem(
    val last_message: LastMessage,
    val receiver: Receiver?
) : Parcelable {

    @Parcelize
    data class Receiver(
        val id: Long,
        val name: String,
        val images: MutableList<UserPhoto>,
        var unread_message: Int,
        var unread_messageIds: MutableList<UnreadMessage>
    ) : Parcelable

    @Parcelize
    data class LastMessage(
        val sender: Message?,
        val receiver: Message?
    ) : Parcelable

    @Parcelize
    data class Message(
        val id: Long,
        val message: String?,
        val file: String?,
        val reaction: String?,
        val created_at: String
    ) : Parcelable
}