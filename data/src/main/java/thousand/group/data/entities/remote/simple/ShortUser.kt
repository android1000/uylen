package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ShortUser(
    val id:Long,
    val name:String,
    val images:MutableList<UserPhoto>
):Parcelable