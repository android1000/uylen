package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Reaction(
    val id: Long,
    val image: String
) : Parcelable