package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MyTariff(
    val id: Long = 0,
    val user_id: Long = 0,
    val activated: Int = 0,
    val end_date: String = "",
    val price: MyTariffPrice?,
    val tariff: Tariff?
) : Parcelable