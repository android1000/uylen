package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChatMessage(
    val id: Long,
    val sender_id: Long,
    val receiver_id: Long,
    val message: String?,
    var is_read: Int,
    val created_at: String,
    val file: String? = null,
    val reaction: String? = null
) : Parcelable