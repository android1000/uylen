package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(
    val id: Long,
    val name: String,
    val phone: String?,
    val email: String?,
    val avatar: String?,
    val interests: MutableList<Interest>,
    val details: UserDetail?,
    val images: MutableList<UserPhoto>,
    val location: UserLocation?,
    var questionnaire_status: String?,
    val about: AboutUser?,
    val users_reaction: MutableList<UserReaction>,
    var show_gender: String
) : Parcelable