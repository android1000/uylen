package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserCardData(
    val id: Long,
    val name: String,
    val phone: String?,
    val email: String?,
    val avatar: String?,
    val interests: MutableList<Interest>,
    val details: UserDetail?,
    val images: MutableList<UserPhoto>,
    val location: UserLocation?,
    val questionnaire_status: String?,
    val about: AboutUser?,
    val users_reaction: MutableList<UserReaction>,
    val show_gender: String,
    val kalyn_male: Int?,
    val bad_habits: String?,
    val zodiac: String?,
    val height: Int?,
    val weight: Int?,
    val chat:Boolean,
    var activePhotoLinkPos: Int = 0,
    var hintMode:Int = 0
) : Parcelable

