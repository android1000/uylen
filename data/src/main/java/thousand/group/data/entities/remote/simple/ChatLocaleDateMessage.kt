package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChatLocaleDateMessage(
    val date: String? = null,
    val message: ChatMessage? = null
) : Parcelable