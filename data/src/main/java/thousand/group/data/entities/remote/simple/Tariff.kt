package thousand.group.data.entities.remote.simple

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Tariff(
    val id: Long,
    val name: String,
    val description: String,
    val banner: MutableList<TariffBanner>,
    val price: MutableList<TariffPrice>,
    val details: MutableList<TarifDetail>
) : Parcelable