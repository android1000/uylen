package thousand.group.data.di

import org.koin.dsl.module
import thousand.group.data.remote.constants.RemoteMainConstants.API
import thousand.group.data.remote.constants.RemoteMainConstants.SERVER_URL
import thousand.group.data.remote.constants.RemoteMainConstants.V_1
import thousand.group.data.remote.utils.RemoteBuilder.createOkHttpClient
import thousand.group.data.remote.utils.RemoteBuilder.createWebService
import thousand.group.data.remote.service.RemoteService

val remoteModule = module {
    single { createOkHttpClient(get()) }
    single { createWebService<RemoteService>(get(), SERVER_URL + API) }
}