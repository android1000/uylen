package thousand.group.data.di

import org.koin.dsl.module
import thousand.group.data.storage.utils.LocaleStorage
import thousand.group.data.storage.utils.LocaleStorageImpl

val storageModule = module {
    single<LocaleStorage> { LocaleStorageImpl() }
}